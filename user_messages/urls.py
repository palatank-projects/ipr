# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.conf import settings

urlpatterns = patterns('',
    url(r'^history','user_messages.views.get_all_user_msg',{'template':'message_list.html'},name='messages'),
	url(r'^dialogs/?','user_messages.views.dialogs',{'template':'dialogs.html'},name='dialogs'),
	url(r'^dialog/(?P<dialog_id>\d+)/?','user_messages.views.dialog',{'template':'dialog.html'},name='dialog'),
	url(r'^new/?','user_messages.views.new_message',{'template':'message.html'},name='new_message'),
	url(r'^ajax/search/?','user_messages.views.ajax_search',name='msg_ajax_search'),
)
