# -*- coding: utf-8 -*-
__author__ = 'andrei'

from django.contrib import admin
from django.db import models
from models import Message,Dialog

admin.site.register(Dialog)
admin.site.register(Message)