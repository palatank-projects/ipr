# -*- coding: utf-8 -*-

from models import Message, Dialog
from education_programmes.models import Education_Institute
from forms import UserMsgForm, SendAdminAnswerForm, SendManyAdminForm
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
import json as simplejson
from common_user.models import User
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from django.db import connection

@login_required
def get_all_dialogs_by_user(request,template):
    dialog_list = Message.objects.filter(Q(member1 = request.user) | Q(member2 = request.user)).group_by(Q('member1') | Q('member2'))
    return render_to_response(template,{'dialog_list':dialog_list},context_instance=RequestContext(request))



@login_required
def get_all_user_msg(request,template):
    if request.user.is_admin:
        dialog_list = Dialog.objects.filter(Q(member1 = request.user) | Q(member2 = request.user))
        msg_list = Message.objects.filter(dialog__in=(dialog_list.values('dialog')))
        msg_form = SendManyAdminForm(request.POST or None,initial={'user_sender':request.user})
    else:
        dialog_list = Dialog.objects.filter(Q(member1 = request.user) | Q(member2 = request.user))
        msg_list = Message.objects.filter(dialog__in=(dialog_list.values('dialog')))
        msg_form = UserMsgForm(request.POST or None,initial={'user_sender':request.user})
    if msg_form.is_valid():
        msg_form.save()
    return render_to_response(template,{'message_list':msg_list,'sendmsg_form':msg_form},context_instance=RequestContext(request))

@login_required	
def dialogs(request,template):
    my_dialogs = Dialog.objects.filter(Q(member1 = request.user) | Q(member2 = request.user))
    
    cursor = connection.cursor()
    cursor.execute("""SELECT umm.msg_text,umm.msg_read,
    DATE_FORMAT(umm.date_sent, '%%d.%%m.%%Y'),
    umm.dialog_id, umm.user_sender_id, umm.user_receiver_id
    FROM user_messages_message as umm
    where umm.date_sent = ( SELECT MAX(umm2.date_sent)
    from user_messages_message as umm2
    where umm2.dialog_id = umm.dialog_id )
    and ( umm.user_sender_id = %s OR umm.user_receiver_id = %s )
    ORDER BY umm.date_sent desc""", (request.user.id,request.user.id))
    dialog_list = cursor.fetchall()

    dialogs = []
	
    for dialog_item in dialog_list:
        dialog = Dialog.objects.get(id = dialog_item[3])
        dialog_dict = {}
        
        dialog_dict["msg_text"]  =  dialog_item[0]
        dialog_dict["msg_read"]  =  dialog_item[1]
        dialog_dict["date_sent"] =  dialog_item[2]
        dialog_dict["dialog"]  =  dialog_item[3]
        dialog_dict["sender"]  =  dialog_item[4]
        dialog_dict["receiver"]  =  dialog_item[4]

        if dialog.member1.id == dialog_item[4]:
            dialog_dict["sender_name"]  =  dialog.member1_name()
        elif dialog.member2.id == dialog_item[4]:
            dialog_dict["sender_name"]  =  dialog.member2_name()

        if dialog.member1.id == dialog_item[5]:
            dialog_dict["receiver_name"] = dialog.member1_name()
        elif dialog.member2.id == dialog_item[5]:
            dialog_dict["receiver_name"] = dialog.member2_name()
        dialogs.append(dialog_dict)           
    print len(dialogs)

    return render_to_response(template,{"dialog_count":my_dialogs.count(),"dialog_list":dialogs},context_instance=RequestContext(request))

@csrf_exempt	
@login_required	
def dialog(request,dialog_id,template):

    dialog = Dialog.objects.get(id = dialog_id)
    
    if request.method == 'POST' and request.POST.has_key("msg_text"):
        msg_text = request.POST["msg_text"]

        if len(msg_text) > 0:
            recipient = dialog.member1 if dialog.member1 != request.user else dialog.member2  
            new_message = Message(msg_text = msg_text,msg_read = False,date_sent=datetime.now(),dialog=dialog,user_sender = request.user,user_receiver = recipient)
            new_message.save()      
    
    messages = dialog.message_set.all().order_by("-date_sent")
    for message in dialog.message_set.filter(user_receiver = request.user):
        message.msg_read = True
        message.save()


    return render_to_response(template,{"dialog":dialog,"messages":messages},context_instance=RequestContext(request))


@csrf_exempt
@login_required	
def new_message(request,template):
    allright = True
    
    if request.method == 'POST':
        if request.POST.has_key("msg_text"):
            msg_text = request.POST["msg_text"]

            if len(msg_text) == 0:
                allright = False
        else:
            allright = False

        if request.POST.has_key("recipients") and allright:
            uniq_recipient = {}
            for recipient_id in request.POST.getlist("recipients"):
                try:
                    if not uniq_recipient.has_key(recipient_id):
                        uniq_recipient[recipient_id] = True
                        recipient = User.objects.get(id = recipient_id)
                        dialogs = Dialog.objects.filter(Q(member1 = request.user,member2 = recipient)|Q(member1 = request.user,member2 = recipient))
                        if len(dialogs)>0:
                            dialog = dialogs[0]
                        else:
                            new_dialog = Dialog(member1 = request.user,member2 = recipient)
                            new_dialog.save()
                            dialog = new_dialog

                        new_message = Message(msg_text = msg_text,msg_read = False,date_sent=datetime.now(),dialog=dialog,user_sender = request.user,user_receiver = recipient)
                        new_message.save()
                except :
                    continue
            return redirect('dialogs')

    return render_to_response(template,context_instance=RequestContext(request))

def json_response(func):
    def wrapper(request, *args, **kwargs):
        data = func(request, *args, **kwargs)
        return HttpResponse(simplejson.dumps(data), content_type='application/javascript')
    return wrapper  	
	
@json_response
def ajax_search(request):
    q = request.GET.get('q', None)
    answer = {}
    if q:
        qs = User.objects.filter(Q(email__icontains = q) | Q(first_name__icontains = q) | Q(last_name__icontains = q),is_active=True,is_curator=False).order_by('last_name')[:7]

        for item in qs:
            answer[item.pk] = dict(fio=item.get_full_name())

        orgs = Education_Institute.objects.filter(name__icontains = q,curator__isnull = False).order_by('name')[:7]


        for item in orgs:
            answer[item.curator.pk] = dict(fio=item.name)

        return answer
    return {}	
	



