# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Dialog.user_sender'
        db.delete_column(u'user_messages_dialog', 'user_sender_id')

        # Deleting field 'Dialog.user_receiver'
        db.delete_column(u'user_messages_dialog', 'user_receiver_id')

        # Adding field 'Dialog.member1'
        db.add_column(u'user_messages_dialog', 'member1',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='member1', to=orm['common_user.User']),
                      keep_default=False)

        # Adding field 'Dialog.member2'
        db.add_column(u'user_messages_dialog', 'member2',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='member2', to=orm['common_user.User']),
                      keep_default=False)

        # Adding field 'Message.user_sender'
        db.add_column(u'user_messages_message', 'user_sender',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='user_sender', to=orm['common_user.User']),
                      keep_default=False)

        # Adding field 'Message.user_receiver'
        db.add_column(u'user_messages_message', 'user_receiver',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='user_receiver', to=orm['common_user.User']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Dialog.user_sender'
        db.add_column(u'user_messages_dialog', 'user_sender',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='user_sender', to=orm['common_user.User']),
                      keep_default=False)

        # Adding field 'Dialog.user_receiver'
        db.add_column(u'user_messages_dialog', 'user_receiver',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='user_receiver', to=orm['common_user.User']),
                      keep_default=False)

        # Deleting field 'Dialog.member1'
        db.delete_column(u'user_messages_dialog', 'member1_id')

        # Deleting field 'Dialog.member2'
        db.delete_column(u'user_messages_dialog', 'member2_id')

        # Deleting field 'Message.user_sender'
        db.delete_column(u'user_messages_message', 'user_sender_id')

        # Deleting field 'Message.user_receiver'
        db.delete_column(u'user_messages_message', 'user_receiver_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'user_messages.dialog': {
            'Meta': {'object_name': 'Dialog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'member1'", 'to': u"orm['common_user.User']"}),
            'member2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'member2'", 'to': u"orm['common_user.User']"})
        },
        u'user_messages.message': {
            'Meta': {'object_name': 'Message'},
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'dialog': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['user_messages.Dialog']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msg_read': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'msg_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user_receiver': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_receiver'", 'to': u"orm['common_user.User']"}),
            'user_sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_sender'", 'to': u"orm['common_user.User']"})
        }
    }

    complete_apps = ['user_messages']