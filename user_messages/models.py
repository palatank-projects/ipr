# -*- coding: utf-8 -*
from django.db import models
from common_user.models import User
from education_programmes.models import Education_Institute

class Dialog(models.Model):
    member1 = models.ForeignKey(User,verbose_name=u'Участник диалога',related_name='member1')
    member2 = models.ForeignKey(User,verbose_name=u'Участник диалога',related_name='member2')
#    dialog = models.IntegerField(blank=True,null=True,verbose_name=u'Диалог')

    class Meta:
        verbose_name = u'Диалог'
        verbose_name_plural = u'Диалоги'

    def  member1_name(self):
        if self.member1.is_curator:
            institute = Education_Institute.objects.get(curator = self.member1)
            member1_name = institute.name
        else:
            member1_name = self.member1.get_full_name()
        return member1_name

    def member2_name(self):
        if self.member2.is_curator:
            institute = Education_Institute.objects.get(curator = self.member2)
            member2_name = institute.name
        else:
            member2_name = self.member2.get_full_name()
        return member2_name

    def __unicode__(self):
        return self.member1_name() + u'  ' + self.member2_name()

class Message(models.Model):
    msg_text = models.TextField(blank=True,null=False,verbose_name=u'Текст сообщения')
    msg_read = models.NullBooleanField(blank=True,null=True,verbose_name=u'Прочитано')
    date_sent = models.DateTimeField(blank=True,null=True,verbose_name=u'Дата сообщения')
    user_sender = models.ForeignKey(User,verbose_name=u'Отправитель',related_name='user_sender')
    user_receiver = models.ForeignKey(User,verbose_name=u'Получатель',related_name='user_receiver')
    dialog = models.ForeignKey(Dialog)
	
    def  sender_name(self):
        if self.user_sender.is_curator:
            institute = Education_Institute.objects.get(curator = self.user_sender)
            sender_name = institute.name
        else:
            sender_name = self.user_sender.get_full_name()
        return sender_name

    def receiver_name(self):
        if self.user_receiver.is_curator:
            institute = Education_Institute.objects.get(curator = self.user_receiver)
            receiver_name = institute.name
        else:
            receiver_name = self.user_receiver.get_full_name()
        return receiver_name


    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Сообщения'

    def __unicode__(self):
        return self.msg_text if len(self.msg_text) < 50 else self.msg_text[:50]






