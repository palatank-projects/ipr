# -*- coding: utf-8 -*-

from django.forms import ModelForm
from django import forms
from common_user.models import User
from models import Message
import datetime


class SendAdminAnswerForm(forms.Form):
    msg_text = forms.CharField(max_length=500,label=u'Текст сообщения')
    user_receiver = forms.ChoiceField(label=u'Получатель')
    user_sender = forms.CharField(widget=forms.HiddenInput())


    class Meta:
        fields = ['msg_text','user_receiver','user_sender']

    def __init__(self,*args, **kwargs):
        if kwargs.has_key('initial'):
            user_sender = self.fields['user_sender'].initial = self.initial['user_sender']


    def clean(self):
        if self.msg_text is None:
            raise forms.ValidationError(u'Необходимо ввести сообщение.')

        return self.cleaned_data

    def save(self):
        super(SendAdminAnswerForm,self).save(commit=False)
        data = self.cleaned_data
        msg = Message()
        msg.msg_text = data['msg_text']
        msg.user_sender = data['user_sender']
        msg.date_sent = datetime.now()
        msg.msg_read = False
        msg.save()

class UserMsgForm(forms.Form):
    msg_text = forms.CharField(max_length=500,label=u'Текст сообщения')
    user_sender = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        fields = ['msg_text','user_sender']


    def __init__(self,*args, **kwargs):
        super(UserMsgForm,self).__init__(*args, **kwargs)
        if kwargs.has_key('initial'):
            print 'init'
            self.fields['user_sender'].initial = self.initial['user_sender']


    def clean(self):
        print self.cleaned_data['user_sender']
        if self.cleaned_data.get('msg_text','') is None:
            raise forms.ValidationError(u'Необходимо ввести сообщение.')
        if self.cleaned_data['user_sender'] is None:
            raise forms.ValidationError(u'Необходимо ввести сообщение.')

        return self.cleaned_data

    def save(self):
        data = self.cleaned_data
        admins = User.objects.filter(is_admin=True)
        for admin in admins:
            msg = Message()
            msg.msg_text = data['msg_text']
            msg.user_sender = User.objects.get(email=data['user_sender'])
            msg.user_receiver = admin
            msg.date_sent = datetime.datetime.now()
            msg.msg_read = False
            msg.save()


class SendManyAdminForm(forms.Form):
    msg_text = forms.CharField(widget=forms.Textarea(),max_length=500,label=u'Текст сообщения')
    user_receiver = forms.ModelMultipleChoiceField(label=u'Получатель',queryset=User.objects.all())
    user_sender = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        fields = ['msg_text','user_sender','user_receiver']

    def __init__(self,*args, **kwargs):
        super(SendManyAdminForm,self).__init__(*args, **kwargs)
        if kwargs.has_key('initial'):
            self.fields['user_sender'].initial = self.initial['user_sender']

    def clean(self):
        print self.cleaned_data['user_sender']
        if self.cleaned_data.get('msg_text','') is None:
            raise forms.ValidationError(u'Необходимо ввести сообщение.')
        if self.cleaned_data['user_sender'] is None:
            raise forms.ValidationError(u'Необходимо ввести сообщение.')

        return self.cleaned_data


    def save(self):
        data = self.cleaned_data
        for user in data['user_receiver']:
            msg = Message()
            msg.msg_text = data['msg_text']
            msg.user_receiver = user
            msg.user_sender = User.objects.get(email=data['user_sender'])
            msg.date_sent = datetime.datetime.now()
            msg.msg_read = False
            msg.save()










