tinyMCEPopup.requireLangPack();

var SimplimgDialog = {
	init : function() {
		var f = document.forms[0];

		tinyMCEPopup.editor.execCommand('mceInsertContent', false, document.forms[0].someval.value);
		tinyMCEPopup.close();
	},

        insert : function() {
		// Insert the contents from the input into the document
                tinyMCEPopup.editor.execCommand('mceInsertContent', false, document.forms[0].someval.value);
		tinyMCEPopup.close();	
}
};

tinyMCEPopup.onInit.add(SimplimgDialog.init, SimplimgDialog);
