tinyMCE.addI18n('en.simplimg',{
    desc : 'Вставка картинки',
    title : 'Вставка картинки',
    upload_button : 'Загрузить',
    delete_button : 'Удалить',
    insertimg_title : 'Вставка картинки',
    browse_title: 'Обзор имеющихся картинок',
    back_button: 'Назад',
    deletelink_title: 'удалить выбранную картинку',
    addlink_title:	'добавить картинку',
    oklink_title: 'вставить выбранную картинку'
});
