$(document).ready(function(){
     
    var r = Raphael('map', 600, 380),

    attributes = {
//            fill: '#E8E8E8',
            fill: '#b7c400', //'#A9BCF5',
            stroke: '#FFFFFF', //'#BDBDBD',
//            'stroke-width': 1,
            'stroke-width': 0.2,
            'stroke-linejoin': 'round'
        },
    arr = new Array();
    var svgHeight = 600* 2;
    var svgWidth = 800* 2;
    //r.setViewBox(0, 0, svgWidth, svgHeight, true);

    for (var country in paths) {
         
        var obj = r.path(paths[country].path);

        obj.attr(attributes);

        arr[obj.id] = country;
         
        obj
        .hover(function(){
            this.animate({
                fill: '#1e5799', //'#B0B0B0'
            }, 300);
        }, function(){
            this.animate({
                fill: attributes.fill
            }, 300);
        })
        .mousemove(function(){
		// aahripunov
        //    document.location.hash = arr[this.id];
             
            var point = this.getBBox(0);
             
            $('#map').next('.point').remove();

            $('#map').after($('<div />').addClass('point'));

            $('.point')
            .html(paths[arr[this.id]].name)
//            .prepend($('<a />').attr('href', '#').addClass('close').text('Close'))
//            .prepend($('<img />').attr('src', 'flags/'+arr[this.id]+'.png'))
            .css({
                left: 300,//+(point.width),
                top: 150//+(point.height)
            })
            .fadeIn();

			
			$.getJSON('ajax/region/statistic/'+arr[this.id], function (data) {
			$('#static_data').empty();

                var has_data = false;

		    $.each(data, function() {

            $('#static_data').append("<div>" + this.text + "<span class='statistic_count' >" + this.count +  "</span></div>");
                has_data = true;
           });

              if(has_data){
//		   $('#triangle').css({
//           "margin-top": ($('#static_data').height() - 30)/2,
//           "margin-bottom": ($('#static_data').height() - 30)/2
//            });

                  $('#region_statistic').fadeIn();
              }

           });

		}).click(function(){
        	$.getJSON('edu/region/'+arr[this.id]+'/', function (jsdata) {
			
			if (jsdata.status == 1){
			$("input[name='region']").val(jsdata.id);
			$("form#organization_filter").submit();
			}
			});

        }).mouseout(function(){

            });
		
    }
    r.setViewBox(0, 0, svgWidth, svgHeight, true);
    //r.scale(0.5);
});
