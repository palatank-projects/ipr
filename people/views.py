# -*- coding: utf-8 -*-
# Create your views here.
from models import *
from common_user.models import User
from olymp_tests.models import *

from forms  import *
from django.db.models import Avg, Max, Min
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response,redirect
from django.template import RequestContext,Context
from django.template.loader import get_template,render_to_string
import json,sys,os, math
from datetime import datetime,time,timedelta
from cStringIO import StringIO
from slugify import slugify


@csrf_exempt
def olymp_university_register(request):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    init_dict = {}
    if not request.user.is_authenticated():

        model_item = University()
        
        if request.method == 'POST':
            form = UniversityRegisterForm(request.POST) # A form bound to the POST data

            if form.is_valid():

                user = User.objects.create_olymp_user(form.cleaned_data['username'], form.cleaned_data['curator_first_name'], form.cleaned_data['curator_last_name'], form.cleaned_data['password1'])
                user.is_active = True
                user.is_univer = True
                user.save()

                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'username' or formfieldname == 'password1' or formfieldname == 'password2':
                        continue
                    model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]

                model_item.user = user
                model_item.save()

                if request.POST.has_key('main_test'):
                    try:
                        test_item = Test.objects.get(id=request.POST['main_test'])
                        new_request = RequestOnTest(test = test_item, university = model_item)
                        new_request.save()
                    except :
                        print str(sys.exc_info())

                fio = model_item.curator_last_name.capitalize() + u' ' + model_item.curator_first_name.capitalize() + u' ' + model_item.curator_patronymic.capitalize()

                return render_to_response("olymp/university_register.html", {'fio':fio,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))
            else:
                print 'NOT VALID'
        else:
            form = UniversityRegisterForm(instance=model_item)

        return render_to_response("olymp/university_register.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('/olymp/')

@csrf_exempt
def olymp_university_edit(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    model_item = get_object_or_404(University,id=id)
    from_self = False

    if request.user.is_authenticated() and request.user.is_olympic  == True and request.user.is_univer  == True:
        iamuniver = University.objects.get(user = request.user)
        if iamuniver.id == model_item.id:
            from_self = True
                                                            

    if request.user.is_authenticated() and ( request.user.is_admin == True or  from_self == True ) :
        if request.method == 'POST':
            form = UniversityEditForm(request.POST)
            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]
                model_item.save()
                message = u'Изменения успешно сохранены!'
                return render_to_response("olymp/university_edit.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'message':message}, context_instance=RequestContext(request))
        else:
            form = UniversityEditForm(instance=model_item)

        return render_to_response("olymp/university_edit.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/olymp/')

@csrf_exempt
def olymp_university_delete(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin == True:
        model_item = get_object_or_404(University,id=id)
        students = model_item.olympstudent_set.all()
        
        if request.method == 'POST':
            for stud in students:
                stud.delete()
            model_item.delete()
            return HttpResponseRedirect('/olymp/')
        else:
            return render_to_response("olymp/university_delete.html", {'item':model_item,'verbose_name':model_item._meta.verbose_name,'students':students}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/olymp/')

@csrf_exempt
def olymp_soup(request):
    #if request.user.is_authenticated() and not request.user.is_olympic:
    #    logout(request)
    #if request.user.is_authenticated() and request.user.is_admin == True:

    if request.method == 'POST':
        form = SoupForm(request.POST)

        if form.is_valid():
            user = User.objects.create_olymp_superuser(username = request.POST['login'],first_name = request.POST['name'], last_name = request.POST['surname'], password= request.POST['password1'])
            user.save()
            return HttpResponseRedirect('/olymp/')
        else:
            return render_to_response("olymp/soup.html", {"form":form}, context_instance=RequestContext(request))

    else:
        form = SoupForm()

    return render_to_response("olymp/soup.html",{"form":form}, context_instance=RequestContext(request))
    #return HttpResponseRedirect('/olymp/')

@csrf_exempt
def olymp_student_edit(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    model_item = get_object_or_404(OlympStudent,id=id)
    from_self = False

    if request.user.is_authenticated() and request.user.is_olympic  == True and request.user.is_participant  == True:
        iamstudent = OlympStudent.objects.get(user = request.user)
        if iamstudent.id == model_item.id:
            from_self = True
                                                            

    if request.user.is_authenticated() and ( request.user.is_admin == True or  from_self == True ) :
        if request.method == 'POST':
            form = OlympStudentForm(request.POST)
            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]
                model_item.save()
                message = u'Изменения успешно сохранены!'
                return render_to_response("olymp/student_edit.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'message':message}, context_instance=RequestContext(request))
        else:
            form = OlympStudentForm(instance=model_item)

        return render_to_response("olymp/student_edit.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/olymp/')

@csrf_exempt
def olymp_student_delete(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin == True:
        model_item = get_object_or_404(OlympStudent,id=id)
        
        if request.method == 'POST':
            model_item.delete()
            return HttpResponseRedirect('/olymp/')
        else:
            return render_to_response("olymp/student_delete.html", {'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/olymp/')

@csrf_exempt    
def olymp_university_changepass(request):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin == True:
        universities = University.objects.filter(user__isnull = False).order_by('city','name')
        return render_to_response("olymp/university_changepass.html", {'universities':universities}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/olymp/')
    
