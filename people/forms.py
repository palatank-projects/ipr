# -*- coding: utf-8 -*-

from django import forms
from django.core import validators
from django.db.models import Q,Avg, Max, Min
from django.forms import ModelForm, Textarea
from django.forms.extras import * #SelectDateWidget
from models import *
from common_user.models import User
import sys,datetime

from django.conf import settings
from olymp.widgets import *

    
class UniversityRegisterForm(ModelForm):
    username  = forms.CharField(max_length=75, label=u'Логин')
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    
    class Meta:
        model = University
        fields = ['name','username','password1','password2','region','city','email','phone','rector_last_name','rector_first_name','rector_patronymic','curator_last_name','curator_first_name','curator_patronymic','curator_position']

    def clean(self):

        username = self.cleaned_data['username']

        if User.objects.filter(username=username).count():
            self._errors["username"] = self.error_class([u"пользователь с таким логином уже существует"])


        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password1"] = self.error_class([u"Пароли не совпадают."])
            #raise forms.ValidationError(u"Пароли не совпадают.")

        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])

        return self.cleaned_data

class UniversityEditForm(ModelForm):
    class Meta:
        model = University
        fields = ['name','region','city','email','phone','rector_first_name','rector_last_name','rector_patronymic','curator_first_name','curator_last_name','curator_patronymic','curator_position']

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])

        return self.cleaned_data



#class StudentForm(ModelForm):
#    id                 = forms.IntegerField(required= False,label = u'ID')
#    must_delete        = forms.IntegerField(initial = 0,label = u'Удалить')
#    delete_checkbox    = forms.BooleanField(required= False,label=u'Удалить')
#
#    class Meta:
#        model = Question
#        fields = ['test','text','option1','option2','option3','option4','correct_option','number']
#
#        widgets = {
#            'test' : forms.HiddenInput(),
#            }
#
#    def __init__(self,*args, **kwargs):
#        super(QuestionForm, self).__init__(*args, **kwargs)
#        self.fields['must_delete'].widget = forms.HiddenInput()
#        self.fields['id'].widget = forms.HiddenInput()
#
#        if kwargs.has_key('instance'):
#            print  kwargs['instance'].id
#            self.fields['id'].initial = kwargs['instance'].id
#
#    def clean(self):
#        if self.cleaned_data.get("option1") == self.cleaned_data.get("option2"):
#            self._errors["option2"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("option1") == self.cleaned_data.get("option3"):
#            self._errors["option3"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("option1") == self.cleaned_data.get("option4"):
#            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])
#
#        if self.cleaned_data.get("option2") == self.cleaned_data.get("option3"):
#            self._errors["option3"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("option2") == self.cleaned_data.get("option4"):
#            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])
#
#        if self.cleaned_data.get("option3") == self.cleaned_data.get("option4"):
#            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])
#        return self.cleaned_data



class OlympStudentShortForm(forms.Form):
    new_student_last_name          = forms.CharField(label = u'Фамилия', max_length=60)
    new_student_first_name           = forms.CharField(label = u'Имя', max_length=60)
    new_student_patronymic          = forms.CharField(label = u'Отчество', max_length=100)
    new_student_email               = forms.EmailField(label = u'Электронный адрес', max_length=100)
    new_student_birthday            = forms.DateField(label = u'Дата рождения')
    new_student_speciality          = forms.CharField(max_length=50,label = u'Направление')
    new_student_course              = forms.ChoiceField(choices=COURSE_CHOICES,label = u'Курс')
    new_student_finish_year         = forms.IntegerField(initial = 2014, label = u'Планируемый год выпуска')
    new_student_must_delete         = forms.IntegerField(initial = 0,label = u'Удалить')
    new_student_delete_checkbox     = forms.BooleanField(required= False,label=u'Удалить')

    def __init__(self,*args, **kwargs):
        super(OlympStudentShortForm, self).__init__(*args, **kwargs)
        self.fields['new_student_finish_year'].widget = forms.TextInput(attrs={'maxlength':4})
        self.fields['new_student_must_delete'].widget = forms.HiddenInput()
#        self.fields['new_student_birthday'].widget = DateWidget()


    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["new_student_email"])
        except:
            self._errors["new_student_email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])


#        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option2"):
#            self._errors["new_question_option2"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option3"):
#            self._errors["new_question_option3"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option4"):
#            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])
#
#        if self.cleaned_data.get("new_question_option2") == self.cleaned_data.get("new_question_option3"):
#            self._errors["new_question_option3"] = self.error_class([u'Вариант ответа не уникален!'])
#        if self.cleaned_data.get("new_question_option2") == self.cleaned_data.get("option4"):
#            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])
#
#        if self.cleaned_data.get("new_question_option3") == self.cleaned_data.get("new_question_option4"):
#            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])
        return self.cleaned_data


class OlympStudentForm(forms.ModelForm):
    id                 = forms.IntegerField(required= False,label = u'id')
    username           = forms.CharField(label = u'Логин', max_length=60)
    last_name         = forms.CharField(label = u'Фамилия', max_length=60)
    first_name          = forms.CharField(label = u'Имя', max_length=60)

    must_delete        = forms.IntegerField(initial = 0,label = u'Удалить')
    delete_checkbox    = forms.BooleanField(required= False,label=u'Удалить')

    class Meta:
        model = OlympStudent
        fields = ['user','id','username','detail','first_name','last_name','patronymic','email','birthday','speciality','course','finish_year']

        widgets = {
            'user' : forms.HiddenInput(),
            }

    def __init__(self,*args, **kwargs):
        super(OlympStudentForm, self).__init__(*args, **kwargs)
        self.fields['must_delete'].widget = forms.HiddenInput()
        self.fields['id'].widget = forms.HiddenInput()

        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['detail'].widget.attrs['readonly'] = True

        if kwargs.has_key('instance'):
            print  kwargs['instance'].id
            self.fields['id'].initial = kwargs['instance'].id

            if kwargs['instance'].user is not None:
                self.fields['last_name'].initial = kwargs['instance'].user.last_name
                self.fields['first_name'].initial = kwargs['instance'].user.first_name
                self.fields['username'].initial = kwargs['instance'].user.username

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])

        return self.cleaned_data
		
class SoupForm(forms.Form):
    login = forms.CharField(max_length=30,label=u'Логин')
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    surname = forms.CharField(max_length=50,label=u'Фамилия')
    name = forms.CharField(max_length=50,label=u'Имя')

    def __init__(self, *args, **kwargs):
        super(SoupForm,self).__init__(*args, **kwargs)

    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password1"] = self.error_class([u"Пароли не совпадают."])
            raise forms.ValidationError(u"Пароли не совпадают.")

        try:
            login_exist = User.objects.get(username=self.cleaned_data["login"])
        except:
            login_exist = None
            #if email_exist is not None:
        if login_exist:
            self._errors["login"] = self.error_class([u'Такой пользователь уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой пользователь уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        return self.cleaned_data