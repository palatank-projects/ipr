# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'OlympUser'
        db.create_table(u'people_olympuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=75)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_univer', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_student', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'people', ['OlympUser'])

        # Adding M2M table for field groups on 'OlympUser'
        m2m_table_name = db.shorten_name(u'people_olympuser_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('olympuser', models.ForeignKey(orm[u'people.olympuser'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['olympuser_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'OlympUser'
        m2m_table_name = db.shorten_name(u'people_olympuser_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('olympuser', models.ForeignKey(orm[u'people.olympuser'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['olympuser_id', 'permission_id'])

        # Adding model 'University'
        db.create_table(u'people_university', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.OlympUser'])),
            ('name', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rector_first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rector_last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rector_patronymic', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('curator_first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('curator_last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('curator_patronymic', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('curator_position', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'people', ['University'])

        # Adding model 'OlympStudent'
        db.create_table(u'people_olympstudent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.OlympUser'])),
            ('patronymic', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100)),
            ('registered_date', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('birthday', self.gf('django.db.models.fields.DateField')()),
            ('speciality', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('course', self.gf('django.db.models.fields.CharField')(default='1', max_length=1)),
            ('finish_year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('detail', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.University'])),
        ))
        db.send_create_signal(u'people', ['OlympStudent'])


    def backwards(self, orm):
        # Deleting model 'OlympUser'
        db.delete_table(u'people_olympuser')

        # Removing M2M table for field groups on 'OlympUser'
        db.delete_table(db.shorten_name(u'people_olympuser_groups'))

        # Removing M2M table for field user_permissions on 'OlympUser'
        db.delete_table(db.shorten_name(u'people_olympuser_user_permissions'))

        # Deleting model 'University'
        db.delete_table(u'people_university')

        # Deleting model 'OlympStudent'
        db.delete_table(u'people_olympstudent')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'people.olympstudent': {
            'Meta': {'ordering': "['user']", 'object_name': 'OlympStudent'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            'course': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'detail': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'finish_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'speciality': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympUser']"})
        },
        u'people.olympuser': {
            'Meta': {'object_name': 'OlympUser'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_univer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'})
        },
        u'people.university': {
            'Meta': {'ordering': "['user']", 'object_name': 'University'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_position': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'rector_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympUser']"})
        }
    }

    complete_apps = ['people']