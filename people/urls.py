# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^university/register/$', 'people.views.olymp_university_register', name='university-register'),
    url(r'^university/edit/(?P<id>\d+)/$', 'people.views.olymp_university_edit', name='university-edit'),
    url(r'^university/delete/(?P<id>\d+)/$', 'people.views.olymp_university_delete', name='university-delete'),
    
    url(r'^student/edit/(?P<id>\d+)/$', 'people.views.olymp_student_edit', name='olymp-student-edit'),
    url(r'^student/delete/(?P<id>\d+)/$', 'people.views.olymp_student_delete', name='olymp-student-delete'),

    url(r'^soup/$', 'people.views.olymp_soup', name='olymp-soup'),

)