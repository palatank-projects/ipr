# -*- coding: utf-8 -*-
from django.contrib import admin
from models import *


class OlympStudentAdmin(admin.ModelAdmin):
    list_display = ('user','university','email')

    list_display_links = ('user',)

admin.site.register(University)
admin.site.register(OlympStudent,OlympStudentAdmin)

