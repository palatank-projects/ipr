# -*- coding: utf-8 -*-
from django.db import models
#from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from common_user.models import User

COURSE = (u'1',u'2',u'3',u'4',u'5',u'6')
COURSE_DICT = {'1' : COURSE[0],
               '2' : COURSE[1],
               '3' : COURSE[2],
               '4' : COURSE[3],
               '5' : COURSE[4],
               '6' : COURSE[5],
}

COURSE_CHOICES = (
    ('1', COURSE[0]),
    ('2', COURSE[1]),
    ('3', COURSE[2]),
    ('4', COURSE[3]),
    ('5', COURSE[4]),
    ('6', COURSE[5]),
    )


# Create your models here.

#class OlympUserManager(BaseUserManager):
#    def create_user(self, username, first_name, last_name, password=None):
#        if not username:
#            raise ValueError('Users must have an username')
#
#        user = self.model(
#            #email=UserManager.normalize_email(email),
#            username = username,
#            first_name=first_name,
#            last_name=last_name,
#        )
#
#        user.set_password(password)
#        user.save(using=self._db)
#
#        return user
#
#    def create_superuser(self, username, first_name, last_name, password=None):
#        user = self.create_user(
#            #email=email,
#            username = username,
#            password=password,
#            first_name=first_name,
#            last_name=last_name,
#        )
#
#        user.is_admin = True
#        user.save(using=self._db)
#        return user
#
#
#
#class OlympUser(AbstractBaseUser,PermissionsMixin):
#    username            = models.CharField(u'username', max_length=75, unique=True)
#    first_name          = models.CharField(u'first_name', max_length=60)
#    last_name           = models.CharField(u'last_name', max_length=60)
#    is_active           = models.BooleanField(default=True)
#    is_admin            = models.BooleanField(default=False)
#    is_univer           = models.BooleanField(default=False)
#    is_student          = models.BooleanField(default=False)
#
#    objects = OlympUserManager()
#
#    USERNAME_FIELD = 'username'
#    REQUIRED_FIELDS = ['first_name', 'last_name']
#
#
#    def get_model(self):
#        return 'olymp'
#
#    def get_full_name(self):
#        return u'%s %s' % (self.last_name, self.first_name)
#
#    def get_short_name(self):
#        return u'%s' % self.first_name
#
#    def __unicode__(self):
#        return self.username
#
#    def has_perm(self, perm, obj=None):
#        return True
#
#    def has_module_perms(self, app_label):
#        return True
#
#    @property
#    def is_staff(self):
#        return self.is_admin
#
#    def get_user_group(self):
#        return
#
#    def treatment(self):
#        if self.is_univer:
#            for univer in self.university_set.all():
#                return univer.name
#        else:
#            return self.get_full_name()
#
#
#    class Meta:
#        verbose_name = u'Пользователь'
#        verbose_name_plural = u'Пользователи'


class University(models.Model):
    user                       = models.ForeignKey(User,verbose_name=u'Логин')
    name                       = models.TextField(max_length=500,verbose_name=u'Полное название ВУЗа')
    region                     = models.CharField(max_length=100,verbose_name=u'Регион') #models.ForeignKey(Region,blank=True,null=True,verbose_name = u'Регион', on_delete=models.SET_NULL)
    city                       = models.CharField(max_length=100,verbose_name=u'Город') #models.ForeignKey(City,blank=True,null=True,verbose_name=u'Город', on_delete=models.SET_NULL)
    rector_first_name          = models.CharField(max_length=100,verbose_name=u'Имя') # ректора')
    rector_last_name           = models.CharField(max_length=100,verbose_name=u'Фамилия') # ректора')
    rector_patronymic          = models.CharField(max_length=100,verbose_name=u'Отчество') # ректора')
    curator_first_name         = models.CharField(max_length=100,verbose_name=u'Имя') # лица, курирующего участие ВУЗа в Олимпиаде')
    curator_last_name          = models.CharField(max_length=100,verbose_name=u'Фамилия') # лица, курирующего участие ВУЗа в Олимпиаде')
    curator_patronymic         = models.CharField(max_length=100,verbose_name=u'Отчество') # лица, курирующего участие ВУЗа в Олимпиаде')
    curator_position           = models.CharField(max_length=100,verbose_name=u'Должность') # лица, курирующего участие ВУЗа в Олимпиаде')
    email                      = models.EmailField(max_length=100,verbose_name=u'Электронный адрес')
    phone                      = models.CharField(max_length=30,verbose_name=u'Контактный телефон')
    faik                       = models.BooleanField(default=False,verbose_name = 'Фейк')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'ВУЗ-участник'
        verbose_name_plural = u'ВУЗы-участники'
        ordering = ['user']


# student
class OlympStudent(models.Model):
    user            = models.ForeignKey(User)
    patronymic      = models.CharField(max_length=100,blank=True,null=True,verbose_name=u'Отчество')
    email           = models.EmailField(max_length=100,verbose_name=u'Электронный адрес')
    registered_date = models.DateField(auto_now=True,verbose_name=u'Дата регистрации')
#    slug            = models.SlugField(max_length=50,unique=True,verbose_name=u'Slug')
    birthday        = models.DateField(verbose_name=u'Дата рождения')
    speciality      = models.CharField(max_length=50,verbose_name=u'Направление')
    course          = models.CharField(max_length=1, choices=COURSE_CHOICES,default='1', verbose_name=u'Курс')
    finish_year     = models.CharField(max_length=4, verbose_name=u'Планируемый год выпуска')

    detail          = models.CharField(max_length = 30,blank=True,null=True,verbose_name = u'Пароль')
	
    faik            = models.BooleanField(default=False,verbose_name = 'Фейк')

#    open = models.CharField
    university      = models.ForeignKey(University)

    def save(self,*args,**kwargs):
        super(OlympStudent,self).save(*args,**kwargs)

    class Meta:
        verbose_name = u'Участник'
        verbose_name_plural = u'Участники'
        ordering = ['user']

    def __unicode__(self):
        if self.user is not None:
            return self.user.username + u" " + self.user.last_name + u" " + self.user.first_name
        else:
            return str(self.id)

    @property
    def get_fio(self):
        return self.user.last_name.capitalize() + " " + self.user.first_name.capitalize() + " " + (self.patronymic.capitalize()  if self.patronymic is not None else "")
