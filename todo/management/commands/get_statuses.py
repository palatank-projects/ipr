# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import simplejson
from todo.models import Status
import os

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        status = Status(id = 1, title=u'Новая')
        status.save()

        status = Status(id = 2, title=u'Принята')
        status.save()

        status = Status(id = 3, title=u'На проверке')
        status.save()

        status = Status(id = 4, title=u'Завершена')
        status.save()