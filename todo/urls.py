# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('',
    #    url(r'^register/?$','common_user.views.register', name='register'),
    url(r'^list/?$','todo.views.list',name='todo_tasks_list'),
    url(r'^projects_list/?$','todo.views.projects_list',name='todo_projects_list'),
    url(r'^details/(?P<task_id>\d+)/?$','todo.views.details',name='todo_task_details'),
    url(r'^edit/(?P<task_id>\d+)/?$','todo.views.edit',name='todo_edit_task'),
    url(r'^add_task/?$','todo.views.add_task',name='todo_add_task'),
    url(r'^delete/(?P<task_id>\d+)/?$','todo.views.delete',name='todo_delete_task'),
    url(r'^project_details/(?P<project_id>\d+)/?$','todo.views.project_details',name='todo_project_details'),
    url(r'^delete_project_attach/(?P<attach_id>\d+)/?$','todo.views.delete_project_attach',name='todo_delete_project_attach'),
    url(r'^delete_task_attach/(?P<attach_id>\d+)/?$','todo.views.delete_task_attach',name='todo_delete_task_attach'),
    url(r'^add_project/?$','todo.views.add_project',name='todo_add_project'),
    url(r'^edit_project/(?P<project_id>\d+)/?$','todo.views.edit_project',name='todo_edit_project'),
    url(r'^delete_project/(?P<project_id>\d+)/?$','todo.views.delete_project',name='todo_delete_project'),
    url(r'^task_to_accepted/(?P<task_id>\d+)/?$','todo.views.task_to_accepted',name='todo_task_to_accepted'),
    url(r'^task_to_done/(?P<task_id>\d+)/?$','todo.views.task_to_done',name='todo_task_to_done'),
    url(r'^task_to_checked/(?P<task_id>\d+)/?$','todo.views.task_to_checked',name='todo_task_to_checked'),
    url(r'^task_to_new/(?P<task_id>\d+)/?$','todo.views.task_to_new',name='todo_task_to_new'),
    url(r'^add_comment/(?P<task_id>\d+)/?$','todo.views.add_comment',name='todo_add_comment'),
    url(r'^del_comment/(?P<comment_id>\d+)/?$','todo.views.del_comment',name='todo_del_comment'),
    url(r'^json_project_users/?$','todo.views.del_comment',name='todo_json_project_users'),
    url(r'^forbidden/?$','todo.views.forbidden',name='todo_forbidden'),
)