from django.db import models
from common_user.models import User




class Message(models.Model):
    message_body = models.TextField()
    message_title = models.TextField()
    read = models.NullBooleanField()
    time_sent = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return u"%s %s" % (self.message_body, self.message_title)


class MessageUser(models.Model):
    message = models.ForeignKey(Message)
    from_user = models.ForeignKey(User)
    to_user = models.ForeignKey(User)


    def __unicode__(self):
        return u"%s %s %s" % (self.message.message_body, self.from_user.email, self.to_user.email)