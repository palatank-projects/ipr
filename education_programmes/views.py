# -*- coding: utf-8 -*-
# System libraries
import json,sys,os, math, locale
from datetime import datetime,time,timedelta

# Third-party libraries
# from xhtml2pdf import pisa
from slugify import slugify
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

# Django modules
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Max, Min
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response,redirect
from django.template import RequestContext,Context
from django.views.decorators.csrf import csrf_exempt
import django.utils

# Django apps
# Current-app modules
from models import *
from forms  import *
from common_user.forms import *
from common_user.models import *
from IPR.fi import fi_icons
import IPR.settings as settings
from IPR.utils import get_from_dict, divide_into_columns, upload_file, directory_cleaning, handle_uploaded_file
import IPR.mail

@csrf_exempt
def get_regions(request):
    if request.is_ajax():
        region = None
        city = None
        institute = None
        level = None
        program = None

        answer = []
        kwargs = {}

        if request.POST.has_key('region') and  request.POST['region'] != u'-1':
            region = request.POST['region']
        if request.POST.has_key('city') and  request.POST['city'] != u'-1':
            city = request.POST['city']
        if request.POST.has_key('institute') and  request.POST['institute'] != u'-1':
            institute = request.POST['institute']
        if request.POST.has_key('level') and  request.POST['level'] != u'-1':
            level = request.POST['level']
        if request.POST.has_key('program') and  request.POST['program'] != u'-1':
            program = request.POST['program']

        if city is not None and city > 0:
            kwargs["city__id"] = city
        if institute is not None and institute > 0:
            kwargs["city__education_institute__id"] = institute
        if program is not None and program > 0:
            kwargs["city__education_institute__education_institute_program__program__id"] = program
        if level is not None and level > 0:
            kwargs["city__education_institute__education_institute_program__program__level"] = level

        kwargs["city__education_institute__education_institute_program__institute__is_archive"] = False
        kwargs["city__education_institute__education_institute_program__is_archive"] = False
        kwargs["city__education_institute__accreditation"] = 'A'

        regions = Region.objects.filter(**kwargs).distinct().values().order_by('name')
        return HttpResponse(json.dumps(list(regions)),content_type='application/json')
    else:
        regions = Region.objects.values().all()
        return HttpResponse(json.dumps(list(regions)),content_type='application/json')

@csrf_exempt
def get_cities(request):
    if request.is_ajax():
        region = None
        city = None
        institute = None
        level = None
        program = None

        answer = []
        kwargs = {}

        if request.POST.has_key('region') and  request.POST['region'] != u'-1':
            region = request.POST['region']
        if request.POST.has_key('city') and  request.POST['city'] != u'-1':
            city = request.POST['city']
        if request.POST.has_key('institute') and  request.POST['institute'] != u'-1':
            institute = request.POST['institute']
        if request.POST.has_key('level') and  request.POST['level'] != u'-1':
            level = request.POST['level']
        if request.POST.has_key('program') and  request.POST['program'] != u'-1':
            program = request.POST['program']


        if region is not None and region > 0:
            kwargs["region"] = region
        if institute is not None and institute > 0:
            kwargs["education_institute__id"] = institute
        if program is not None and program > u'0':
            kwargs["education_institute__education_institute_program__program__id"] = program
        if level is not None and level > 0:
            kwargs["education_institute__education_institute_program__program__level"] = level

        kwargs["education_institute__education_institute_program__institute__is_archive"] = False
        kwargs["education_institute__education_institute_program__is_archive"] = False
        kwargs["education_institute__education_institute_program__institute__accreditation"] = 'A'

        cities = City.objects.filter(**kwargs).distinct().values()
        return HttpResponse(json.dumps(list(cities)),content_type='application/json')

    else:
        return HttpResponse(json.dumps(list()),content_type='application/json')

@csrf_exempt
def get_programm(request):
    if request.is_ajax():
        region = None
        city = None
        institute = None
        level = None
        program = None

        answer = []
        kwargs = {}

        if request.POST.has_key('region') and  request.POST['region'] != u'-1':
            region = request.POST['region']
        if request.POST.has_key('city') and  request.POST['city'] != u'-1':
            city = request.POST['city']
        if request.POST.has_key('institute') and  request.POST['institute'] != u'-1':
            institute = request.POST['institute']
        if request.POST.has_key('level') and  request.POST['level'] != u'-1':
            level = request.POST['level']
        if request.POST.has_key('program') and  request.POST['program'] != u'-1':
            program = request.POST['program']


        if region is not None and region > 0:
            kwargs["education_institute_program__institute__city__region__id"] = region

        if city is not None and city > 0:
            kwargs["education_institute_program__institute__city__id"] = city
        if institute is not None and institute > 0:
            kwargs["education_institute_program__institute__id"] = institute
        if program is not None and program > 0:
            kwargs["id"] = program
        if level is not None and level > 0:
            kwargs["level"] = level

        kwargs["is_archive"] = False
        kwargs["education_institute_program__institute__is_archive"] = False
        kwargs["education_institute_program__is_archive"] = False
        kwargs["education_institute_program__institute__accreditation"] = 'A'

        programms = Education_Programme.objects.filter(**kwargs).distinct().values()
        return HttpResponse(json.dumps(list(programms)),content_type='application/json')
    else:
        return HttpResponse(json.dumps(list()),content_type='application/json')

@csrf_exempt
def get_university(request):
    if request.is_ajax():
        region = city = institute = level = program = None

        answer = []
        kwargs = {}

        if request.POST.has_key('region') and  request.POST['region'] != u'-1':
            region = request.POST['region']
        if request.POST.has_key('city') and  request.POST['city'] != u'-1':
            city = request.POST['city']
        if request.POST.has_key('institute') and  request.POST['institute'] != u'-1':
            institute = request.POST['institute']
        if request.POST.has_key('level') and  request.POST['level'] != u'-1':
            level = request.POST['level']
        if request.POST.has_key('program') and  request.POST['program'] != u'-1':
            program = request.POST['program']


        if region is not None and region > 0:
            kwargs["city__region__id"] = region
        if city is not None and city > 0:
            kwargs["city__id"] = city
        if institute is not None and institute > 0:
            kwargs["id"] = institute
        if program is not None and program > 0:
            kwargs["education_institute_program__program__id"] = program
            kwargs["education_institute_program__program__is_archive"] = False
            kwargs["education_institute_program__is_archive"] = False
        if level is not None and level > 0:
            kwargs["education_institute_program__program__level"] = level
            kwargs["education_institute_program__program__is_archive"] = False
            kwargs["education_institute_program__is_archive"] = False

        kwargs["is_archive"] = False
        kwargs["accreditation"] = 'A'

        institutes = Education_Institute.objects.filter(**kwargs).distinct().values()
        return HttpResponse(json.dumps(list(institutes), cls=DjangoJSONEncoder),content_type='application/json')
    else:
        return HttpResponse(json.dumps(list()),content_type='application/json')

@csrf_exempt
def get_seminar_attribute(request):
    if request.is_ajax():
        institute = program = None

        institute    = get_from_dict(institute,request.POST, 'institute')
        program      = get_from_dict(program,request.POST, 'program')
        has_seminar  = False

        try:
            institute_program = Education_Institute_Program.objects.get(institute = institute,program = program,is_archive = False)
            has_seminar = institute_program.has_seminar
        except :
            has_seminar = False

        return HttpResponse(json.dumps(has_seminar, cls=DjangoJSONEncoder),content_type='application/json')
    else:
        return HttpResponse(json.dumps(False, cls=DjangoJSONEncoder),content_type='application/json')


@csrf_exempt
@login_required
def item(request,_type,id = None):
    step = -1
    init_dict = {}
    if request.user.is_authenticated() and ( request.user.is_admin == True or ( request.user.is_curator == True and _type == "group" ) ):

        if _type == "region":
            ItemModel = Region
            ItemForm = RegionForm
        elif _type == "city":
            ItemModel = City
            ItemForm = CityForm
        else:
            return redirect('personal_cabinet')

        if id is None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)

        if request.method == 'POST' and step == -1:
            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'user':
                        partfieldname = formfieldname + '_id'
                    elif formfieldname == 'region':
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif _type == "institute" and formfieldname == 'city':
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif _type == "institute" and  ( formfieldname == 'password1' or formfieldname == 'password2' or  formfieldname == 'programs' or formfieldname == 'fio'):
                        continue
                    elif formfieldname == 'edu_institute':
                        continue
                    else:
                        partfieldname = formfieldname

                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                model_item.save()
                return HttpResponseRedirect('/edu/'+_type + '/' + str(model_item.id) + '/')
        elif len(init_dict.keys()) > 0:
            form = ItemForm(instance=model_item,initial=init_dict)

        else:
            form = ItemForm(instance=model_item)

        return render_to_response("form.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':_type}, context_instance=RequestContext(request))

    else:
#        ====================================================VIEW ITEM=============================================
        if _type == "region":
            ItemModel = Region
            template  = 'personal_cabinet_region.html'
        elif _type == "city":
            ItemModel = City
            template  = 'personal_cabinet_city.html'
        else:
            return redirect('home')

        model_item = get_object_or_404(ItemModel,id=id)
        return render_to_response(template, {'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':_type}, context_instance=RequestContext(request))

@csrf_exempt
#@login_required
def institute(request,id = None):
    init_dict = {}

    # ============================= ADMIN edit/create institute ==========================================
    if request.user.is_authenticated() and request.user.is_admin is True:

        ItemModel = Education_Institute

        if id is None:
            ItemForm  = Education_InstituteCreateForm
        else:
            ItemForm  = Education_InstituteForm

        if id is None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)

        if request.method == 'POST':

            form = ItemForm(request.POST,instance=model_item) # A form bound to the POST data

            lt_program_id = request.POST.getlist('programs')

            if len(lt_program_id) > 0:
                lt_program_obj = Education_Programme.objects.filter(id__in = lt_program_id,ipr_program = True)
            else:
                lt_program_obj = []

            l_valid_form = form.is_valid()

            if form.cleaned_data['access_ipr_program'] != True and len(lt_program_obj) > 0:
                l_valid_programmes = False
                form._errors['access_ipr_program'] = form.error_class([u'Организация аккредитована на программы Палаты'])
            else:
                l_valid_programmes = True

            if l_valid_form and l_valid_programmes:
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'user':
                        partfieldname = formfieldname + '_id'
                    elif formfieldname == 'region':
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'city':
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif ( formfieldname == 'password1' or formfieldname == 'password2' or  formfieldname == 'programs' or formfieldname == 'fio'):
                        continue
                    elif formfieldname == 'edu_institute':
                        continue
                    elif formfieldname == 'certificate':

                        model_item.certificate = delete_loaded_file(request, model_item.certificate, formfieldname)
                        continue

                    elif formfieldname == 'certificate2':
                        model_item.certificate2 = delete_loaded_file(request, model_item.certificate2, formfieldname)
                        continue

                    else:
                        partfieldname = formfieldname

                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if request.FILES.has_key('certificate'):
                    model_item.certificate = upload_file(request, u'/media/uploads/certificates/',
                                                          model_item.certificate, 'certificate', id)

                if request.FILES.has_key('certificate2'):
                    model_item.certificate2 = upload_file(request, u'/media/uploads/certificates2/', model_item.certificate2, 'certificate2',id)

                if id is None:
                    #print form.cleaned_data['fio'].split(' ',1)
                    try:
                        last_name,first_name =  form.cleaned_data['fio'].split(' ',1)
                    except:
                        last_name  = form.cleaned_data['fio']
                        first_name = form.cleaned_data['fio']

                    user = User.objects.create_user(form.cleaned_data['email'], first_name, last_name, form.cleaned_data['password1'])
                    user.is_active = True
                    user.is_curator = True
                    user.save()
                    model_item.curator = user

                if id is not None and request.POST.has_key('password1'):
                    try:
                        user = User.objects.get(email = form.cleaned_data['email'])
                        user.set_password(request.POST['password1'])
                    except:
                        user = User.objects.create_user(form.cleaned_data['email'], u'', u'', request.POST['password1'])

                    user.is_active = True
                    user.is_curator = True
                    user.save()
                    model_item.curator = user

                if id is not None and request.POST.has_key('fio') and model_item.curator is not None:
                    try:
                        model_item.curator.last_name,model_item.curator.first_name =  request.POST['fio'].split(' ',1)
                    except:
                        None
                    model_item.curator.save()

                model_item.save()

                change_programs = []

                for program in request.POST.getlist('programs'):
                    try:
                        program_institute = request.POST["program_institute_"+str(program)]
                    except :
                        program_institute = None

                    try:
                        noon = timedelta(hours=12)
                        if id is None or program_institute is None or program_institute == u'None' or program_institute == u'':
                            #print  "CREATE"
                            new_education_institute_program,created = Education_Institute_Program.objects.get_or_create(institute = model_item,program = Education_Programme.objects.get(id=program))
                            new_education_institute_program.accreditation_finish = (datetime.strptime(request.POST["accreditation_finish_"+str(program)], '%d.%m.%Y') + noon if request.POST["accreditation_finish_"+str(program)] is not None and request.POST["accreditation_finish_"+str(program)] != u'None' and request.POST["accreditation_finish_"+str(program)] != u'' else None)

                            if created is False:
                                new_education_institute_program.get_archive()
                                #Education_Institute_Program(institute = Education_Institute.objects.get(id=institute),program = model_item,accreditation_finish = (datetime.strptime(request.POST["accreditation_finish_"+str(institute)], '%d.%m.%Y') + noon if request.POST["accreditation_finish_"+str(institute)] is not None and request.POST["accreditation_finish_"+str(institute)] != u'None' and request.POST["accreditation_finish_"+str(institute)] != u'' else None)) #,recruiting_date = datetime.strptime(request.POST["recruiting_date_"+str(institute)], '%d.%m.%Y') + noon).save()
                            new_education_institute_program.save()
                            change_programs.append(new_education_institute_program.id)
                        else:
                            change_programs.append(program_institute)
                            edu_inst_pro = Education_Institute_Program.objects.get(id = program_institute)
                            edu_inst_pro.accreditation_finish = ( datetime.strptime(request.POST["accreditation_finish_"+str(program)], '%d.%m.%Y') + noon  if request.POST["accreditation_finish_"+str(program)] is not None and request.POST["accreditation_finish_"+str(program)] != u'None' and request.POST["accreditation_finish_"+str(program)] != u'' else None)
                            edu_inst_pro.save()
                    except :
                        None

                for each in model_item.education_institute_program_set.filter(~Q(id__in=change_programs)):
                    each.set_archive()
                change_seminars = []

                for seminar in request.POST.getlist('seminars'):
                    try:
                        program_institute = request.POST["program_institute_"+str(seminar)]
                        if program_institute.isdigit():
                            edu_inst_pro = Education_Institute_Program.objects.get(id = program_institute)
                        else:
                            edu_inst_pro = Education_Institute_Program.objects.get(institute = model_item,program = Education_Programme.objects.get(id=seminar))

                        edu_inst_pro.has_seminar = True
                        edu_inst_pro.save()
                        change_seminars.append(edu_inst_pro.id)
                    except :
                        program_institute = None

                for each in model_item.education_institute_program_set.filter(~Q(id__in=change_seminars),Q(has_seminar = True)):
                    each.has_seminar = False
                    each.save()

                return HttpResponseRedirect('/edu/institute/' + str(model_item.id) + '/')
        else:
            form = ItemForm(instance=model_item)

        model_item.accreditations = ACCREDITATION_DICT
        if id is None:
            model_item.programs = Education_Programme.objects.all()
        up_groups = model_item.up_groups()
        base_groups = model_item.base_groups()

        requests_count = 0
        edurequests = []
        if id is not None:
            edurequests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=model_item),Q(is_archive = False))
            requests_count = edurequests.count()

        return render_to_response("institute_form.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,
                                                          'up_groups':up_groups,'base_groups':base_groups,
                                                          'requests_count':requests_count,'edurequests':edurequests}, context_instance=RequestContext(request))

    else:
#        ====================================================VIEW ITEM=============================================
        man = None

        if request.user.is_authenticated() and request.user.is_ipr:
            man = IprStudent.objects.get(user = request.user)

        ItemModel = Education_Institute
        template  = 'personal_cabinet_institute.html'
        model_item = get_object_or_404(ItemModel,id=id)
        up_recruitments = Recruitment.objects.filter(institute_program__institute = model_item,institute_program__program__level = 'U',is_archive = False).order_by('begin_date')
        base_recruitments = Recruitment.objects.filter(institute_program__institute = model_item,institute_program__program__level = 'B',is_archive = False).order_by('begin_date')
        up_groups = model_item.up_groups()
        base_groups = model_item.base_groups()

        return render_to_response(template, {'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':'institute','base_recruitments':base_recruitments,'up_recruitments':up_recruitments, 'man':man,'up_groups':up_groups,'base_groups':base_groups}, context_instance=RequestContext(request))

@csrf_exempt
#@login_required
def program(request,id = None):
    init_dict = {}
    if request.user.is_authenticated() and request.user.is_admin == True:

        ItemModel = Education_Programme
        ItemForm  = Education_ProgrammeForm

        if id == None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)

        if request.method == 'POST':
            form = ItemForm(request.POST)

            l_form_valid = form.is_valid()

            lt_institute_id = request.POST.getlist('institutes')

            if len(lt_institute_id) > 0:
                lt_institute_obj = Education_Institute.objects.filter(id__in = lt_institute_id) #,access_ipr_program = False)
            else:
                lt_institute_obj = []

            if form.cleaned_data['ipr_program'] == True and len(lt_institute_obj) > 0:
                l_institutes_valid = False
                form._errors['ipr_program'] = form.error_class([u'На программу аккредитованы организации, не имеющие доступа'])
            else:
                l_institutes_valid = True


            if l_form_valid and l_institutes_valid:
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'user':
                        partfieldname = formfieldname + '_id'
                    elif formfieldname == 'region':
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'presentation':
                        model_item.presentation = delete_loaded_file(request, model_item.presentation, formfieldname)
                        continue
                    else:
                        partfieldname = formfieldname

                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if request.FILES.has_key('presentation'):
                    model_item.presentation = upload_file(request, u'/media/uploads/presentations/',
                                                          model_item.presentation, 'presentation', id)

                model_item.save()

                change_institutes = []
                for institute in request.POST.getlist('institutes'):
                    try:
                        program_institute = request.POST["program_institute_"+str(institute)]
                    except :
                        program_institute = None

                    try:
                        noon = timedelta(hours=12)
                        if id is None or program_institute is None or program_institute == u'None' or program_institute == u'':
                            new_education_institute_program,created = Education_Institute_Program.objects.get_or_create(institute = Education_Institute.objects.get(id=institute),program = model_item)
                            new_education_institute_program.accreditation_finish = (datetime.strptime(request.POST["accreditation_finish_"+str(institute)], '%d.%m.%Y') + noon if request.POST["accreditation_finish_"+str(institute)] is not None and request.POST["accreditation_finish_"+str(institute)] != u'None' and request.POST["accreditation_finish_"+str(institute)] != u'' else None)

                            if created is False:
                                new_education_institute_program.get_archive()
                            new_education_institute_program.save()
                            change_institutes.append(new_education_institute_program.id)
                        else:
                            change_institutes.append(program_institute)
                            edu_inst_pro = Education_Institute_Program.objects.get(id = program_institute)
                            edu_inst_pro.accreditation_finish = ( datetime.strptime(request.POST["accreditation_finish_"+str(institute)], '%d.%m.%Y') + noon  if request.POST["accreditation_finish_"+str(institute)] is not None and request.POST["accreditation_finish_"+str(institute)] != u'None' and request.POST["accreditation_finish_"+str(institute)] != u'' else None)
                            edu_inst_pro.save()
                    except :
                        None

                if len(change_institutes) > 0:
                    for each in model_item.education_institute_program_set.filter(~Q(id__in=change_institutes)):
                        each.set_archive()

                if model_item.level == 'U':
                    change_seminars = []
                    for seminar in request.POST.getlist('seminars'):
                        try:
                            program_institute = request.POST["program_institute_"+str(seminar)]
                            if program_institute.isdigit():
                                edu_inst_pro = Education_Institute_Program.objects.get(id = program_institute)
                            else:
                                edu_inst_pro = Education_Institute_Program.objects.get(program = model_item,institute = Education_Institute.objects.get(id=seminar))

                            edu_inst_pro.has_seminar = True
                            edu_inst_pro.save()
                            change_seminars.append(edu_inst_pro.id)
                        except :
                            program_institute = None

                    for each in model_item.education_institute_program_set.filter(~Q(id__in=change_seminars),Q(has_seminar = True)):
                        each.has_seminar = False
                        each.save()

                return HttpResponseRedirect('/edu/program/' + str(model_item.id) + '/')
        else:
            form = ItemForm(instance=model_item)

        model_item.accreditations = ACCREDITATION_DICT
        if id is None:
            model_item.institutes = Education_Institute.objects.filter(is_archive = False).order_by('name')
        return render_to_response("program_form.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    else:
        return redirect('program_catalog')
#        ====================================================VIEW ITEM=============================================
#        ItemModel = Education_Programme
#        template  = 'personal_cabinet_program.html'
#        model_item = get_object_or_404(ItemModel,id=id)
#
#        return render_to_response(template, {'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':'program'}, context_instance=RequestContext(request))


@csrf_exempt
@login_required
def realize_program(request,id):
    response = {}

    if request.method == 'POST' and request.user.is_curator == True:
        form = RealizeProgram(request.POST)
        if form.is_valid():
            curator = Education_Institute.objects.get(curator=request.user)
            model_item = get_object_or_404(Education_Institute_Program,id=id)

            for formfieldname in form._meta.__dict__['fields']:
                if formfieldname == 'form': #period':
                    partfieldname = formfieldname + '_id'
                else:
                    partfieldname = formfieldname

                model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

            model_item.save()
            if request.is_ajax:
                response["status"] = 1
                response["accreditation"] = str(model_item.accreditation_finish)
                return HttpResponse(json.dumps(response))
            else:
                return redirect('personal_cabinet')
        else:
            print form.errors
            if request.is_ajax:
                response["status"] = 0
                return HttpResponse(json.dumps(response))
            else:
                return redirect('personal_cabinet')
    else:
        return redirect('personal_cabinet')

@login_required
def item_delete(request,_type,id):
    if request.user.is_admin == True:

        if _type == "region":
            ItemModel = Region
        elif _type == "city":
            ItemModel = City
        elif _type == "institute":
            ItemModel = Education_Institute
        elif _type == "program":
            ItemModel = Education_Programme
#        elif _type == "curatorassign":
#            ItemModel = InstituteCurator
        elif _type == "group":
            ItemModel = Group
        elif _type == "recruitment":
            ItemModel = Recruitment
        elif _type == "request":
            ItemModel = EducationRequest
        elif _type == "profstatus":
            ItemModel = ProfStatus
        elif _type == "mode":
            ItemModel = StudyMode
        elif _type == "result":
            ItemModel = ExamResult
        elif _type == "menuitem":
            ItemModel = MenuItem

        model_item = get_object_or_404(ItemModel,id=id)

        if _type == "profstatus" or  _type == "mode" or  _type == "result" or  _type == "menuitem":
            model_item.delete()
        else:
            model_item.set_archive()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        if _type == "recruitment" and request.user.is_curator == True:
            model_item = get_object_or_404(Recruitment,id=id)
            if model_item.institute_program.institute.curator == request.user:
                model_item.set_archive()
        
        return redirect('personal_cabinet')

@csrf_exempt
#@login_required
def item_ajax(request,_type):
    region     = city     = institute = level     = program = None
    begin_date = end_date = student   = groupname = None
    exam_begin_date = exam_end_date = None

    answer = []
    kwargs = {}

    print request.POST
    print _type

    if request.is_ajax(): #and ( request.user.is_admin == True or request.user.is_curator == True ):
        region = get_from_dict(region, request.POST, 'region',True)
        city   = get_from_dict(city, request.POST, 'city',True)
        institute   = get_from_dict(institute, request.POST, 'institute',True)
        level       = get_from_dict(level, request.POST, 'level',False)
        program     = get_from_dict(program, request.POST, 'program',True)

        if request.POST.has_key('begin_date') and  request.POST['begin_date'] != u'':
            begin_date = request.POST['begin_date']
        if request.POST.has_key('end_date') and  request.POST['end_date'] != u'':
            end_date = request.POST['end_date']
        if request.POST.has_key('student') and  request.POST['student'] != u'':
            student = request.POST['student']
        if request.POST.has_key('exam_begin_date') and  request.POST['exam_begin_date'] != u'':
            exam_begin_date = request.POST['exam_begin_date']
        if request.POST.has_key('exam_end_date') and  request.POST['exam_end_date'] != u'':
            exam_end_date = request.POST['exam_end_date']
        if request.POST.has_key('groupname') and  request.POST['groupname'] != u'':
            groupname = request.POST['groupname']

        if _type == "region":
            ItemModel = Region

            if region is not None:
                kwargs["id"] = region
            if city is not None:
#                kwargs["id__in"] = City.objects.filter(id=city).values("region")
                kwargs["city__id"] = city

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["city__education_institute__curator"] = request.user

        elif _type == "city":
            ItemModel = City

            if region is not None:
                kwargs["region"] = region
            if city is not None:
                kwargs["id"] = city
            if institute is not None:
#                kwargs["id__in"] = Education_Institute.objects.filter(id = institute).values("city")
                kwargs["education_institute__id"] = institute
                kwargs["education_institute_is_archive"] = False

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["education_institute__curator"] = request.user

        elif _type == "institute":
            ItemModel = Education_Institute

            if region is not None:
                kwargs["city__region"] = region
            if city is not None:
                kwargs["city"] = city
            if institute is not None:
                kwargs["id"] = institute
            if program is not None:
#                kwargs["id__in"] = Education_Programme.objects.filter(id = program).values("edu_institute")
                kwargs["education_institute_program__program__id"] = program
                kwargs["education_institute_program__program__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
            if level is not None:
                kwargs["education_institute_program__program__level"] = level
                kwargs["education_institute_program__program__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["curator"] = request.user

            kwargs["is_archive"] = False

        elif _type == "program":
            ItemModel = Education_Programme

            if region is not None:
                kwargs["education_institute_program__institute__city__region__id"] = region
                kwargs["education_institute_program__institute__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
            if city is not None:
                kwargs["education_institute_program__institute__city__id"] = city
                kwargs["education_institute_program__institute__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
            if institute is not None:
                kwargs["education_institute_program__institute__id"] = institute
                kwargs["education_institute_program__institute__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
            if program is not None:
                kwargs["id"] = program
                kwargs["education_institute_program__institute__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
            if level is not None:
                kwargs["education_institute_program__institute__is_archive"] = False
                kwargs["education_institute_program__is_archive"] = False
                kwargs["level"] = level
            kwargs["is_archive"] = False
            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["education_institute_program__institute__curator"] = request.user

        elif _type == "level":
            ItemModel = Education_Programme

            if region is not None:
                kwargs["education_institute_program__institute__city__region"] = region
            elif city is not None:
                kwargs["education_institute_program__institute__city"] = city
            elif institute is not None:
                kwargs["education_institute_program__institute"] = institute
            elif program is not None:
                kwargs["id"] = program
            elif level is not None:
                kwargs["level"] = level

            kwargs["education_institute_program__institute__is_archive"] = False
            kwargs["education_institute_program__is_archive"] = False
            kwargs["is_archive"] = False

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["education_institute_program__institute__curator"] = request.user

        elif _type == "group":
            ItemModel = Group
            kwargs["is_archive"] = False
            kwargs["education_institute__is_archive"] = False

            if region is not None:
                kwargs["education_institute__city__region"] = region
            if city is not None:
                kwargs["education_institute__city"] = city
            if institute is not None:
                kwargs["education_institute"] = institute
            if program is not None:
                kwargs["studentgroup__request__institute_program__program__id"] = program
            if level is not None:
                kwargs["level"] = level
            if begin_date is not None:
                kwargs["education_start__gte"] = datetime.strptime(begin_date, '%d.%m.%Y')
            if end_date is not None:
                kwargs["education_end__lte"] = datetime.strptime(end_date, '%d.%m.%Y')
            if student is not None:

                words = student.split()
                Qr = None
                for field in words:
                    q = Q(**{"user__last_name__icontains": field })
                    if Qr:
                        Qr = Qr | q # or & for filtering
                    else:
                        Qr = q

                    q = Q(**{"user__first_name__icontains": field })
                    if Qr:
                        Qr = Qr | q # or & for filtering
                    else:
                        Qr = q

                kwargs["studentgroup__student__in"] = Student.objects.filter(Qr)

            if exam_begin_date is not None:
                kwargs["exam_date__gte"] = datetime.strptime(exam_begin_date, '%d.%m.%Y')
            if exam_end_date is not None:
                kwargs["exam_date__lte"] = datetime.strptime(exam_end_date, '%d.%m.%Y')
            if groupname is not None:
                kwargs["name__icontains"] = groupname

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["education_institute__curator"] = request.user

        elif _type == "request":
            ItemModel = EducationRequest
            kwargs["institute_program__institute__is_archive"] = False
            kwargs["institute_program__program__is_archive"] = False
            kwargs["institute_program__is_archive"] = False

            if institute is not None:
                kwargs["institute_program__institute"] = institute
            if program is not None:
                kwargs["institute_program__program__id"] = program

            if request.user.is_authenticated() == True and request.user.is_curator == True:
                kwargs["institute_program__institute__curator"] = request.user

        elif _type == "years":
            ItemModel = MembershipCost
            kwargs["region__is_archive"] = False

            if region is not None:
                kwargs["region"] = region

        items = ItemModel.objects.filter(**kwargs)

        if _type == "level":
            answer = [item.level for item in items]
        elif _type == "years":
            answer = [{"id":item.id,"year":item.year,"cost":str(item.cost)} for item in items]
        else:
            answer = [item.id for item in items]

#        if len(answer) == 0 and _type == "program":
#            answer = [item.id for item in Education_Programme.objects.all()]
        return HttpResponse(json.dumps(answer),content_type='application/json')
    else:
        return HttpResponse(json.dumps([]),content_type='application/json')

#@login_required
@csrf_exempt
def program_catalog(request):
    if not request.user.is_authenticated():
        ipr_programs = Education_Programme.objects.filter(level='U',is_archive = False)
        student_programs = Education_Programme.objects.filter(level='B',is_archive = False)
        return render_to_response('catalog_program.html',{"ipr_programs":ipr_programs,"student_programs":student_programs},context_instance=RequestContext(request))
    elif request.user.is_admin == True:
        cities = City.objects.all().order_by('name')
        regions = Region.objects.all()
        institutes  = Education_Institute.objects.filter(is_archive = False)

        region = city = institute = only_pnk = only_seminars = None

        region           = get_from_dict(region,request.POST, 'region')
        city             = get_from_dict(city,request.POST, 'city')
        institute        = get_from_dict(institute,request.POST, 'institute')
        only_pnk         = get_from_dict(only_pnk,request.POST, 'only_pnk',True)
        only_seminars    = get_from_dict(only_seminars,request.POST, 'only_seminars',True)

        kwargs = {}
        base_kwargs = {}
        up_kwargs = {}

        kwargs["is_archive"] = False

        if region is not None:
            kwargs["education_institute_program__institute__city__region"] = region
        if city is not None:
            kwargs["education_institute_program__institute__city"] = city
        if institute is not None:
            kwargs["education_institute_program__institute"] = institute
            kwargs["education_institute_program__is_archive"] = False
        if only_pnk == 1:
            kwargs["ipr_program"] = True
        if only_seminars == 1:
            kwargs["education_institute_program__has_seminar"] = True
            kwargs["education_institute_program__is_archive"] = False
#            kwargs["education_institute_program__accreditation_finish__"] = False

        print kwargs

        base_kwargs = kwargs.copy()
        up_kwargs = kwargs.copy()

        base_kwargs['level'] = 'B'
        up_kwargs['level']   = 'U'

        ipr_programs = Education_Programme.objects.filter(**up_kwargs).distinct()
        student_programs = Education_Programme.objects.filter(**base_kwargs).distinct()
        return render_to_response('catalog_program.html',{"cities":cities,"regions": regions,"institutes":institutes,"ipr_programs":ipr_programs,"student_programs":student_programs,"levels":PROGRAM_LEVEL_CHOICES,'region':region,'city':city,'institute':institute,'only_seminars':only_seminars,'only_pnk':only_pnk},context_instance=RequestContext(request))
#    elif request.user.is_curator == True:
##        curator = InstituteCurator.objects.get(user=request.user)
#        curator = Education_Institute.objects.get(curator = request.user)
#        programs = curator.education_institute_program_set.filter(is_archive = False)
#        forms = StudyMode.objects.all()
##        periods = Periodicity.objects.all()
#        return render_to_response('catalog_program.html',{"forms":forms,"curator":curator,"programs":programs,"levels":PROGRAM_LEVEL_CHOICES},context_instance=RequestContext(request))
    else:
        ipr_programs = Education_Programme.objects.filter(level='U',is_archive = False)
        student_programs = Education_Programme.objects.filter(level='B',is_archive = False)
        return render_to_response('catalog_program.html',{"ipr_programs":ipr_programs,"student_programs":student_programs},context_instance=RequestContext(request))

    return redirect('personal_cabinet')

#@login_required
#@csrf_exempt
def groups(request):
    region = city = institute = program = begin_date = end_date = student = groupname = None
    exam_begin_date = exam_end_date = volum_min = volum_max = up_current_page = base_current_page = None
    attestat_number = None
    item_on_page = 30 #None

    kwargs = {}
    up_kwargs = {}
    base_kwargs = {}

    asc_name = asc_institute = asc_city = asc_begdate = asc_examdate = asc_volum = 0

    region               = get_from_dict(region,request.POST, 'region')
    city                 = get_from_dict(city,request.POST, 'city')
    institute            = get_from_dict(institute,request.POST, 'institute')
    program              = get_from_dict(program,request.POST, 'program')
    begin_date           = get_from_dict(begin_date,request.POST, 'begin_date',False,False,u'')
    end_date             = get_from_dict(end_date,request.POST, 'end_date',False,False,u'')
    student              = get_from_dict(student,request.POST, 'student',False,False,u'')
    exam_begin_date      = get_from_dict(exam_begin_date,request.POST, 'exam_begin_date',False,False,u'')
    exam_end_date        = get_from_dict(exam_end_date,request.POST, 'exam_end_date',False,False,u'')
    groupname            = get_from_dict(groupname,request.POST, 'groupname',False,False,u'')
    attestat_number      = get_from_dict(attestat_number,request.POST, 'attestat_number',False,False,u'')
    volum_min            = get_from_dict(volum_min,request.POST, 'volum_min',False,False,u'')
    volum_max            = get_from_dict(volum_max,request.POST, 'volum_max',False,False,u'')
    asc_name             = get_from_dict(asc_name,request.POST, 'asc_name',True,True)
    asc_institute        = get_from_dict(asc_institute,request.POST, 'asc_institute',True,True)
    asc_city             = get_from_dict(asc_city,request.POST, 'asc_city',True,True)
    asc_begdate          = get_from_dict(asc_begdate,request.POST, 'asc_begdate',True,True)
    asc_examdate         = get_from_dict(asc_examdate,request.POST, 'asc_examdate',True,True)
    asc_volum            = get_from_dict(asc_volum,request.POST, 'asc_volum',True,True)
    up_current_page      = get_from_dict(up_current_page,request.POST, 'up_current_page',True)
    base_current_page    = get_from_dict(base_current_page,request.POST, 'base_current_page',True)

    if base_current_page is None or base_current_page < 1:
        base_current_page = 1
    if up_current_page is None or up_current_page < 1:
        up_current_page = 1

    up_llimit = item_on_page * ( up_current_page - 1 )
    up_rlimit = item_on_page * up_current_page

    base_llimit = item_on_page * ( base_current_page - 1 )
    base_rlimit = item_on_page * base_current_page

#    if ( request.user.is_authenticated() and request.user.is_admin != True ) or not request.user.is_authenticated():
    kwargs["is_archive"] = False
    kwargs["education_institute__is_archive"] = False

    if region is not None:
        kwargs["education_institute__city__region"] = region
    if city is not None:
        kwargs["education_institute__city"] = city
    if institute is not None:
        kwargs["education_institute"] = institute
    if program is not None:
        kwargs["studentgroup__request__institute_program__program__id"] = program

    if begin_date is not None:
        kwargs["education_start__gte"] = datetime.strptime(begin_date, '%d.%m.%Y')
    if end_date is not None:
        kwargs["education_end__lte"] = datetime.strptime(end_date, '%d.%m.%Y')

    if student is not None or attestat_number is not None:
        Qr    = None
        words = []

        if student is not None:
            words = student.split()

        for field in words:
            q = Q(**{"user__last_name__icontains": field })
            if Qr:
                Qr = Qr | q # or & for filtering
            else:
                Qr = q

            q = Q(**{"user__first_name__icontains": field })
            if Qr:
                Qr = Qr | q # or & for filtering
            else:
                Qr = q

            q = Q(**{"patronymic__icontains": field })
            if Qr:
                Qr = Qr | q # or & for filtering
            else:
                Qr = q

        if attestat_number is not None:
            q = Q(**{"id__in": IprStudent.objects.filter(attestat_number = attestat_number) })
            if Qr:
                Qr = Qr & q # or & for filtering
            else:
                Qr = q

        kwargs["studentgroup__student__in"] = Student.objects.filter(Qr)

    if groupname is not None:
        kwargs["name__icontains"] = groupname

    up_kwargs = kwargs.copy()
    base_kwargs = kwargs.copy()

    base_kwargs["level"] = "B"
    up_kwargs["level"] = "U"

    if exam_begin_date is not None:
        base_kwargs["exam_date__gte"] = datetime.strptime(exam_begin_date, '%d.%m.%Y')
    if exam_end_date is not None:
        base_kwargs["exam_date__lte"] = datetime.strptime(exam_end_date, '%d.%m.%Y')

    if volum_min is not None:
        up_kwargs["volume_academ_hours__gte"] = volum_min
    if volum_max is not None:
        up_kwargs["volume_academ_hours__lte"] = volum_max

    if request.user.is_authenticated() and request.user.is_admin == 'P':
        cities = City.objects.all().order_by('name')
        regions = Region.objects.all()
        institutes  = Education_Institute.objects.filter(is_archive = False)
        programs = Education_Programme.objects.filter(is_archive = False)
        base_groups = Group.objects.filter(**base_kwargs)
        up_groups = Group.objects.filter(**up_kwargs)
#        edurequests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),institute_program__is_archive = False,institute_program__program__is_archive = False,institute_program__institute__is_archive = False,is_archive = False)

        return render_to_response('groups.html',{"cities":cities,"regions": regions,"institutes":institutes,
                                                 "region":region,"city": city,"institute":institute,"program":program,
                                                 #"edurequests":edurequests,
                                                 "exam_begin_date":exam_begin_date,"exam_end_date": exam_end_date,
                                                 "begin_date":begin_date,"end_date": end_date,"levels":PROGRAM_LEVEL_CHOICES,
                                                 "programs":programs,"upgroups":up_groups,"basegroups":base_groups,"student":student,
                                                 "groupname":groupname}
            ,context_instance=RequestContext(request))

    elif request.user.is_authenticated() and request.user.is_curator == True:
        curator = Education_Institute.objects.get(curator = request.user)
        programs = curator.education_institute_program_set.filter(is_archive = False,program__is_archive = False)
        up_groups = curator.group_set.filter(**up_kwargs).distinct() #is_archive = False,level = 'U')
        base_groups = curator.group_set.filter(**base_kwargs).distinct() #is_archive = False,level = 'B')

        return render_to_response('groups.html',{"curator":curator, #"edurequests":edurequests,
                                                 "begin_date":begin_date,"end_date": end_date,"levels":PROGRAM_LEVEL_CHOICES,
                                                 "exam_begin_date":exam_begin_date,"exam_end_date": exam_end_date,
                                                 "programs":programs,"upgroups":up_groups,"basegroups":base_groups,"student":student,
                                                 "groupname":groupname}
            ,context_instance=RequestContext(request))
    else:
        cities = City.objects.all().order_by('name')
        regions = Region.objects.all()
        institutes  = Education_Institute.objects.filter(is_archive = False)

        base_groups = Group.objects.filter(**base_kwargs).distinct()
        up_groups = Group.objects.filter(**up_kwargs).distinct()

        asc_arg = []

        if asc_city == 1:
            asc_arg.append('education_institute__city')
        elif asc_city == -1:
            asc_arg.append('-education_institute__city')

        if asc_name == 1:
            asc_arg.append('name')
        elif asc_name == -1:
            asc_arg.append('-name')

        if asc_institute == 1:
            asc_arg.append('education_institute')
        elif asc_institute == -1:
            asc_arg.append('-education_institute')

        if asc_begdate == 1:
            asc_arg.append('-education_start')
        elif asc_begdate == -1:
            asc_arg.append('education_start')

        base_groups = base_groups.order_by(*asc_arg)
        up_groups   = up_groups.order_by(*asc_arg)

        if asc_examdate == 1:
            base_groups = base_groups.order_by('-exam_date')
        elif asc_examdate == -1:
            base_groups = base_groups.order_by('exam_date')

        if asc_volum == 1:
            up_groups = up_groups.order_by('volume_academ_hours')
        elif asc_volum == -1:
            up_groups = up_groups.order_by('-volume_academ_hours')

        base_count_groups = base_groups.count()
        base_all_pages = base_count_groups / item_on_page
        if base_count_groups % item_on_page > 0:
            base_all_pages = base_all_pages + 1

        if base_llimit > base_count_groups or base_current_page == 1:
            base_current_page = 1
            base_groups = base_groups[:item_on_page]
        else:
            base_groups = base_groups[base_llimit:base_rlimit]

        up_count_groups = up_groups.count()
        up_all_pages = up_count_groups / item_on_page
        if up_count_groups % item_on_page > 0:
            up_all_pages = up_all_pages + 1

        if up_llimit > up_count_groups or up_current_page == 1:
            up_current_page = 1
            up_groups = up_groups[:item_on_page]
        else:
            up_groups = up_groups[up_llimit:up_rlimit]

        return render_to_response('groups.html',{"cities":cities,"regions": regions,"institutes":institutes,
                                                 "region":region,"city": city,"institute":institute,
                                                 "begin_date":begin_date,"end_date": end_date,"student":student,
                                                 "groupname":groupname,"exam_begin_date":exam_begin_date,"exam_end_date": exam_end_date,
                                                 "volum_min":volum_min,"volum_max":volum_max,"levels":PROGRAM_LEVEL_CHOICES,
                                                 "up_groups":up_groups,"base_groups":base_groups,
                                                 "asc_institute":asc_institute,"asc_name":asc_name,"asc_city":asc_city,
                                                 "asc_begdate":asc_begdate,"asc_examdate":asc_examdate,"asc_volum":asc_volum,
                                                 "base_current_page":base_current_page,"up_current_page":up_current_page,"base_all_pages":base_all_pages,
                                                 "up_all_pages":up_all_pages,"up_count_groups":up_count_groups,"base_count_groups":base_count_groups}
            ,context_instance=RequestContext(request))
    return redirect('personal_cabinet')

def recruits(request):
    delete_success = None
    if request.session.has_key('delete_success'):
        delete_success = request.session['delete_success']
        request.session.pop('delete_success',None)


    region      = city        = institute   = program     = studymode   = None
    begin_date  = end_date    = begin_price = end_price   = None
    
    kwargs = {}

    if request.user.is_authenticated() and request.user.is_curator == True:
        curator     = Education_Institute.objects.get(curator = request.user)
        region      = curator.city.region.id
        city        = curator.city.id
        institute   = curator.id
    else:
        region      = get_from_dict(region,request.POST, 'region')
        city        = get_from_dict(city,request.POST, 'city')

    asc_program    = asc_institute  = asc_region     = asc_begdate    = 0
    asc_price      = 0

    institute      = get_from_dict(program,request.POST, 'institute')
    program        = get_from_dict(program,request.POST, 'program')
    studymode      = get_from_dict(studymode,request.POST, 'studymode')
    begin_date     = get_from_dict(begin_date,request.POST, 'begin_date')
    end_date       = get_from_dict(end_date,request.POST, 'end_date')
    begin_price    = get_from_dict(begin_price,request.POST, 'begin_price')
    end_price      = get_from_dict(end_price,request.POST, 'end_price')

    asc_region     = get_from_dict(asc_region,request.POST, 'asc_region',True,True)
    asc_program    = get_from_dict(asc_program,request.POST, 'asc_program',True,True)
    asc_institute  = get_from_dict(asc_institute,request.POST, 'asc_institute',True,True)
    asc_begdate    = get_from_dict(asc_begdate,request.POST, 'asc_begdate',True,True)
    asc_price      = get_from_dict(asc_price,request.POST, 'asc_price',True,True)


    kwargs["is_archive"] = False
    kwargs["institute_program__institute__is_archive"] = False
    kwargs["institute_program__program__is_archive"] = False
    kwargs["institute_program__is_archive"] = False
    kwargs["institute_program__institute__accreditation"] = 'A'

    if region is not None:
        kwargs["institute_program__institute__city__region"] = region
    if city is not None:
        kwargs["institute_program__institute__city"] = city
    if institute is not None:
        kwargs["institute_program__institute"] = institute
    if program is not None:
        kwargs["institute_program__program"] = program
    if studymode is not None:
        kwargs["form"] = studymode

    try:
        if begin_date is not None:
            kwargs["begin_date__gte"] = datetime.strptime(begin_date, '%d.%m.%Y')
    except ValueError:
        None

    try:
        if end_date is not None:
            kwargs["begin_date__lte"] = datetime.strptime(end_date, '%d.%m.%Y')
    except ValueError:
        None

    if begin_price is not None and begin_price != '':
        kwargs["price__gte"] = begin_price
    if end_price is not None and end_price != '':
        kwargs["price__lte"] = end_price

    cities      = City.objects.all().order_by('name')
    regions     = Region.objects.all()
    institutes  = Education_Institute.objects.filter(is_archive = False)
    programs    = Education_Programme.objects.filter(is_archive = False)
    studymods   = StudyMode.objects.all()
	
    recruits = Recruitment.objects.filter(**kwargs).distinct()

    asc_arg = []
    
    if asc_region == 1:
        asc_arg.append('institute_program__institute__city__name')
    elif asc_region == -1:
        asc_arg.append('-institute_program__institute__city__name')
	
    if asc_institute == 1:
        asc_arg.append('institute_program__institute__name')
    elif asc_institute == -1:
        asc_arg.append('-institute_program__institute__name')
    
    if asc_program == 1:
        asc_arg.append('institute_program__program__name')
    elif asc_program == -1:
        asc_arg.append('-institute_program__program__name')

    if asc_begdate == 1:
        asc_arg.append('-begin_date')
    elif asc_begdate == -1:
        asc_arg.append('begin_date')
    else:
        asc_arg.append('begin_date')

    if asc_price == 1:
        asc_arg.append('price')
    elif asc_price == -1:
        asc_arg.append('-price')

    recruits = recruits.order_by(*asc_arg)
    base_recruits = recruits.filter(institute_program__program__level = "B")
    up_recruits = recruits.filter(institute_program__program__level = "U")

    man = None

    if request.user.is_authenticated() and request.user.is_ipr:
        man = IprStudent.objects.get(user = request.user)
    return render_to_response('recruitments.html',{"cities":cities,"regions": regions,"institutes":institutes,"programs":programs,"studymods":studymods,"up_recruits":up_recruits,"base_recruits":base_recruits,"levels":PROGRAM_LEVEL_CHOICES, "region":region,"city":city,"institute":institute,"program":program,"studymode":studymode,"begin_date":begin_date,"end_date":end_date,"begin_price":begin_price,"end_price":end_price,"asc_institute":asc_institute,"asc_program":asc_program,"asc_region":asc_region,"asc_begdate":asc_begdate,"asc_price":asc_price,"man":man,"delete_success":delete_success},context_instance=RequestContext(request))

def  search_sql_query(i_count,i_ipr,i_where_clause,i_limit_count = None):

    if i_count == True:
        l_expr = "COUNT(*)"
    else:
        l_expr = """ cuu.id,CONCAT( cuu.last_name, ' ', cuu.first_name, ' ', cus.patronymic ),
        cuu.is_student,cuu.is_ipr,
        cuu.has_edu_request,
        cus.id,cus.slug,
        cuis.attestat_number,cuis.attestat_date, cuis.attestat_length,
        cuis.confirmed,cuis.rejected,cuis.reason,
        cuis.profstatus_id, cups.title,
        eper.id,eper.is_archive,
        epeip.id,epeip.is_archive,
        epei.id, epei.name,epei.is_archive,
        epep.id, epep.name,epep.is_archive,
        epsg.id,epsg.exam_date,epsg.exam_result_id,
        epex.title,epex.is_finish,epsg.is_archive,
        epg.id,epg.name,epg.education_start,epg.education_end,
        epg.send_pnk_date,epg.get_pnk_date,epg.exam_date,
        epg.level,epg.form_id,epsm.title,epg.volume_academ_hours,
        epg.is_finished,epg.is_archive,
        cuu.email, cuis.membership_number """

    if i_ipr == True:
        l_type_clause = "cuu.is_ipr = 1"
    else:
        l_type_clause = "cuu.is_student = 1"

    l_limit_clause = ''
    if i_limit_count is not None:
        l_limit_clause = """ LIMIT """ + i_limit_count

    l_query = """SELECT {0} FROM ipr.common_user_user AS cuu
    JOIN ipr.common_user_student AS cus
    ON cuu.id = cus.user_id
    JOIN ipr.common_user_iprstudent AS cuis
    ON cus.id = cuis.student_ptr_id
    LEFT JOIN ipr.common_user_profstatus AS cups
    ON cuis.profstatus_id = cups.id
    LEFT JOIN ( SELECT * FROM ipr.education_programmes_educationrequest WHERE is_archive = %s ) AS eper
    ON cus.id = eper.student_id
    LEFT JOIN ( SELECT * FROM ipr.education_programmes_education_institute_program WHERE is_archive = %s ) AS epeip
    ON eper.institute_program_id = epeip.id
    LEFT JOIN ( SELECT * FROM ipr.education_programmes_education_institute WHERE is_archive = %s ) AS epei
    ON epeip.institute_id = epei.id
    LEFT JOIN ( SELECT * FROM ipr.education_programmes_education_programme WHERE is_archive = % s ) AS epep
    ON epeip.program_id = epep.id
    LEFT JOIN ipr.education_programmes_studentgroup AS epsg
    ON epsg.request_id = eper.id AND epsg.student_id = eper.student_id
    LEFT JOIN ipr.education_programmes_examresult AS epex
    ON epsg.exam_result_id = epex.id
    LEFT JOIN ( SELECT * FROM ipr.education_programmes_group WHERE is_archive = %s ) AS epg
    ON epsg.group_id = epg.id
    LEFT JOIN ipr.education_programmes_studymode AS epsm
    ON epg.form_id = epsm.id
    WHERE {1} AND cuu.is_olympic = 0 """.format(l_expr,l_type_clause)

    l_query = l_query + i_where_clause + l_limit_clause + " ;"
    return l_query

month_titles = ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек']

def search(request):
    tab_sel = 'B'
    limit = 50

    first_name             = last_name          = patronymic      = None
    email                  = education          = program         = city           = institute       = None
    show_archive           = show_noconfirmed          = show_reject        = False
    membership_number      = attestat_number           = telephone          = postaddress     = None
    profstatus             = length_of_work            = job_place          = position        = special_notes  = region          = None
    studymode              = length_of_work_sign       = group_name         = None
    attestat_date          = attestat_length    = None

    cities      = City.objects.all().order_by('name')
    regions     = Region.objects.all()
    institutes  = Education_Institute.objects.filter(is_archive = False)
    programs    = Education_Programme.objects.filter(is_archive = False)
    studymods   = StudyMode.objects.all()
    educations  = Education.objects.all()
    profstatuses = ProfStatus.objects.all()

    base_results = []
    base_count = 0
    base_page = 1
    base_pages = []

    up_results = []
    up_count = 0
    up_page = 1
    up_pages = []

    limit_list = range(10,100,10)
    parameter_list = []

    if request.method == "POST":
        limit                  = get_from_dict(limit,request.POST,'limit',True)
        base_page              = get_from_dict(base_page,request.POST,'base_page',True)
        up_page                = get_from_dict(up_page,request.POST,'up_page',True)

        first_name             = get_from_dict(first_name,request.POST,'first_name')
        last_name              = get_from_dict(last_name,request.POST,'last_name')
        patronymic             = get_from_dict(patronymic,request.POST,'patronymic')
        email                  = get_from_dict(email,request.POST,'email')
        telephone              = get_from_dict(telephone,request.POST,'telephone')
        education              = get_from_dict(education,request.POST,'education')
        postaddress            = get_from_dict(postaddress,request.POST,'postaddress')
        membership_number      = get_from_dict(membership_number,request.POST,'membership_number')
        attestat_number        = get_from_dict(attestat_number,request.POST,'attestat_number')
        profstatus             = get_from_dict(profstatus,request.POST,'profstatus')
        length_of_work         = get_from_dict(length_of_work,request.POST,'length_of_work',True,False,"")
        length_of_work_sign    = get_from_dict(length_of_work_sign,request.POST,'length_of_work_sign',True,False,"")
        job_place              = get_from_dict(job_place,request.POST,'job_place')
        position               = get_from_dict(position,request.POST,'position')
        special_notes          = get_from_dict(special_notes,request.POST,'special_notes')
        region                 = get_from_dict(region,request.POST,'region')
        city                   = get_from_dict(city,request.POST,'city')
        institute              = get_from_dict(institute,request.POST,'institute')
        program                = get_from_dict(program,request.POST,'program')
        studymode              = get_from_dict(studymode,request.POST,'studymode')

        group_name             = get_from_dict(group_name,request.POST,'group_name')
#        birthday               = get_date_from_dict(request.POST,'birthday')
        attestat_date          = get_date_from_dict(request.POST,'attestat_date')
        attestat_length        = get_date_from_dict(request.POST,'attestat_length')

        tab_sel                = get_from_dict(tab_sel,request.POST,'tab_sel')

        if request.POST.has_key('show_archive') and  request.POST['show_archive'] == u'1':
            show_archive = True

        if request.POST.has_key('show_noconfirmed') and  request.POST['show_noconfirmed'] == u'1':
            show_noconfirmed = True

        if request.POST.has_key('show_reject') and  request.POST['show_reject'] == u'1':
            show_reject = True

        cursor = connection.cursor()

        where_clause = ''

        if show_archive == True:
            eper_is_archive = 1
            epeip_is_archive = 1
            epei_is_archive = 1
            epep_is_archive = 1
            epg_is_archive = 1
        else:
            eper_is_archive = 0
            epeip_is_archive = 0
            epei_is_archive = 0
            epep_is_archive = 0
            epg_is_archive = 0

        parameter_list =    [eper_is_archive,epeip_is_archive,epei_is_archive,epep_is_archive,epg_is_archive]

        if request.user.is_authenticated() and request.user.is_admin == True and email is not None and not email.isspace() and len(email) > 0:
            where_clause = where_clause + like_operand("cuu.email",email,True,"AND")

#        if fio is not None:
#            fio_clause = ''
#
#            words = fio.split()
#            Qr = None
#            if len(words) == 3:
#                fio_clause = fio_clause + like_operand("cuu.last_name",words[0],not (fio_clause == ''))
#                fio_clause = fio_clause + like_operand("cuu.first_name",words[1],not (fio_clause == ''))
#                fio_clause = fio_clause + like_operand("cus.patronymic",words[2],not (fio_clause == ''))
#            else:
#                for field in words:
#                    fio_clause = fio_clause + like_operand("cuu.last_name",field,not (fio_clause == ''),"OR")
#                    fio_clause = fio_clause + like_operand("cuu.first_name",field,not (fio_clause == ''),"OR")
#                    fio_clause = fio_clause + like_operand("cus.patronymic",field,not (fio_clause == ''),"OR")
#
#            if fio_clause != '':
#                where_clause = where_clause + " AND ( " + fio_clause + " ) "

        if first_name is not None and first_name != '':
            where_clause = where_clause + like_operand("cuu.first_name",first_name,True)

        if last_name is not None and last_name != '':
            where_clause = where_clause + like_operand("cuu.last_name",last_name,True)

        if patronymic is not None and patronymic != '':
            where_clause = where_clause + like_operand("cus.patronymic",patronymic,True)

#        if birthday['year'] is not None:
#            where_clause = where_clause + ' AND EXTRACT(YEAR FROM cuis.birthday) = ' + birthday['year']
#        if birthday['month'] is not None:
#            where_clause = where_clause + ' AND EXTRACT(MONTH FROM cuis.birthday) = ' + birthday['month']
#        if birthday['day'] is not None:
#            where_clause = where_clause + ' AND EXTRACT(DAY FROM cuis.birthday) = ' + birthday['day']

        if attestat_date['year'] is not None:
            where_clause = where_clause + ' AND EXTRACT(YEAR FROM cuis.attestat_date) = ' + attestat_date['year']
        if attestat_date['month'] is not None:
            where_clause = where_clause + ' AND EXTRACT(MONTH FROM cuis.attestat_date) = ' + attestat_date['month']
        if attestat_date['day'] is not None:
            where_clause = where_clause + ' AND EXTRACT(DAY FROM cuis.attestat_date) = ' + attestat_date['day']

        if attestat_length['year'] is not None:
            where_clause = where_clause + ' AND EXTRACT(YEAR FROM cuis.attestat_length) = ' + attestat_length['year']
        if attestat_length['month'] is not None:
            where_clause = where_clause + ' AND EXTRACT(MONTH FROM cuis.attestat_length) = ' + attestat_length['month']
        if attestat_length['day'] is not None:
            where_clause = where_clause + ' AND EXTRACT(DAY FROM cuis.attestat_length) = ' + attestat_length['day']

        if membership_number is not None and len(membership_number) > 0 and not membership_number.isspace():
            where_clause = where_clause + like_operand("cuis.membership_number",membership_number,True,"AND")

        if attestat_number is not None  and len(attestat_number) > 0 and not attestat_number.isspace():
            where_clause = where_clause + like_operand("cuis.attestat_number",attestat_number,True,"AND")

        if telephone is not None  and len(telephone) > 0 and not telephone.isspace():
            where_clause = where_clause + like_operand("cuis.telephone",telephone,True,"AND")

        if postaddress is not None and len(postaddress) > 0  and not postaddress.isspace():
            where_clause = where_clause + like_operand("cuis.postaddress",postaddress,True,"AND")

        if job_place is not None  and len(job_place) > 0 and not job_place.isspace():
            where_clause = where_clause + like_operand("cuis.job_place",job_place,True,"AND")

        if position is not None  and len(position) > 0 and not position.isspace():
            where_clause = where_clause + like_operand("cuis.position",position,True,"AND")

        if special_notes is not None  and len(special_notes) > 0 and not special_notes.isspace():
            where_clause = where_clause + like_operand("cuis.special_notes",special_notes,True,"AND")

        if education is not None and education.isdigit():
            where_clause = where_clause + ' AND cuis.education_id = ' + education

        if length_of_work is not None:
            if length_of_work_sign == 1:
                where_clause = where_clause + ' AND cuis.length_of_work > ' + str(length_of_work)
            elif length_of_work_sign == 2:
                where_clause = where_clause + ' AND cuis.length_of_work >= ' + str(length_of_work)
            elif length_of_work_sign == 3:
                where_clause = where_clause + ' AND cuis.length_of_work = ' + str(length_of_work)
            elif length_of_work_sign == 4:
                where_clause = where_clause + ' AND cuis.length_of_work <= ' + str(length_of_work)
            elif length_of_work_sign == 5:
                where_clause = where_clause + ' AND cuis.length_of_work < ' + str(length_of_work)

        if profstatus is not None and profstatus.isdigit():
            where_clause = where_clause + ' AND cuis.profstatus_id = ' + profstatus

        if region is not None and region.isdigit():
            cities_from_region = City.objects.filter(region = region).values_list('id', flat=True).distinct()
            if len(cities_from_region) > 0:
                where_clause = where_clause + ' AND epei.city_id IN ( ' + ','.join(["%d" % city_id for city_id in cities_from_region]) + ' ) '

        if city is not None and city.isdigit():
            where_clause = where_clause + ' AND epei.city_id = ' + city

        if program is not None and program.isdigit():
            where_clause = where_clause + ' AND epeip.program_id = ' + program

        if studymode is not None and studymode.isdigit():
            where_clause = where_clause + ' AND epeip.form_id = ' + studymode

        if institute is not None and institute.isdigit():
            where_clause = where_clause + ' AND epeip.institute_id = ' + institute

        if group_name is not None and group_name != ' ' and group_name != '':
            groups = Group.objects.filter(name__icontains = group_name).values_list('id', flat=True).distinct()
            if len(groups) > 0:
                where_clause = where_clause + ' AND epsg.group_id IN ( ' + ','.join(["%d" % group_id for group_id in groups]) + ' ) '


        if where_clause == '':
            base_results = []
            up_results = []
        else:
            # смотрим, сколько всего записей по такому запросу

            cursor.execute(search_sql_query(True,False,where_clause), parameter_list)

            base_results_count = cursor.fetchone()


            base_count = base_results_count[0]
            base_count = int(base_count)

            # пагинация
            base_pages_count = base_count / limit

            if base_count % limit > 0:
                base_pages_count = base_pages_count + 1

            if base_pages_count > 1:
                base_pages = range(1,base_pages_count + 1,1)
            else:
                base_pages = range(1,base_pages_count,1)

            if  base_page > base_pages_count:
                base_page = base_pages_count

            limit_clause = " "
            if base_page > 1:
                limit_offset = ( base_page - 1 ) * limit

                limit_clause = " " + str(limit_offset) + " , "
            limit_clause = limit_clause + str(limit)

            if base_count > 0 :
                cursor.execute(search_sql_query(False,False,where_clause,limit_clause), parameter_list)
                base_results = cursor.fetchall()

            if show_noconfirmed == True:
                where_clause = where_clause + ' AND cuis.confirmed = 0'
            else:
                where_clause = where_clause + ' AND NOT cuis.confirmed = 0'

            if show_reject == True:
                where_clause = where_clause + ' AND cuis.rejected = 1'
            else:
                where_clause = where_clause + ' AND NOT cuis.rejected = 1'

            cursor.execute(search_sql_query(True,True,where_clause), parameter_list)
            up_results_count = cursor.fetchone()
            up_count = int(up_results_count[0])

            # пагинация
            up_pages_count = up_count / limit

            if up_count % limit > 0:
                up_pages_count = up_pages_count + 1

            if up_pages_count > 1:
                up_pages = range(1,up_pages_count + 1,1)
            else:
                up_pages = range(1,up_pages_count,1)

            if  up_page > up_pages_count:
                up_page = up_pages_count

            limit_clause = " "
            if up_page > 1:
                limit_offset = ( up_page - 1 ) * limit

                limit_clause = " " + str(limit_offset) + " , "
            limit_clause = limit_clause + str(limit)

            if up_count > 0:
                cursor.execute(search_sql_query(False,True,where_clause,limit_clause), parameter_list)
                up_results = cursor.fetchall()

    if base_count > 0 and up_count == 0 :
        tab_sel = 'B'
    elif base_count == 0 and up_count > 0 :
        tab_sel = 'U'

    year = date.today().year
    years = range(year+50,year-100,-1)
    months = [{"name": month_titles[numb],"number": numb + 1} for numb in range(0,12)]
    days = range(1,31)

    result_dict = {"cities":cities,"regions": regions,"institutes":institutes,"programs":programs,
                   "studymods":studymods,"levels":PROGRAM_LEVEL_CHOICES, "profstatuses": profstatuses, "educations":educations,
                   "tab_sel": tab_sel,"limit_list":limit_list,"limit":limit,
                   "base_pages":base_pages,"up_pages":up_pages,"base_page":base_page,"up_page":up_page,
                   "first_name":first_name,"last_name":last_name,"patronymic":patronymic,
                   "email":email,"telephone":telephone,"education":education,
                   "postaddress":postaddress,"profstatus":profstatus,"length_of_work":length_of_work,"length_of_work_sign":length_of_work_sign, "job_place":job_place,
                   "membership_number":membership_number,"attestat_number":attestat_number,
                   "attestat_date":attestat_date,"attestat_length":attestat_length,
                   "position":position,"special_notes":special_notes,"studymode":studymode,
                   "program":program,"region":region,"city":city,"institute":institute,"group_name":group_name,
                   "base_results":base_results, "up_results":up_results, "base_count":base_count,"up_count":up_count,
                   "show_archive":show_archive,"show_noconfirmed":show_noconfirmed,"show_reject":show_reject,
                   "years":years,"months":months,"days":days}

    if request.user.is_authenticated() and request.user.is_admin == True:
        return render_to_response('search.html',result_dict,context_instance=RequestContext(request))
    else:
        return render_to_response('search.html',result_dict,context_instance=RequestContext(request))


@csrf_exempt                                                                                                                                                                                                                                                        
def news(request,slug = None,_type = "news"):                                                                                                                                                                                                                                      
    now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
                                                                                                                                                                                                                                                                    
    if request.user.is_authenticated() and request.user.is_admin == True:
        ItemModel = News
        
        if _type == "news":
            ItemForm  = NewsForm
        else:
            ItemForm  = StaticPageForm

        if slug == None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,slug=slug)
            
        if request.method == 'POST':
            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'image':
                        model_item.image = delete_loaded_file(request, model_item.image, formfieldname)
                        continue
                    partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if _type == "staticpage":
                    model_item.is_static = True

                if request.FILES.has_key('image'):
                    model_item.image = upload_file(request, u'/media/uploads/banners/',
                                                   model_item.image, 'image', id)

                if slug is None:
                    model_item.slug = slugify(model_item.__dict__['title'])				

                if _type == "news" and model_item.__dict__['order'] is None:
                    orders = News.objects.filter(is_archive= False,simple_banner=False,main_banner=False,is_static = False).aggregate(Max('order'))
                    if orders['order__max'] is None:
                        model_item.__dict__['order'] = 1
                    else:
                        model_item.__dict__['order'] = orders['order__max'] + 1
                    print orders
                model_item.save()

                return HttpResponseRedirect('/edu/' + _type +'/' + model_item.slug + '/')
            else:
                field_errors = [ (field.label, field.errors) for field in form]
        else:
            form = ItemForm(instance=model_item)

        return render_to_response("news_form.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,"now":now,"type":_type}, context_instance=RequestContext(request))
    else:
        model_item = get_object_or_404(News,slug=slug)
        return render_to_response('personal_cabinet_news.html', {'item':model_item,'verbose_name':model_item._meta.verbose_name,"type":_type}, context_instance=RequestContext(request))

@login_required
def news_delete(request,slug):
    if request.user.is_admin == True:
        model_item = get_object_or_404(News,slug=slug)
        model_item.set_archive()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        return redirect('personal_cabinet')

@csrf_exempt
def group(request,id = None):
    step = -1
    institute = level = level_name = None
    requests = []
    free_requests = []
    now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
    noon = timedelta(hours=12)

    if request.user.is_authenticated() and ( request.user.is_admin == True or request.user.is_curator == True ):

        ItemModel = Group

        if id is None:
            ItemForm  = CreateGroupForm
            
            if id is None and request.method == 'POST':
                if request.POST.has_key('step') and request.POST["step"] == u'0':
                    step = 0

                if request.POST.has_key('edu_institute') and request.POST["edu_institute"] is not None and request.POST["edu_institute"] != u'':
                    try:
                        institute = Education_Institute.objects.get(id=request.POST["edu_institute"])
                    except:
                        institute = None


                if request.POST.has_key('level') and request.POST["level"] is not None and request.POST["level"] != u'':
                    try:
                        level = request.POST["level"]
                        if PROGRAM_LEVEL_DICT.has_key(level):
                            level_name = PROGRAM_LEVEL_DICT[level]
                        else:
                            level_name = None
                    except:
                        level = None
                        level_name = None

                if request.POST.has_key('requests') and len(request.POST.getlist('requests'))>0:
                    requests = EducationRequest.objects.filter(id__in = request.POST.getlist('requests'))

        if id is None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)
            if request.user.is_curator is True and model_item.education_institute.curator != request.user:
                #     ====================================================VIEW ITEM=============================================
                cur_group =  model_item
                cur_group.students = cur_group.studentgroup_set.all().order_by('student__user__last_name','student__user__first_name','student__patronymic')
                return render_to_response('personal_cabinet_group.html', {'item':cur_group}, context_instance=RequestContext(request))

            if request.user.is_admin is True:
                ItemForm  = GroupForm
                if model_item.level == 'U':
                    ItemForm  = GroupUpForm

            else:
                ItemForm  = GroupCuratorForm
                print "GroupCuratorForm"
                if model_item.status == -1:
                    ItemForm  = GroupCuratorFinishForm
                    print "GroupCuratorFinishForm"
                elif model_item.level == 'U' and model_item.status == -1:
                    ItemForm  = GroupUpCuratorFinishForm
                    print "GroupUpCuratorFinishForm"
                elif model_item.level == 'U':
                    ItemForm  = GroupUpCuratorForm
                    print "GroupUpCuratorForm"

        if request.method == 'POST' and step == -1:
            if id is not None and request.POST.has_key('requests'):
                for _request in request.POST.getlist('requests'):
                    edurequest = EducationRequest.objects.get(id = _request)
                    student_group = StudentGroup(student = edurequest.student ,request = edurequest, group = model_item)
                    student_group.save()

            if id is not None and request.POST.has_key('delete_students'):
                for delete_student in request.POST.getlist('delete_students'):
                    try:
                        studentgroup = StudentGroup.objects.get(id = delete_student)
                        studentgroup.expulsion()
                    except:
                        continue

            form = ItemForm(request.POST,request.FILES)

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'education_institute':
                        model_item.education_institute = form.cleaned_data[formfieldname]
                        continue
                    elif formfieldname == 'students':
                        continue
                    elif formfieldname == 'remove_students':
                        continue
                    elif formfieldname == 'requests':
                        continue
                    elif formfieldname == 'edu_institute':
                        continue
                    elif model_item.status == -1 and ( formfieldname == 'form' or formfieldname == 'timetable'  or formfieldname == 'name' ):
                        continue
                    elif formfieldname == 'form':
                        model_item.__dict__['form_id'] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'timetable':
                        model_item.timetable = delete_loaded_file(request, model_item.timetable, formfieldname)
                        continue
                    else:
                        partfieldname = formfieldname

                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if id is None:
                    model_item.education_institute = Education_Institute.objects.get(id=request.POST['edu_institute'])
                    model_item.level = request.POST['level']

                if request.FILES.has_key('timetable'):
                    model_item.timetable = upload_file(request, u'/media/uploads/timetables/',
                                                       model_item.timetable, 'timetable', id)

                if math.fabs(model_item.status) == 1 and model_item.level == 'B': #and request.user.is_admin is True:
                    for student_group in  model_item.studentgroup_set.all():
                        if student_group.student.user.is_ipr is True:
                            None

                        elif student_group.to_other_group is True:
                            None

                        else:
                            try:
                                student_group.exam_result = ExamResult.objects.get(id=request.POST['exam_result_'+str(student_group.id)])
                            except:
                                print str(sys.exc_info())
                        
                            if student_group.exam_result is not None and student_group.exam_result.is_finish:
                                print 'membership_number'
                                try:
                                    print request.POST['membership_number_'+str(student_group.id)]
                                    iprstudent = student_group.get_iprstudent()
                                    iprstudent.membership_number = request.POST['membership_number_'+str(student_group.id)]
                                
                                    try:
                                        iprstudent.attestat_date =  (datetime.strptime(request.POST['attestat_date_'+str(student_group.id)], '%d.%m.%Y') + noon if request.POST['attestat_date_'+str(student_group.id)] is not None and request.POST['attestat_date_'+str(student_group.id)] != u'None' and request.POST['attestat_date_'+str(student_group.id)] != u'' else None)
                                    except:
                                        None
                                    iprstudent.attestat_number = request.POST['attestat_number_'+str(student_group.id)]

                                    try:
                                        iprstudent.profstatus = ProfStatus.objects.get(id=request.POST['profstatus_'+str(student_group.id)])
                                    except:
                                        print str(sys.exc_info())

                                    try:
                                        iprstudent.attestat_length = (datetime.strptime(request.POST['attestat_length_'+str(student_group.id)], '%d.%m.%Y') + noon if request.POST['attestat_length_'+str(student_group.id)] is not None and request.POST['attestat_length_'+str(student_group.id)] != u'None' and request.POST['attestat_length_'+str(student_group.id)] != u'' else None)
                                    except:
                                        None
                                    iprstudent.save()
                                except:
                                    print str(sys.exc_info())

                            elif student_group.exam_result is not None and not student_group.exam_result.is_finish:
                                try:
                                    other_group = Group.objects.get(id=request.POST['to_group_'+str(student_group.id)])
                                    student_group.shift_group(other_group)

                                except:
                                    print str(sys.exc_info())


                            if student_group.can_become_ipr():
                                try:
                                    become_ipr = request.POST['become_ipr_'+str(student_group.id)]
                                    if int(become_ipr) == 1:
                                        student_group.set_archive()
                                        student_group.request.set_archive()
                                        student_group.student.user.is_ipr = True
                                        student_group.student.user.is_student = False
                                        student_group.student.user.has_edu_request = False
                                        student_group.student.user.save()

                                        iprstudent = student_group.get_iprstudent()
                                        iprstudent.confirmed = True
                                        iprstudent.save()
                                except:
                                    print str(sys.exc_info())
                        student_group.save()

                if model_item.status != -2 and model_item.level == 'U': # and request.user.is_admin is True:
                    for student_group in  model_item.studentgroup_set.all():
                        try:
                            iprstudent = student_group.get_iprstudent()
                            try:
                                iprstudent.attestat_length = (datetime.strptime(request.POST['attestat_length_'+str(student_group.id)], '%d.%m.%Y') + noon if request.POST['attestat_length_'+str(student_group.id)] is not None and request.POST['attestat_length_'+str(student_group.id)] != u'None' and request.POST['attestat_length_'+str(student_group.id)] != u'' else None)
                            except:
                                print str(sys.exc_info())
                            try:
                                iprstudent.special_notes = request.POST['special_notes_'+str(student_group.id)] if request.POST['special_notes_'+str(student_group.id)] is not None and request.POST['special_notes_'+str(student_group.id)] != u'None' and request.POST['special_notes_'+str(student_group.id)] != u'' else None
                            except:
                                print str(sys.exc_info())
                            iprstudent.save()
                        except:
                            print str(sys.exc_info())


                        student_group.save()

                model_item.save()

                if id is None:
                    for _request in request.POST.getlist('requests'):
                        edurequest = EducationRequest.objects.get(id = _request)
                        student_group = StudentGroup(student = edurequest.student,request = edurequest, group = model_item)
                        student_group.save()

                return HttpResponseRedirect('/edu/group/' + str(model_item.id) + '/')
            else:
                field_errors = [ (field.label, field.errors) for field in form]
        else:
            form = ItemForm(instance=model_item)

        if id is not None:
            model_item.students           = model_item.studentgroup_set.filter(is_archive=False).order_by('student__user__last_name','student__user__first_name','student__patronymic')
            model_item.confirmed_students = model_item.studentgroup_set.filter(is_archive=True).order_by('student__user__last_name','student__user__first_name','student__patronymic') #filter(is_archive=True,to_other_group = False,student__user__is_ipr = True).order_by('student__user__last_name','student__user__first_name','student__patronymic')
            if len(requests) > 0:
                free_requests                 = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),~Q(id__in=requests),Q(institute_program__institute=model_item.education_institute),Q(institute_program__program__level = model_item.level),Q(is_archive = False))
            else:
                free_requests                 = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=model_item.education_institute),Q(institute_program__program__level = model_item.level),Q(is_archive = False))
            model_item.requests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=model_item.education_institute),Q(institute_program__program__level = model_item.level),Q(is_archive = False))
            institute = model_item.education_institute
            model_item.exam_results       = ExamResult.objects.all()
            model_item.profstatuses       = ProfStatus.objects.all()
            model_item.other_groups       = Group.objects.filter(Q(education_institute = model_item.education_institute),Q(level = model_item.level),~Q(id=model_item.id),Q(is_archive = False),Q(education_end__gte = now),Q(education_start__lte = now))
        else:
            if len(requests) > 0:
                free_requests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),~Q(id__in=requests),Q(institute_program__institute=institute,institute_program__program__level = level),Q(is_archive = False)).order_by('institute_program')
            else:
                free_requests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=institute,institute_program__program__level = level),Q(is_archive = False)).order_by('institute_program')


        return render_to_response("group_form.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'institute':institute,'level':level,"level_name":level_name,"requests":requests,"now":now,"free_requests":free_requests}, context_instance=RequestContext(request))

    elif request.user.is_authenticated() and ( request.user.is_student is True or request.user.is_ipr is True ):
        cur_group =  get_object_or_404(Group,id=id)       
        cur_group.students = cur_group.studentgroup_set.all().order_by('student__user__last_name','student__user__first_name','student__patronymic')
        if cur_group.studentgroup_set.filter(student__user = request.user).count() > 0:
            my_group = True
        else:
            my_group = False
        return render_to_response('personal_cabinet_group.html', {'item':cur_group,'my_group':my_group}, context_instance=RequestContext(request))
    else:
        #     ====================================================VIEW ITEM=============================================
        cur_group =  get_object_or_404(Group,id=id)
        cur_group.students = cur_group.studentgroup_set.filter(Q(exam_result__isnull = True)|Q(exam_result__is_finish = True)).order_by('student__user__last_name','student__user__first_name','student__patronymic')
        return render_to_response('personal_cabinet_group.html', {'item':cur_group}, context_instance=RequestContext(request))

def group_view(request,id):
    cur_group =  get_object_or_404(Group,id=id)
    cur_group.students = cur_group.studentgroup_set.all().order_by('student__user__last_name','student__user__first_name','student__patronymic')
    return render_to_response('personal_cabinet_group.html', {'item':cur_group}, context_instance=RequestContext(request))

def group_disembody(request,id):
    cur_group =  get_object_or_404(Group,id=id)
    cur_group.disembody()
    return redirect('personal_cabinet')

def group_finish(request,id):
    cur_group =  get_object_or_404(Group,id=id)
    cur_group.set_finish()
    return HttpResponseRedirect('/edu/group/' + str(id) + '/')

def program_id(request,region_id  = None,city_id  = None,institute_id  = None):
    kwargs = {}
    level = None

    if request.method == 'POST':
        try:
            kwargs["level"] = request.POST['level']
        except :
            level = None

    if region_id is not None:
        kwargs["education_institute_program__institute__city__region__id"] = region_id

    if city_id is not None:
        kwargs["education_institute_program__institute__city__id"] = city_id

    if institute_id is not None:
        kwargs["education_institute_program__institute__id"] = institute_id

    programs = Education_Programme.objects.filter(**kwargs)
    program_list = [program.id for program in programs]
    return HttpResponse(json.dumps(program_list),content_type='application/json')

def region(request,raphael_slug):
    answer = {}
    try:
        region_obj = Region.objects.get(raphael_slug=raphael_slug)
        answer['id'] = region_obj.id
        answer['status'] = 1
    except:
        answer['status'] = -1
    return HttpResponse(json.dumps(answer),content_type='application/json')
			

@csrf_exempt
#@login_required
def organizations(request,region_slug = None, city_slug = None):
    if request.user.is_authenticated() and request.user.is_admin is True:

        cities = City.objects.all().order_by('name')
        regions = Region.objects.all()
        programs = Education_Programme.objects.filter(education_institute_program__institute__isnull=False,is_archive =False,education_institute_program__is_archive =False,education_institute_program__institute__is_archive =False).distinct()

        region = city = program = only_pnk = only_seminars = None

        region           = get_from_dict(region,request.POST, 'region')
        city             = get_from_dict(city,request.POST, 'city')
        program          = get_from_dict(program,request.POST, 'program')
        only_pnk         = get_from_dict(only_pnk,request.POST, 'only_pnk',True)
        only_seminars    = get_from_dict(only_seminars,request.POST, 'only_seminars',True)

        kwargs = {}
        kwargs["is_archive"] = False

        if region is not None:
            kwargs["city__region"] = region
        if city is not None:
            kwargs["city"] = city
        if program is not None:
            kwargs["education_institute_program__program"] = program
        if only_pnk == 1:
            kwargs["access_ipr_program"] = True
            kwargs["education_institute_program__program__ipr_program"] = True
            kwargs["education_institute_program__is_archive"] = False
        if only_seminars == 1:
            kwargs["education_institute_program__has_seminar"] = True
            kwargs["education_institute_program__is_archive"] = False

        institutes  = Education_Institute.objects.filter(**kwargs).distinct()
        return render_to_response('organizations.html',{"cities":cities,"regions": regions,"institutes":institutes,"programs":programs,'region':region,'city':city,'program':program,'only_pnk':only_pnk,'only_seminars':only_seminars},context_instance=RequestContext(request))
    else:

        name_city = ""
        name_region = ""

        cursor = connection.cursor()

        cursor.execute(u"""SELECT SUBSTRING(REPLACE(name,'Республика ',''),1,1),id,name,raphael_slug
        FROM education_programmes_region
        WHERE is_archive = 0
        order by REPLACE(name,'Республика ','')""")

        db_regions = cursor.fetchall()
        region_columns = divide_into_columns( db_regions )

        cursor.execute("""SELECT SUBSTRING(city.name,1,1), city.id,city.name, city.slug, region.raphael_slug
        FROM education_programmes_city as city
        JOIN education_programmes_region as region
          ON city.region_id  = region.id
        WHERE city.is_archive = 0
        order by city.name""")

        db_cities = cursor.fetchall()
        city_columns = divide_into_columns( db_cities )

        if region_slug is not None and city_slug is None:

            try:
                select_region = Region.objects.get(raphael_slug = region_slug,is_archive = False)

                cursor.execute("""SELECT DISTINCT SUBSTRING(pei.name,1,1), pei.id, pei.name
                FROM education_programmes_education_institute as pei
                JOIN education_programmes_city as pc ON pei.city_id = pc.id
                JOIN education_programmes_education_institute_program as epeip
                ON pei.id = epeip.institute_id
                where pc.region_id = %s
                and pc.is_archive = 0
                and pei.accreditation = 'A'
                AND pei.is_archive = 0
                AND epeip.is_archive = 0
                order by pei.name""",[select_region.id])

                name_region = select_region.name

            except:
                print str(sys.exc_info())
                redirect('organizations')

        elif  city_slug is not None:

            try:
                select_city = City.objects.get(slug = city_slug,is_archive = False)

                name_city   = select_city.name
                name_region = select_city.region.name

                cursor.execute("""SELECT DISTINCT SUBSTRING(pei.name,1,1),pei.id,pei.name
                FROM education_programmes_education_institute  as pei
                JOIN education_programmes_education_institute_program as epeip
                ON pei.id = epeip.institute_id
                where pei.city_id = %s
                and pei.accreditation = 'A'
                AND pei.is_archive = 0
                AND epeip.is_archive = 0
                order by pei.name""",[select_city.id])

            except:
                print str(sys.exc_info())
                redirect('organizations')

        else:
            cursor.execute("""SELECT DISTINCT SUBSTRING(epei.name,1,1),epei.id,epei.name
            FROM education_programmes_education_institute as epei
            JOIN education_programmes_education_institute_program as epeip
            ON epei.id = epeip.institute_id
            WHERE epei.accreditation = 'A'
            AND epei.is_archive = 0
            AND epeip.is_archive = 0
            order by name""")

        db_institutes = cursor.fetchall()

        return render_to_response('organizations.html',{"city":name_city,"region":name_region,
                                                        "institutes":db_institutes,
                                                        "cities":city_columns,"regions" : region_columns
        }
            ,context_instance=RequestContext(request))
    return redirect('personal_cabinet')

def organizations_id(request,region_id  = None,city_id  = None):
    if region_id is None and  city_id is None:
        organizations = Education_Institute.objects.all().order_by('name')
    elif region_id is not None and  city_id is None:
        organizations = Education_Institute.objects.filter(city__region__id = region_id).order_by('name')
    else:
        organizations = Education_Institute.objects.filter(city__region__id = region_id,city__id = city_id).order_by('name')
    organizations_list = [organization.id for organization in organizations]
    return HttpResponse(json.dumps(organizations_list),content_type='application/json')

@csrf_exempt
@login_required
def no_confirmed(request):
    if request.user.is_admin is True:
        if request.method == 'POST':
            selected_ipr     = request.POST.getlist('selected')
            reject_comments  = request.POST.getlist('comment')
            accept = reject = None
            accept = get_from_dict(accept,request.POST,'accept')
            reject = get_from_dict(reject,request.POST,'reject')

            if len(selected_ipr) > 0:
                selected_indx = 0
                while selected_indx < len(selected_ipr):
                    try:
                        ipr_item = IprStudent.objects.get(id = selected_ipr[selected_indx])
                        if accept is not None:
                            ipr_item.confirm()
                        elif reject is not None:
                            ipr_item.reject(reject_comments[selected_indx])
                    except:
                        None
                    selected_indx = selected_indx + 1
        noconfirmed = IprStudent.objects.filter(user__is_ipr = True,confirmed = False,rejected = False)
        if len(noconfirmed) == 0:
            return redirect('personal_cabinet')
        return render_to_response('no_confirmed.html',{"noconfirmed":noconfirmed},context_instance=RequestContext(request))
    return redirect('personal_cabinet')

@csrf_exempt
@login_required
def consult_confirm(request):
    answer = {"status":-1}
    if request.POST:
        ids =  json.loads(request.POST["id"])
        students = IprStudent.objects.filter(id__in = ids)
        for student in students:
            student.confirmed = True
            student.confirmed_date = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
            student.save()
        answer = {"status":1}
    return HttpResponse(json.dumps(answer))

@csrf_exempt
@login_required
def consult_reject(request):
    answer = {"status": -1 }
    if request.POST:
        ids =  json.loads(request.POST["id"])
        for to_reject in ids:
            try:
                student = IprStudent.objects.get(id = to_reject["item"])
                student.rejected = True
                student.reason = to_reject["reason"]
                student.save()
            except :
                print str(sys.exc_info())

        answer = {"status": 1 }
    return HttpResponse(json.dumps(answer))


def response_dict_for_new_group(curator):
    if isinstance(curator, Education_Institute):
        programs = curator.education_institute_program_set.filter(is_archive=False)
        edurequests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=curator),Q(is_archive = False)).order_by('institute_program')
        return {"curator":curator,"programs":programs,"levels":PROGRAM_LEVEL_CHOICES,"edurequests":edurequests}
    return {}

@csrf_exempt
@login_required
def new_group(request):
    new_group_template = 'groups_new.html'
    if request.user.is_admin is True and request.method == 'POST':
        institute = None
        institute = get_from_dict(institute,request.POST, 'institute')
        try:
            curator = Education_Institute.objects.get(id = institute)
            return render_to_response(new_group_template,response_dict_for_new_group(curator),context_instance=RequestContext(request))
        except :
            return redirect('groups')
    elif request.user.is_curator == False:
        return redirect('groups')         
    elif request.user.is_curator is True:
        curator = Education_Institute.objects.get(curator = request.user)
        return render_to_response(new_group_template,response_dict_for_new_group(curator),context_instance=RequestContext(request))
    return redirect('groups')

def stickers(request):
    now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
    if not request.user.is_authenticated():
        stickers_for_all = Sticker.objects.filter(type = 'A',active_till__gte = now,is_archive=False,is_active=True).order_by('-active_from')
        return render_to_response('stickers.html',{"stickers_for_all":stickers_for_all},context_instance=RequestContext(request))
    
    elif request.user.is_authenticated() and request.user.is_admin is True:
        stickers_for_all = Sticker.objects.filter(active_till__gte = now,is_archive=False,is_active=True).order_by('-active_from')
        return render_to_response('stickers.html',{"stickers_for_all":stickers_for_all},context_instance=RequestContext(request))
    
    elif request.user.is_authenticated() and request.user.is_curator is True:
        stickers_for_all = Sticker.objects.filter(Q(active_till__gte = now),Q(is_active=True),Q(is_archive=False),Q(type = 'A') | Q(institute__curator = request.user)).order_by('-active_from')
        return render_to_response('stickers.html',{"stickers_for_all":stickers_for_all},context_instance=RequestContext(request))
    else:
        # не придумал
        stickers_for_all = Sticker.objects.filter(Q(active_till__gte = now),Q(is_archive=False),Q(is_active=True),Q(type = 'A') | Q(institute__curator = request.user)).order_by('-active_from')
        return render_to_response('stickers.html',{"stickers_for_all":stickers_for_all},context_instance=RequestContext(request))
    return redirect('home')
        

def sticker(request, id):
    model_item = get_object_or_404(Sticker,id=id)
    now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)

    if request.user.is_authenticated() and ( model_item.creator == request.user or request.user.is_admin is True):
        ItemForm = StickerRecruitmentForm

        if request.method == 'POST':
            print " BU "

            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                print form._meta.__dict__['fields']
                print model_item.__dict__
                #                print model_item.status

                for formfieldname in form._meta.__dict__['fields']:
                    partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                model_item.save()

                return HttpResponseRedirect('/edu/sticker/' + str(model_item.id) + '/')
            else:
                field_errors = [ (field.label, field.errors) for field in form]
        else:
            form = ItemForm(instance=model_item)

        return render_to_response("stickerform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,"now":now}, context_instance=RequestContext(request))


        #edit sticker
    else:
        None
        #view sticker
        return render_to_response('sticker_view.html',{"sticker":model_item},context_instance=RequestContext(request))


    return redirect('stickers')

@csrf_exempt
@login_required	
def administration(request):
    if request.user.is_admin == True:

        fio             = u''
        filterLastName = filterFirstName = filterEmail = current_page = filterProgram  = filterInstitute = None
        filterLevel = None
        all_pages       = item_on_page    = 30
        tab             = 'requests'

        print request.POST

        tab           = get_from_dict(tab,request.POST, 'tab')
        filterLastName     = get_from_dict(filterLastName,request.POST, 'last_name')
        filterFirstName    = get_from_dict(filterFirstName,request.POST, 'first_name')
        filterInstitute     = get_from_dict(filterInstitute,request.POST, 'institute')
        filterProgram       = get_from_dict(filterProgram,request.POST, 'program')
        filterEmail         = get_from_dict(filterEmail, request.POST, 'email')
        filterLevel = get_from_dict(filterLevel, request.POST, 'level')
        current_page  = get_from_dict(current_page, request.POST, 'current_page',True)
        item_on_page  = get_from_dict(item_on_page, request.POST, 'item_on_page',True)
        item_on_pages = range(10,50,10) + range(50,510,50)

        if current_page is None or current_page < 1:
            current_page = 1

        llimit = item_on_page * ( current_page - 1 )
        rlimit = item_on_page * current_page

        Qr = Q(is_archive = False,student__isnull = False, student__user__isnull = False)
        if filterEmail:
            Qr = Qr & Q(**{"student__user__email__icontains": filterEmail })
        if filterLastName:
            Qr = Qr & Q(**{"student__user__last_name__icontains": filterLastName })
        if filterFirstName:
            Qr = Qr & Q(**{"student__user__first_name__icontains": filterFirstName })
        if filterInstitute:
            Qr = Qr & Q(**{"institute_program__institute": filterInstitute })
        if filterProgram is not None:
            Qr = Qr & Q(**{"institute_program__program": filterProgram })
        if filterLevel is not None:
            Qr = Qr & Q(**{"institute_program__program__level": filterLevel })

        count_user = EducationRequest.objects.filter(Qr).count()

        all_pages = count_user / item_on_page
        if count_user % item_on_page > 0:
            all_pages = all_pages + 1

        if llimit > count_user or current_page == 1:
            current_page = 1
            edurequests = EducationRequest.objects.filter(Qr).order_by('institute_program__program')[:item_on_page]
        else:
            edurequests = EducationRequest.objects.filter(Qr).order_by('institute_program__program')[llimit:rlimit]

        modes          = StudyMode.objects.all()
        results        = ExamResult.objects.all()
        profstatuses   = ProfStatus.objects.all()
        menuitems      = MenuItem.objects.filter(is_archive = False)
        institutes     = Education_Institute.objects.filter(is_archive = False)
        programs       = Education_Programme.objects.filter(is_archive = False)

        common_texts = []

        for key, value in COMMON_TEXT_SLUGS_DICT.items():
            common_texts.append(CommonText.get_instance(key))

        return render_to_response('administration.html',{"edurequests":edurequests,"modes":modes,"results":results,"profstatuses":profstatuses,"menuitems":menuitems,"email_templates":IPR.mail.EMAIL_TEMPLATES,
                                                         "institutes": institutes,"programs":programs,"common_texts":common_texts,"levels":PROGRAM_LEVEL_DICT,"item_on_pages":item_on_pages,
                                                         "current_page": current_page, "item_on_page": item_on_page,"all_pages": all_pages,
                                                         "filterEmail":filterEmail,"filterLastName":filterLastName,"filterFirstName":filterFirstName,"filterInstitute":filterInstitute,"filterProgram":filterProgram,"filterLevel":filterLevel},
                                  context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')

@csrf_exempt
@login_required	
def administration_item(request,_type,id=None):
    fi_icon = []

    if request.user.is_admin == True:
        if _type == "profstatus":
            ItemModel = ProfStatus
            ItemForm = AdministrationForm
        elif _type == "mode":
            ItemModel = StudyMode
            ItemForm = AdministrationForm
        elif _type == "result":
            ItemModel = ExamResult
            ItemForm = AdministrationForm
        elif _type == "menuitem":
            ItemModel = MenuItem
            ItemForm = MenuItemForm
            fi_icon = fi_icons


        if id == None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)

        if request.method == 'POST':
            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                model_item.save()
                return redirect('eduadmin')
            else:
                return render_to_response("adminform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':_type,'fi_icon':fi_icon}, context_instance=RequestContext(request))

        form = ItemForm(instance=model_item)
        return render_to_response("adminform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'type':_type,'fi_icon':fi_icon}, context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')

@csrf_exempt
@login_required
def administration_email_template(request,template_name):
    if not request.user.is_admin:
        return redirect('personal_cabinet')

    if not IPR.mail.EMAIL_TEMPLATES.has_key(template_name):
        return redirect('eduadmin')

    filename = template_name
    html_filename = 'upd_' + template_name

    file_content      = get_file_content(u'templates/mail/',filename)
    html_file_content = get_file_content(u'templates/mail/',html_filename)

    if request.method == 'POST' and request.POST.has_key('template_content') and  request.POST['template_content'] != u'':

        file_content = request.POST['template_content']
        set_file_content(u'templates/mail/',filename,file_content.encode("utf-8"))

        if request.POST.has_key('html_template_content') and  request.POST['html_template_content'] != u'':
            html_file_content = request.POST['html_template_content']
            set_file_content(u'templates/mail/',html_filename,html_file_content.encode("utf-8"))

        return HttpResponseRedirect('/edu/admin/others/email_template/' + template_name + '/')
    return render_to_response("email_templateform.html", {"file_content" : file_content, "template_name": template_name, "mailing_name": IPR.mail.EMAIL_TEMPLATES[template_name],'html_file_content':html_file_content}, context_instance=RequestContext(request))

@login_required
def administration_email_template_preview(request,template_name):
    if not request.user.is_admin:
        return redirect('personal_cabinet')

    if not IPR.mail.EMAIL_TEMPLATES.has_key(template_name):
        return redirect('eduadmin')
    full_template_name = u'mail/' + 'upd_' + template_name
    context_dict = {}

    if template_name.find('edurequest') != -1:

        id_request_aggregate = EducationRequest.objects.aggregate(Min('id'))

        if id_request_aggregate.has_key('id__min') and id_request_aggregate['id__min'] is not None:
            edurequest = EducationRequest.objects.get(id = id_request_aggregate['id__min'])

            institute   = edurequest.institute_program.institute
            program     = edurequest.institute_program.program
            user        = edurequest.student.user
            context_dict = {'institute_name': institute.name,'program_name': program.name,'fio': user.get_full_name()}

    return render_to_response(full_template_name, context_dict, context_instance=RequestContext(request))

@login_required
def administration_common_text(request,slug):
    if not request.user.is_admin:
        return redirect('personal_cabinet')

#    if not COMMON_TEXT_SLUGS_DICT.has_key(slug):
#        return redirect('eduadmin')

    current_text = get_object_or_404(CommonText,slug=slug)

    if request.method == 'POST':
        form = CommonTextForm(request.POST)
        if form.is_valid():
            current_text.content = form.cleaned_data['content']
            current_text.save( )
            return redirect('common-text-edit',slug = slug)
    else:
        form = CommonTextForm(instance=current_text)
    return render_to_response("common_text_form.html", {'Form':form,'item':current_text}, context_instance=RequestContext(request))

@login_required
def administration_common_text_preview(request,slug):
    if not request.user.is_admin:
        return redirect('personal_cabinet')

    current_text = get_object_or_404(CommonText,slug=slug)
    content = u''
    if request.method == 'POST':
        content = get_from_dict(content,request.POST,u'content')
    else:
        content = current_text.content
    return render_to_response("common_text_preview.html", {'content':content,'item':current_text}, context_instance=RequestContext(request))

def common_text(request,slug):
    current_text = u""
    try:
        current_text = CommonText.objects.get(slug=slug)
    except:
        return redirect('home')
    return render_to_response("common_text.html", {'item':current_text}, context_instance=RequestContext(request))

@csrf_exempt
@login_required
def recruitment(request,id=None):
    response = {}
    lv_ShortForm = False

    if request.user.is_curator == True:
        ItemModel = Recruitment

        if id is not None and request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find('/auth/personal') != -1:
            lv_ShortForm = True

        if not request.is_ajax() and not lv_ShortForm:
            ItemForm = RecruitmentForm
        else:
            ItemForm = RecruitmentShortForm

        if id == None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)

        if request.method == 'POST':
            form = ItemForm(request.user,request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if ( formfieldname == 'institute_program' or formfieldname == 'payment_type' or formfieldname == 'currency' or formfieldname == 'form' ) and form.cleaned_data[formfieldname] is not None:
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'program':
                        model_item.program = delete_loaded_file(request, model_item.program, formfieldname)
                        continue
                    else:
                        partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if request.FILES.has_key('program'):
                    model_item.program = upload_file(request, u'/media/uploads/recruits/',
                                                     model_item.program, 'program', id)

                model_item.save()

                if request.is_ajax() or lv_ShortForm:
                    if settings.DEBUG:
                        print "ajax"
                    response["status"] = 1
                    response["begin_date"] = str(model_item.begin_date)
                    response["program"] = model_item.program_url()
                    response["program_url"] = model_item.program_url()
                    response["program_filename"] = model_item.program_filename()
                    return HttpResponse(json.dumps(response))
                else:
                    return redirect('personal_cabinet')
            else:
                if settings.DEBUG:
                    print 'not valid'
                if request.is_ajax() or lv_ShortForm:
                    response["status"] = 0
                    return HttpResponse(json.dumps(response))
                else:
                    return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

        form = ItemForm(request.user,instance=model_item)
        if request.is_ajax() or lv_ShortForm:
            response["status"] = 0
            return HttpResponse(json.dumps(response))
        else:
            return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    elif request.user.is_admin and id is not None:
        init_dict = {}
        ItemModel = Recruitment

        l_super_creation = False
        curator = None

        model_item = get_object_or_404(ItemModel,id=id)

        ItemForm = RecruitmentSuperUserForm

        init_dict["edu_institute"] = model_item.institute_program.institute
        init_dict["edu_programm"] = model_item.institute_program.program

        if request.method == 'POST':
            form = ItemForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if ( formfieldname == 'institute_program' or formfieldname == 'payment_type' or formfieldname == 'currency' or formfieldname == 'form' ) and form.cleaned_data[formfieldname] is not None:
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'program':
                        model_item.program = delete_loaded_file(request, model_item.program, formfieldname)
                        continue
                    else:
                        partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if request.FILES.has_key('program'):
                    model_item.program = upload_file(request, u'/media/uploads/recruits/',
                                                     model_item.program, 'program', id)
                    
                model_item.save()
                if settings.DEBUG:
                    print 'SAVE'
                return HttpResponseRedirect('/edu/recruitment/' + str(model_item.id) + '/')

            else:
                if settings.DEBUG:
                    print 'not valid'
                    print form.errors
                return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))
        else:
            form = ItemForm(instance=model_item,initial=init_dict)
        return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

    elif request.is_ajax():
        response["status"] = 0
        return HttpResponse(json.dumps(response))
    else:
        return redirect('personal_cabinet')

@csrf_exempt
@login_required
def institute_recruitment(request,institute_id):
    response = {}
    lv_ShortForm = False

    model_institute = get_object_or_404(Education_Institute,id=institute_id)

    if request.user.is_admin is True:
        ItemModel = Recruitment

        ItemForm = RecruitmentForm
        model_item = ItemModel()

        if request.method == 'POST':
            form = ItemForm(model_institute.curator,request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if ( formfieldname == 'institute_program' or formfieldname == 'payment_type' or formfieldname == 'currency' or formfieldname == 'form' ) and form.cleaned_data[formfieldname] is not None:
                        partfieldname = formfieldname + '_id'
                        model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'program':
                        model_item.program = delete_loaded_file(request, model_item.program, formfieldname)
                        continue
                    else:
                        partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                if request.FILES.has_key('program'):
                    model_item.program = upload_file(request, u'/media/uploads/recruits/',
                                                     model_item.program, 'program', id)

                model_item.save()
                return HttpResponseRedirect('/edu/institute/' + str(model_institute.id) + '/#recruitments')
            else:
                if settings.DEBUG:
                    print 'not valid'
                return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))

        form = ItemForm(model_institute.curator,instance=model_item)
        return render_to_response("recruitmentform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'curator':model_institute}, context_instance=RequestContext(request))
    return redirect('personal_cabinet')


@csrf_exempt
@login_required
def recruitment_delete(request,id):
    answer = {}
    answer[u"status"] = 0

    if request.user.is_admin == True:
        try:
            model_item = get_object_or_404(Recruitment,id=id)
            model_item.set_archive()
            if request.is_ajax():
                answer[u"status"] = 1
                answer[u'delete_success'] = u'Набор успешно удален'
                return HttpResponse(json.dumps(answer),content_type='application/json')
            else:
                request.session['delete_success'] = u'Набор успешно удален'
                return redirect('recruitments')
        except:
            if request.is_ajax():
                answer[u'delete_success'] = u'Произошла ошибка'
                return HttpResponse(json.dumps(answer),content_type='application/json')
            else:
                request.session['delete_success'] = u'Произошла ошибка'
                return redirect('recruitments')

    else:
        if request.is_ajax():
            return HttpResponse(json.dumps(answer),content_type='application/json')
        else:
            return redirect('recruitments')


def sticker_generate(request,id):
    recruitment = get_object_or_404(Recruitment,id=id)
    print str(recruitment.__dict__)
#    now = date(datetime.now().year, datetime.now().month, datetime.now().day)
#    now_plus_month = date(datetime.now().year, datetime.now().month+1, datetime.now().day)
#    try:
#        sticker = Sticker.objects.get(recruitment__id = id)
#    except:
#        sticker = Sticker()
#        sticker.title = recruitment.institute_program.institute.name + u' объявляет о наборе на программу ' + recruitment.institute_program.program.name
#        sticker.content = recruitment.institute_program.institute.name + u' объявляет о наборе на программу ' + recruitment.institute_program.program.name
#        sticker.creator = recruitment.institute_program.institute.curator
#        sticker.type = 'A'
#        sticker.institute = recruitment.institute_program.institute
#        sticker.active_from = now
#        sticker.active_till = now_plus_month
#        sticker.save()
#    return HttpResponse(sticker.content)
    response = {}
    print request.POST

    if request.user.is_curator == True:
        ItemModel = Sticker
        ItemForm = StickerRecruitmentForm

        model_item = get_object_or_404(ItemModel,id= recruitment.sticker.id)

        if request.method == 'POST':
            print 'POST'
            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                print 'valid'
                for formfieldname in form._meta.__dict__['fields']:
                    partfieldname = formfieldname
                    model_item.__dict__[partfieldname] = form.cleaned_data[formfieldname]

                model_item.__dict__['about_recruit'] = True

                model_item.save()
                print 'SAVE'
                return redirect('personal_cabinet')
            else:
                print 'not valid'
                print form.errors
                return render_to_response("stickerform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'generate': True,'recruitment':recruitment}, context_instance=RequestContext(request))

        form = ItemForm(instance=model_item)
        return render_to_response("stickerform.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'generate': True,'recruitment':recruitment}, context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')

def is_edurequest_relevant_for_user(edurequest,user):
    if user.is_admin:
        return True

    elif user.is_curator:
        try:
            curator = Education_Institute.objects.get(curator = user)
        except:
            print str(sys.exc_info())
            return False

        if edurequest.institute_program is None:
            return False
        if edurequest.institute_program.institute is None:
            return False
        if edurequest.institute_program.institute != curator:
            return False

    else:
        try:
            student = IprStudent.objects.get(user = user)
            if edurequest.student.id != student.id:
                return False
        except:
            #print str(sys.exc_info())
            return False
    return True

@login_required
def request_form(request,id):
    edurequest = get_object_or_404(EducationRequest,id= id)
    ipr = edurequest.get_iprstudent()

    if not is_edurequest_relevant_for_user(edurequest,request.user):
        return redirect('personal_cabinet')

    if edurequest.institute_program.program.level == 'B':
        RequestForm = EducationRequestBForm
    else:
        RequestForm = EducationRequestUForm

    if request.user.is_admin == True:
        if request.method == 'POST':
            ipr_form     = EditStudentForm(request.POST)
            request_form = RequestForm(request.POST)

            if ipr_form.is_valid() and request_form.is_valid():
                for formfieldname in ipr_form._meta.__dict__['fields']:
                    if formfieldname == 'profstatus' and ipr_form.cleaned_data[formfieldname] is not None:
                        try:
                            ipr.__dict__['profstatus_id'] = ipr_form.cleaned_data[formfieldname].id
                        except:
                            None
                        continue
                    elif formfieldname == 'education':
                        ipr.__dict__['education_id'] = ipr_form.cleaned_data[formfieldname].id
                        continue
                    elif formfieldname == 'email' or  formfieldname == 'last_name' or  formfieldname == 'first_name':
                        ipr.user.__dict__[formfieldname] = ipr_form.cleaned_data[formfieldname]
                        continue
                    else:
                        partfieldname = formfieldname

                    ipr.__dict__[partfieldname] = ipr_form.cleaned_data[formfieldname]
                ipr.user.save()
                ipr.save()

                education_institute  = request_form.cleaned_data['education_institute']
                education_program    = request_form.cleaned_data['education_program']

                try:
                    edurequest.institute_program = Education_Institute_Program.objects.get(institute = education_institute,program = education_program)
                    edurequest.level = education_program.level

                    if edurequest.institute_program.has_seminar == True:
                        edurequest.practic_period = request_form.cleaned_data['practic_period']
                        edurequest.payment_method = request_form.cleaned_data['payment_method']
                        edurequest.details        = request_form.cleaned_data['details']
                    else:
                        edurequest.practic_period = ''
                        edurequest.payment_method = 'F'
                        edurequest.details        = ''

                    edurequest.save()
                except:
                    print(str(sys.exc_info()))

                return HttpResponseRedirect('/edu/request/' + str(edurequest.id) + '/')

            else:
                return render_to_response("request_form.html", {'item':edurequest,'verbose_name':EducationRequest._meta.verbose_name,'Form':ipr_form,'RequestForm':request_form,'level':edurequest.institute_program.program.level}, context_instance=RequestContext(request))
        else:
            ipr_form      = EditStudentForm(instance=ipr)
            request_form  = RequestForm(instance=edurequest)
        return render_to_response("request_form.html", {'item':edurequest,'verbose_name':EducationRequest._meta.verbose_name,'Form':ipr_form,'RequestForm':request_form,'level':edurequest.institute_program.program.level}, context_instance=RequestContext(request))

    else:
        request_form = RequestForm(instance=edurequest)

        return render_to_response("request_form.html", {'item':edurequest,'verbose_name':EducationRequest._meta.verbose_name,'RequestForm':request_form,'level':edurequest.institute_program.program.level}, context_instance=RequestContext(request))


# Convert HTML URIs to absolute system paths so xhtml2pdf can access those resources
def link_callback(uri, rel):
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/ http://127.0.0.1:8000/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/

    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                    'media URI must start with %s or %s' % \
                    (sUrl, mUrl))
    return path

@login_required
def request_pdf(request,id):
    edurequest = get_object_or_404(EducationRequest,id= id)

    if not is_edurequest_relevant_for_user(edurequest,request.user):
        return redirect('personal_cabinet')

    pdf = edurequest.pdf_content(True)
    return HttpResponse(pdf, content_type='application/pdf')


def person_edit(request,student_slug):
    if request.user.is_authenticated() and request.user.is_admin == True:
        person = get_object_or_404(IprStudent,slug=student_slug)

        iprmembership_forms = []
        iprmembership_valid = True

        if request.method == 'POST':

            if person.user.is_ipr:
                iprmemberships_count = person.iprstudentmembership_set.all().count()
            else:
                iprmemberships_count = 0

            if iprmemberships_count > 0:
                iprmembership_memberships  = request.POST.getlist('membership')
                iprmembership_paids        = request.POST.getlist('paid')
                iprmembership_benefits     = request.POST.getlist('benefit')
                iprmembership_comments     = request.POST.getlist('comment')

                line_index = 0

                while line_index < iprmemberships_count:
                    iprmembership_dict = {}

                    iprmembership_dict["iprstudent"] = person

                    if len(iprmembership_memberships) > line_index:
                        iprmembership_dict["membership"] = iprmembership_memberships[line_index]

                    if len(iprmembership_paids) > line_index:
                        iprmembership_dict["paid"] = iprmembership_paids[line_index]

                    if len(iprmembership_benefits) > line_index:
                        iprmembership_dict["benefit"] = iprmembership_benefits[line_index]

                    if len(iprmembership_comments) > line_index:
                        iprmembership_dict["comment"] = iprmembership_comments[line_index]

                    iprmembership_form = IprStudentMembershipShortForm(iprmembership_dict)

                    if iprmembership_form.is_valid():
                        membership = iprmembership_form.cleaned_data['membership']
                        try:
                            iprstudentmembership_model = person.iprstudentmembership_set.filter(membership = membership)[0]
                            iprstudentmembership_model.paid =  iprmembership_form.cleaned_data['paid']
                            iprstudentmembership_model.benefit =  iprmembership_form.cleaned_data['benefit']
                            iprstudentmembership_model.comment =  iprmembership_form.cleaned_data['comment']
                            iprstudentmembership_model.save()
                        except:
                            print str(sys.exc_info())
                    else:
                        iprmembership_valid = False
                        iprmembership_forms.append(iprmembership_form)

                    line_index = line_index + 1

            eduReqFormset = PersonEdReqFormset(request.POST, request.FILES, instance=person)
            form = EditPersonForm(request.POST)

            if form.is_valid() and eduReqFormset.is_valid() and iprmembership_valid:
                form.save()
                eduReqFormset.save()

                if request.POST.has_key('confirm'):
                    person.confirm()
                if request.POST.has_key('reject'):
                    person.reject(u'без причины')

                person = IprStudent.objects.get(id = person.id)

                return HttpResponseRedirect('/edu/person/edit/' + person.slug + '/')

            else:
                return render_to_response('person_edit.html',{'person':person,'Form':form,'eduReqFormset':eduReqFormset,'iprmembership_forms':iprmembership_forms},context_instance=RequestContext(request))
        else:
            form = EditPersonForm(instance=person)

            eduReqFormset = PersonEdReqFormset(instance=person)

            iprmemberships = person.iprstudentmembership_set.all().order_by('year')

            for iprmembership in iprmemberships:
                iprmembership_form = IprStudentMembershipShortForm(instance=iprmembership) #,prefix = "new"
                iprmembership_forms.append(iprmembership_form)

            return render_to_response('person_edit.html',{'person':person,'Form': form, 'iprmembership_forms':iprmembership_forms,'eduReqFormset':eduReqFormset},context_instance=RequestContext(request))
        return render_to_response('person_edit.html',{'person':person},context_instance=RequestContext(request))
    elif request.user.is_authenticated():
        return redirect('personal_cabinet')
    else:
        return redirect('home')

@login_required
def person_request(request,student_slug):
    if request.user.is_authenticated() and request.user.is_admin == True:
        person = get_object_or_404(IprStudent,slug=student_slug)

        if person.user is None or person.user.has_edu_request:
            return redirect('personal_cabinet')

        if person.user.is_student:
            RequestForm = RequestBPersonForm
            level = 'B'
        else:
            RequestForm = EducationRequestUForm #RequestUPersonForm
            level = 'U'

        if request.method == 'POST':
            form = RequestForm(request.POST)

            if form.is_valid():
                if form.save(person):
                    return HttpResponseRedirect('/edu/person/edit/' + person.slug + '/')
        else:
            form = RequestForm()

        return render_to_response('person_request.html',{'Form':form, 'person':person,'level':level},context_instance=RequestContext(request))
    elif request.user.is_authenticated():
        return redirect('personal_cabinet')
    else:
        return redirect('home')


@login_required
def person_delete(request,student_slug):
    try:
        person = IprStudent.objects.select_related('user','educationrequest','studentgroup','studentgroup__group').get(slug=student_slug)
        if request.method == 'POST' and request.user.is_admin:
            fio = person.get_fio
            person.delete()
            return render_to_response('person_delete.html',{'finish':1,'fio':fio},context_instance=RequestContext(request))
        edurequests = []
        if person is not None:
            cursor = connection.cursor()
            cursor.execute("""SELECT eper.id, eper.student_id, eper.date_request, eper.is_archive, eper.institute_program_id,
                epeip.is_archive, 
                epeip.institute_id, epei.name,  
                epeip.program_id, epep.name,
                epsg.id, epsg.group_id, epsg.exam_result_id,
                epg.name
                FROM education_programmes_educationrequest as eper
                LEFT JOIN education_programmes_education_institute_program as epeip
                ON eper.institute_program_id = epeip.id
                LEFT JOIN education_programmes_education_institute as epei
                ON epeip.institute_id = epei.id
                LEFT JOIN education_programmes_education_programme as epep
                ON epeip.program_id = epep.id
                LEFT JOIN education_programmes_studentgroup as epsg
                ON eper.id = epsg.request_id
                LEFT JOIN education_programmes_group as epg
                ON epsg.group_id = epg.id
                where eper.student_id = %s""", [person.id])
            edurequests = cursor.fetchall()  
        return render_to_response('person_delete.html',{'person':person,'edurequests':edurequests,'finish':0},context_instance=RequestContext(request))
    except:
        return redirect('search')

@login_required
def person_membership(request,student_slug):
    if request.user.is_authenticated() and request.user.is_admin == True:
        initial_dict = {}
        person = get_object_or_404(IprStudent,slug=student_slug)
        initial_dict["iprstudent"] = person

        region_choice = person.get_region()

        if len(region_choice) == 1:
            initial_dict["region"] = Region.objects.get(id = region_choice[0])

        ItemForm  = MembershipCreatePersonForm

        if request.method == 'POST':
            form = ItemForm(request.POST)

            iprmembership = IprStudentMembership()

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    if formfieldname == 'membership':
                        iprmembership.membership = form.cleaned_data[formfieldname]

                        continue
                    elif formfieldname == 'iprstudent':
                        iprmembership.iprstudent = form.cleaned_data[formfieldname]
                        continue
                    elif formfieldname == 'benefit':
                        iprmembership.benefit = form.cleaned_data[formfieldname]
                        continue

                    else:
                        partfieldname = formfieldname

                    iprmembership.__dict__[partfieldname] = form.cleaned_data[formfieldname]
                iprmembership.save()
                iprmembership.year = iprmembership.membership.year
                return HttpResponseRedirect('/edu/person/edit/' + person.slug + '/')

        else:
            form = ItemForm(initial=initial_dict)

        return render_to_response('person_membership.html',{'person':person,'Form':form},context_instance=RequestContext(request))
    else:
        return redirect('search')

@csrf_exempt
@login_required
def person_membership_delete(request,id):
    response = {}
    if request.user.is_authenticated() and request.user.is_admin:
        model_item = IprStudentMembership.objects.get(id  = id)
        model_item.delete()
        response["status"] = 1
        return HttpResponse(json.dumps(response))
    else:
        response["status"] = 0
        return HttpResponse(json.dumps(response))


def program_present_url(request,id):
    model_item = get_object_or_404(Education_Programme,id=id)
    return HttpResponse(model_item.presentation_filename())

def program_present_name(request,id):
    model_item = get_object_or_404(Education_Programme,id=id)
    return HttpResponse(model_item.presentation_url())

@csrf_exempt
@login_required
def membershipcost_region(request):
    if request.user.is_authenticated() and request.user.is_admin:
        if request.method == 'POST':
            form = MembershipCostRegionChoiceForm(request.POST)
            if form.is_valid():
                region = form.cleaned_data['region']
                return HttpResponseRedirect('/edu/membershipcost/region/' + str(region.id) + '/')
        else:
            form = MembershipCostRegionChoiceForm()
        return render_to_response("membershipcost_region.html", {'Form':form,'verbose_name':u'Взносы. Регион'}, context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')

@csrf_exempt
@login_required
def membershipcost(request,region_id):
    if request.user.is_authenticated() and request.user.is_admin:
        region = Region.objects.get(id = region_id)
        model_item = MembershipCost()

        old_membercosts = []
        new_membercosts = []
        new_costs_count = 0

        if request.method == 'POST':
            new_invalid = False
            old_invalid = False

            regions = request.POST.getlist('new-region')
            years = request.POST.getlist('new-year')
            costs = request.POST.getlist('new-cost')

            new_costs_count =  len(regions)
            new_costs_index = 0

            while new_costs_index < new_costs_count:
                year = years[new_costs_index]
                cost = costs[new_costs_index]
                new_membercost_dict = {"new-region":region_id,"new-year":year,"new-cost":cost,"prefix":"new"}
                new_membercost_form = MembershipCostCreateForm(new_membercost_dict,prefix = "new")
                if not new_membercost_form.is_valid():
                    new_invalid = True
                new_membercosts.append(new_membercost_form)
                new_costs_index = new_costs_index + 1

            regions = request.POST.getlist('region')
            years = request.POST.getlist('year')
            costs = request.POST.getlist('cost')

            old_costs_count =  len(regions)
            old_costs_index = 0

            while old_costs_index < old_costs_count:
                year = years[old_costs_index]
                cost = costs[old_costs_index]
                old_membercost_queries = region.membershipcost_set.filter(year = year)

                if len(old_membercost_queries) > 0:
                    old_membercost = old_membercost_queries[0]
                else:
                    continue

                old_membercost_dict = {"region":region_id,"year":year,"cost":cost}
                old_membercost_form = MembershipCostChangeForm(old_membercost_dict,instance = old_membercost)
                if not old_membercost_form.is_valid():
                    old_invalid = True
                old_membercosts.append(old_membercost_form)
                old_costs_index = old_costs_index + 1

            if old_invalid == False and new_invalid == False:

                for new_membercost_form in new_membercosts:
                    new_membercost = MembershipCost()

                    for formfieldname in new_membercost_form._meta.__dict__['fields']:
                        if formfieldname == 'region':
                            new_membercost.region = new_membercost_form.cleaned_data[formfieldname]
                            continue
                        else:
                            partfieldname = formfieldname

                        new_membercost.__dict__[partfieldname] = new_membercost_form.cleaned_data[formfieldname]
                    new_membercost.save()

                for old_membercost_form in old_membercosts:
                    old_membercost_queries = region.membershipcost_set.filter(year = old_membercost_form.cleaned_data['year'])

                    if len(old_membercost_queries) > 0:
                        old_membercost = old_membercost_queries[0]
                    else:
                        continue

                    for formfieldname in old_membercost_form._meta.__dict__['fields']:
                        if formfieldname == 'region' or formfieldname == 'year':
                            continue
                        else:
                            partfieldname = formfieldname

                        old_membercost.__dict__[partfieldname] = old_membercost_form.cleaned_data[formfieldname]
                    old_membercost.save()

                return HttpResponseRedirect('/edu/membershipcost/region/' + str(region.id) + '/')
        else:
            for membercost in region.membershipcost_set.all():
                change_form = MembershipCostChangeForm(instance=membercost)
                old_membercosts.append(change_form)

        new_count = len(new_membercosts)
        return render_to_response("membershipcost.html", {'item':model_item,'verbose_name':model_item._meta.verbose_name,"region":region,"old_membercosts":old_membercosts,"new_membercosts":new_membercosts,"new_count":new_count}, context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')


@csrf_exempt
@login_required
def membershipcost_edit(request,id):
    response = {}
    if request.user.is_authenticated() and request.user.is_admin and request.method == 'POST':
        form = MembershipCostChangeForm(request.POST)
        model_item = MembershipCost.objects.get(id  = id)

        if form.is_valid():
            for formfieldname in form._meta.__dict__['fields']:
                if formfieldname == 'region':
                    modelfieldname = formfieldname + '_id'
                else:
                    modelfieldname = formfieldname
                model_item.__dict__[modelfieldname] = form.cleaned_data[formfieldname]

            model_item.save()
            response["status"] = 1
            return HttpResponse(json.dumps(response))
        else:
            print form.errors
            response["status"] = 0
            return HttpResponse(json.dumps(response))
    else:
        return redirect('personal_cabinet')

@csrf_exempt
@login_required
def membershipcost_delete(request,id):
    response = {}
    if request.user.is_authenticated() and request.user.is_admin:
        model_item = MembershipCost.objects.get(id  = id)
        model_item.delete()
        response["status"] = 1
        return HttpResponse(json.dumps(response))
    else:
        response["status"] = 0
        return HttpResponse(json.dumps(response))

@csrf_exempt
@login_required
def membershipbenefit_edit(request,id=None):
    response = {}
    if request.user.is_authenticated() and request.user.is_admin and request.method == 'POST':
        form = MembershipBenefitForm(request.POST)
        if form.is_valid():
            if id is None:
                model_item = MembershipBenefit()
            else:
                model_item = MembershipBenefit.objects.get(id = id)

            for formfieldname in form._meta.__dict__['fields']:
                model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]

            model_item.save()
            response["status"] = 1
            response["id"] = str(model_item.id)
            response["action"] = '/edu/membershipbenefit/' + str(model_item.id) + '/'
            return HttpResponse(json.dumps(response))
        else:
            response["status"] = 0
            return HttpResponse(json.dumps(response))
    else:
        return redirect('personal_cabinet')


@login_required
def membershipbenefit_delete(request,id):
    response = {}
    if request.user.is_authenticated() and request.user.is_admin:
        try:
            model_item = MembershipBenefit.objects.get(id = id)
            model_item.delete()
            response["status"] = 1
            response["id"] = str(id)
            return HttpResponse(json.dumps(response))
        except :
            response["status"] = 0
            return HttpResponse(json.dumps(response))
    else:
        return redirect('personal_cabinet')

@login_required
def memberships_entrance(request):
    if request.user.is_authenticated() and request.user.is_admin:
        number_lines = 1
        entrance_forms = []
        entrance_items = []
        create_form = MembershipEntranceForm()

        if request.method == 'POST' and request.POST.has_key('number_lines'):
            number_lines = int(request.POST['number_lines'])
            print number_lines

            membership_numbers = request.POST.getlist('membership_number')
            iprstudents = request.POST.getlist('iprstudent')
            regions = request.POST.getlist('region')
            years = request.POST.getlist('year')
            memberships = request.POST.getlist('membership')
            paids = request.POST.getlist('paid')
            benefits = request.POST.getlist('benefit')
            comments = request.POST.getlist('comment')

            line_index = 0

            while line_index < number_lines:
                entrance_dict = {}

                if len(membership_numbers) > line_index:
                    entrance_dict["membership_number"] = membership_numbers[line_index]
#                else:
#                    membership_number = None

                if len(iprstudents) > line_index:
                    entrance_dict["iprstudent"] = iprstudents[line_index]
#                else:
#                    iprstudent = None


                if len(regions) > line_index:
                    entrance_dict["region"] = regions[line_index]
#                else:
#                    region = None

                if len(years) > line_index:
                    entrance_dict["year"] = years[line_index]
#                else:
#                    year = None

                if len(memberships) > line_index:
                    entrance_dict["membership"] = memberships[line_index]
#                else:
#                    membership = None

                if len(paids) > line_index:
                    entrance_dict["paid"] = paids[line_index]
#                else:
#                    paid = None

                if len(benefits) > line_index:
                    entrance_dict["benefit"] = benefits[line_index]
#                else:
#                    benefit = None

                if len(comments) > line_index:
                    entrance_dict["comment"] = comments[line_index]
#                else:
#                    comment = None

                entrance_form = MembershipEntranceForm(entrance_dict)

                if entrance_form.is_valid():
                    entrance_item = IprStudentMembership()
                    for formfieldname in entrance_form._meta.__dict__['fields']:
                        if formfieldname == 'membership_number' or formfieldname == 'region':
                            continue
                        elif ( formfieldname == 'iprstudent' or formfieldname == 'membership' or formfieldname == 'benefit' ) and entrance_form.cleaned_data[formfieldname] is not None:
                            modelfieldname = formfieldname + '_id'
                            entrance_item.__dict__[modelfieldname] = entrance_form.cleaned_data[formfieldname].id
                            continue
                        entrance_item.__dict__[formfieldname] = entrance_form.cleaned_data[formfieldname]
                    entrance_item.save()
                    entrance_items.append(entrance_item)
                else:
                    entrance_forms.append(entrance_form)

                line_index = line_index + 1

        if len(entrance_forms) > 0 :
            number_lines = len(entrance_forms)

        return render_to_response('membership_entrance.html',{'number_lines':number_lines,'create_form':create_form,'entrance_items':entrance_items,'entrance_forms':entrance_forms},context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')


@login_required
def memberships_unload(request):
    if request.user.is_authenticated() and request.user.is_admin:

    #        "cities":cities,"regions": regions,"institutes":institutes,"programs":programs,"studymods":studymods,"levels":PROGRAM_LEVEL_CHOICES, "profstatuses": profstatuses, "educations":educations,"tab_sel": tab_sel,"email":email,"program":program,"city":city,"institute":institute, "base_results":base_results, "up_results":up_results, "fio":fio,"birthday":birthday,"education":education
        return render_to_response('membership_unload.html',{},context_instance=RequestContext(request))
    else:
        return redirect('personal_cabinet')