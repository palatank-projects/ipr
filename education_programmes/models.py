# -*- coding: utf-8 -*-
# System libraries
from datetime import datetime,date,timedelta
import sys, os
from slugify import slugify

# Third-party libraries
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

# Django modules
from django.db import models, connection
from django.utils.encoding import force_text

# Django apps

# Current-app modules
from common_user.models import  User,Student,IprStudent
import IPR.settings as settings

PROGRAM_LEVEL = (u'Подготовка налоговых консультантов',u'Повышение квалификации налоговых консультантов')
PROGRAM_LEVEL_DICT = {'B' : PROGRAM_LEVEL[0],
                      'U' : PROGRAM_LEVEL[1]
}
PROGRAM_LEVEL_CHOICES = (
    ('B', PROGRAM_LEVEL[0]),
    ('U', PROGRAM_LEVEL[1]),
    )

ACCREDITATION = (u'Аккредитовано',u'Не аккредитовано',u'Аккредитация временно приостановленна')
ACCREDITATION_DICT = {'A' : ACCREDITATION[0],
                      'N' : ACCREDITATION[1],
                      'P' : ACCREDITATION[2]
}
ACCREDITATION_CHOICES = (
    ('A', ACCREDITATION[0]),
    ('N', ACCREDITATION[1]),
    ('P', ACCREDITATION[2]),
    )

PAYMENT_METHOD = (u'Физ. лицо',u'Юр. лицо')
PAYMENT_METHOD_DICT = {'F' : PAYMENT_METHOD[0],
                      'U' : PAYMENT_METHOD[1]
}
PAYMENT_METHOD_CHOICES = (
    ('F', PAYMENT_METHOD[0]),
    ('U', PAYMENT_METHOD[1]),
    )

	
STICKER_TYPE = (u'Всеобщее',u'Для организации',u'Для группы')

STICKER_TYPE_CHOICES = (
    ('A', STICKER_TYPE[0]),
    ('I', STICKER_TYPE[1]),
    ('G', STICKER_TYPE[2]),
)
class StudyMode(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Текст')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Форма обучения'
        verbose_name_plural = u'Формы обучения'
        ordering = ['title']

class ExamResult(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Текст')
    is_finish = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Результат экзамена'
        verbose_name_plural = u'Результаты экзаменов'
        ordering = ['title']

class PaymentType(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Текст')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Тип оплаты'
        verbose_name_plural = u'Типы оплаты'
        ordering = ['title']

class Currency(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Наименование')
    short_title = models.CharField(max_length = 50,verbose_name = u'Сокращенное написание')
    symbol = models.CharField(max_length = 5,verbose_name = u'Символ')

    def __unicode__(self):
        return self.symbol

    class Meta:
        verbose_name = u'Валюта'
        verbose_name_plural = u'Валюта'
        ordering = ['title']
		
#class Periodicity(models.Model):
#    title = models.CharField(max_length = 100,verbose_name = u'Текст')

#    def __unicode__(self):
#        return self.title

#    class Meta:
#        verbose_name = u'Периодичность обучения'
#        verbose_name_plural = u'Периодичность обучения'
#        ordering = ['title']


class Region(models.Model):
    name  = models.TextField(verbose_name = u'Наименование')
    id_city = models.IntegerField(blank=True,null=True,verbose_name = u'Хрень')
	
    raphael_path = models.TextField(blank=True,null=True,verbose_name = u'Координаты на карте')
    raphael_slug = models.SlugField(max_length=23,verbose_name = u'Slug')
    raphael_id   = models.IntegerField(blank=True,null=True,verbose_name = u'Идентификатор на карте')

    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Регион'
        verbose_name_plural = u'Регионы'
        ordering = ['name']

    def set_archive(self):
        for city in self.city_set.all():
            city.set_archive()
        self.is_archive = True
        self.save()


class City(models.Model):
    name       = models.TextField(verbose_name = u'Наименование')
    region     = models.ForeignKey(Region,blank=True,null=True,verbose_name = u'Регион', on_delete=models.SET_NULL)
    slug       = models.SlugField(max_length=23,verbose_name = u'Slug')
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

    def __unicode__(self):
        return (self.region.name if self.region is not None else u'') + u' ' + self.name

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'
        ordering = ['region','name']

    def set_archive(self):
        for education_institute in self.education_institute_set.all():
            education_institute.set_archive()
        self.is_archive = True
        self.save()

    def save(self,*args,**kwargs):
        if self.slug is None or self.slug.isspace() or len(self.slug) == 0:
            self.slug = slugify(self.name)
        super(City,self).save(*args,**kwargs)

class MembershipCost(models.Model):
    year = models.CharField(max_length = 4,verbose_name = u'Год')
    region = models.ForeignKey(Region,blank=False,null=True,verbose_name = u'Регион', on_delete=models.SET_NULL)
    cost = models.DecimalField(max_digits=11, decimal_places=2,verbose_name=u'Взнос, руб')

    def __unicode__(self):
        return self.year + u' г.  ' + str(self.cost) + u' руб. ' + ( self.region.name if self.region is not None else u'')


    def delete(self):
        for membership in self.iprstudentmembership_set.all():
            membership.delete()
        super(MembershipCost, self).delete()


    class Meta:
        verbose_name = u'Стоимость членства в год'
        verbose_name_plural = u'Стоимость членства в год'
        ordering = ['region','-year']

class MembershipBenefit(models.Model):
    title = models.CharField(max_length = 50,verbose_name = u'Название')
    percent = models.DecimalField(max_digits=5, decimal_places=2,verbose_name=u'Процент, %')

    def __unicode__(self):
        return str(self.percent) + u' % ' + self.title

    class Meta:
        verbose_name = u'Льгота'
        verbose_name_plural = u'Льготы'
        ordering = ['title']


class IprStudentMembership(models.Model):
    iprstudent = models.ForeignKey(IprStudent,null=True, on_delete=models.SET_NULL,verbose_name = u'Налоговый консультант')
    membership = models.ForeignKey(MembershipCost,null=True, on_delete=models.SET_NULL,verbose_name = u'Регион, год, стоимость')
    benefit = models.ForeignKey(MembershipBenefit,null=True, on_delete=models.SET_NULL,verbose_name = u'Льгота')

    year = models.CharField(max_length = 4,verbose_name = u'Год')
    paid = models.DecimalField(max_digits=11, decimal_places=2,verbose_name=u'Плата, руб')

    ticket = models.FileField(max_length=500,upload_to="/uploads/tickets",blank=True,null=True,verbose_name=u'Квитанция')

    comment = models.TextField(null= True, blank=True,verbose_name=u'Комментарий')

    def __unicode__(self):
        return str(self.year) + u' г. ' + (self.iprstudent.get_fio if self.iprstudent is not None else u'') + str(self.paid)

    def save(self):
        if self.membership is not None:
            self.year = self.membership.year
        super(IprStudentMembership, self).save()


    class Meta:
        verbose_name = u'Членский взнос'
        verbose_name_plural = u'Членские взносы'
        ordering = ['iprstudent','-year']

    @property
    def debt(self):
        debt = 0
        if self.membership is not None:
            cost = self.membership.cost

            if self.benefit is not None:
                cost = cost - ( self.benefit.percent * cost ) / 100

            debt = cost - self.paid
        return debt

class InstituteCurator(models.Model):
    user = models.ForeignKey(User)
    date_started = models.DateField(null=True,blank=True,editable=True, verbose_name = u'Действует с')
    date_ended = models.DateField(null=True,blank=True,editable=True, verbose_name = u'Действует по')
    created = models.DateTimeField(auto_now=True,editable=True)

    def __unicode__(self):
        if self.user is not None:
            return self.user.email
        else:
            return str(self.id)

    @property
    def get_fio(self):
        return self.user.first_name.capitalize() + " " + self.user.last_name.capitalize()

    class Meta:
        verbose_name = u'Куратор учебного заведения'
        verbose_name_plural = u'Кураторы учебных заведений'

class Education_Institute(models.Model):
    name = models.TextField(verbose_name=u'Название')
    accreditation_number = models.TextField(blank=True,null=True,verbose_name=u'Номер свидетельства об аккредитации')
    web_site = models.URLField(blank=True,null=True,verbose_name=u'Веб-сайт')
    telephone = models.TextField(blank=True,null=True,verbose_name=u'Телефон')
    post_address = models.TextField(blank=True,null=True,verbose_name=u'Адрес')
    city = models.ForeignKey(City,blank=True,null=True,verbose_name=u'Город', on_delete=models.SET_NULL)
    email = models.EmailField(verbose_name=u'Электронный адрес')
    director = models.TextField(verbose_name=u'Руководитель')
    created = models.DateTimeField(auto_now=True)
    curator = models.ForeignKey(User,blank=True,null=True,verbose_name=u'Менеджер', on_delete=models.SET_NULL) #InstituteCurator,blank=True,null=True,verbose_name=u'Координатор')
    accreditation = models.CharField(max_length=1, choices=ACCREDITATION_CHOICES,default='A', verbose_name=u'Аккредитация')
    additional = models.TextField(blank=True,null=True,verbose_name=u'Дополнительная информация')
    access_ipr_program = models.BooleanField(default = False,verbose_name=u'Имеет доступ к программа Палаты')
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

    certificate = models.FileField(max_length=500, upload_to="/uploads/certificates", blank=True, null=True,
                                   verbose_name=u'Аккредитационный сертификат')

    certificate2 = models.FileField(max_length=500, upload_to="/uploads/certificates2", blank=True, null=True,
                                   verbose_name=u'Дополнительный сертификат')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Образовательная организация'
        verbose_name_plural = u'Образовательные организации'
        ordering = ['name']

    def program_list(self):
        programs = []
        for prog in self.education_institute_program_set.filter(is_archive = False):
            if not self.access_ipr_program and prog.program.ipr_program:
                continue
            programs.append({"id":prog.id,"name":prog.program.__unicode__(),"item":prog})
        return programs

    def program_base_list(self):
        programs = []
        for prog in self.education_institute_program_set.filter(is_archive = False,program__level = 'B'):
            if not self.access_ipr_program and prog.program.ipr_program:
                continue
            programs.append({"id":prog.id,"name":prog.program.__unicode__(),"item":prog})
        return programs

    def program_up_list(self):
        programs = []
        for prog in self.education_institute_program_set.filter(is_archive = False,program__level = 'U'):
            if not self.access_ipr_program and prog.program.ipr_program:
                continue
            programs.append({"id":prog.id,"name":prog.program.__unicode__(),"item":prog})
        return programs

    @property
    def get_programs(self):
        cursor = connection.cursor()

        if not self.access_ipr_program:
            cursor.execute("""SELECT epep.id as programid, epep.name, epeip.id,
            DATE_FORMAT(epeip.accreditation_finish, '%%d.%%m.%%Y'), epeip.is_archive, epeip.has_seminar, epep.level
            FROM education_programmes_education_programme as epep
            LEFT JOIN (select * from education_programmes_education_institute_program where institute_id = %s ) as epeip
            ON epep.id = epeip.program_id
            WHERE epep.ipr_program = 0
              AND epep.is_archive = 0
            ORDER BY epep.name""", [self.id])
        else:
            cursor.execute("""SELECT epep.id as programid, epep.name, epeip.id,
            DATE_FORMAT(epeip.accreditation_finish, '%%d.%%m.%%Y'), epeip.is_archive, epeip.has_seminar, epep.level
            FROM education_programmes_education_programme as epep
            LEFT JOIN (select * from education_programmes_education_institute_program where institute_id = %s ) as epeip
            ON epep.id = epeip.program_id
            WHERE epep.is_archive = 0
            ORDER BY epep.name""", [self.id])

        result_of_select = cursor.fetchall()
        return result_of_select

    def up_groups(self):
        now = date(datetime.now().year, datetime.now().month, datetime.now().day)
        return self.group_set.filter(models.Q(is_archive = False,level = 'U',is_finished=False,education_end__isnull=True)|models.Q(is_archive = False,level = 'U',is_finished=False,education_end__gte=now))

    def base_groups(self):
        now = date(datetime.now().year, datetime.now().month, datetime.now().day)
        return self.group_set.filter(models.Q(is_archive = False,level = 'B',is_finished=False,education_end__isnull=True)|models.Q(is_archive = False,level = 'B',is_finished=False,education_end__gte=now))

    def over_up_groups(self):
        now = date(datetime.now().year, datetime.now().month, datetime.now().day)
        return self.group_set.filter(models.Q(level = 'U'),models.Q(is_archive = True)|models.Q(is_finished=True)|models.Q(education_end__lt=now))

    def over_base_groups(self):
        now = date(datetime.now().year, datetime.now().month, datetime.now().day)
        return self.group_set.filter(models.Q(level = 'B'),models.Q(is_archive = True)|models.Q(is_finished=True)|models.Q(education_end__lt=now))

    @property
    def get_recruitments(self):
        cursor = connection.cursor()

        cursor.execute("""SELECT epr.id,DATE_FORMAT(epr.begin_date, '%%d.%%m.%%Y'),
        epep.id as programid,epep.name
        FROM education_programmes_recruitment as epr
        LEFT JOIN (select * from education_programmes_education_institute_program
        where institute_id = %s
        and is_archive = 0 ) as epeip
        ON epr.institute_program_id = epeip.id
        JOIN education_programmes_education_programme as epep
        ON epep.id = epeip.program_id
        WHERE epr.is_archive = 0
        and epep.is_archive = 0
        ORDER BY epr.begin_date DESC
        """, [self.id])

        result_of_select = cursor.fetchall()
        return result_of_select


    @property
    def accreditation_name(self):
        return ACCREDITATION_DICT[self.accreditation]

    @property
    def fio(self):
        return (self.curator.last_name if self.curator.last_name is not None else u'') + u' ' + (self.curator.first_name if self.curator.first_name is not None else u'')

    def set_archive(self):
        for education_institute_program in self.education_institute_program_set.all():
            education_institute_program.set_archive()
        for group in self.group_set.all():
            group.set_archive()
        for sticker in self.sticker_set.all():
            sticker.set_archive()
        self.is_archive = True
        self.save()

    def is_accredited_on(self, program):
        if len(self.education_institute_program_set.filter(is_archive = False,program = program, program__is_archive = False)) > 0 :
            return True
        return False

    def certificate_filename(self):
        last_slash = force_text(self.certificate).rfind('/')
        return force_text(self.certificate)[last_slash+1:]

    def certificate_url(self):
        media_slash = force_text(self.certificate.url).find(u'/media/')
        return 	(self.certificate.url)[media_slash:]

    def certificate2_filename(self):
        last_slash = force_text(self.certificate2).rfind('/')
        return force_text(self.certificate2)[last_slash+1:]

    def certificate2_url(self):
        media_slash = force_text(self.certificate2.url).find(u'/media/')
        return 	(self.certificate2.url)[media_slash:]

# class Education_Institute_Certificate(models.Model):
#     institute = models.ForeignKey(Education_Institute,blank=False,null=True, on_delete=models.CASCADE)
#     description = models.TextField(blank=False,null=False,verbose_name=u'Описание')
#     certificate = models.FileField(max_length=500,upload_to="/uploads/certificates",blank=False,null=False,verbose_name=u'Скан сертификата')
#
#     def __unicode__(self):
#         return self.description
#
#     def certificate_filename(self):
#         last_slash = force_text(self.certificate).rfind('/')
#         return force_text(self.certificate)[last_slash+1:]
#
#     def certificate_url(self):
#         media_slash = force_text(self.certificate.url).find(u'/media/')
#         return 	(self.certificate.url)[media_slash:]
#
#     class Meta:
#         verbose_name = u'Аккредитационный сертификат'
#         verbose_name_plural = u'Аккредитационный сертификаты'
#         ordering = ['institute']


class Education_Programme(models.Model):
    name = models.CharField(max_length=255,verbose_name=u'Название')
    description = models.TextField(blank=True,null=True,verbose_name=u'Описание')
    presentation = models.FileField(max_length=500,upload_to="/uploads/presentations",blank=True,null=True,verbose_name=u'Презентация')
    level = models.CharField(max_length=1, choices=PROGRAM_LEVEL_CHOICES,default='B', verbose_name=u'Статус')
    ipr_program = models.BooleanField(default = False,verbose_name=u'Программа Палаты')
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

#    @property
    def institute_list(self):
        institutes = []
        for inst in self.education_institute_program_set.filter(is_archive = False): #filter(accreditation = 'A'):
            if self.ipr_program and not inst.institute.access_ipr_program:
                continue
            institutes.append({"id":inst.id,"name":inst.institute.__unicode__(),"item":inst})
        return institutes

    @property
    def get_institutes(self):
        cursor = connection.cursor()

        if self.ipr_program:
            cursor.execute("""SELECT epei.id as instituteid, epei.name, epeip.id,
            DATE_FORMAT(epeip.accreditation_finish, '%%d.%%m.%%Y'), IFNULL(epeip.is_archive,0), epeip.has_seminar
            FROM education_programmes_education_institute as epei
            LEFT JOIN (select * from education_programmes_education_institute_program where program_id = %s ) as epeip
            ON epei.id = epeip.institute_id
            where epei.access_ipr_program = 1
              and epei.is_archive = 0
            ORDER BY epei.name""", [self.id])
        else:
            cursor.execute("""SELECT epei.id as instituteid, epei.name, epeip.id,
            DATE_FORMAT(epeip.accreditation_finish, '%%d.%%m.%%Y'), IFNULL(epeip.is_archive,0), epeip.has_seminar
            FROM education_programmes_education_institute as epei
            LEFT JOIN (select * from education_programmes_education_institute_program where program_id = %s ) as epeip
            ON epei.id = epeip.institute_id
            where epei.is_archive = 0
            ORDER BY epei.name""", [self.id])

        result_of_select = cursor.fetchall()
        return result_of_select


    @property
    def level_name(self):
        return PROGRAM_LEVEL_DICT[self.level]

    def __unicode__(self):
        return self.name

    def set_archive(self):
        for education_institute_program in self.education_institute_program_set.all():
            education_institute_program.set_archive()
        self.is_archive = True
        self.save()

        return True

    def presentation_filename(self):
        last_slash = force_text(self.presentation).rfind('/')
        return force_text(self.presentation)[last_slash+1:]

    def presentation_url(self):
        media_slash = force_text(self.presentation.url).find(u'/media/')
        return 	(self.presentation.url)[media_slash:]

    class Meta:
        verbose_name = u'Образовательная программа'
        verbose_name_plural = u'Образовательные программы'
        ordering = ['name']


class Education_Institute_Program(models.Model):
    institute = models.ForeignKey(Education_Institute,blank=True,null=True, on_delete=models.SET_NULL)
    program = models.ForeignKey(Education_Programme,blank=True,null=True, on_delete=models.SET_NULL)
    accreditation_finish = models.DateTimeField(blank=True,null=True,verbose_name=u'Окончание акредитации')
    recruiting_date = models.DateTimeField(blank=True,null=True,verbose_name=u'Новый набор')
    cost        = models.DecimalField(max_digits=11, decimal_places=2,blank=True,null=True,verbose_name=u'Цена обучения')
    form        = models.ForeignKey(StudyMode,blank=True,null=True, verbose_name=u'Форма обучения')
    period      = models.CharField(max_length=30,blank=True,null=True, verbose_name=u'Периодичность занятий')
    has_seminar = models.BooleanField(default=False,verbose_name = u'Пользователь должен указать даты семинаров')
    is_archive  = models.BooleanField(default = False,verbose_name=u'Архивно')
  
    class Meta:
        verbose_name = u'Аккредитация организации на программу'
        verbose_name_plural = u'Аккредитация организаций на программы'
        ordering = ['institute','program']

    def __unicode__(self):
        if self.program is not None and self.institute is not None:
            return self.institute.name + u' - ' + self.program.name;
        else:
            return str(self.id)

    def set_archive(self):
#        Все наборы делаем архивными и объявления, с ними связанные тоже
        for recruitment in self.recruitment_set.all():
            recruitment.set_archive()

        self.is_archive = True
        self.save()

        return True

    def get_archive(self):
        self.is_archive = False
        self.save()

    def nearest_recruit(self):
        q = self.recruitment_set.filter(is_archive = False).order_by('-begin_date')
        if len(q) > 0:
            return q[0]
        else:
            return None		

#    @property
#    def accreditation_name(self):
#        return ACCREDITATION_DICT[self.accreditation]

class Group(models.Model):
    education_institute = models.ForeignKey(Education_Institute,blank=True,null=True,verbose_name=u'Образовательная организация', on_delete=models.SET_NULL)
    #    education_programm = models.ForeignKey(Education_Programme,blank=True,null=True,verbose_name=u'Программа обучения', on_delete=models.SET_NULL)
    level = models.CharField(max_length=1, choices=PROGRAM_LEVEL_CHOICES,default='B', verbose_name=u'Статус') #Уровень образования')
    name = models.TextField(blank=True,null=True,verbose_name=u'Номер группы')
    #    education_form = models.TextField(blank=True,null=True,verbose_name=u'Форма обучения')
    form = models.ForeignKey(StudyMode,null=True, verbose_name=u'Форма обучения')
    education_start = models.DateField(blank=True,null=True,verbose_name=u'Дата начала обучения') #input_formats=('%Y-%m-%d','%m/%d/%Y','%m/%d/%y','%d.%m.%Y','%d.%m.%y'))
    education_end = models.DateField(blank=True,null=True,verbose_name=u'Дата окончания обучения')
    timetable = models.FileField(max_length = 500,upload_to="/uploads/timetables",blank=True,null=True,verbose_name=u'Расписание')
    send_pnk_date = models.DateField(blank=True,null=True,verbose_name=u'Дата отправки личных дел в ПНК')
    get_pnk_date = models.DateField(blank=True,null=True,verbose_name=u'Дата получения личных дел ПНК')
    exam_date = models.DateField(blank=True,null=True,verbose_name=u'Дата квалификационного экзамена')
    make_documents_length = models.DateField(blank=True,null=True,verbose_name=u'Срок изготовления документов')
    send_documents_to_institute = models.DateField(blank=True,null=True,verbose_name=u'Дата отправки документов в образовательную организацию')
    give_documents_date = models.DateField(blank=True,null=True,verbose_name=u'Дата вручения документов')
    fact_get_documents = models.DateField(blank=True,null=True,verbose_name=u'Дата фактического получения документов')
    created = models.DateTimeField(auto_now=True)
    group_started = models.DateField(blank=True,null=True,verbose_name=u'Дата начала обучения группы')
    group_ended = models.DateField(blank=True,null=True,verbose_name=u'Дата окончания обучения группы')
    volume_academ_hours = models.IntegerField(blank=True,null=True,verbose_name=u'Объем ( ак. час. )')
    created = models.DateTimeField(auto_now_add=True,blank=True,null=True,verbose_name=u'Дата создания')
    is_finished = models.BooleanField(default = False,verbose_name=u'Обучение завершено')
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Группа'
        verbose_name_plural = u'Группы'

    @property
    def level_name(self):
        return PROGRAM_LEVEL_DICT[self.level]

    def set_archive(self):
        for student in self.studentgroup_set.all():
            student.set_archive()
        self.is_archive = True
        self.save()

    def set_finish(self):
        for studentgroup in self.studentgroup_set.all():
            if studentgroup.exam_result is not None and studentgroup.exam_result.is_finish == True:
                studentgroup.student.user.is_student = False
                studentgroup.student.user.is_ipr = True
                studentgroup.student.user.has_edu_request = False
                studentgroup.student.user.save()
                studentgroup.student.confirmed = True				
                studentgroup.student.save()
            studentgroup.set_archive()
        self.is_finished = True
        self.save()

    @property
    def status(self):

        if self.id is None:
            return 0

        if self.is_archive:
            return -2

        if self.is_finished:
            return -1
            #Обучение закончилось!!!

        now = date(datetime.now().year, datetime.now().month, datetime.now().day)
        if self.education_end is not None and now  > self.education_end:
            return -1
            #Обучение закончилось!!!

        if self.education_start is not None and self.education_end is not None and now >= self.education_start and now <= self.education_end:
            return 1
            #Обучение идет!!!

        if self.education_end is not None and now < self.education_end: #self.education_start:
            return 0
            #Обучения еще не было!!!
        return 0

    def timetable_filename(self):
        last_slash = force_text(self.timetable).rfind('/')
        return force_text(self.timetable)[last_slash+1:]

    def timetable_url(self):
        media_slash = force_text(self.timetable.url).find('/media/')
        return 	(self.timetable.url)[media_slash:]
        #media_slash = self.timetable.url.find('/media/')
        #return self.timetable.url[media_slash:]

    def disembody(self):
        for student in self.studentgroup_set.all():
            student.expulsion()
        self.delete()

    def can_be_finished(self):
        if self.is_finished:
            return False
        if self.is_archive:
            return False
        if  self.level == 'U':
            return True
        #elif self.studentgroup_set.filter(to_other_group = False,student__user__is_ipr = False).count() > 0:
        elif self.studentgroup_set.filter(exam_result__isnull = True).count() > 0:
            return False
        else:
            return True
        return False

    def can_be_archived(self):
        if self.is_archive:
            return False
        if self.is_finished:
            return True
        return False

class EducationRequest(models.Model):
    student = models.ForeignKey(Student,blank=True,null=True, on_delete=models.SET_NULL)
#    iprstudent = models.ForeignKey(IprStudent,blank=True,null=True, on_delete=models.SET_NULL,related_nema='iprstudent')
    institute_program = models.ForeignKey(Education_Institute_Program,blank=True,null=True, on_delete=models.SET_NULL)
    level             = models.CharField(max_length=1, choices=PROGRAM_LEVEL_CHOICES,default='B', verbose_name=u'Статус')

    practic_period = models.CharField(max_length=50, blank=True,null=True,verbose_name=u'Сроки проведения семинара')
    practic_begda = models.DateTimeField(blank=True,null=True,verbose_name=u'Дата начала семинаров')
    practic_endda = models.DateTimeField(blank=True,null=True,verbose_name=u'Дата окончания семинаров')
    payment_method = models.CharField(max_length=1, choices=PAYMENT_METHOD_CHOICES,default='F',blank=True,null=True,verbose_name=u'Способ оплаты')
    details = models.TextField(blank=True,null=True,verbose_name=u'Реквизиты')

    attestat_number = models.TextField(blank=True,null=True,verbose_name=u'Номер аттестата')
    attestat_date = models.DateField(blank=True,null=True,verbose_name=u'Дата получения аттестата')
    attestat_length = models.DateField(blank=True,null=True,verbose_name=u'Срок действия аттестата')

    date_request = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')

    is_frash = models.BooleanField(default = False,verbose_name=u'Свежий запрос на обучение')

    def __unicode__(self):
        return (self.student.user.first_name if self.student is not None and self.student.user is not None and self.student.user.first_name is not None else str(self.id)) # + u' ' + (self.institute_program.program.name if self.institute_program.program.name is not None else u'')
    
    def get_iprstudent(self):
        return IprStudent.objects.get(id=self.student.id)

    class Meta:
        verbose_name = u'Запрос на обучение'
        verbose_name_plural = u'Запросы на обучение'
        ordering = ['student','institute_program']

    def set_archive(self):
        if self.student is not None and self.student.user is not None:
            self.student.user.has_edu_request = False
            self.student.user.save()
        self.is_archive = True
        self.save()

    def institute(self):
        if self.institute_program is None:
            return u"-"
        else:
            return self.institute_program.institute.name

    def program(self):
        if self.institute_program is None:
            return u"-"
        else:
            return self.institute_program.program.name

    def delete(self):
        super(EducationRequest, self).delete()

    def can_be_deleted(self):
        return self.is_archive is False and self.group() is None

    def copy_attestat_data(self):

        if self.student is not None:
            nalog_consultant = self.get_iprstudent()
            self.attestat_date   = nalog_consultant.attestat_date
            self.attestat_length = nalog_consultant.attestat_length
            self.attestat_number = nalog_consultant.attestat_number


    def payment_method_name(self):
        if PAYMENT_METHOD_DICT.has_key(self.payment_method):
            return  PAYMENT_METHOD_DICT[self.payment_method]
        else:
            return u''

    def group(self):
        for studentgroup in self.studentgroup_set.filter(to_other_group = False):
            return studentgroup
        return None

    def pdf_title(self):
        filename = str(self.id) + "_"
        if self.student is not None:
            filename = filename + self.student.get_short_fio(True)
        filename = filename + '.pdf'
        return filename

    def pdf_path(self):
        return settings.MEDIA_ROOT + self.pdf_title()

    def pdf_content(self, refresh_file = False):
        if not os.path.exists(self.pdf_path()) or refresh_file:
            self.make_pdf_file()
        file = open(self.pdf_path(), "r")
        file.seek(0)
        pdf = file.read()
        file.close()
        return pdf

    def make_pdf_file(self):
        noon = timedelta(hours=12)
        c = canvas.Canvas(self.pdf_path())
        c.setTitle(self.pdf_title())
        #    print A4   596 x 842
        # русские шрифты
        pdfmetrics.registerFont(TTFont('Arial', settings.MEDIA_ROOT + 'fonts/arial.ttf'))
        pdfmetrics.registerFont(TTFont('Arialbd', settings.MEDIA_ROOT + 'fonts/arialbd.ttf'))
        c.setLineWidth(1)

        y1 = 720

        c.setFont('Arialbd', 10)
        #    @frame logo { left: 0pt; width: 800pt; top: 1pt; height: 120pt; }
        #    logo
        c.drawImage(settings.MEDIA_ROOT + "/img/pnk_logo.PNG",10,y1,width=580,height=100)

        y1 = y1 - 20 #700

        #    commission
        c.drawString(390,y1,u"В Единую аттестационную комиссию")

        c.setFont('Arialbd', 14)

        #   h1

        y1 = y1 -70 # 650
        if self.institute_program.program.level == 'U':
            c.drawCentredString(297,y1,u"Заявление о допуске к повышению квалификации")
        else:
            c.drawCentredString(297,y1,u"Заявление о допуске к подготовке по программе")
            y1 = y1 - 18 #630
            c.drawCentredString(297,y1,u"Палаты налоговых консультантов")

        c.setFont('Arial', 10)
        y1 = y1 -70 #560
        if self.institute_program.program.level == 'U':
            c.drawString(10,y1,u"Прошу допустить меня к повышению квалификации")
        else:
            c.drawString(10,y1,u"Прошу допустить меня к подготовке в")

        y1 = y1 - 20 #540
        if len(self.institute_program.institute.name) < 70:
            c.drawCentredString(297,y1,self.institute_program.institute.name)
        else:
            words = self.institute_program.institute.name.split(' ')
            begin_symb = 0
            iter_pos = 0
            string_len = 0

            for word in words:
                string_len = string_len + len(word) + 1
                iter_pos = iter_pos + len(word) + 1

                if abs(string_len - 70) <= 15:
                    c.drawCentredString(297,y1,self.institute_program.institute.name[begin_symb:begin_symb + string_len])
                    begin_symb = iter_pos
                    string_len = 0
                    if not abs(iter_pos - len(self.institute_program.institute.name)) <= 2:
                        y1 = y1 - 20

            if string_len > 0:
                c.drawCentredString(297,y1,self.institute_program.institute.name[begin_symb:begin_symb + string_len])
                #y1 = y1 - 20

        y1 = y1 - 3
        c.line(10,y1,585,y1)

        y1 = y1 - 20
        if self.institute_program.program.level == 'U':
            c.drawString(10,y1,u"в форме")
        else:
            c.drawString(10,y1,u"по программе дополнительного профессионального образования Палаты налоговых консультантов:")

        c.setFont('Arialbd', 10)
        y1 = y1 - 30
        c.drawString(10,y1,self.institute_program.program.name)

        c.setFont('Arial', 10)
        y1 = y1 - 40
        c.drawString(20,y1,u"О себе сообщаю следующие сведения:")

        y1 = y1 - 30
        c.drawString(20,y1,u"Ф.И.О.: " + self.student.get_fio)

        iprstudent = self.get_iprstudent()

        if self.institute_program.program.level == 'U':
            y1 = y1 - 20
            c.drawString(20,y1,u"Профессиональный статус: " + ( iprstudent.profstatus.title if iprstudent.profstatus is not None else u'' ))

            if self.attestat_number is not None and not self.attestat_number.isspace() :
                y1 = y1 - 20
                c.drawString(20,y1,u"Номер квалификационного аттестата: " + ( self.attestat_number if self.attestat_number is not None else u'' ))

                if self.attestat_date is not None:
                    y1 = y1 - 20
                    c.drawString(20,y1,u"Дата выдачи квалификационного аттестата: " + ( self.attestat_date.strftime('%d.%m.%Y') if self.attestat_date is not None else u""))

                if self.attestat_length is not None:
                    y1 = y1 - 20
                    c.drawString(20,y1,u"Срок действия квалификационного аттестата: " + ( self.attestat_length.strftime('%d.%m.%Y') if self.attestat_length is not None else u""))

            if self.institute_program.has_seminar:
                y1 = y1 - 20
                #c.drawString(20,y1,u"Дата начала семинаров : " + ( (edurequest.practic_begda + noon).strftime('%d.%m.%Y')  if edurequest.practic_begda is not None else u'' ))
                #y1 = y1 - 20
                #c.drawString(20,y1,u"Дата окончания семинаров : " + ( (edurequest.practic_endda + noon).strftime('%d.%m.%Y')  if edurequest.practic_endda is not None else u'' ))
                #y1 = y1 - 20
                c.drawString(20,y1,u"Сроки проведения семинара : " + ( self.practic_period   if self.practic_period is not None else u'' ))
                y1 = y1 - 20
                c.drawString(20,y1,u"Метод оплаты : " + self.payment_method_name())
                if self.payment_method == 'U':
                    if len(self.details) < 70:
                        y1 = y1 - 20
                        c.drawString(20,y1,u"Реквизиты : " + ( self.details  if self.details is not None else u'' ))
                    else:
                        words = self.details.split(' ')
                        begin_symb = 0
                        iter_pos = 0
                        string_len = 0

                        for word in words:
                            string_len = string_len + len(word) + 1
                            iter_pos = iter_pos + len(word) + 1

                            if abs(string_len - 70) <= 7:
                                y1 = y1 - 20
                                if begin_symb == 0:
                                    c.drawString(20,y1,u"Реквизиты: " + self.details[begin_symb:string_len])
                                else:
                                    c.drawString(77,y1,self.details[begin_symb:begin_symb + string_len])
                                begin_symb = iter_pos
                                string_len = 0

                        if string_len > 0:
                            y1 = y1 - 20
                            if begin_symb == 0:
                                c.drawString(20,y1,u"Реквизиты: " + self.details[begin_symb:string_len])
                            else:
                                c.drawString(77,y1,self.details[begin_symb:begin_symb + string_len])

#            lv_passport_date =  ( iprstudent.passport_seria if iprstudent.passport_seria is not None else u"") + u" " + (iprstudent.passport_number if iprstudent.passport_number is not None else u"") + u", выдан  " + ( iprstudent.passport_who_gave if iprstudent.passport_who_gave is not None else u"") + u" " + (iprstudent.passport_when_gave.strftime('%d.%m.%Y') if iprstudent.passport_when_gave is not None else u"") +  u" код подразделения " + ( iprstudent.passport_code if iprstudent.passport_code is not None else u"")

#            if not lv_passport_date.isspace() and len(lv_passport_date) < 70:
#                y1 = y1 - 20
#                c.drawString(20,y1,u"Паспорт: " + lv_passport_date)
#            elif not lv_passport_date.isspace():
#                words = lv_passport_date.split(' ')
#                begin_symb = 0
#                iter_pos = 0
#                string_len = 0
#
#                for word in words:
#                    string_len = string_len + len(word) + 1
#                    iter_pos = iter_pos + len(word) + 1
#
#                    if abs(string_len - 70) <= 5:
#                        y1 = y1 - 20
#                        if begin_symb == 0:
#                            c.drawString(20,y1,u"Паспорт: " + lv_passport_date[begin_symb:string_len])
#                        else:
#                            c.drawString(62,y1,lv_passport_date[begin_symb:begin_symb + string_len])
#                        begin_symb = iter_pos
#                        string_len = 0
#
#                if string_len > 0:
#                    y1 = y1 - 20
#                    if begin_symb == 0:
#                        c.drawString(20,y1,u"Паспорт: " + lv_passport_date[begin_symb:string_len])
#                    else:
#                        c.drawString(62,y1,lv_passport_date[begin_symb:begin_symb + string_len])

#        elif iprstudent.birthday is not None:
#            y1 = y1 - 20
#            c.drawString(20,y1,u"Дата рождения: " + ( iprstudent.birthday.strftime('%d.%m.%Y') if iprstudent.birthday is not None else u""))

        y1 = y1 - 20
        if len(iprstudent.postaddress) < 70:
            c.drawString(20,y1,u"Регион и город проживания: " + ( iprstudent.postaddress if iprstudent.postaddress is not None else u""))
            y1 = y1 - 20
        else:
            words = iprstudent.postaddress.split(' ')
            begin_symb = 0
            iter_pos = 0
            string_len = 0

            for word in words:
                string_len = string_len + len(word) + 1
                iter_pos = iter_pos + len(word) + 1

                if abs(string_len - 70) <= 5:
                    if begin_symb == 0:
                        c.drawString(20,y1,u"Регион и город проживания: " + iprstudent.postaddress[begin_symb:string_len])
                    else:
                        c.drawString(157,y1,iprstudent.postaddress[begin_symb:begin_symb + string_len])
                    begin_symb = iter_pos
                    string_len = 0

                    y1 = y1 - 20

            if string_len > 0:
                if begin_symb == 0:
                    c.drawString(20,y1,u"Регион и город проживания: " + iprstudent.postaddress[begin_symb:string_len])
                else:
                    c.drawString(157,y1,iprstudent.postaddress[begin_symb:begin_symb + string_len])
                y1 = y1 - 20

        c.drawString(20,y1,u"Телефон: " + ( iprstudent.telephone if iprstudent.telephone is not None else u""))
        y1 = y1 - 20
        c.drawString(20,y1,u"email: " + (iprstudent.user.email if iprstudent.user.email is not None else u""))

        if self.institute_program.program.level == 'U':
            y1 = y1 - 20
            c.drawString(20,y1,u"Место работы и должность в настоящее время:  " + (iprstudent.job_place if iprstudent.job_place is not None else u"")  + ((u", " + iprstudent.position) if iprstudent.position is not None else u""))
        else:
            y1 = y1 - 20
            c.drawString(20,y1,u"Образование:  " + (iprstudent.education.title if iprstudent.education is not None else u""))
            y1 = y1 - 20
            c.drawString(20,y1,u"Общий подтвержденный стаж работы в области экономики и/или права, лет : " + ( str(iprstudent.length_of_work) if iprstudent.length_of_work is not None else u""))


        c.setFont('Arial', 7)
        yt = y1
        yt = yt - 45
        c.drawString(45,yt,u"Даю  согласие на обработку Палатой налоговых консультантов (г. Москва, ул.  Ярославская,  д.  8, корп. 4) моих персональных данных, указанных")
        yt = yt - 7
        c.drawString(45,yt,u"при   регистрации   в   Информационно-поисковом   ресурсе   налогового консультанта  на  сайте  www.ipr.palata-nk.ru на неограниченный срок с")
        yt = yt - 7
        c.drawString(45,yt,u"целью   организации   моей   подготовки  (повышения  квалификации)  по программам  Палаты  налоговых  консультантов.  Обработка  персональных")
        yt = yt - 7
        c.drawString(45,yt,u"данных  включает сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, уничтожение.")

        y1 = y1 - 60
        c.rect(20,y1,20,20,1,0)

        # Место для подписи
        yt = yt - 30
        c.line(300,yt,400,yt)

        yt2 = yt + 10
        c.line(401,yt,405,yt2)

        c.setFont('Arial', 10)
        l_sign_y = yt + 3
        c.drawString(407,l_sign_y,self.student.get_short_fio())
        c.line(406,yt,556,yt)

        c.setFont('Arial', 7)
        yt = yt - 30
        c.drawString(400,yt,u"Дата подачи заявления " + ((self.date_request + noon).strftime('%d.%m.%Y') if self.date_request is not None else u" - " ))


        c.setLineWidth(2)
        c.line(25,y1+15,30,y1 + 5)
        c.line(30,y1+ 5,40,y1 + 25)

        c.showPage()
        c.save()

#@receiver(post_save, sender = EducationRequest)
#def send_mail_to_curator(instance,created, **kwargs):
#    print 'send_mail_to_curator'
#    print created
#    if created:
#        try:
#            print instance.institute_program.institute
#            print instance.institute_program.program
#            print instance.student.user
#            print instance
#            new_edurequest_mail(instance)
#        except :
#            print str(sys.exc_info())

class StudentGroup(models.Model):
    student = models.ForeignKey(Student,blank=True,null=True, on_delete=models.SET_NULL)
#    iprstudent = models.ForeignKey(IprStudent,blank=True,null=True, on_delete=models.SET_NULL,related_name='iprstudent')
    request = models.ForeignKey(EducationRequest,blank=True,null=True, on_delete=models.SET_NULL)
    group = models.ForeignKey(Group)
    exam_date = models.DateField(blank=True,null=True,verbose_name=u'Дата квалификационного экзамена')
    exam_result = models.ForeignKey(ExamResult,null= True,verbose_name=u'Результат квалификационного экзамена') #IntegerField(blank=True,null=True,verbose_name=u'Результат квалификационного экзамена')

    from_other_group = models.BooleanField(default=False,verbose_name=u'Перевелся из другой группы')
    from_group = models.ForeignKey(Group,related_name=u'come',blank=True,null=True,verbose_name =u'Откуда')

    to_other_group = models.BooleanField(default=False,verbose_name=u'Перевелся в другую группу')
    to_group = models.ForeignKey(Group,related_name=u'go',blank=True,null=True,verbose_name =u'Куда')

    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')
    created = models.DateTimeField(auto_now_add=True,blank=True,null=True,verbose_name=u'Дата создания')

    def __unicode__(self):
        if self.student is not None and self.student.user is not None and self.group is not None:
            return u"%s %s" % (self.student.user.email,self.group.name)
        else:
            return str(self.id)

    class Meta:
        verbose_name = u'Студент группы'
        verbose_name_plural = u'Студенты групп'

    def get_iprstudent(self):
        return IprStudent.objects.get(id=self.student.id)

    def can_become_ipr(self):
        iprstudent = self.get_iprstudent()

        if iprstudent is not None:

            if iprstudent.attestat_length is not None and len(iprstudent.attestat_number) > 0 and iprstudent.attestat_date is not None and len(iprstudent.membership_number) > 0:
                return True
            return False
        else:
            return False
        return False

    def delete(self):
        try:
            if self.request is not None:
                self.request.delete()
            super(StudentGroup,self).delete()
        except:
            print str(sys.exc_info())

     # отчисление из группы, без удаления запроса на обучение
    def expulsion(self):
        super(StudentGroup,self).delete()

    def set_archive(self):
        self.request.set_archive()
        self.is_archive = True
        self.save()

    def institute(self):
        if self.request is None:
            return u"-"
        else:
            return self.request.institute()

    def program(self):
        if self.request is None:
            return u"-"
        else:
            return self.request.program()

#    перевод в другую группу
    def shift_group(self, to_group):
        if self.exam_result is not None and self.exam_result.is_finish == True:
            return 0
        elif self.exam_result is None:
            return 0
        else:
            try:
                new_request = EducationRequest()
                new_request.student = self.student

                self.student.user.has_edu_request = True
                self.student.user.save()

                new_request.institute_program = Education_Institute_Program.objects.get(institute = self.group.education_institute,program = self.request.institute_program.program,program__level=self.group.level)
                new_request.save()

                new_student_group = StudentGroup()
                new_student_group.student = self.student
                new_student_group.request = new_request
                new_student_group.group = to_group
                new_student_group.from_other_group = True
                new_student_group.from_group = self.group
                new_student_group.save()

                self.to_other_group = True
                self.to_group = to_group

                if self.request.is_archive == False:
                    self.request.set_archive()
                return 1
            except:
                print str(sys.exc_info())

                return -1



class Sticker(models.Model):
    title = models.CharField(max_length = 150,blank=True,null=True,verbose_name = u'Заголовок объявления')
    content = models.TextField(verbose_name = u'Текст')
    creator = models.ForeignKey(User,verbose_name = u'Создатель')
    type = models.CharField(max_length=1, choices = STICKER_TYPE_CHOICES,default='A', verbose_name=u'Тип объявления')
    institute = models.ForeignKey(Education_Institute,blank=True,null=True)
    group = models.ForeignKey(Group,blank=True,null=True)
    is_active = models.BooleanField(default= False,verbose_name=u'Активно')
    active_from = models.DateField(blank=True,null=True,verbose_name=u'Активно с')
    active_till = models.DateField(blank=True,null=True,verbose_name=u'Активно по')
    is_archive = models.BooleanField(default = False,verbose_name=u'Архивно')
    about_recruit = models.BooleanField(default = False,verbose_name=u'О наборе')
    created = models.DateTimeField(auto_now_add=True,blank=True,null=True,verbose_name=u'Дата создания')
 
    def __unicode__(self):
        return self.content

    class Meta:
        verbose_name = u'Объявление'
        verbose_name_plural = u'Объявления'

    def set_archive(self):
        self.is_archive = True
        self.save()


class Recruitment(models.Model):
    institute_program   = models.ForeignKey(Education_Institute_Program,null=True,verbose_name=u'Указать программу')
    price               = models.DecimalField(max_digits=11, decimal_places=2,blank=True,null=True,verbose_name=u'Стоимость обучения, руб')
    payment_type        = models.ForeignKey(PaymentType,blank=True,null=True,on_delete=models.SET_NULL,verbose_name = u'Тип оплаты')
    currency            = models.ForeignKey(Currency,blank=True,null=True,on_delete=models.SET_NULL,verbose_name = u'Валюта')
    begin_date          = models.DateField(blank=True,null=True,verbose_name=u'Дата начала занятий')
    form                = models.ForeignKey(StudyMode,blank=True,null=True, verbose_name=u'Форма обучения')
#    periodicity = models.ForeignKey(Periodicity,null=True,blank=True,verbose_name=u'Периодичность занятий')
    periodicity         = models.CharField(max_length=100,blank=True,null=True, verbose_name=u'Периодичность занятий')
    sticker             = models.ForeignKey(Sticker,null=True,blank=True,verbose_name=u'Сгенерированное объявление')
    volume_academ_hours = models.IntegerField(blank=True,null=True,verbose_name=u'Объем ( ак. час. )')
    is_archive          = models.BooleanField(default = False,verbose_name=u'Архивно')
    created             = models.DateTimeField(auto_now_add=True,blank=True,null=True,verbose_name=u'Дата создания')

    program             = models.FileField(max_length=500,upload_to="/uploads/recruits",blank=True,null=True,verbose_name=u'Программа')

    contract_with_pnk   = models.BooleanField(default = False,verbose_name=u'Слушатель заключает договор с образовательной организацией и ПНК')
    topic               = models.CharField(max_length = 150,blank=True,null=True,verbose_name = u'Тема курса')


    def __unicode__(self):
        noon = timedelta(hours=12)

        return ( (self.begin_date + noon).strftime('%d.%m.%Y') if self.begin_date is not None else u"-" )

    def is_possible_submit_application(self):
        if self.institute_program is not None and self.institute_program.is_archive == False:
            if self.institute_program.program is not None and self.institute_program.program.is_archive == False:
                if self.institute_program.institute is not None and self.institute_program.institute.is_archive == False:
                    return True
        return False


    class Meta:
        verbose_name = u'Подать объявление о наборе группы'
        verbose_name_plural = u'Наборы в группы'

    def set_archive(self):
        self.is_archive = True
        if self.sticker is not None:
            self.sticker.set_archive()
        self.save()

    def program_filename(self):
        last_slash = force_text(self.program).rfind('/')
        return force_text(self.program)[last_slash+1:]

    def program_url(self):
        try:
            media_slash = force_text(self.program.url).find('/media/')
            return 	(self.program.url)[media_slash:]
        except:
            return ""

class News(models.Model):
    title = models.CharField(max_length=50,blank=False,null=False, verbose_name=u'Заголовок')
    slug = models.SlugField()
    main_banner =  models.BooleanField(default = False,verbose_name=u'Главный баннер')
    simple_banner =  models.BooleanField(default = False,verbose_name=u'Простой баннер')
    preview = models.CharField(max_length=400,blank=True,null=True, verbose_name=u'Превью')
    text = models.TextField(blank=True,null=True,verbose_name = u'Текст')
    image = models.ImageField(max_length = 500,upload_to="/uploads/news",blank=True,null=True,verbose_name=u'Картинка')
    link = models.URLField(blank=True,null=True,verbose_name = u'Ссылка на другой ресурс')
    is_archive = models.BooleanField(default = False,verbose_name=u'В архиве')
    is_static = models.BooleanField(default = False,verbose_name=u'Статическая страница')
    is_attic = models.BooleanField(default = False,verbose_name=u'Ссылка на чердаке')
    is_cellar = models.BooleanField(default = False,verbose_name=u'Ссылка в подвале')
    created = models.DateTimeField(auto_now_add=True,blank=True,null=True,verbose_name=u'Создано')
    order = models.IntegerField(blank=True,null=True,verbose_name=u'Порядковый номер')

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return self.title

    def url(self):
        if self.link is not None and len(self.link) > 0:
            return self.link
        return u'/edu/news/' + self.slug + u'/'

    def image_url(self):
        media_slash = force_text(self.image.url).find('/media/')
        return 	(self.image.url)[media_slash:]
        #media_slash = self.image.url.find('/media/')
        #return self.image.url[media_slash:]

    def set_archive(self):
        self.is_archive = True
        self.save()

class MenuItem(models.Model):
    title = models.CharField(max_length=50,blank=False,null=False, verbose_name=u'Название')
    link = models.URLField(blank=False,null=True,verbose_name = u'Ссылка')
    order = models.IntegerField(blank=True,null=True,verbose_name=u'Порядковый номер')
    picture = models.CharField(blank=True, null=True, max_length = 50, verbose_name =u'Иконка')
    is_archive = models.BooleanField(default = False,verbose_name=u'В архиве')


    class Meta:
        verbose_name = u'Пункт меню'
        verbose_name_plural = u'Пункты меню'
        ordering = ['order']

    def __unicode__(self):
        return self.title

    def set_archive(self):
        self.is_archive = True
        self.save()