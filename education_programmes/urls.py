# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.conf import settings

urlpatterns = patterns('',
    url(r'^delete/(?P<_type>region|city|institute|program|group|recruitment|request|profstatus|result|mode|menuitem)/(?P<id>\d+)/?$','education_programmes.views.item_delete', name='edu-item-del'),

#    url(r'^(?P<_type>region|city|curatorassign)/(?P<id>\d+)/?$','education_programmes.views.item', name='edu-item'),
    url(r'^(?P<_type>region|city)/(?P<id>\d+)/?$','education_programmes.views.item', name='edu-item'),
    url(r'^(?P<_type>region|city)/?$','education_programmes.views.item', name='edu-item-new'),

    url(r'^groups/?$','education_programmes.views.groups', name='groups'),
    url(r'^groups/new/?$','education_programmes.views.new_group', name='groups_new'),

    url(r'^group/disembody/(?P<id>\d+)/?$','education_programmes.views.group_disembody', name='group-disembody'),
    url(r'^group/finish/(?P<id>\d+)/?$','education_programmes.views.group_finish', name='group-finish'),
    url(r'^group/view/(?P<id>\d+)/?$','education_programmes.views.group_view', name='group-view'),
    url(r'^group/(?P<id>\d+)/?$','education_programmes.views.group', name='edu-group'),
    url(r'^group/?$','education_programmes.views.group', name='group-new'),

    url(r'^news/(?P<slug>[\w-]+)/?$','education_programmes.views.news',{"_type":"news"}, name='edu-news'),
    url(r'^news/?$','education_programmes.views.news',{"_type":"news"}, name='news-new'),
    url(r'^delete/news/(?P<slug>[\w-]+)/?$','education_programmes.views.news_delete', name='edu-news-del'),
    
    url(r'^staticpage/(?P<slug>[\w-]+)/?$','education_programmes.views.news',{"_type":"staticpage"}, name='edu-staticpage'),
    url(r'^staticpage/?$','education_programmes.views.news',{"_type":"staticpage"}, name='staticpage-new'),
    url(r'^delete/staticpage/(?P<slug>[\w-]+)/?$','education_programmes.views.news_delete', name='edu-news-del'),

    url(r'^program/(?P<id>\d+)/?$','education_programmes.views.program', name='edu-program'),
    url(r'^program/(?P<id>\d+)/present/url/?$','education_programmes.views.program_present_url', name='edu-program-present-url'),
    url(r'^program/(?P<id>\d+)/present/name/?$','education_programmes.views.program_present_name', name='edu-program-present-name'),
    url(r'^program/?$','education_programmes.views.program', name='program-new'),


#    url(r'^institute/(?P<id>\d+)/group/?$','education_programmes.views.institute_group', name='edu-institute-group'),
    url(r'^institute/(?P<institute_id>\d+)/recruitment/?$','education_programmes.views.institute_recruitment', name='edu-institute-recruitment'),
    url(r'^institute/(?P<id>\d+)/?$','education_programmes.views.institute', name='edu-institute'),
    url(r'^institute/?$','education_programmes.views.institute', name='institute-new'),

    url(r'^realize_program/(?P<id>\d+)/?$','education_programmes.views.realize_program', name='edu-realize-program'),

    url(r'^programs/?$','education_programmes.views.program_catalog', name='program_catalog'),
    
    url(r'^ajax/(?P<_type>region|city|institute|program|level|group|request|years)/?$','education_programmes.views.item_ajax', name='edu-item-ajax'),

    url(r'^ajax/programs/?$','education_programmes.views.program_id', name='program_id_all'),
    url(r'^ajax/programs/(\d+)/?$','education_programmes.views.program_id', name='program_id_region'),
    url(r'^ajax/programs/(\d+)/(\d+)/?$','education_programmes.views.program_id', name='program_id_city'),
    url(r'^ajax/programs/(\d+)/(\d+)/(\d+)/?$','education_programmes.views.program_id', name='program_id_institute'),

    url(r'^ajax/organizations/(\d+)/?$','education_programmes.views.organizations_id', name='organizations_id_region'),
    url(r'^ajax/organizations/(\d+)/(\d+)/?$','education_programmes.views.organizations_id', name='organizations_id_city'),

    url(r'^organizations/(?P<region_slug>[\w-]+)/?$','education_programmes.views.organizations', name='organizations_region'),
    url(r'^organizations/(?P<region_slug>[\w-]+)/(?P<city_slug>[\w-]+)/?$','education_programmes.views.organizations', name='organizations_city'),
    url(r'^organizations/?$','education_programmes.views.organizations', name='organizations'),

    url(r'^region/(?P<raphael_slug>\w+)/?$','education_programmes.views.region', name='region'),
    url(r'^no_confirmed/?$','education_programmes.views.no_confirmed', name='no_confirmed'),
    
    url(r'^recruitment/?$','education_programmes.views.recruitment', name='recruitment-new'),
    url(r'^recruitment/(\d+)/?$','education_programmes.views.recruitment', name='recruitment-edit'),
    url(r'^recruitment/delete/(\d+)/?$','education_programmes.views.recruitment_delete', name='recruitment-delete'),
    url(r'^recruitments/?$','education_programmes.views.recruits', name='recruitments'),
    
    url(r'^request/(\d+)/?$','education_programmes.views.request_form', name='request-form'),
    url(r'^request/pdf/(\d+)/?$','education_programmes.views.request_pdf', name='request-pdf'),

    url(r'^sticker/generate/(\d+)/?$','education_programmes.views.sticker_generate', name='sticker-generate'),

    url(r'^ajax/consult/confirm/?$','education_programmes.views.consult_confirm', name='ajax_consult_confirm'),
    url(r'^ajax/consult/reject/?$','education_programmes.views.consult_reject', name='ajax_consult_reject'),
    
    url(r'^search/?$','education_programmes.views.search', name='search'),

    url(r'^person/edit/([-\w]+)/?$','education_programmes.views.person_edit', name='person-edit'),
    url(r'^person/request/([-\w]+)/?$','education_programmes.views.person_request', name='person-request'),
    url(r'^person/delete/([-\w]+)/?$','education_programmes.views.person_delete', name='person-delete'),
    url(r'^person/membership/([-\w]+)/?$','education_programmes.views.person_membership', name='person_membership-new'),
    url(r'^person/membership/delete/([-\w]+)/?$','education_programmes.views.person_membership_delete', name='person_membership-delete'),

    url(r'^admin/?$',  'education_programmes.views.administration', name='eduadmin'),
    url(r'^admin/others/(?P<_type>profstatus|period|mode|result|menuitem)/?$','education_programmes.views.administration_item', name='eduadmin-new'),
    url(r'^admin/others/(?P<_type>profstatus|period|mode|result|menuitem)/(?P<id>\d+)/?$','education_programmes.views.administration_item', name='eduadmin-edit'),
    url(r'^admin/others/email_template/(?P<template_name>[\w\.]+)/?$','education_programmes.views.administration_email_template', name='email-template-edit'),
    url(r'^admin/others/email_template/(?P<template_name>[\w\.]+)/preview/?$','education_programmes.views.administration_email_template_preview', name='email-template-preview'),

    url(r'^admin/others/common_texts/(?P<slug>[-\w\.]+)/?$','education_programmes.views.administration_common_text', name='common-text-edit'),
    url(r'^admin/others/common_texts/preview/(?P<slug>[-\w\.]+)/?$','education_programmes.views.administration_common_text_preview', name='common-text-preview'),

    url(r'^memberships/entrance/?$','education_programmes.views.memberships_entrance', name='memberships-entrance'),
    url(r'^memberships/unload/?$','education_programmes.views.memberships_unload', name='memberships-unload'),

    url(r'^membershipcost/?$','education_programmes.views.membershipcost_region', name='membershipcost-region-choice'),
    url(r'^membershipcost/region/(\d+)/?$','education_programmes.views.membershipcost', name='membershipcost-create'),
    url(r'^membershipcost/(\d+)/?$','education_programmes.views.membershipcost_edit', name='membershipcost-edit'),
    url(r'^membershipcost/delete/(\d+)/?$','education_programmes.views.membershipcost_delete', name='membershipcost-delete'),

    url(r'^membershipbenefit/?$','education_programmes.views.membershipbenefit_edit', name='membershipbenefit-new'),
    url(r'^membershipbenefit/(\d+)/?$','education_programmes.views.membershipbenefit_edit', name='membershipbenefit-edit'),
    url(r'^membershipbenefit/delete/(\d+)/?$','education_programmes.views.membershipbenefit_delete', name='membershipbenefit-delete'),

)
