# -*- coding: utf-8 -*-
# System libraries
import sys
from datetime import datetime

# Third-party libraries

# Django modules
from django import forms
from django.core import validators
from django.db.models import Q,Avg, Max, Min
from django.forms import ModelForm, Textarea, TextInput
from django.forms.models import BaseInlineFormSet, inlineformset_factory
from django.forms.extras import *

# Django apps

# Current-app modules
from models import *
from common_user.models import User
from IPR.mail import *
from IPR.widgets import *

# aahripunov begin
class CityForm(ModelForm):
    class Meta:
        model = City
        fields = ['name','region']


class RecruitmentForm(ModelForm):
    class Meta:
        model = Recruitment
        fields = ['institute_program','topic','price','begin_date','form','periodicity','volume_academ_hours','program']
        widgets = {
            'program': MyClearableFileInput(),
            'contract_with_pnk' : CheckboxIPRStyle(),
            }

    def __init__(self,user, *args, **kwargs):
        super(RecruitmentForm, self).__init__(*args, **kwargs)
        if user.is_curator:
            self.fields['institute_program'].queryset = Education_Institute_Program.objects.filter(institute__curator=user,is_archive = False)
        self.fields['begin_date'].required = True
        self.fields['form'].required = True
        self.fields['price'].required = True
        self.fields['periodicity'].required = True

    def clean(self):
        now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)

        if self.cleaned_data.has_key("begin_date") and self.cleaned_data.get("begin_date") < now.date():
            self._errors["begin_date"] = self.error_class([u'Нельзя создать объявление о наборе из прошлого'])
#            raise forms.ValidationError(u"Нельзя создать объявление о наборе из прошлого")

        return self.cleaned_data


class RecruitmentCreationForm(ModelForm):
    class Meta:
        model = Recruitment
        fields = ['institute_program','topic','price','begin_date','form','periodicity','volume_academ_hours','program']

        widgets = {
            'program': MyClearableFileInput(),
            'contract_with_pnk' : CheckboxIPRStyle(),
            }

    def __init__(self,user, *args, **kwargs):
        super(RecruitmentCreationForm, self).__init__(*args, **kwargs)
        if user.is_curator:
            self.fields['institute_program'].queryset = Education_Institute_Program.objects.filter(institute__curator=user,is_archive = False)

        self.fields['begin_date'].required = True
        self.fields['form'].required = True
        self.fields['price'].required = True
        self.fields['periodicity'].required = True

    def clean(self):
        now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)

        if self.cleaned_data.has_key("begin_date") and self.cleaned_data.get("begin_date") < now.date():
            self._errors["begin_date"] = self.error_class([u'Нельзя создать объявление о наборе из прошлого'])
        #            raise forms.ValidationError(u"Нельзя создать объявление о наборе из прошлого")

        return self.cleaned_data


class RecruitmentSuperUserForm(ModelForm):
    education_institute = forms.ModelChoiceField(label=u'Образовательная организация',queryset = Education_Institute.objects.filter(is_archive=False,education_institute_program__program__isnull=False).distinct())
    education_programm = forms.ModelChoiceField(label=u'Образовательная программа',queryset=Education_Programme.objects.filter(is_archive=False,education_institute_program__institute__isnull=False).distinct())

    class Meta:
        model = Recruitment
        fields = ['education_institute','education_programm','topic','price','begin_date','form','periodicity','volume_academ_hours','program','contract_with_pnk']

        widgets = {
            'program': MyClearableFileInput(),
            'contract_with_pnk' : CheckboxIPRStyle(),
            }

    def __init__(self,*args, **kwargs):
        super(RecruitmentSuperUserForm, self).__init__(*args, **kwargs)

        if kwargs.has_key('initial'):
            if self.initial.has_key('edu_institute'):
                self.fields['education_institute'].initial = self.initial['edu_institute']
                self.fields['education_institute'].widget = SelectInInput()

            if self.initial.has_key('edu_programm'):
                self.fields['education_programm'].initial = self.initial['edu_programm']
                self.fields['education_programm'].widget = SelectInInput()
            elif self.initial.has_key('edu_institute'):
                self.fields['institute_program'].queryset = Education_Institute_Program.objects.filter(institute=self.initial['edu_institute'],is_archive = False)

        self.fields['begin_date'].required = True
        self.fields['form'].required = True
        self.fields['price'].required = True
        self.fields['periodicity'].required = True

    def clean(self):
        return self.cleaned_data


class RecruitmentShortForm(ModelForm):
    class Meta:
        model = Recruitment
        fields = ['topic','price','payment_type','currency','begin_date','form','periodicity','volume_academ_hours','program']

        widgets = {
            'contract_with_pnk' : CheckboxIPRStyle(),
            }

    def __init__(self,user, *args, **kwargs):
        super(RecruitmentShortForm, self).__init__(*args, **kwargs)
        self.fields['price'].required = True
        self.fields['begin_date'].required = True
        self.fields['form'].required = True
    #        self.fields['periodicity'].required = True

    def clean(self):
        return self.cleaned_data

class RecruitmentBriefForm(ModelForm):
    class Meta:
        model = Recruitment
        fields = ['topic','price','payment_type','currency','begin_date','form','periodicity','volume_academ_hours','program']

    def __init__(self,*args, **kwargs):
        super(RecruitmentBriefForm, self).__init__(*args, **kwargs)
        self.fields['price'].required = True
        self.fields['begin_date'].required = True
        self.fields['form'].required = True

    def clean(self):
        return self.cleaned_data

class StickerFullForm(ModelForm):
    class Meta:
        model = Sticker
        fields = ['title','content','type','institute','group','is_active','active_from','active_till']
    
    def __init__(self, *args, **kwargs):
        super(StickerFullForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = True
        self.fields['content'].required = True
        self.fields['active_from'].required = True
        self.fields['active_till'].required = True
        
class StickerInstituteForm(ModelForm):
    class Meta:
        model = Sticker
        fields = ['title','content','type','institute','is_active','active_from','active_till']
    
    def __init__(self,user, *args, **kwargs):
        super(StickerInstituteForm, self).__init__(*args, **kwargs)
        if user.is_curator:
            self.fields['institute'].queryset = Education_Institute.objects.filter(curator=user)
            #self.fields['group'].queryset = Group.objects.filter(education_institute__curator=user)
        self.fields['title'].required = True
        self.fields['content'].required = True
        self.fields['active_from'].required = True
        self.fields['active_till'].required = True

class StickerGroupForm(ModelForm):
    class Meta:
        model = Sticker
        fields = ['title','content','type','institute','group','is_active','active_from','active_till']

    def __init__(self,user, *args, **kwargs):
        super(StickerGroupForm, self).__init__(*args, **kwargs)
        print user
        if user.is_curator:
            self.fields['institute'].queryset = Education_Institute.objects.filter(curator=user)
            self.fields['group'].queryset = Group.objects.filter(education_institute__curator=user)
        self.fields['title'].required = True
        self.fields['content'].required = True
        self.fields['active_from'].required = True
        self.fields['active_till'].required = True

class StickerRecruitmentForm(ModelForm):
    class Meta:
        model = Sticker
        fields = ['title','content','is_active','active_from','active_till']
    
    def __init__(self, *args, **kwargs):
        super(StickerRecruitmentForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = True
        self.fields['content'].required = True
#        self.fields['active'].required = True
#        self.fields['active_from'].required = True
#        self.fields['active_till'].required = True

class AdministrationForm(ModelForm):
    class Meta:
        model = StudyMode
        fields = ['title']

class MenuItemForm(ModelForm):
    class Meta:
        model = MenuItem
        fields = ['title','link','order','picture','is_archive']

        widgets = {
            'picture': PictureInput(),
            'is_archive': CheckboxIPRStyle()
        }


class CurrencyForm(ModelForm):
    class Meta:
        model = Currency
        fields = ['title','short_title','symbol']

class RegionForm(ModelForm):
    class Meta:
        model = Region
        fields = ['name','raphael_path','raphael_slug','raphael_id']

class RealizeProgram(ModelForm):
    class Meta:
        model = Education_Institute_Program
        fields = ['program','cost','form','period']
		
class Education_InstituteForm(ModelForm):
    class Meta:
        model = Education_Institute
        fields = ['name','accreditation_number','accreditation','web_site','telephone','post_address','city','email','director','access_ipr_program','certificate','certificate2']
        widgets  = {
            'web_site': forms.URLInput(attrs = {'pattern':".+"}),
            'access_ipr_program' : CheckboxIPRStyle(),
            'certificate': MyClearableFileInput(),
            'certificate2': MyClearableFileInput()
        }


    def __init__(self, *args, **kwargs):
        super(Education_InstituteForm, self).__init__(*args, **kwargs)
        if kwargs.has_key('instance'):
            try:
                if kwargs['instance'].curator is None:
                    self.fields['password1'] =  forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
                    self.fields['password2'] =  forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
                    self.fields['fio'] =  forms.CharField(max_length=50,label=u'Менеджер')
                else:
                    self.fields['fio'] =  forms.CharField(initial=kwargs['instance'].fio,max_length=50,label=u'Менеджер')
                    self.fields['fio'].initial =  kwargs['instance'].curator.last_name + u' ' + kwargs['instance'].curator.first_name

                    if kwargs['instance'].curator.email !=  kwargs['instance'].email:
                        self.fields['password1'] =  forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
                        self.fields['password2'] =  forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
            except :
                print str(sys.exc_info())
                None

    def clean(self):

        if self.cleaned_data.has_key("password1") and len(self.cleaned_data.get("password1", "").strip()) == 0:
            self._errors["password1"] = self.error_class([u'Пароль пустой'])
            raise forms.ValidationError(u"Пароль пустой")


        if self.cleaned_data.has_key("password1") and (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password1"] = self.error_class([u'Пароли не совпадают.'])
            raise forms.ValidationError(u"Пароли не совпадают.")

        if self.cleaned_data.has_key("password1"):
            try:
                email_exist = User.objects.get(email=self.cleaned_data["email"])
                inst = Education_Institute.objects.filter(curator = email_exist)
                if len(inst) == 0:
                    self._errors["email"] = self.error_class([u'Такой e-mail является логином для ранее зарегистрированного пользователя'])
                    raise forms.ValidationError(u"Такой e-mail является логином для ранее зарегистрированного пользователя")
                else:
                    self._errors["email"] = self.error_class([u'Такой e-mail привязан к другой образовательной организации'])
                    raise forms.ValidationError(u"Такой e-mail привязан к другой образовательной организации")

            except:
                email_exist = None

        return self.cleaned_data

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            self._errors["password2"] = self.error_class([u'Пароли не совпадают.'])
            raise forms.ValidationError(u"Пароли не совпадают.")
        return password2

    def clean_password1(self):
        password1 = self.cleaned_data.get("password1", "")
        if len(password1) == 0:
            self._errors["password1"] = self.error_class([u'Пароль пустой'])
            raise forms.ValidationError(u"Пароль пустой")
        return password1


class Education_InstituteCreateForm(ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')

    class Meta:
        model = Education_Institute
        fields = ['name','accreditation_number','accreditation','web_site','telephone','post_address','city','email','director','access_ipr_program','certificate','certificate2']

        widgets  = {
            'web_site': forms.URLInput(attrs = {'pattern':".+"}),
            'access_ipr_program' : CheckboxIPRStyle(),
            'certificate': MyClearableFileInput(),
            'certificate2': MyClearableFileInput()
        }

    def __init__(self, *args, **kwargs):
        super(Education_InstituteCreateForm, self).__init__(*args, **kwargs)
        self.fields['city'].required = True
        self.fields['fio'] =  forms.CharField(max_length=50,label=u'Менеджер')
        self.fields['fio'].widget.attrs['placeholder'] = u'Укажите имя и фамилию'


    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password2"] = self.error_class([u'Пароли не совпадают.'])
            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')
        try:
            email_exist = User.objects.get(email=self.cleaned_data["email"])
        except:
            email_exist = None
        if email_exist:
            self._errors["email"] = self.error_class([u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            last_name,first_name =  self.cleaned_data.get('fio').split(' ',1)
        except :
            self._errors["fio"] = self.error_class([u'Укажите фамилию и имя'])
            raise forms.ValidationError(u'Укажите фамилию и имя')

        return self.cleaned_data


class Education_ProgrammeForm(ModelForm):
    class Meta:
        model = Education_Programme
        fields = ['name','description','level','ipr_program','presentation']

        widgets = {
            'presentation': MyClearableFileInput(),
            'ipr_program' : CheckboxIPRStyle()
            }

    def __init__(self, *args, **kwargs):
        super(Education_ProgrammeForm, self).__init__(*args, **kwargs)


class NewsForm(ModelForm):
    class Meta:
        model = News
        fields = ['title','main_banner','simple_banner','preview','text','image','link','order','is_archive']
        
        widgets = {
            'image': MyClearableFileInput(),
            'preview':Textarea(attrs={'cols': 80, 'rows': 20}),
            'main_banner': CheckboxIPRStyle(),
            'simple_banner': CheckboxIPRStyle(),
            'is_archive': CheckboxIPRStyle()
            }

    def __init__(self, *args, **kwargs):
        super(NewsForm, self).__init__(*args, **kwargs)

        max_order = 1
        orders = News.objects.filter(is_archive= False,simple_banner=False,main_banner=False,is_static = False).aggregate(Max('order'))
        if orders['order__max'] is None:
            max_order = 1
        else:
            max_order = orders['order__max'] + 1

        self.fields['order'].widget.attrs['placeholder'] = u'Введите ' + str(max_order) + u' и новость будет первой в списке'


class StaticPageForm(ModelForm):
    class Meta:
        model = News
        fields = ['title','preview','text','is_cellar','is_attic','is_archive']

        widgets = {
            'is_cellar' : CheckboxIPRStyle(),
            'is_attic'  : CheckboxIPRStyle(),
            'is_archive' : CheckboxIPRStyle()
        }
        
    def __init__(self, *args, **kwargs):
        super(StaticPageForm, self).__init__(*args, **kwargs)

class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name','form',
                  'education_start','education_end','timetable','send_pnk_date','get_pnk_date',
                  'exam_date','make_documents_length','send_documents_to_institute','give_documents_date']#,
                  #'fact_get_documents'] #,'group_started','group_ended']

        widgets = {
            'timetable': MyClearableFileInput(),
            }

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

    def clean(self):
        if self.cleaned_data.get("education_start", "") != "" and  self.cleaned_data.get("education_end", "") != "" and (self.cleaned_data.get("education_start", "") > self.cleaned_data.get("education_end", "")):
            self._errors["education_start"] = self.error_class([u'Срок обучения некорректный'])
            self._errors["education_end"] = self.error_class([u'Срок обучения некорректный'])
            raise forms.ValidationError(u"Срок обучения некорректный")
        return self.cleaned_data


class GroupFinishForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name','form',
                  'education_start','education_end','timetable','send_pnk_date','get_pnk_date',
                  'exam_date','make_documents_length','send_documents_to_institute','give_documents_date']
        widgets = {
            'timetable':  MyClearableFileInput(),
            'form' : SelectInInput(),
            }

    def __init__(self, *args, **kwargs):
        super(GroupFinishForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['readonly'] = True

    def clean(self):
        if self.cleaned_data.get("education_start", "") != "" and  self.cleaned_data.get("education_end", "") != "" and (self.cleaned_data.get("education_start", "") > self.cleaned_data.get("education_end", "")):
            self._errors["education_start"] = self.error_class([u'Срок обучения некорректный'])
            self._errors["education_end"] = self.error_class([u'Срок обучения некорректный'])
            raise forms.ValidationError(u"Срок обучения некорректный")
        return self.cleaned_data


class GroupCuratorForm(GroupForm):
    def __init__(self, *args, **kwargs):
        super(GroupCuratorForm, self).__init__(*args, **kwargs)
        self.fields['make_documents_length'].widget.attrs['readonly'] = True
        self.fields['exam_date'].widget.attrs['readonly'] = True
        self.fields['get_pnk_date'].widget.attrs['readonly'] = True
        self.fields['send_documents_to_institute'].widget.attrs['readonly'] = True

        if kwargs.has_key('instance') and kwargs['instance'].status == -1:
            self.fields['name'].widget.attrs['readonly'] = True
            self.fields['form'].widget = SelectInInput() #forms.TextInput(attrs={'size':50})
            self.fields['form'].widget.attrs['readonly'] = True
#            self.fields['timetable'].widget = FileInputReadonly()
#            self.fields['timetable'].widget.attrs['readonly'] = True

class GroupCuratorFinishForm(GroupForm):
    def __init__(self, *args, **kwargs):
        super(GroupCuratorFinishForm, self).__init__(*args, **kwargs)
        self.fields['make_documents_length'].widget.attrs['readonly'] = True
        self.fields['exam_date'].widget.attrs['readonly'] = True
        self.fields['get_pnk_date'].widget.attrs['readonly'] = True
        self.fields['send_documents_to_institute'].widget.attrs['readonly'] = True


class GroupUpForm(GroupForm):
    class Meta:
        model = Group
        fields = ['name','form','volume_academ_hours',
                  'education_start','education_end','timetable','send_pnk_date','get_pnk_date',
                  'exam_date','make_documents_length','send_documents_to_institute','give_documents_date']
        widgets = {
            'timetable': MyClearableFileInput(),
            }

    def __init__(self, *args, **kwargs):
        super(GroupUpForm, self).__init__(*args, **kwargs)
        self.fields['timetable'].label = u'Программа'

class GroupUpFinishForm(ModelForm):
    class Meta:
        model = Group
        fields = ['volume_academ_hours','name','form',
                  'education_start','education_end','timetable','send_pnk_date','get_pnk_date',
                  'make_documents_length','send_documents_to_institute','give_documents_date']

        widgets = {
            'timetable':  MyClearableFileInput(),
            'form' : SelectInInput(),
            }

    def __init__(self, *args, **kwargs):
        super(GroupUpFinishForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['readonly'] = True
        self.fields['timetable'].label = u'Программа'

    def clean(self):
        if self.cleaned_data.get("education_start", "") != "" and  self.cleaned_data.get("education_end", "") != "" and (self.cleaned_data.get("education_start", "") > self.cleaned_data.get("education_end", "")):
            self._errors["education_start"] = self.error_class([u'Срок обучения некорректный'])
            self._errors["education_end"] = self.error_class([u'Срок обучения некорректный'])
            raise forms.ValidationError(u"Срок обучения некорректный")
        return self.cleaned_data


class GroupUpCuratorForm(GroupUpForm):
    class Meta:
        model = Group
        fields = ['volume_academ_hours','name','form',
                  'education_start','education_end','timetable','send_pnk_date','get_pnk_date',
                  'make_documents_length','send_documents_to_institute','give_documents_date'] #,

        widgets = {
            'timetable': MyClearableFileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(GroupUpCuratorForm, self).__init__(*args, **kwargs)
        self.fields['make_documents_length'].widget.attrs['readonly'] = True
        self.fields['volume_academ_hours'].required = True
        self.fields['timetable'].label = u'Программа'
#        self.fields['timetable'].required = True

        self.fields['get_pnk_date'].widget.attrs['readonly'] = True
        self.fields['send_documents_to_institute'].widget.attrs['readonly'] = True


class GroupUpCuratorFinishForm(GroupUpFinishForm):

    def __init__(self, *args, **kwargs):
        super(GroupUpCuratorFinishForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['readonly'] = True
        self.fields['make_documents_length'].widget.attrs['readonly'] = True
        self.fields['volume_academ_hours'].required = True
        self.fields['timetable'].label = u'Программа'
        self.fields['get_pnk_date'].widget.attrs['readonly'] = True
        self.fields['send_documents_to_institute'].widget.attrs['readonly'] = True

class CreateGroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name','form','education_start','education_end']

    def __init__(self,*args, **kwargs):
        super(CreateGroupForm,self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['education_start'].required = True
        self.fields['education_end'].required = True

    def clean(self):
        if self.cleaned_data.get("education_start", "") != "" and  self.cleaned_data.get("education_end", "") != "" and (self.cleaned_data.get("education_start", "") > self.cleaned_data.get("education_end", "")):
            self._errors["education_start"] = self.error_class([u'Срок обучения некорректный'])
            self._errors["education_end"] = self.error_class([u'Срок обучения некорректный'])
            raise forms.ValidationError(u"Срок обучения некорректный")
        return self.cleaned_data

    def save(self):
        super(CreateGroupForm,self).save(commit=False)
        data = self.cleaned_data
        group = Group()
        group.education_institute = data['education_institute']
        group.level = data['level']
        group.name = data['name']
        group.form = data['form']
        group.education_start = data['education_start']
        group.education_end = data['education_end']
        group.save()

class RequestSeminarForm(ModelForm):
    education_institute = forms.ModelChoiceField(label=u'Выбранная образовательная организация',queryset = Education_Institute.objects.filter(education_institute_program__program__level='U').distinct())
    education_programm = forms.ModelChoiceField(label=u'Выбранная программа подготовки',queryset=Education_Programme.objects.filter(level='U',education_institute_program__institute__isnull=False).distinct())

    class Meta:
        model = EducationRequest
        fields = ['education_institute','education_programm','practic_period','payment_method','details']


    def __init__(self,*args, **kwargs):
        super(RequestSeminarForm,self).__init__(*args, **kwargs)
        self.fields['practic_period'].required = True
        self.fields['payment_method'].required = True

        if kwargs.has_key('initial'):
            if self.initial.has_key('edu_institute'):
                self.fields['education_institute'].initial = self.initial['edu_institute']
            if self.initial.has_key('edu_programm'):
                self.fields['education_programm'].initial = self.initial['edu_programm']

        self.fields['education_institute'].widget = SelectInInput()
        self.fields['education_programm'].widget = SelectInInput()


class EducationRequestBForm(ModelForm):
    education_institute = forms.ModelChoiceField(label=u'Образовательная организация',queryset = Education_Institute.objects.filter(is_archive = False,education_institute_program__is_archive = False,education_institute_program__program__is_archive = False,education_institute_program__program__level = 'B').distinct())
    education_program = forms.ModelChoiceField(label=u'Программа подготовки',queryset=Education_Programme.objects.filter(is_archive = False,level = 'B',education_institute_program__is_archive = False,education_institute_program__institute__is_archive = False).distinct())

    class Meta:
        model = EducationRequest
        fields = ['education_institute','education_program']

    def __init__(self,*args, **kwargs):
        super(EducationRequestBForm,self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].required = True

        if kwargs.has_key('instance'):
            self.fields['education_institute'].initial = self.instance.institute_program.institute
            self.fields['education_program'].initial = self.instance.institute_program.program

    def clean(self):
        education_institute = self.cleaned_data.get('education_institute')
        education_program   = self.cleaned_data.get('education_program')

        if isinstance(education_institute, Education_Institute) and isinstance(education_program, Education_Programme) and not education_institute.is_accredited_on(education_program):
            self._errors["education_program"] = self.error_class([u'Учреждение не аккредитовано на данную программу'])
            raise forms.ValidationError(u"Учреждение не аккредитовано на данную программу")
        return self.cleaned_data

class EducationRequestUForm(ModelForm):
    education_institute = forms.ModelChoiceField(label=u'Образовательная организация',queryset = Education_Institute.objects.filter(is_archive = False,education_institute_program__is_archive = False,education_institute_program__program__is_archive = False,education_institute_program__program__level = 'U').distinct())
    education_program = forms.ModelChoiceField(label=u'Программа подготовки',queryset=Education_Programme.objects.filter(is_archive = False,level = 'U',education_institute_program__is_archive = False,education_institute_program__institute__is_archive = False).distinct())
    has_seminar = forms.BooleanField(required=False)

    class Meta:
        model = EducationRequest
        fields = ['education_institute','education_program','practic_period','payment_method','details']

    def __init__(self,*args, **kwargs):
        super(EducationRequestUForm,self).__init__(*args, **kwargs)

        self.fields['education_institute'].required = True
        self.fields['education_program'].required   = True

        self.fields['has_seminar'].widget  = forms.HiddenInput()

        if kwargs.has_key('instance'):
            self.fields['education_institute'].initial = self.instance.institute_program.institute
            self.fields['education_program'].initial = self.instance.institute_program.program
            self.fields['has_seminar'].initial = self.instance.institute_program.has_seminar

    def clean(self):
        education_institute = self.cleaned_data.get('education_institute')
        education_program   = self.cleaned_data.get('education_program')

        if isinstance(education_institute, Education_Institute) and isinstance(education_program, Education_Programme) and not education_institute.is_accredited_on(education_program):
            self._errors["education_program"] = self.error_class([u'Учреждение не аккредитовано на данную программу'])
            raise forms.ValidationError(u"Учреждение не аккредитовано на данную программу")

        if isinstance(education_institute, Education_Institute) and isinstance(education_program, Education_Programme):
            try:
                institute_program = Education_Institute_Program.objects.get(institute = education_institute,program = education_program)
            except:
                self._errors["education_program"] = self.error_class([u'Учреждение не аккредитовано на данную программу'])
                raise forms.ValidationError(u"Учреждение не аккредитовано на данную программу")

            if institute_program.has_seminar:
                if self.cleaned_data.get('practic_period') == '':
                    self._errors['practic_period'] = self.error_class([u'Поле обязательно для заполнения'])
                    raise forms.ValidationError(u'Поле обязательно для заполнения')
                if self.cleaned_data.get('payment_method') == '':
                    self._errors['payment_method'] = self.error_class([u'Поле обязательно для заполнения'])
                    raise forms.ValidationError(u'Поле обязательно для заполнения')
                if self.cleaned_data.get('payment_method') == 'U' and self.cleaned_data.get('details') == '':
                    self._errors['details'] = self.error_class([u'Поле обязательно для заполнения'])
                    raise forms.ValidationError(u'Поле обязательно для заполнения')

        return self.cleaned_data

    def save(self, person):
        super(EducationRequestUForm,self).save(commit=False)

        if person.user.has_edu_request == True:
            return False

        edurequest         = EducationRequest()
        edurequest.student = person

        education_institute  = self.cleaned_data['education_institute']
        education_program    = self.cleaned_data['education_program']

        try:
            edurequest.institute_program = Education_Institute_Program.objects.get(institute = education_institute,program = education_program)
            edurequest.level = education_program.level

            if edurequest.institute_program.has_seminar == True:
                edurequest.practic_period = self.cleaned_data['practic_period']
                edurequest.payment_method = self.cleaned_data['payment_method']
                edurequest.details        = self.cleaned_data['details']
            else:
                edurequest.practic_period = ''
                edurequest.payment_method = 'F'
                edurequest.details        = ''

            edurequest.copy_attestat_data()

            edurequest.save()

            if person.user.has_edu_request == False:
                person.user.has_edu_request = True
            person.user.save( )

            try:
                new_edurequest_mail(edu_request)
            except :
                print str(sys.exc_info())

            return True
        except:
            print(str(sys.exc_info()))
            return False

class PersonEdReqForm(ModelForm):
    class Meta:
        model = EducationRequest
        fields = ['institute_program','is_archive','practic_period','payment_method','details','attestat_number','attestat_date','attestat_length']
        widgets = {
            'institute_program': forms.HiddenInput()
            }

    def __init__(self,*args, **kwargs):
        super(PersonEdReqForm,self).__init__(*args, **kwargs)

        self.seminar_fields = ['practic_period','payment_method','details']

        if kwargs.has_key('instance') and isinstance(kwargs['instance'],EducationRequest):
            lv_edu_req =  kwargs['instance']

            if lv_edu_req.is_archive:
                self.fields['is_archive'].widget.attrs['readonly'] = True
                self.fields['is_archive'].widget.attrs['onclick'] = "return false"
            else:
                self.fields['is_archive'].widget = forms.HiddenInput()

            if lv_edu_req.institute_program is not None:
                lv_edu_program = lv_edu_req.institute_program

                for field in self.seminar_fields:
                    if not self.fields.has_key(field):
                        continue
                    if lv_edu_program.has_seminar:
                        self.fields[field].widget.attrs['class'] = 'seminar_' + field

                        if field != 'details':
                            self.fields[field].required = True
                    else:
                        self.fields[field].widget = forms.HiddenInput()

        else:
            pass

    def clean(self):
        is_archive = self.cleaned_data.get('is_archive')

        if is_archive is True:
            return self.cleaned_data

        institute_program = self.cleaned_data.get('institute_program')

        if isinstance(institute_program, Education_Institute_Program):
            if institute_program.has_seminar:
                for field in self.seminar_fields:
                    if self.cleaned_data.get(field) == '' or self.cleaned_data.get(field) is None:
                        if field == 'details' and self.cleaned_data.get('payment_method') == 'F':
                            continue
                        self._errors[field] = self.error_class([u'Поле обязательно для заполнения'])
                        raise forms.ValidationError(u'Поле обязательно для заполнения')

        return self.cleaned_data


    def save(self, commit):
        super(PersonEdReqForm,self).save(commit=commit)
        is_archive = self.cleaned_data.get('is_archive')
        instance = self.cleaned_data.get('id')

        if self.changed_data is not None and 'is_archive' in self.changed_data and is_archive is True:
            instance.set_archive()

class BaseEdReqFormset(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseEdReqFormset, self).__init__(*args, **kwargs)
        # сначала будем показывать активные, потом архивные
        self.forms = sorted(self.forms, key=lambda form: form.instance.is_archive)

    def add_fields(self, form, index):
        super(BaseEdReqFormset, self).add_fields(form, index)
        #form.fields['DELETE'].widget = forms.HiddenInput()

PersonEdReqFormset = inlineformset_factory(parent_model=IprStudent,model=EducationRequest,form=PersonEdReqForm,formset=BaseEdReqFormset,can_delete=False, extra=0)

year = date.today().year

class MembershipCostRegionChoiceForm(ModelForm):
    class Meta:
        model = MembershipCost
        fields=['region']

    def __init__(self, *args, **kwargs):
        super(MembershipCostRegionChoiceForm,self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].required = True

    def clean(self):
        return self.cleaned_data

class MembershipCostCreateForm(ModelForm):
    class Meta:
        model = MembershipCost
        fields=['region','year','cost']

    def __init__(self, *args, **kwargs):
        self.prefix =  "new"
        super(MembershipCostCreateForm,self).__init__(*args, **kwargs)
        self.fields['region'].widget = forms.HiddenInput()  #SelectInInput()
        for field in self.fields:
            self.fields[field].required = True
#
    def clean(self):

        if self.cleaned_data.get("year", "") != "" and int(self.cleaned_data.get("year", "")) not in range(2006,year + 1,1):
            self._errors["year"] = self.error_class([u'Год указан некорректно!'])
            raise forms.ValidationError(u"Год указан некорректно!")
#
        if self.cleaned_data.get("year", "") != "" and  self.cleaned_data.get("region", "") != "":
            try:
                cost_exist = MembershipCost.objects.get(year = self.cleaned_data.get("year", ""),region = self.cleaned_data.get("region", ""))
                self._errors["year"] = self.error_class([u'Цена за этот год в данном регионе уже существует'])
                self._errors["region"] = self.error_class([u'Цена за этот год в данном регионе уже существует'])
                raise forms.ValidationError(u"Цена за этот год в данном регионе уже существует")
            except:
                None
        return self.cleaned_data


class MembershipCostChangeForm(ModelForm):
    class Meta:
        model = MembershipCost
        fields=['region','year','cost']

    def model_id(self):
        if self.instance is not None:
            return self.instance.id
        else:
            return ""


    def __init__(self, *args, **kwargs):
        super(MembershipCostChangeForm,self).__init__(*args, **kwargs)
#        self.fields['region'].widget.attrs['readonly'] = True
        self.fields['region'].widget = forms.HiddenInput()
        self.fields['year'].widget.attrs['readonly'] = True

    def clean(self):
        return self.cleaned_data

class MembershipBenefitForm(ModelForm):
    class Meta:
        model = MembershipBenefit
        fields=['title','percent']

    def __init__(self, *args, **kwargs):
        super(MembershipBenefitForm,self).__init__(*args, **kwargs)

    def clean(self):
        try:
            if int(self.cleaned_data.get("percent", "0")) <= 0 or int(self.cleaned_data.get("percent", "0")) > 100 :
                self._errors["percent"] = self.error_class([u'Льгота может составлять от 1 до 100 %'])
                raise forms.ValidationError(u"Льгота может составлять от 1 до 100 %")
            return self.cleaned_data
        except:
            print str(sys.exc_info())
        return self.cleaned_data


class IprStudentMembershipShortForm(ModelForm):
    class Meta:
        model = IprStudentMembership
        fields=['iprstudent','membership','paid','benefit','comment']
        widgets = {'membership': SelectInInput(attrs={'class':'membershipshort_membership'}),
                   'comment':forms.Textarea(), #TextInput(attrs={'size':'40'}),
                   'iprstudent':forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(IprStudentMembershipShortForm,self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'membershipshort_' + field
            if field == 'benefit' or field == 'comment':
                self.fields[field].required = False


    def model_id(self):
        if self.instance is not None:
            return self.instance.id
        else:
            return ""

    def clean(self):
        return self.cleaned_data


class MembershipCreatePersonForm(ModelForm):
#    region     = forms.ModelChoiceField(label=u'Регион',queryset= Region.objects.filter(is_archive=False).distinct())

    class Meta:
        model = IprStudentMembership
        fields=['iprstudent','membership','paid','benefit','comment']
        widgets = {'iprstudent': forms.HiddenInput(), #SelectInInput(),
                   'comment':forms.TextInput(attrs={'size':'40'}),
        }

    def __init__(self, *args, **kwargs):
        super(MembershipCreatePersonForm,self).__init__(*args, **kwargs)

        for field in self.fields:
            if field == 'iprstudent' or field == 'membership' or field == 'paid':
                self.fields[field].required = True
            else:
                self.fields[field].required = False

        self.fields['iprstudent'].label=u'Налоговый консультант'
        self.fields['membership'].label=u'Период взноса'

    def clean(self):
        if self.cleaned_data.get("membership", "") != "":
            try:
                cost_exist = IprStudentMembership.objects.get(membership = self.cleaned_data.get("membership", ""),iprstudent = self.cleaned_data.get("iprstudent", ""))
                self._errors["membership"] = self.error_class([u'За этот период уже осуществлена оплата'])
                raise forms.ValidationError(u"За этот период уже осуществлена оплата")
            except:
                None
                print str(sys.exc_info())
                print "NO ERROR :)"

        return self.cleaned_data

class MembershipEntranceForm(ModelForm):
    membership_number = forms.CharField(max_length=50,label=u'Номер членского билета')
    region = forms.ModelChoiceField(label=u'Регион',queryset= Region.objects.filter(is_archive=False).distinct())
    year = forms.ChoiceField(choices= list([y,y] for y in range(2006,year + 1,1)),label=u'Год')

    class Meta:
        model = IprStudentMembership
        fields=['membership_number','iprstudent','region','year','membership','paid','benefit','comment']
        widgets = {'iprstudent':forms.HiddenInput(),
                   'membership': forms.HiddenInput(), #SelectInInput(),
                   'comment':forms.TextInput(attrs={'size':'40'}),
                   }

    def __init__(self, *args, **kwargs):
        super(MembershipEntranceForm,self).__init__(*args, **kwargs)

        for field in self.fields:
            if field == 'membership_number' or field == 'paid' or field == 'year'  or field == 'region':
                self.fields[field].required = True
            else:
                self.fields[field].required = False
            self.fields[field].widget.attrs['form'] = 'entrance'

        self.fields['membership'].label=u'Период взноса'
        self.fields['region'].widget.attrs['class'] = 'region_choice'

    def clean(self):
        iprstudent = None

        if self.cleaned_data.get('membership_number', "") != "":
            iprs = IprStudent.objects.filter(membership_number__icontains = self.cleaned_data.get('membership_number', ""))

            if len(iprs) == 1:
                iprstudent = iprs[0]

                if  iprstudent.user.is_ipr == False:
                    self._errors["membership_number"] = self.error_class([u'Некорректен'])
                    raise forms.ValidationError(u"Некорректен")

                self.cleaned_data['iprstudent'] = iprstudent

            else:
                self._errors["membership_number"] = self.error_class([u'Некорректен'])
                raise forms.ValidationError(u"Некорректен")

        membershipcost = None
        if self.cleaned_data.get('region', "") != "" and self.cleaned_data.get('year', "") != "":
            membershipcosts = MembershipCost.objects.filter(region = self.cleaned_data.get('region', ""),year = self.cleaned_data.get('year', ""))

            if len(membershipcosts) == 1:
                membershipcost = membershipcosts[0]
                self.cleaned_data['membership'] = membershipcost
            else:
                self._errors["year"] = self.error_class([u'Не корректен'])
                raise forms.ValidationError(u"Не корректен")

        if iprstudent is not None and membershipcost is not None:
            try:
                cost_exist = IprStudentMembership.objects.get(iprstudent = iprstudent,membership = membershipcost)
                self._errors["paid"] = self.error_class([u'Уже есть оплата за этот период'])
                raise forms.ValidationError(u"Уже есть оплата за этот период")
            except:
                None
                print str(sys.exc_info())
                print "NO ERROR :)"

        if membershipcost is not None and self.cleaned_data.get('paid', "") != "":
            cost = membershipcost.cost

            if self.cleaned_data.get('benefit', "") != "" and self.cleaned_data.get('benefit', "") is not None:
                print self.cleaned_data.get('benefit', "")
                cost = cost - ( self.cleaned_data.get('benefit', "").percent * cost ) / 100

            debt = cost - self.cleaned_data.get('paid', "")

            if debt < 0 :
                self._errors["paid"] = self.error_class([u'Переплата'])
                raise forms.ValidationError(u"Переплата")

        return self.cleaned_data