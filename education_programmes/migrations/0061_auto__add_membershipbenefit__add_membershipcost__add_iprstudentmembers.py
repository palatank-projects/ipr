# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MembershipBenefit'
        db.create_table(u'education_programmes_membershipbenefit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('percent', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal(u'education_programmes', ['MembershipBenefit'])

        # Adding model 'MembershipCost'
        db.create_table(u'education_programmes_membershipcost', (
            ('year', self.gf('django.db.models.fields.CharField')(max_length=4, primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Region'], null=True, on_delete=models.SET_NULL)),
            ('cost', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=2)),
        ))
        db.send_create_signal(u'education_programmes', ['MembershipCost'])

        # Adding model 'IprStudentMembership'
        db.create_table(u'education_programmes_iprstudentmembership', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('iprstudent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.IprStudent'], null=True, on_delete=models.SET_NULL)),
            ('membership', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.MembershipCost'], null=True, on_delete=models.SET_NULL)),
            ('benefit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.MembershipBenefit'], null=True, on_delete=models.SET_NULL)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('paid', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=2)),
            ('ticket', self.gf('django.db.models.fields.files.FileField')(max_length=500, null=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['IprStudentMembership'])


    def backwards(self, orm):
        # Deleting model 'MembershipBenefit'
        db.delete_table(u'education_programmes_membershipbenefit')

        # Deleting model 'MembershipCost'
        db.delete_table(u'education_programmes_membershipcost')

        # Deleting model 'IprStudentMembership'
        db.delete_table(u'education_programmes_iprstudentmembership')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.education': {
            'Meta': {'ordering': "['title']", 'object_name': 'Education'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common_user.iprstudent': {
            'Meta': {'ordering': "['user']", 'object_name': 'IprStudent', '_ormbases': [u'common_user.Student']},
            'agreedata': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_length': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'confirmed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'education': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Education']", 'null': 'True'}),
            'have_bad_try': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'i_am_ip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'job_place': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'job_place_visible': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'length_of_work': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'membership_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_seria': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_when_gave': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'passport_who_gave': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'postaddress': ('django.db.models.fields.TextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'profstatus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.ProfStatus']", 'null': 'True', 'blank': 'True'}),
            'reason': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rejected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'special_notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'student_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['common_user.Student']", 'unique': 'True', 'primary_key': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'common_user.profstatus': {
            'Meta': {'ordering': "['title']", 'object_name': 'ProfStatus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common_user.student': {
            'Meta': {'ordering': "['user']", 'object_name': 'Student'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.TextField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olesya': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olympic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_participant': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_univer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'education_programmes.city': {
            'Meta': {'ordering': "['region', 'name']", 'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Region']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        u'education_programmes.currency': {
            'Meta': {'ordering': "['title']", 'object_name': 'Currency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'education_programmes.education_institute': {
            'Meta': {'ordering': "['name']", 'object_name': 'Education_Institute'},
            'accreditation': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'}),
            'accreditation_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'additional': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.City']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'curator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'director': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'post_address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'web_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.education_institute_program': {
            'Meta': {'ordering': "['institute', 'program']", 'object_name': 'Education_Institute_Program'},
            'accreditation_finish': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '2', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.StudyMode']", 'null': 'True', 'blank': 'True'}),
            'has_seminar': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'period': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Programme']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'recruiting_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.education_programme': {
            'Meta': {'ordering': "['name']", 'object_name': 'Education_Programme'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'presentation': ('django.db.models.fields.files.FileField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.educationrequest': {
            'Meta': {'ordering': "['student', 'institute_program']", 'object_name': 'EducationRequest'},
            'attestat_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_length': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date_request': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'details': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institute_program': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute_Program']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_frash': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'payment_method': ('django.db.models.fields.CharField', [], {'default': "'F'", 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'practic_begda': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'practic_endda': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'practic_period': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Student']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        u'education_programmes.examresult': {
            'Meta': {'ordering': "['title']", 'object_name': 'ExamResult'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'education_programmes.group': {
            'Meta': {'object_name': 'Group'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'education_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'education_institute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'education_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'exam_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fact_get_documents': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.StudyMode']", 'null': 'True'}),
            'get_pnk_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'give_documents_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'group_ended': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'group_started': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'}),
            'make_documents_length': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'send_documents_to_institute': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'send_pnk_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'timetable': ('django.db.models.fields.files.FileField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'volume_academ_hours': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.institutecurator': {
            'Meta': {'object_name': 'InstituteCurator'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'date_ended': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_started': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'education_programmes.iprstudentmembership': {
            'Meta': {'ordering': "['iprstudent', '-year']", 'object_name': 'IprStudentMembership'},
            'benefit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.MembershipBenefit']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iprstudent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.IprStudent']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'membership': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.MembershipCost']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'paid': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '2'}),
            'ticket': ('django.db.models.fields.files.FileField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        u'education_programmes.membershipbenefit': {
            'Meta': {'ordering': "['title']", 'object_name': 'MembershipBenefit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'percent': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'education_programmes.membershipcost': {
            'Meta': {'ordering': "['-year']", 'object_name': 'MembershipCost'},
            'cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '2'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Region']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4', 'primary_key': 'True'})
        },
        u'education_programmes.menuitem': {
            'Meta': {'ordering': "['order']", 'object_name': 'MenuItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'education_programmes.news': {
            'Meta': {'object_name': 'News'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_attic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_cellar': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_static': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'main_banner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'preview': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'simple_banner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'education_programmes.paymenttype': {
            'Meta': {'ordering': "['title']", 'object_name': 'PaymentType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'education_programmes.recruitment': {
            'Meta': {'object_name': 'Recruitment'},
            'begin_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Currency']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.StudyMode']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institute_program': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute_Program']", 'null': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'payment_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.PaymentType']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'periodicity': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '2', 'blank': 'True'}),
            'program': ('django.db.models.fields.files.FileField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'sticker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Sticker']", 'null': 'True', 'blank': 'True'}),
            'volume_academ_hours': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.region': {
            'Meta': {'ordering': "['name']", 'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_city': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'raphael_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'raphael_path': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'raphael_slug': ('django.db.models.fields.SlugField', [], {'max_length': '23'})
        },
        u'education_programmes.sticker': {
            'Meta': {'object_name': 'Sticker'},
            'about_recruit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'active_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'active_till': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Group']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'education_programmes.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'exam_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'exam_result': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.ExamResult']", 'null': 'True'}),
            'from_group': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'come'", 'null': 'True', 'to': u"orm['education_programmes.Group']"}),
            'from_other_group': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'request': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.EducationRequest']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Student']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'to_group': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'go'", 'null': 'True', 'to': u"orm['education_programmes.Group']"}),
            'to_other_group': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'education_programmes.studymode': {
            'Meta': {'ordering': "['title']", 'object_name': 'StudyMode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['education_programmes']