# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Region'
        db.create_table(u'education_programmes_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('id_city', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('raphael_path', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('raphael_slug', self.gf('django.db.models.fields.SlugField')(max_length=23)),
            ('raphael_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['Region'])

        # Adding model 'City'
        db.create_table(u'education_programmes_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Region'])),
        ))
        db.send_create_signal(u'education_programmes', ['City'])

        # Adding model 'InstituteCurator'
        db.create_table(u'education_programmes_institutecurator', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.User'])),
            ('date_started', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_ended', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['InstituteCurator'])

        # Adding model 'Education_Institute'
        db.create_table(u'education_programmes_education_institute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('accreditation_number', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('web_site', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('telephone', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('post_address', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.City'], null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('director', self.gf('django.db.models.fields.TextField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('curator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.InstituteCurator'], null=True, blank=True)),
            ('accreditation', self.gf('django.db.models.fields.CharField')(default='N', max_length=1)),
        ))
        db.send_create_signal(u'education_programmes', ['Education_Institute'])

        # Adding model 'Education_Programme'
        db.create_table(u'education_programmes_education_programme', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('level', self.gf('django.db.models.fields.CharField')(default='B', max_length=1)),
        ))
        db.send_create_signal(u'education_programmes', ['Education_Programme'])

        # Adding M2M table for field edu_institute on 'Education_Programme'
        m2m_table_name = db.shorten_name(u'education_programmes_education_programme_edu_institute')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('education_programme', models.ForeignKey(orm[u'education_programmes.education_programme'], null=False)),
            ('education_institute', models.ForeignKey(orm[u'education_programmes.education_institute'], null=False))
        ))
        db.create_unique(m2m_table_name, ['education_programme_id', 'education_institute_id'])

        # Adding model 'EducationRequest'
        db.create_table(u'education_programmes_educationrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.Student'])),
            ('edu_programme', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Education_Programme'])),
            ('edu_institute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Education_Institute'])),
            ('date_request', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['EducationRequest'])

        # Adding model 'Group'
        db.create_table(u'education_programmes_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('education_institute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Education_Institute'])),
            ('education_programm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Education_Programme'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_form', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('education_start', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('education_end', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('send_pnk_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('get_pnk_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('exam_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('make_documents_length', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('send_documents_to_institute', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('give_documents_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('fact_get_documents', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('group_started', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('group_ended', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['Group'])

        # Adding model 'StudentGroup'
        db.create_table(u'education_programmes_studentgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.Student'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['education_programmes.Group'])),
            ('exam_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('exam_result', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_archive', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'education_programmes', ['StudentGroup'])


    def backwards(self, orm):
        # Deleting model 'Region'
        db.delete_table(u'education_programmes_region')

        # Deleting model 'City'
        db.delete_table(u'education_programmes_city')

        # Deleting model 'InstituteCurator'
        db.delete_table(u'education_programmes_institutecurator')

        # Deleting model 'Education_Institute'
        db.delete_table(u'education_programmes_education_institute')

        # Deleting model 'Education_Programme'
        db.delete_table(u'education_programmes_education_programme')

        # Removing M2M table for field edu_institute on 'Education_Programme'
        db.delete_table(db.shorten_name(u'education_programmes_education_programme_edu_institute'))

        # Deleting model 'EducationRequest'
        db.delete_table(u'education_programmes_educationrequest')

        # Deleting model 'Group'
        db.delete_table(u'education_programmes_group')

        # Deleting model 'StudentGroup'
        db.delete_table(u'education_programmes_studentgroup')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.student': {
            'Meta': {'object_name': 'Student'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.TextField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'education_programmes.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Region']"})
        },
        u'education_programmes.education_institute': {
            'Meta': {'object_name': 'Education_Institute'},
            'accreditation': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'}),
            'accreditation_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.City']", 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'curator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.InstituteCurator']", 'null': 'True', 'blank': 'True'}),
            'director': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'post_address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'web_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.education_programme': {
            'Meta': {'object_name': 'Education_Programme'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'edu_institute': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['education_programmes.Education_Institute']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.CharField', [], {'default': "'B'", 'max_length': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'education_programmes.educationrequest': {
            'Meta': {'object_name': 'EducationRequest'},
            'date_request': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'edu_institute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute']"}),
            'edu_programme': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Programme']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Student']"})
        },
        u'education_programmes.group': {
            'Meta': {'object_name': 'Group'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'education_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'education_form': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_institute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Institute']"}),
            'education_programm': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Education_Programme']"}),
            'education_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'exam_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fact_get_documents': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'get_pnk_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'give_documents_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'group_ended': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'group_started': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'make_documents_length': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'send_documents_to_institute': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'send_pnk_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'education_programmes.institutecurator': {
            'Meta': {'object_name': 'InstituteCurator'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'date_ended': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_started': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'education_programmes.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_city': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'raphael_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'raphael_path': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'raphael_slug': ('django.db.models.fields.SlugField', [], {'max_length': '23'})
        },
        u'education_programmes.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            'exam_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'exam_result': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['education_programmes.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Student']"})
        }
    }

    complete_apps = ['education_programmes']