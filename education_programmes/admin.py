# -*- coding: utf-8 -*-

# System libraries
# Third-party libraries

# Django modules
from django.contrib import admin
#from django.db import models

# Django apps

# Current-app modules
from models import *

class StudentGroupInline(admin.TabularInline):
    model = StudentGroup

class StudentGroupSpecInline(admin.TabularInline):
    model = StudentGroup
    fk_name = "group"

class GroupInline(admin.TabularInline):
    model = Group

class Education_Institute_ProgramInline(admin.TabularInline):
    model = Education_Institute_Program

class RecruitmentInline(admin.TabularInline):
    model = Recruitment

class NewsAdmin(admin.ModelAdmin):
    list_display = ('slug','title','main_banner','simple_banner','is_static','is_attic','is_cellar','is_archive')

    list_display_links = ('slug',)

class MenuItemAdmin(admin.ModelAdmin):
    list_display = ('title','link')

    list_display_links = ('title',)

class RecruitmentAdmin(admin.ModelAdmin):
    list_display = ('institute_program','begin_date')

    list_display_links = ('institute_program',)

class RegionAdmin(admin.ModelAdmin):
    list_display = ('name','raphael_id')

    list_display_links = ('name',)

class CuratorAdmin(admin.ModelAdmin):
    list_display = ('user','date_started','date_ended','created')

    list_display_links = ('user',)

class Education_Institute_ProgramAdmin(admin.ModelAdmin):
    list_display = ('institute','program','has_seminar','is_archive')

    list_display_links = ('institute',)


    inlines = [
        RecruitmentInline,
        ]


class Education_InstituteAdmin(admin.ModelAdmin):
    list_display = ('name','city','accreditation','is_archive')

    list_display_links = ('name',)

    inlines = [
        Education_Institute_ProgramInline,GroupInline,
        ]

class Education_ProgrammeAdmin(admin.ModelAdmin):
    list_display = ('name','level','is_archive')

    list_display_links = ('name',)

    inlines = [
        Education_Institute_ProgramInline,
        ]


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name','education_institute','is_finished','is_archive')

    list_display_links = ('name',)

    inlines = [
        StudentGroupSpecInline,
        ]

class StudentGroupAdmin(admin.ModelAdmin):
    list_display = ('student','group','institute','program','request')

    list_display_links = ('student',)

class EducationRequestAdmin(admin.ModelAdmin):
    list_display = ('student','institute_program','institute','program','payment_method','is_archive')

    list_display_links = ('student',)

    inlines = [
        StudentGroupInline,
        ]


class StickerAdmin(admin.ModelAdmin):
    list_display = ('title','active_from','active_till','type','creator')

    list_display_links = ('title',)

class MembershipCostAdmin(admin.ModelAdmin):
    list_display = ('year','region','cost')

    list_display_links = ('year',)

class MembershipBenefitAdmin(admin.ModelAdmin):
    list_display = ('title','percent')

    list_display_links = ('title',)

class IprStudentMembershipAdmin(admin.ModelAdmin):
    list_display = ('iprstudent','membership')

    list_display_links = ('iprstudent',)

admin.site.register(MenuItem,MenuItemAdmin)
admin.site.register(News,NewsAdmin)
admin.site.register(StudyMode)
admin.site.register(PaymentType)
admin.site.register(Currency)
#admin.site.register(Periodicity)
admin.site.register(ExamResult)
admin.site.register(Education_Institute_Program,Education_Institute_ProgramAdmin)
admin.site.register(Sticker,StickerAdmin)
admin.site.register(Recruitment,RecruitmentAdmin)
admin.site.register(Group,GroupAdmin)
admin.site.register(Region,RegionAdmin)
admin.site.register(City)
admin.site.register(Education_Programme,Education_ProgrammeAdmin)
admin.site.register(Education_Institute,Education_InstituteAdmin)
admin.site.register(EducationRequest,EducationRequestAdmin)
#admin.site.register(InstituteCurator,CuratorAdmin)
admin.site.register(StudentGroup,StudentGroupAdmin)
admin.site.register(MembershipCost,MembershipCostAdmin)
admin.site.register(MembershipBenefit,MembershipBenefitAdmin)
admin.site.register(IprStudentMembership,IprStudentMembershipAdmin)