# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import simplejson
from education_programmes.models import Region
import os

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        max_length = 0 
        working_dir = os.path.dirname(__file__)
        self.stdout.write(working_dir)
        path_file = open(working_dir + '/russia_path.js', 'rb')
        file_content = ""
		
        Region.objects.all().delete()
        i = 1
        while i < 446:
            file_content = file_content + path_file.readline().strip()
            i = i + 1
            self.stdout.write(str(i))			
        file_content = file_content.strip()
        path_file.close()
        dict_content = simplejson.loads(file_content)
        
        for key in dict_content.keys():
            if len(key) > max_length:
                max_length = len(key)
            self.stdout.write(" slug = " + key + "  " + " name = " + dict_content[key]["name"] + " id = " + str(dict_content[key]["id"]) + "\n")    
            new_region = Region(name = dict_content[key]["name"],id_city = 0,raphael_path = dict_content[key]["path"],raphael_slug = key,raphael_id = dict_content[key]["id"])
            new_region.save()        
         