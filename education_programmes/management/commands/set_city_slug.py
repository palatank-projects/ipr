__author__ = 'khriptoni'
# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from slugify import slugify
from education_programmes.models import City


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        cities = City.objects.all()

        for city in cities:
            city.slug = slugify(city.name)
            city.save()
        self.stdout.write(" finish \n")


