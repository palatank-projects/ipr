# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from common_user.models import *
from education_programmes.models import *
from django.db.models import Q
import os, sys, re
from django.core.mail import send_mail

class Command(BaseCommand):

    def st_write(self, text):
        self.stdout.write(text)

    def handle(self, *args, **options):

        self.stdout.write("Parser start")

        active_requests = EducationRequest.objects.filter(student__isnull=False,institute_program__program__level = 'U',is_archive = False)

        for req in active_requests:
            self.stdout.write(" ID request {0} ".format(req.id))
            req.copy_attestat_data()
            req.save( )

#        iprs = IprStudent.objects.filter(Q(user__isnull=False),Q(user__is_ipr = True) | Q(user__is_student = True))
#
#        for ipr in iprs:
#            self.stdout.write(" ID student {0} ".format(ipr.id))
#
#            ipr.birthday = None
#            ipr.postcode = None
#            ipr.street   = None
#            ipr.house    = None
#            ipr.flat     = None
#            ipr.passport_code = None
#
#            ipr.passport_seria = None
#            ipr.passport_number = None
#            ipr.passport_who_gave = None
#            ipr.passport_when_gave = None
#            ipr.passport_code  = None
#
#            ipr.save( )

#            try:
#                self.stdout.write(" User {0} Telephone {1} ".format(ipr.slug,ipr.telephone))
#            except :
#                self.stdout.write(" User {0} Telephone {1} ".format(ipr.slug,str(ipr.telephone)))
#            l_telephone = ipr.telephone
#            l_new = re.sub(r'\D+', '', l_telephone)
#            self.stdout.write(" Only digits {0} ".format(l_new))

        self.stdout.write("Parser end")

