# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models.fields.related import ForeignKey
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils import simplejson
from education_programmes.models import Education_Programme, City, EducationRequest, Education_Institute, Region
from forms import RegisterStudentForm, LoginForm, RegisterConsultForm
from models import IprStudent
import sys

def personal(request,model,form,template):
    if request.POST:
        HttpResponseRedirect('/')
    else:
        return render_to_response(template,{'create_form':form,'edit_form':form},context_instance=RequestContext(request))

@csrf_protect
def register_student(request,template,form_action):
    if request.user.is_authenticated():
        return HttpResponseRedirect(get_url_redirect(request.user))
    if request.POST:
        if request.POST.has_key('make_nalog'):
            request.session['id_institute'] = request.POST['university']
            request.session['id_programm'] = request.POST['programm']
            form = RegisterStudentForm(initial={'edu_institute':request.session['id_institute'],'edu_programm':request.session['id_programm']})
            return render_to_response(template, {'form':form,'form_action':form_action},context_instance=RequestContext(request))
        elif request.POST.has_key('student_request'):
            form = RegisterStudentForm(request.POST,initial={'edu_institute':request.session['id_institute'],'edu_programm':request.session['id_programm']})
            if form.is_valid():
                form.save()
                return render_to_response(template,{'registration_done':'true','form_action':form_action},context_instance=RequestContext(request))
            else:
                return render_to_response(template,{'form':form,'form_action':form_action},context_instance=RequestContext(request))
        else:
            return render_to_response(template,{'not_any_post':'true','form_action':form_action},context_instance=RequestContext(request))
    else:
        return render_to_response(template,{'not_post':'true','form_action':form_action},context_instance=RequestContext(request))




@csrf_protect
def register_consult(request,template,form_action):
    if request.user.is_authenticated():
        return HttpResponseRedirect(get_url_redirect(request.user))
    if request.POST:
        form = RegisterConsultForm(request.POST)
        if form.is_valid():
            form.save()
            return render_to_response(template,{'registration_done':'true','form_action':form_action},context_instance=RequestContext(request))
        else:
            return render_to_response(template,{'form':form,'form_action':form_action},context_instance=RequestContext(request))
    else:
        form = RegisterConsultForm()
        return render_to_response(template,{'form':form,'form_action':form_action},context_instance=RequestContext(request))


def get_url_redirect(user):
    return '/account/personal'

@login_required
def student_personal_cabinet(request,template):
    if request.POST:
        HttpResponseRedirect('/')
    else:
        if request.user.is_admin:
            cities = City.objects.all()
            regions = Region.objects.all()
            institutes  = Education_Institute.objects.all()
            return render_to_response(template,{"cities":cities,"regions": regions,"institutes":institutes},context_instance=RequestContext(request))
        else:
            student = IprStudent.objects.get(id=1)

        # first to check in student_group

            edu_request = EducationRequest.objects.get(student=student)
            edu_institute = Education_Institute.objects.get(id=edu_request.edu_institute.id)
            city = City.objects.get(id=edu_institute.city.id)

            setattr(student,'study_city',city)

            return render_to_response(template,{'student':student},context_instance=RequestContext(request))



@csrf_exempt
def authorize(request):
    data = {'login':request.POST['login'], 'password':request.POST['password']}
    form = LoginForm(request.POST,data)
    response = {}
    if form.is_valid():
        if request.is_ajax:
            user = authenticate(email = request.POST['login'],password = request.POST['password'])
            				
            if user is not None:
                login(request,user)
                response["status"] = 1
                response["body"] = '/' # personal cabinet
            else:
                response["status"] = 0
                response["body"] = "Логин или пароль некорректны"
        else:
            response["status"] = 0
            response["body"] = "Авторизация возможна только через ajax"
    else:
        response["status"] = 0
        response["body"] = "Логин или пароль некорректны" #form.errors.as_text()
    return HttpResponse(simplejson.dumps(response))


def ulogout(request):
    logout(request)
    return HttpResponseRedirect("/")
