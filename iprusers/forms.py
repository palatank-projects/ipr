# -*- coding: utf-8 -*-

__author__ = 'andrei'
from django.forms import ModelForm
from models import IprStudent,User
from django import forms
from django.forms.extras import SelectDateWidget
from common_user.models import User
from education_programmes.models import Education_Institute, Education_Programme, EducationRequest
import datetime
from django.core import validators

year = datetime.date.today().year


from django.utils.safestring import mark_safe

class PlainTextWidget(forms.Widget):
    def render(self, _name, value, _attrs):
        return mark_safe(value) if value is not None else '-'


class RegisterStudentForm(ModelForm):
    email = forms.EmailField(max_length=50, label=u'E-mail')
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    name = forms.CharField(max_length=50,label=u'Имя')
    surname = forms.CharField(max_length=50,label=u'Фамилия')
    patronymic = forms.CharField(max_length=50,label=u'Отчество')
    education_institute = forms.ModelChoiceField(label=u'Выбранное учебное заведение',queryset = Education_Institute.objects.all())
    education_programm = forms.ModelChoiceField(label=u'Выбранная учебная программа',queryset=Education_Programme.objects.all())
    agreedata = forms.BooleanField(label=u'Согласие на обработку данных')

    class Meta:
        model = IprStudent
        fields=['email','password1','password2','name','surname','patronymic','birthday','placebirth',
                'postaddress','telephone','study','length_of_work','education_institute','education_programm','agreedata']
        widgets = {
            #'birthday':SelectDateWidget(years = range(year,year-100,-1)),
            'placebirth': forms.TextInput(attrs={'size':'40'}),
            'postaddress':forms.TextInput(attrs={'size':'40'}),
            'telephone':forms.TextInput(attrs={'size':'40'}),
            'study':forms.TextInput(attrs={'size':'40'}),
        }




    def __init__(self, *args, **kwargs):
        super(RegisterStudentForm,self).__init__(*args, **kwargs)
        #self.fields['education_institute'].widget.attrs['disabled'] = 'disabled'
        #self.fields['education_programm'].widget.attrs['disabled'] = 'disabled'
        if kwargs.has_key('initial'):
            if self.initial.has_key('edu_institute'):
                self.fields['education_institute'].initial = self.initial['edu_institute']
            if self.initial.has_key('edu_programm'):
                self.fields['education_programm'].initial = self.initial['edu_programm']

    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')
        try:
            email_exist = User.objects.get(email=self.cleaned_data["email"])
        except:
            email_exist = None
            #if email_exist is not None:
        if email_exist:
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        return self.cleaned_data


    def save(self):
        super(RegisterStudentForm,self).save(commit=False)
        data = self.cleaned_data
        user = User.objects.create_user(data['email'], data['name'], data['surname'], data['password1'])
        user.is_active = True
        #user.save()
        iprstudent = IprStudent()
        iprstudent.user = user
        iprstudent.patronymic = data['patronymic']
        iprstudent.birthday = data['birthday']
        iprstudent.placebirth = data['placebirth']
        iprstudent.postaddress = data['postaddress']
        iprstudent.telephone = data['telephone']
        iprstudent.study = data['study']
        iprstudent.length_of_work = data['length_of_work']
        iprstudent.consult = False
        iprstudent.save()

        #*****save request*****
        edu_request = EducationRequest()
        edu_request.student = iprstudent
        edu_request.edu_institute = data['education_institute']
        edu_request.edu_programme = data['education_programm']
        edu_request.save()





class LoginForm(forms.Form):
    login = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["login"])
        except:
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')

        return self.cleaned_data


class RegisterConsultForm(ModelForm):
    email = forms.EmailField(max_length=50, label=u'E-mail')
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    name = forms.CharField(max_length=50,label=u'Имя')
    surname = forms.CharField(max_length=50,label=u'Фамилия')
    patronymic = forms.CharField(max_length=50,label=u'Отчество')
    agreedata = forms.BooleanField(label=u'Согласие на обработку данных')

    class Meta:
        model = IprStudent
        fields=['email','password1','password2','name','surname','patronymic','birthday','placebirth',
                'postaddress','telephone','attestat_number','attestat_date','attestat_length','job_place',
                'passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code','agreedata']


    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')
        try:
            email_exist = User.objects.get(email=self.cleaned_data["email"])
        except:
            email_exist = None
            #if email_exist is not None:
        if email_exist:
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        return self.cleaned_data


    def save(self):
        super(RegisterConsultForm,self).save(commit=False)
        data = self.cleaned_data
        user = User.objects.create_user(data['email'], data['name'], data['surname'], data['password1'])
        user.is_active = False
        #user.save()
        iprstudent = IprStudent()
        iprstudent.user = user
        iprstudent.patronymic = data['patronymic']
        iprstudent.birthday = data['birthday']
        iprstudent.placebirth = data['placebirth']
        iprstudent.postaddress = data['postaddress']
        iprstudent.telephone = data['telephone']
        iprstudent.attestat_number = data['attestat_number']
        iprstudent.attestat_date = data['attestat_date']
        iprstudent.attestat_length = data['attestat_length']
        iprstudent.job_place = data['job_place']
        iprstudent.passport_seria = data['passport_seria']
        iprstudent.passport_number = data['passport_number']
        iprstudent.passport_who_gave = data['passport_who_gave']
        iprstudent.passport_when_gave = data['passport_when_gave']
        iprstudent.passport_code = data['passport_code']
        iprstudent.consult = True
        iprstudent.confirmed = False
        iprstudent.save()