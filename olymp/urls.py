# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

#from education_programmes.models import Education_Institute
#from education_programmes.forms import InstituteForm

urlpatterns = patterns('',
    # Examples:
#    url(r'^$', home, name='home'),

    url(r'^$', olymp, name='olymp'),
    url(r'^host/?$', host, name='host'),
    
    url(r'^tests/', include('olymp_tests.urls')),
    url(r'^people/', include('people.urls')),

    url(r'^content/(?P<id>\d+)/$', content, name='content-edit'),

    #url(r'^universities/(?P<limit>all|alone|\d+)/$', olymp_university_list, name='olymp-university-list'),
    
    url(r'^changepass/(\d+)/?$', customchangepass, name='olymp-changepass'),
    url(r'^login/?$', customlogin, name='olymp-login'), #'IPR.views.customlogin', name='login'),
    url(r'^logout/?$',customlogout, name='olymp-logout'), #'IPR.views.customlogout', name='logout'),
    # url(r'^olymp/', include('olymp.foo.urls')),

    url(r'^simplimg/', include('simplimg.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),


    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.PROJECT_ROOT + '/media/olymp/'}),
    url(r'^static/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.PROJECT_ROOT + '/static/'}),
)