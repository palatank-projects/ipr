# -*- coding: utf-8 -*-
from django.core.context_processors import csrf
from django.core.urlresolvers import resolve
from django.conf import settings
from olymp.forms import  OlympLoginForm
from olymp_tests.models import Test
from django.contrib.auth import authenticate, login, logout
from people.models import *
import sys
from datetime import datetime

def olymp_login_form(request):
#    match = resolve(request.path)
#    claim = 0
#    curator = None
#    main_banners = News.objects.filter(main_banner = True,is_archive = False).order_by('created')
#    if len(main_banners) > 0:
#        main_banner = main_banners[0]
#    else:
#        main_banner = None
#
#    attics = News.objects.filter(is_attic = True,is_archive = False).order_by('created')
#    cellars = News.objects.filter(is_cellar = True,is_archive = False).order_by('created')
#    sidenews = News.objects.filter(is_archive= False,simple_banner=False,main_banner=False,is_static = False).order_by('-order','-created')[:3]
#    menuitems = MenuItem.objects.filter(is_archive = False)
#
#    #    c = dict(http_host   = request.path,match = match.url_name)
#    c = {"http_host":request.path,"match": match.url_name,"main_banner":main_banner,"cellars":cellars,"attics":attics,"sidenews":sidenews,"menuitems":menuitems}
    c = {}


    tests = Test.objects.filter(slug = datetime.now().year,is_active = True)

    if len(tests) > 0:
        c["main_test"] = tests[0]
		
    if request.user.is_authenticated() and request.user.is_olympic and request.get_host() == u'ipr.palata-nk.ru':
        logout(request)
    if request.user.is_authenticated() and not request.user.is_olympic and request.get_host() == u'palata-nk.ru':
        logout(request)
		

    if not request.user.is_authenticated() or ( request.user.is_authenticated() and not request.user.is_olympic ):
        login_form = OlympLoginForm()
        c["login_form"] = login_form
    
    if request.user.is_authenticated() and request.user.is_olympic and request.user.is_univer:
        try:
            univer = University.objects.get(user = request.user)
        except:
            univer = str(sys.exc_info())
        c["univer"] = univer
    print c
    c.update(csrf(request))
    return c