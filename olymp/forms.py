# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.contrib.auth import authenticate
from common_user.models import User
from olymp_tests.models import OlympContent
import sys


class OlympLoginForm(forms.Form):
    login = forms.CharField(max_length=30,label=u'Логин')
    password = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')

    def clean(self):
        try:
            user = User.objects.get(username = self.cleaned_data.get('login'))
            user = authenticate(email = user.email,password = self.cleaned_data.get('password'))

            if user is None:
                self._errors["login"] = self.error_class([u'Логин или пароль некорректны'])
        except:
            self._errors["login"] = self.error_class([u'Логин или пароль некорректны'])

        return self.cleaned_data

class OlympContentForm(ModelForm):
    class Meta:
        model = OlympContent
        fields = ['content']