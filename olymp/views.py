# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from datetime import datetime,date,time,timedelta
from forms import *
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from olymp_tests.models import *
from people.models import *
#from django.utils import simplejson
import json as simplejson

def olymp(request):
    univer = None
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    try:
        olymp_content = OlympContent.objects.get(id = 1)
        content = olymp_content.content
    except :
        content = u''

    if request.user.is_authenticated() and request.user.is_admin:
        all_tests = Test.objects.all()
    else:
        all_tests = []


    if request.user.is_authenticated() and request.user.is_participant:
        olympstudent = get_object_or_404(OlympStudent,user=request.user) #OlympStudent.objects.get(user = request.user)
    else:
        olympstudent = None

    return render_to_response("olymp/palata_index.html", {"olymp_content":content,"olympstudent": olympstudent,"all_tests":all_tests}, context_instance=RequestContext(request))

def host(request):
    return HttpResponse(request.get_host())

def olymp_university_list(request,limit):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    tests = Test.objects.all()

    if limit == 'all':
        universities = University.objects.all()
    elif limit == 'alone':
        universities = University.objects.filter(~Q(id__in=RequestOnTest.objects.all().values('university')))
    else:
        try:
            current_test = tests.get(id = limit)
            universities = University.objects.filter(Q(id__in=RequestOnTest.objects.filter(test = current_test).values('university')))

        except:
            print str(sys.exc_info())
            limit = 'all'
            universities = University.objects.all()
    return render_to_response("olymp/university_list.html", {'universities':universities,'tests':tests,'limit':limit}, context_instance=RequestContext(request))


@csrf_exempt
def customlogin(request):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() or not request.is_ajax():
        return HttpResponseRedirect('/olymp/')

    if request.method == 'POST':
        print request.POST
        form = OlympLoginForm(request.POST)

        response = {}

        if form.is_valid():
            user_model = User.objects.get(username = form.cleaned_data.get('login'))
            user = authenticate(email = user_model.email,password = form.cleaned_data.get('password'))
            if user is not None:
                login(request,user)
                response["status"] = 1
                response["body"] = '/olymp/'
                if request.is_ajax():
                    return HttpResponse(simplejson.dumps(response))
                else:
                    return HttpResponseRedirect('/olymp/')
            else:
                print 'fail authenticate'
                response["status"] = 0
                response["body"] = "Логин или пароль некорректны"
                print "user is None"

        else:
            print 'fail valid'
            response["status"] = 0
            response["body"] = "Логин или пароль некорректны"
            if request.is_ajax():
                return HttpResponse(simplejson.dumps(response))
            else:
                return render_to_response("olymp/login.html", {"login_form":form}, context_instance=RequestContext(request))
    else:
        form = OlympLoginForm()

    return render_to_response("olymp/login.html",{"login_form":form}, context_instance=RequestContext(request))

def customlogout(request):
    if request.user.is_authenticated():
        logout(request)
    return HttpResponseRedirect("/olymp/")

@csrf_exempt    
def customchangepass(request,id):
    response = {}
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin == True and request.method == 'POST':
        user = User.objects.get(id=id)
        if request.POST.has_key('password') and request.POST.has_key('password2') and request.POST['password'] == request.POST['password2']:
            user.set_password(request.POST['password'])
            user.save()
            response["status"] = 1
            return HttpResponse(simplejson.dumps(response))
    response["status"] = 0
    return HttpResponse(simplejson.dumps(response))

def content(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin:
        try:
            model_item = OlympContent.objects.get(id = id)
        except :
            print str(sys.exc_info())
            model_item = OlympContent()

        if request.method == 'POST':
            form = OlympContentForm(request.POST) # A form bound to the POST data

            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]

                model_item.save()
        else:
            form = OlympContentForm(instance=model_item)
        return render_to_response("olymp/content.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'current_id':id}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/olymp/')

    
    

