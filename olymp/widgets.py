# -*- coding: utf-8 -*-

from django.forms import widgets
from django.utils.translation import ugettext, ugettext_lazy
from django.utils.html import conditional_escape, format_html, format_html_join
from django.utils.encoding import force_text, python_2_unicode_compatible
from django.utils.safestring import mark_safe
import sys, datetime


class DateWidget(widgets.TextInput):
    template_with_initial = u'<span>%(dateinput)s  / <label class="datewidgetlabel" >день</label></span> <span>%(monthinput)s  / <label class="datewidgetlabel">месяц</label></span> <span>%(yearinput)s <label class="datewidgetlabel">год</label></span> %(hidden_input)s'

    def clear_display_input_value(self, name, value):
        return value

    def clear_display_input_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name

    def clear_display_input_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_id'

    def clear_display_input_day(self, name, value):
        if isinstance(value, datetime.date):
            return value.day
        else:
            return '00'
        return value

    def clear_display_day_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_day'

    def clear_display_day_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_day_id'

    def clear_display_input_month(self, name, value):
        if isinstance(value, datetime.date):
            return value.month
        else:
            return '00'
        return value

    def clear_display_month_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_month'

    def clear_display_month_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_month_id'

    def clear_display_input_year(self, name, value):
        if isinstance(value, datetime.date):
            return value.year
        else:
            return '0000    '
        return value

    def clear_display_year_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_year'

    def clear_display_year_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_year_id'

    def render(self, name, value, attrs=None):
        substitutions = {}
        substitutions['dateinput'] = widgets.TextInput().render(self.clear_display_day_name(name), self.clear_display_input_day(name,value), attrs={'id':self.clear_display_day_id(name),'size':2,'maxlength':2})
        substitutions['monthinput'] = widgets.TextInput().render(self.clear_display_month_name(name), self.clear_display_input_month(name,value), attrs={'id':self.clear_display_month_id(name),'size':2,'maxlength':2})
        substitutions['yearinput'] = widgets.TextInput().render(self.clear_display_year_name(name), self.clear_display_input_year(name,value), attrs={'id':self.clear_display_year_id(name),'size':4,'maxlength':4})

        hidden_input_name = self.clear_display_input_name(name)
        hidden_input_id = self.clear_display_input_id(hidden_input_name)
        substitutions['hidden_input'] = widgets.HiddenInput().render(hidden_input_name, value, attrs={'id': hidden_input_id})

#    substitutions['input'] = widgets.TextInput().render(self.clear_display_input_name(name), self.clear_display_input_value(name,value), attrs={'id':self.clear_display_input_id(name),'readonly':True})

        return mark_safe(self.template_with_initial % substitutions)

    def value_from_datadict(self, data,files,name):
        if data.has_key(name+'_day'):
            day = data[name+'_day']
        else:
            day = None

        if data.has_key(name+'_month'):
            month = data[name+'_month']
        else:
            month = None


        if data.has_key(name+'_year'):
            year = data[name+'_year']
        else:
            year = None


        data_new = data.copy()

        if day is not None and month is not None and year is not None and day != u'' and month != u'' and year != u'':
            try:
                value = datetime.date(int(year), int(month), int(day))
            except:
                print str(sys.exc_info())
                value = None #day + '.' +month + '.' + year
            data_new[name] = value



        upload = super(DateWidget, self).value_from_datadict(data_new,files, name)

        return upload

class TimeWidget(widgets.TextInput):
    template_with_initial = u'<span>%(hourinput)s  / <label class="datewidgetlabel" >час</label></span> <span>%(minutinput)s  / <label class="datewidgetlabel">минуты</label></span> <span>%(secondinput)s <label class="datewidgetlabel">секунды</label></span> %(hidden_input)s'

    def clear_display_input_value(self, name, value, _type = None):
        if isinstance(value, datetime.time):
            if _type == 'hour':
                return value.hour
            elif _type == 'minute':
                return value.minute
            elif _type == 'second':
                return value.second
        else:
            if _type is None:
                return '00:00:00'
            else:
                return '00'

        return value

    def clear_display_input_name(self, name, _type = None):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
#        return name + '_display'
        return name + ( '_' +  _type if _type is not None else '' )

    def clear_display_input_id(self, name, _type = None):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_' + ( _type if _type is not None else 'display' ) + '_id'

    def render(self, name, value, attrs=None):
        substitutions = {}

        hidden_input_name = self.clear_display_input_name(name)
        hidden_input_id = self.clear_display_input_id(hidden_input_name)

        substitutions['hourinput'] = widgets.TextInput().render(self.clear_display_input_name(name,'hour'), self.clear_display_input_value(name,value,'hour'), attrs={'id':self.clear_display_input_id(name,'hour'),'size':2,'maxlength':2})
        substitutions['minutinput'] = widgets.TextInput().render(self.clear_display_input_name(name,'minute'), self.clear_display_input_value(name,value,'minute'), attrs={'id':self.clear_display_input_id(name,'minute'),'size':2,'maxlength':2})
        substitutions['secondinput'] = widgets.TextInput().render(self.clear_display_input_name(name,'second'), self.clear_display_input_value(name,value,'second'), attrs={'id':self.clear_display_input_id(name,'second'),'size':2,'maxlength':2})
        substitutions['hidden_input'] = widgets.HiddenInput().render(hidden_input_name, value, attrs={'id': hidden_input_id})

        #    substitutions['input'] = widgets.TextInput().render(self.clear_display_input_name(name), self.clear_display_input_value(name,value), attrs={'id':self.clear_display_input_id(name),'readonly':True})

        return mark_safe(self.template_with_initial % substitutions)

    def value_from_datadict(self, data,files, name):
        if data.has_key(name+'_hour'):
            hour = data[name+'_hour']
        else:
            hour = None

        if data.has_key(name+'_minute'):
            minute = data[name+'_minute']
        else:
            minute = None

        if data.has_key(name+'_second'):
            second = data[name+'_second']
        else:
            second = None

        data_new = data.copy()

        if hour is not None and minute is not None and second is not None and hour != u'' and minute != u'' and second != u'':
            try:
                value = datetime.time(int(hour), int(minute), int(second))
            except:
                print str(sys.exc_info())
                value = None
            data_new[name] = value

        upload = super(TimeWidget, self).value_from_datadict(data_new,files, name)
        return upload
