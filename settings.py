# coding: utf-8

import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
working_dir = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ipr',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': 'sepultura',
        'HOST': 'mysql.baze.palata-nk.ru',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '65510',                      # Set to empty string for default.
    }
}


EMAIL_HOST = "mail.palata-nk.ru"
EMAIL_HOST_PASSWORD = "coord8nato4"
EMAIL_HOST_USER = "coordinator@palata-nk.ru"
EMAIL_PORT = 25
EMAIL_EMAIL_TLS = False


AUTH_USER_MODEL = 'common_user.User'

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-RU'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/bhome/part3/02/palata-nk/ipr.palata-nk.ru/www/media/'


LOGIN_URL = '/auth/authorize/'
USER_AVATAR_UPLOAD_DIR = '/media/img/avatars/'

SIMPLIMG_ROOT = MEDIA_ROOT + '/simplimg/'
SIMPLIMG_URL = '/media/simplimg/'
SIMPLIMG_TEMPLATES = MEDIA_ROOT +'/tiny_mce/plugins/simplimg/'
SIMPLIMG_FORMATS = 'jpg,jpeg,gif,png,bmp,tga,tiff,xpm'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = 'http://ipr.palata-nk.ru/media/'

YACAPTCHA_KEY = 'cw.1.1.20141228T134014Z.091652c08445f3eb.068b8e00c16340451c8eebd47fb1721c23cf4c32'
YACAPTCHA_WIDGET_TEMPLATE = '/yacaptcha/templates/yacaptcha/widget.html'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = '/bhome/part3/02/palata-nk/ipr.palata-nk.ru/www/static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '4!k72d$r2nn!6c4dr#z#$*e@b+1^tue+s_kgvq2wiznaa48^u^'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
#    'django.middleware.doc.XViewMiddleware',
#    'todo.middleware.Custom403Middleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    #'django.core.context_processors.request',
    #'django.core.context_processors.i18n',
    #'django.contrib.messages.context_processors.messages',
    #'django.core.context_processors.static',
    'IPR.context_processors.determine_current_menu_item',
    'IPR.context_processors.host',
    'IPR.context_processors.my_media_url',
    'olymp.context_processors.olymp_login_form',
    )

ROOT_URLCONF = 'IPR.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'IPR.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, 'templates'),
    SIMPLIMG_TEMPLATES,
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'common_user',
    'education_programmes',
#    'iprusers',
    'user_messages',
    'south',
    'people',
    'olymp',
    'olymp_tests',

    'yacaptcha',
    #'simplimg',
    #'todo',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
#LOGGING = {
#    'version': 1,
#    'disable_existing_loggers': False,
#    'filters': {
#        'require_debug_false': {
#            '()': 'django.utils.log.RequireDebugFalse'
#        }
#    },
#    'handlers': {
#        'mail_admins': {
#            'level': 'ERROR',
#            'filters': ['require_debug_false'],
#            'class': 'django.utils.log.AdminEmailHandler'
#        }
#    },
#    'loggers': {
#        'django.request': {
#            'handlers': ['mail_admins'],
#            'level': 'ERROR',
#            'propagate': True,
#        },
#    }
#}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True, #False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        },

    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'default': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': '/bhome/part3/02/palata-nk/ipr.palata-nk.ru/www/access.log',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 15,
            'formatter':'standard',
            },
        'request_handler': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': '/bhome/part3/02/palata-nk/ipr.palata-nk.ru/www/django.log',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 15,
            'formatter':'standard',
            },

        },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'ERROR',
            'propagate': True
        },
        'django.request': {
            #'handlers': ['mail_admins'],
            'handlers': ['request_handler'],
            'level': 'DEBUG',
            'propagate': False, #True,
        },
        }
}


#try:
#    from local import *
#except ImportError:
#    pass


TINYMCE_JS_URL = 'http://ipr.palata-nk.ru/media/tiny_mce/tiny_mce.js'
TINYMCE_DEFAULT_CONFIG={
'theme': "advanced",
'mode': "textareas",
'width': "512",
'theme_advanced_toolbar_location': "top",
'theme_advanced_toolbar_align': "left",
'theme_advanced_buttons1': "bold,italic,underline,table,separator,justifyleft,justifycenter,justifyright,justifyfull",
'theme_advanced_buttons2': "separator",
'theme_advanced_buttons3': "",
'theme_advanced_statusbar_location': "bottom",
'theme_advanced_resizing' : "true",
'extended_valid_elements' : "source[src|title|style|controls|id]"
}
