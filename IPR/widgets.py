# -*- coding: utf-8 -*-

# System libraries

# Third-party libraries
from recaptcha import captcha

# Django modules
from django import forms
from django.forms import widgets, CharField, ValidationError, BooleanField
from django.utils.translation import ugettext, ugettext_lazy
from django.utils.html import conditional_escape, format_html, format_html_join
from django.utils.encoding import force_text, python_2_unicode_compatible
from django.utils.safestring import mark_safe
from django.utils.encoding import smart_unicode

# Django apps

# Current-app modules
from IPR.settings import RECAPTCHA_PUBLIC_KEY, RECAPTCHA_PRIVATE_KEY
from education_programmes.models import *
from common_user.models import CommonText

FILE_INPUT_CONTRADICTION = object()


class MyClearableFileInput(widgets.FileInput):
    initial_text = ugettext_lazy('Currently')
    input_text = ugettext_lazy('Change')
    clear_checkbox_label = ugettext_lazy('Clear')

#    template_with_initial = '%(initial_text)s: %(initial)s %(clear_template)s<br />%(input_text)s: %(input)s'
    template_with_initial = u'%(initial)s %(hidden_input)s %(input)s'
#    template_with_initial = u'%(initial)s %(input)s'

    template_with_clear = u'%(clear)s <label for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</label>'

    def clear_checkbox_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '-clear'

    def clear_checkbox_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_id'

    def clear_hidden_input_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_hidden'

    def clear_hidden_input_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_id'

    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
            }
        template = '%(input)s'

        if value and hasattr(value, "url"):
            attrs['class'] = 'file_change'

        substitutions['input'] = super(MyClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            last_slash = force_text(value).rfind('/')
            template = self.template_with_initial

            media_slash = value.url.find('/media/')
            hidden_input_name = self.clear_hidden_input_name(name)
            hidden_input_id = self.clear_hidden_input_id(hidden_input_name)

            substitutions['initial'] = format_html(u'<table><tr><td><a class="file_link" for_item="{2}" href="{0}">{1}</a></td><td><i class="delete_loaded_file fi-x size-24" for_item="{2}" title="удалить загруженный файл" ></i></td></tr></table>',
                value.url[media_slash:],
                force_text(value)[last_slash+1:],hidden_input_id)

            substitutions['hidden_input'] = widgets.HiddenInput().render(hidden_input_name, value.url, attrs={'id': hidden_input_id})

        return mark_safe(template % substitutions)

    def value_from_datadict(self, data, files, name):
        upload = super(MyClearableFileInput, self).value_from_datadict(data, files, name)

        if not self.is_required and widgets.CheckboxInput().value_from_datadict(
            data, files, self.clear_checkbox_name(name)):
            if upload:
                return FILE_INPUT_CONTRADICTION
                # False signals to clear any existing value, as opposed to just None
            return False

#        if ( upload is None or upload =='' ) and data.has_key(self.clear_hidden_input_name(name)):
#            upload = data[self.clear_hidden_input_name(name)]

        return upload

class SelectInInput(widgets.TextInput):
    template_with_initial = '%(hidden_input)s <label class="selectininput" >%(value)s</label>'
#    template_with_initial = '%(hidden_input)s %(input)s'

    def clear_display_input_value(self, name, value):
        if name == "form":
            mode = StudyMode.objects.get(id=value)
            return mode.title
        elif name == "education_institute":
            mode = Education_Institute.objects.get(id=value)
            return mode.name
        elif name == "education_programm"  or name == "education_program":
            mode = Education_Programme.objects.get(id=value)
            return mode.name
        elif name == "payment_method":
            if PAYMENT_METHOD_DICT.has_key(value):
                return PAYMENT_METHOD_DICT[value]
            else:
                return value
        elif name == "region":
            mode = Region.objects.get(id=value)
            return mode.name
        elif name == "iprstudent":
            mode = IprStudent.objects.get(id=value)
            return mode.get_fio
        elif name == "membership":
            mode = MembershipCost.objects.get(id = value)
            return mode
        return value

    def clear_display_input_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_display'

    def clear_display_input_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_display_id'

    def render(self, name, value, attrs=None):
        substitutions = {}
        substitutions['hidden_input'] = widgets.HiddenInput().render(name, value, attrs=attrs)
        substitutions['value'] = self.clear_display_input_value(name,value)
        return mark_safe(self.template_with_initial % substitutions)


class PictureInput(widgets.TextInput):
    template_with_initial = u'%(input)s <i class="step fi-photo size-24" id="choose_picture" title="Выбрать иконку" ></i> </br> <i class="step %(picture)s size-48" id="current_picture"></i>'


    def clear_display_input_value(self, name, value):
        menuitem = MenuItem.objects.get(id=value)
        return menuitem.picture

    def clear_display_input_name(self, name):
        """
        Given the name of the file input, return the name of the clear checkbox
        input.
        """
        return name + '_display'

    def clear_display_input_id(self, name):
        """
        Given the name of the clear checkbox input, return the HTML id for it.
        """
        return name + '_id'

    def render(self, name, value, attrs=None):
        substitutions = {}
        substitutions['input'] = widgets.TextInput().render(name, value, attrs={'id':self.clear_display_input_id(name),'readonly':True})
        substitutions['picture'] = value

        return mark_safe(self.template_with_initial % substitutions)

class CheckboxIPRStyle(widgets.CheckboxInput):
    html_code = """<div class="slider_item_main_checkbox %(checked)s"> %(checkbox)s </div>"""

    def render(self, name, value, attrs=None):
        substitutions = {}
        substitutions['checkbox'] = widgets.CheckboxInput().render(name, value, attrs={'style':"display: none"})
        if value:
            substitutions['checked'] = "checked"
        else:
            substitutions['checked'] = ""

        return mark_safe(self.html_code % substitutions)

class PrivatePoliticCheckboxField(BooleanField):
    def __init__(self, *args, **kwargs):
        kwargs['label'] = u'Я согласен на <a class="private_politic_link" target="_blank" href="%s">обработку персональных данных</a>' % CommonText.url('PP')
        kwargs['label'] = u"""Даю согласие на обработку Палатой налоговых консультантов (г.Москва, ул. Земляной вал, д.4, стр.1) моих персональных данных, указанных при регистрации в Информационно-поисковом ресурсе налогового
        консультанта на сайте www.ipr.palata-nk.ru на неограниченный срок с целью организации моей подготовки (повышения квалификации) по программам Палаты налоговых консультантов. Обработка персональных
        данных включает сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование в уставных целях, уничтожение."""
        super(PrivatePoliticCheckboxField, self).__init__(*args, **kwargs)

class GReCaptchaWidget(widgets.TextInput):
    html_code = u"<div class='g-recaptcha' data-sitekey='%(sitekey)s'></div>"
    g_recaptcha_response = "g-recaptcha-response"

    def render(self, name, value, attrs=None):
        substitutions = {}
        substitutions['sitekey'] = RECAPTCHA_PUBLIC_KEY
        return mark_safe(self.html_code % substitutions)

    def value_from_datadict(self, data, files, name):
        return [data.get(self.g_recaptcha_response, None),]

class GReCaptchaField(CharField):
    default_error_messages = {
        'captcha_invalid': u"Докажите, что вы не робот"
    }

    def __init__(self, *args, **kwargs):
        self.widget   = GReCaptchaWidget
        self.required = False
        super(GReCaptchaField, self).__init__(*args, **kwargs)

    def clean(self, values):
        super(GReCaptchaField, self).clean(values[0])
        g_recaptcha_response = smart_unicode(values[0])
        check_captcha = captcha.g_submit(g_recaptcha_response, RECAPTCHA_PRIVATE_KEY, {})

        if not check_captcha.is_valid:
            raise ValidationError(self.error_messages['captcha_invalid'])
        return values[0]





