# -*- coding: utf-8 -*-
__author__ = 'andrei'

from common_user.models             import *
from datetime                       import datetime,time,timedelta
from django.contrib.auth.decorators import login_required
from django.core.exceptions         import ObjectDoesNotExist
from django.core.mail               import send_mail, EmailMessage
from django.http                    import HttpResponseRedirect, HttpResponse
from django.shortcuts               import render_to_response, get_object_or_404, redirect
from django.template                import RequestContext, Context
from django.template.loader         import get_template
from education_programmes.models    import *
import IPR.settings as settings
from IPR.utils import word_generator
import json
from django.core.serializers.json import DjangoJSONEncoder


def home(request):
    man          = None
    noconfirmed  = None
    now          = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
    programs     = Education_Programme.objects.all()
    staticpages  = News.objects.filter(is_archive = False,is_static = True).order_by('-created')
    news         = News.objects.filter(is_archive= False,simple_banner=False,main_banner=False,is_static = False).order_by('-created')[:2]
    banners      = News.objects.filter(is_archive= False,simple_banner=True,main_banner=False).order_by('-created')[:2]
    recruits     = Recruitment.objects.filter(begin_date__gte = now,is_archive = False,institute_program__is_archive = False,institute_program__program__is_archive = False,institute_program__institute__is_archive = False,institute_program__institute__accreditation = 'A').order_by('begin_date')[:2]

    if request.user.is_authenticated():
        if request.user.is_ipr:
            try:
                man = IprStudent.objects.get(user = request.user)
            except ObjectDoesNotExist:
                return redirect('revival')
#                return render_to_response("not_found.html",{},context_instance=RequestContext(request))
        elif request.user.is_student:
            try:
                man = IprStudent.objects.get(user = request.user)
            except ObjectDoesNotExist:
                return redirect('revival')
                return render_to_response("not_found.html",{},context_instance=RequestContext(request))
        elif request.user.is_curator:
            try:
                man = Education_Institute.objects.get(curator = request.user) #InstituteCurator.objects.get(user = request.user)
            except ObjectDoesNotExist:
                return render_to_response("not_found.html",{},context_instance=RequestContext(request))
        elif request.user.is_admin:
            noconfirmed = IprStudent.objects.filter(confirmed = False)
    return render_to_response("index.html",{'man':man,'noconfirmed':noconfirmed,'programs':programs, 'news':news,'recruits':recruits,'banners':banners,'staticpages':staticpages},context_instance=RequestContext(request))

def region_statistic(request,raphael_slug):
    rows = []
    cursor = connection.cursor()

    if raphael_slug == 'Russia':
        cursor.execute("""SELECT DISTINCT pei.id
            FROM education_programmes_education_institute as pei
            JOIN education_programmes_city as pc ON pei.city_id = pc.id
            JOIN education_programmes_education_institute_program as epeip
            ON pei.id = epeip.institute_id
            where pc.is_archive = 0
            and pei.accreditation = 'A'
            AND pei.is_archive = 0
            AND epeip.is_archive = 0
            order by pei.name""")

    else:
        region = get_object_or_404(Region,raphael_slug=raphael_slug)

        cursor.execute("""SELECT DISTINCT pei.id
            FROM education_programmes_education_institute as pei
            JOIN education_programmes_city as pc ON pei.city_id = pc.id
            JOIN education_programmes_education_institute_program as epeip
            ON pei.id = epeip.institute_id
            where pc.region_id = %s
            and pc.is_archive = 0
            and pei.accreditation = 'A'
            AND pei.is_archive = 0
            AND epeip.is_archive = 0
            order by pei.name""",[region.id])

    try:
        l_count = len( cursor.fetchall() )
    except:
        l_count = 0

    rows.append({"text":u'Аккредитованно образовательных организаций: ',"count":l_count})
    return HttpResponse(json.dumps(rows),content_type='application/json')
    
def archive(request,_type):
    if _type == 'region':
        ItemModel = Region
    elif _type == 'city':
        ItemModel = City
    elif _type == 'program':
        ItemModel = Education_Programme
    elif _type == 'institute':
        ItemModel = Education_Institute
    elif _type == 'group':
        ItemModel = Education_Institute
    return HttpResponse(json.dumps({}),content_type='application/json')


@login_required
def notifications(request):
    return render_to_response('notifications.html',{},context_instance=RequestContext(request))


@login_required
def mail2(request):
#    print 'Ba'
    if request.user.is_admin:
#        print 'asd'
        edu_request = EducationRequest.objects.filter(student__isnull=False,institute_program__isnull=False,institute_program__institute__isnull=False,institute_program__program__isnull=False)

        fio             = u'Паршивлюк Сергей Анатолиевич'
        program_name    = u'Вся солнечная энергия - рабочим!'
        institute_name  = u'Судебная власть в Росии XIX века'
        id              = 23

        if len(edu_request) > 0:
            need = edu_request[0]
            fio = need.student.get_fio
            program_name = need.institute_program.program.name
            institute_name = need.institute_program.institute.name
            id              = need.id

        email = EmailMessage(subject = u'Новое заявление на обучение (ПНК)',
            body = get_template('mail/ipr_example.html').render(
                Context({
                    'institute_name': institute_name,
                    'program_name': program_name,
                    'fio':fio,
                    'id':id
                })),
            from_email = settings.EMAIL_HOST_USER_ALIAS,
            to= ['khriptoni@gmail.com','consument@inbox.ru'],
#            'web-OpImKp@mail-tester.com'
            headers = {'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

        #        email.attach(edurequest.pdf_title(),edurequest.pdf_content(),'application/pdf')
        email.content_subtype = 'html'
        email.send(fail_silently = False)
        return HttpResponse(json.dumps({"Finish":"ok"}),content_type='application/json')

    else:
        return redirect('personal_cabinet')

#@login_required
def mail(request):
    new_users = User.objects.filter(is_olympic = False, is_student = True)
    new_user = new_users[0]
    password = word_generator(10)

    return render_to_response('mail/registration_user_upd.html',{'user':new_user,'password':password},context_instance=RequestContext(request))

#    new_iprs = User.objects.filter(is_olympic = False, is_ipr = True)
#    new_ipr = new_iprs[0]
#    password = word_generator(10)
#
#    return render_to_response('mail/registration_ipr_upd.html',{'user':new_ipr,'password':password},context_instance=RequestContext(request))

#    edu_request = EducationRequest.objects.filter(student__isnull=False,institute_program__isnull=False,institute_program__institute__isnull=False,institute_program__program__isnull=False)
#
#    fio             = u'Паршивлюк Сергей Анатолиевич'
#    program_name    = u'Вся солнечная энергия - рабочим!'
#    institute_name  = u'Судебная власть в Росии XIX века'
#    id              = 23
#
#    if len(edu_request) > 0:
#        need = edu_request[0]
#        fio = need.student.get_fio
#        program_name = need.institute_program.program.name
#        institute_name = need.institute_program.institute.name
#        id              = need.id
#
#    return render_to_response('mail/ipr_example.html',{'fio':fio,'program_name':program_name,'institute_name':institute_name,'id':id},context_instance=RequestContext(request))

@login_required
def mail_recovery_request(request):
    recovery_request = Recovery.objects.filter(user__isnull=False)

    fio             = u'Паршивлюк Сергей Анатолиевич'
    recovery_url    = request.get_host()

    if len(recovery_request) > 0:
        need = recovery_request[0]
        fio  = need.user.get_full_name()
        recovery_url = need.path

    print recovery_url

    if recovery_url.find('http://') < 0:
        recovery_url = 'http://' + recovery_url

    print recovery_url

    return render_to_response('mail/recovery_request.html',{'fio':fio,'recovery_url':recovery_url},context_instance=RequestContext(request))

@login_required
def abc(request):
    edurequest = EducationRequest.objects.filter(student__isnull=False,institute_program__isnull=False,institute_program__institute__isnull=False,institute_program__program__isnull=False)[0]

    ld_pdf_data = {'pdf_title':edurequest.pdf_title()}

    # email = EmailMessage(u'Новое заявление на обучение (ПНК)',
    #                      get_template('mail/new_edurequest_message.html').render(
    #                          Context({
    #                              'institute_name': institute.name,
    #                              'program_name': program.name,
    #                              'fio': user.get_full_name()
    #                          })),
    #                      settings.EMAIL_HOST_USER,
    #                      [institute.email], [],
    #                      headers={})
    #
    # email.attach(edurequest.pdf_title(), edurequest.pdf_content(), 'application/pdf')
    # email.send(fail_silently=True)

    return HttpResponse(json.dumps(ld_pdf_data, cls=DjangoJSONEncoder), content_type='application/json')

#    return render_to_response('mail/ipr_example.html',{'fio':fio,'program_name':program_name,'institute_name':institute_name,'id':id},context_instance=RequestContext(request))

@login_required
def abc2(request):
    edurequest = EducationRequest.objects.get(id=16346)

    institute = edurequest.institute_program.institute
    program = edurequest.institute_program.program
    user = edurequest.student.user

    email = EmailMessage(u'Новое заявление на обучение (ПНК)',
            get_template('mail/new_edurequest_message.html').render(
                              Context({
                                  'institute_name': institute.name,
                                  'program_name': program.name,
                                  'fio': user.get_full_name()
                              })),
                          settings.EMAIL_HOST_USER_ALIAS,
                          ['khriptoni@gmail.com'], [],
                         headers={'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.attach(edurequest.pdf_title(), edurequest.pdf_content(), 'application/pdf')
    email.send(fail_silently=True)

    return HttpResponse(json.dumps({"Finish": "ok"}), content_type='application/json')
