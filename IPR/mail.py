# -*- coding: utf-8 -*-
from django.core.mail import send_mail, EmailMessage
from django.template import Context, RequestContext
from django.template.loader import get_template
import IPR.settings as settings

EMAIL_TEMPLATES = {
    'recovery_message.html' : u'Восстановление пароля',
    'newpass_message.html' : u'Новый пароль на Информационно-поисковом ресурсе',
    'newmail_message.html' : u'Новый email/логин на Информационно-поисковом ресурсе',
    'registration_user_message.html' : u'Регистрация слушателя',
    'registration_ipr_message.html' : u'Регистрация налогового консультанта',
    'new_edurequest_message.html' : u'Новое заявление на обучение (ПНК)',
    'new_edurequest_upgrade_message.html' : u'Новое заявление на обучение (ПНК), улучшенное',
    }

def recovery_mail(model_item):
    send_mail(
        u'Восстановление пароля',
        get_template('mail/recovery_message.html').render(
            Context({
                'fio': model_item.user.get_full_name(),
                'recovery_url': model_item.path
                })
            ),
        settings.EMAIL_HOST_USER_ALIAS,
        [model_item.user.email],
        fail_silently = True
        )


def newpass_mail(user,password):
    email = EmailMessage(subject = u'Новый пароль на Информационно-поисковом ресурсе',
        body = get_template('mail/newpass_upd.html').render(
            Context({
                'user': user,
                'password': password
                })),
        from_email = settings.EMAIL_HOST_USER_ALIAS,
        to= [user.email],
        headers = {'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.content_subtype = 'html'
    email.send(fail_silently = False)

    return True

def newmail_mail(user):
    send_mail(
        u'Новый email/логин на Информационно-поисковом ресурсе',
        get_template('mail/newmail_message.html').render(
            Context({
                'fio': user.get_full_name(),
                'email': user.email
            })
        ),
        settings.EMAIL_HOST_USER_ALIAS,
        [user.email],
        fail_silently = True
    )

def student_registration_mail(user,password):
    send_mail(
        u'Регистрация в инфо-ресурсе пройдена',
        get_template('mail/registration_user_message.html').render(
            Context({
                'fio': user.get_full_name(),
                'email': user.email,
                'password' : password
                })
            ),
        settings.EMAIL_HOST_USER_ALIAS,
        [user.email],
        fail_silently = True
        )

def student_registration_mail_upd(user,password):
    email = EmailMessage(subject = u'Регистрация в Информационно-поисковом ресурсе ПНК',
        body = get_template('mail/registration_user_upd.html').render(
            Context({
                'user': user,
                'password': password
            })),
        from_email = settings.EMAIL_HOST_USER_ALIAS,
        to= [user.email],
        headers = {'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.content_subtype = 'html'
    email.send(fail_silently = False)

    return True

def ipr_registration_mail(user,password):
    send_mail(
        u'Регистрация в инфо-ресурсе пройдена',
        get_template('mail/registration_ipr_message.html').render(
            Context({
                'fio': user.get_full_name(),
                'email': user.email,
                'password' : password
                })
            ),
        settings.EMAIL_HOST_USER_ALIAS,
        [user.email],
        fail_silently = True
        )

def ipr_registration_mail_upd(user,password):
    email = EmailMessage(subject = u'Регистрация в Информационно-поисковом ресурсе ПНК',
        body = get_template('mail/registration_ipr_upd.html').render(
            Context({
                'user': user,
                'password': password
            })),
        from_email = settings.EMAIL_HOST_USER_ALIAS,
        to= [user.email],
        headers = {'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.content_subtype = 'html'
    email.send(fail_silently = False)

    return True


def new_edurequest_mail(edurequest):
    institute = edurequest.institute_program.institute
    program   = edurequest.institute_program.program
    user      = edurequest.student.user

    email = EmailMessage(u'Новое заявление на обучение (ПНК)',
        get_template('mail/new_edurequest_message.html').render(
            Context({
                'institute_name': institute.name,
                'program_name': program.name,
                'fio': user.get_full_name()
            })),
        settings.EMAIL_HOST_USER_ALIAS,
        [institute.email], [],headers={'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.attach(edurequest.pdf_title(),edurequest.pdf_content(),'application/pdf')
    email.send(fail_silently = True)


def test_edurequest_mail(institute,program,user,edurequest):
    email = EmailMessage(u'Новое заявление на обучение (ПНК)',
        get_template('mail/new_edurequest_upgrade_message.html').render(
            Context({
                'institute_name': institute.name,
                'program_name': program.name,
                'fio': user.get_full_name()
                })),
        settings.EMAIL_HOST_USER_ALIAS,
        ['khriptoni@gmail.com'], [],headers={'Reply-To': settings.EMAIL_HOST_USER_ALIAS})

    email.attach(edurequest.pdf_title(),edurequest.pdf_content(),'application/pdf')
    email.content_subtype = 'html'
    email.send(fail_silently = True)
