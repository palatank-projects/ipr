# -*- coding: utf-8 -*-
from django.contrib.auth import logout
from django.core.context_processors import csrf
from django.core.urlresolvers import resolve
from education_programmes.models import Education_Institute, News, MenuItem
from user_messages.models import Message
from common_user.models import IprStudent
import settings, sys


def determine_current_menu_item(request):
    match = resolve(request.path)
    claim = 0
    curator = None
    main_banners = News.objects.filter(main_banner = True,is_archive = False).order_by('created')
    if len(main_banners) > 0:
        main_banner = main_banners[0]
    else:
        main_banner = None

    attics = News.objects.filter(is_attic = True,is_archive = False).order_by('created')
    cellars = News.objects.filter(is_cellar = True,is_archive = False).order_by('created')
    sidenews = News.objects.filter(is_archive= False,simple_banner=False,main_banner=False,is_static = False).order_by('-order','-created')[:3]
    menuitems = MenuItem.objects.filter(is_archive = False)

#    c = dict(http_host   = request.path,match = match.url_name)
    c = {"http_host":request.path,"match": match.url_name,"main_banner":main_banner,"cellars":cellars,"attics":attics,"sidenews":sidenews,"menuitems":menuitems}
    
    if request.user.is_authenticated() and request.user.is_admin == True:
        noconfirmed = IprStudent.objects.filter(user__is_ipr = True,confirmed = False,rejected = False)
        claim = len(noconfirmed) #+ len(nosigned)
#        c = dict(http_host   = request.path,match = match.url_name,claim = claim)
        c["claim"] = claim
    if request.user.is_authenticated() and request.user.is_curator == True:
        try:
            curator = Education_Institute.objects.get(curator = request.user)
            c["curator"] = curator
        except:
            older_curator = None
            try:
                older_curator = User.objects.get(id = request.user.id )
            except :
                None
            if older_curator is not None:
                older_curator.delete()
            logout(request)

    if request.user.is_authenticated():
        messages = Message.objects.filter(msg_read = False,user_receiver = request.user)
#        c = dict(new_message_count = messages.count())
        c["new_message_count"] = messages.count()

    c.update(csrf(request))
    return c

def host(request):
    return {'HOST': request.get_host()}

def my_media_url(request):
    my_media_url = settings.MEDIA_URL
    if not my_media_url.endswith('/'):
        my_media_url += '/'
    return {'MEDIA_URL': my_media_url}