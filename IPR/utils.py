# -*- coding: utf-8 -*-
# System libraries
import os, sys, string,random

# Third-party libraries

# Django modules
from django.db.models import FileField
from django.utils.encoding import force_text

# Django apps

# Current-app modules
import IPR.settings as settings

def unique_email(i_email):
    from common_user.models            import User
    try:
        user_exist = User.objects.get(email = i_email, is_olympic = False)
        return {"exist": True,"id": user_exist.id,"user":user_exist}
    except:
        return {"exist": False,"id": None}

def get_from_dict(i_value,i_dict = {},i_fieldname = '', i_get_int = False, i_maybe_negative = False, default_value = u'-1'):
    if i_dict.has_key(i_fieldname) and  ( i_maybe_negative or i_dict[i_fieldname] != default_value ):
        if i_get_int:
            try:
                r_value = int(i_dict[i_fieldname])
            except :
                r_value = None
        else:
            r_value = i_dict[i_fieldname]
    else:
        r_value = i_value
    return r_value

def get_date_from_dict(i_dict,i_fieldname):
    result = {'day':None,'month':None,'year':None}
    result['day']           = get_from_dict(result['day'],i_dict,i_fieldname + '_day',False,False,u'0')
    result['month']         = get_from_dict(result['month'],i_dict,i_fieldname + '_month',False,False,u'0')
    result['year']          = get_from_dict(result['year'],i_dict,i_fieldname + '_year',False,False,u'0')
    return result


def like_operand(i_sql_attribute,i_value,i_with_operator = False,i_operator="AND"):
    l_result = " "
    if i_with_operator:
        l_result = " " + i_operator + " "
    l_result = l_result + i_sql_attribute + " LIKE '%%" + i_value + "%%'"
    return l_result

def get_file_content(directory,file_name):
    file_path    = os.path.join(settings.PROJECT_ROOT,directory + file_name)
    try:
        file_obj     = open(file_path,"r+")
        file_content = file_obj.read()
        file_obj.close()
    except IOError:
        file_content = ''

    return file_content

def set_file_content(directory,file_name,content):
    file_path    = os.path.join(settings.PROJECT_ROOT,directory + file_name)
    file_obj     = open(file_path,"w+")
    file_obj.write(content)
    file_obj.close()
    return True

#загрузка файла
def handle_uploaded_file(f, filename,dirname):
    path_name = dirname  + filename
    root,ext = os.path.splitext(filename)

    if os.path.exists(path_name):
        i=1
        while True:
            name = dirname  + root + '('+str(i)+')'+ext
            if not os.path.exists(name):
                path_name = name
                filename = root + '('+str(i)+')'+ext
                break
            i+=1

    destination = open(path_name, 'wb+')

    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

    return dirname+filename

def directory_cleaning(dirname):
    for filename in os.listdir(dirname):
        try:
            full_filename = os.path.join(dirname,filename) #dirname + filename
            os.remove(full_filename)
        except:
            print str(sys.exc_info())

def upload_file(request,directory,model_field,files_key,id=None):
    if files_key is None or not request.FILES.has_key(files_key):
        return model_field

    absolute_dirname = settings.PROJECT_ROOT + directory
    if not os.path.exists(absolute_dirname):
        os.mkdir(absolute_dirname)
    if id is not None:
        absolute_dirname = absolute_dirname + str(id) + u'/'
        if not os.path.exists(absolute_dirname):
            os.mkdir(absolute_dirname)

    directory_cleaning(absolute_dirname)
    url = handle_uploaded_file(request.FILES[files_key],
                               request.FILES[files_key].name.encode("utf-8"),
                               absolute_dirname.encode("utf-8"))

    r_model_field = model_field
    if url != 0:
        r_model_field = url
    return r_model_field

def custom_deleting(iFieldFile, iSave=True):
    if not iFieldFile:
        return
    # Only close the file if it's already open, which we know by the
    # presence of self._file
    if hasattr(iFieldFile, '_file'):
        iFieldFile.close()
        del iFieldFile.file

    last_slash = force_text(iFieldFile.name).rfind('/')
    lvDirName = force_text(iFieldFile.name)[:last_slash + 1]

    for filename in os.listdir(lvDirName):
        try:
            full_filename = os.path.join(lvDirName, filename)
            os.remove(full_filename)
        except:
            pass

    iFieldFile.name = None
    setattr(iFieldFile.instance, iFieldFile.field.name, iFieldFile.name)
    iFieldFile._committed = False

    if iSave:
        iFieldFile.instance.save()


def delete_loaded_file(request,model_field,files_key):
    if not model_field or not files_key:
        return model_field

    if request.FILES.has_key(files_key):
        return model_field

    hiddenFileNameLabel = files_key + '_hidden'
    hiddenFileNameValue = None
    hiddenFileNameValue = get_from_dict(hiddenFileNameValue, request.POST, hiddenFileNameLabel)

    if not hiddenFileNameValue:
        try:
            lvFilePath = model_field.name

            if model_field.storage and model_field.storage.exists(lvFilePath):
                model_field.delete(save=False)
            else:
                custom_deleting(model_field, iSave=False)
        except:
            pass
    return model_field


def divide_into_columns( db_data ):
    letter = None
    data_list = []
    l_letter_count = 0

    for item in db_data:
        item_dict = {}
        if letter != item[0]:
            letter = item[0]
            l_letter_count = l_letter_count + 1
            data_list.append({"letter":letter,"row_type":"L"})

        item_dict =  {"letter":letter,"row_type":"I","id":item[1],"name":item[2],"slug":item[3]}
        if len( item ) > 4:
            item_dict["region_slug"] = item[4]
        data_list.append(item_dict)

    len_data = len(data_list)

    if len_data%3 == 2:
        len_data = len_data + 1

    data_divisor = len_data/3

    column_first  = []
    column_second = []
    column_third  = []

    if len(data_list) < 10:
        column_first = regions_list
        column_second = []
        column_third = []
    else:
        column_first  = data_list[:data_divisor]
        column_second = data_list[data_divisor:data_divisor*2]
        column_third  = data_list[data_divisor*2:min(data_divisor*3,len(data_list))]
    return zip(column_first,column_second, column_third)

def is_address_field_no_empty(source_field,merge_flag):
    dest_field = u''
    if source_field is not None and not source_field.isspace():
        merge_flag = True
        dest_field = source_field
    return dest_field, merge_flag

def word_generator(size=16, chars=string.ascii_lowercase + string.ascii_uppercase):
    return ''.join(random.choice(chars) for x in range(size))

