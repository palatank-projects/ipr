# -*- coding: utf-8 -*-
import sys, os

PYTHONPATH= os.path.dirname(__file__)
sys.path.insert(0, os.path.dirname(__file__))
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/xhtml2pdf-0.0.5-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/html5lib-1.0b3-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/pyPdf-1.13-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/Pillow-2.2.2-py2.7-freebsd-8.3-STABLE-amd64.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/reportlab-2.7-py2.7-freebsd-8.3-STABLE-amd64.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/six-1.4.1-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/python_slugify-0.0.6-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/Unidecode-0.04.14-py2.7.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/reportlab-2.7-py2.7-freebsd-8.3-STABLE-amd64.egg')
sys.path.insert(0, '/bhome/part3/02/palata-nk/lib/virtualenv/lib/python2.7/site-packages/cleanweb-2.0-py2.7.egg-info')


os.environ.setdefault("PYTHONPATH", "/bhome/part3/02/palata-nk/lib")
os.environ.setdefault("LD_LIBRARY_PATH","/bhome/part3/02/palata-nk/lib")
# Set the DJANGO_SETTINGS_MODULE environment variable.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IPR.settings")
os.environ['PYTHON_EGG_CACHE'] = "/bhome/part3/02/palata-nk/lib"

from ctypes import *
lib1 = cdll.LoadLibrary('/bhome/part3/02/palata-nk/lib/libmysqlclient.so.18')

for path in sys.path:
    print path


for k in os.environ: 
    print "%s: %s" % (k, os.environ[k]) 

import django
django.setup()

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
