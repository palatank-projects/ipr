# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'IPR.views.home', name='home'),
    url(r'^favicon.ico$', RedirectView.as_view(url='/media/img/favicon_pnk.ico', permanent=True), name='favicon'),

    url(r'^simplimg/', include('simplimg.urls')),
    url(r'^olymp/', include('olymp.urls')),
    url(r'^edu/', include('education_programmes.urls')),
#    url(r'^mail/recovery/request/?$', 'IPR.views.mail_recovery_request',  name='mail-example-recovery-request'),
#    url(r'^mail2/?$', 'IPR.views.mail2',  name='mail-example-to'),
    url(r'^mail/?$', 'IPR.views.mail',  name='mail-example'),
    url(r'^mail2/?$', 'IPR.views.mail2',  name='mail-example-to'),

    url(r'^abc/?$', 'IPR.views.abc',  name='abc'),
    url(r'^abc2/?$', 'IPR.views.abc2',  name='abc-to'),

    url(r'^auth/notifications/?$', 'IPR.views.notifications',  name='notifications'),
    url(r'^auth/', include('common_user.urls')),
    #url(r'^todo/', include('todo.urls')),
    url(r'^msg/',include('user_messages.urls')),
	url(r'^ajax/region/statistic/(?P<raphael_slug>\w+)/?$','IPR.views.region_statistic', name='ajax_region_statistic'),

    url(r'^ajax/regions/?$','education_programmes.views.get_regions', name='ajax_get_regions'),
    url(r'^ajax/cities/?$','education_programmes.views.get_cities', name='ajax_get_cities'),
    url(r'^ajax/programs/?$','education_programmes.views.get_programm', name='ajax_get_programms'),
    url(r'^ajax/institutes/$','education_programmes.views.get_university', name='ajax_get_unvirsity_by_programm'),
    url(r'^ajax/has_seminar/$','education_programmes.views.get_seminar_attribute', name='ajax_seminar_attribute'),

    url(r'^archive/regions/?$',  'IPR.views.archive',  {'_type':'region'},    name='archive_regions'),
    url(r'^archive/cities/?$',   'IPR.views.archive',  {'_type':'city'},      name='archive_cities'),
    url(r'^archive/programs/?$', 'IPR.views.archive',  {'_type':'program'},   name='archive_programs'),
    url(r'^archive/institutes/$','IPR.views.archive',  {'_type':'institute'}, name='archive_unvirsity'),
    url(r'^archive/groups/$',    'IPR.views.archive',  {'_type':'group'},     name='archive_groups'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/?', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.PROJECT_ROOT + '/media/'}),
#    url(r'^favicon.ico$', RedirectView.as_view(url=settings.PROJECT_ROOT + '/media/images/favicon.ico')),

    url(r'^(?P<slug>[-\w\.]+)/?$','education_programmes.views.common_text', name='common-text-view'),
)
