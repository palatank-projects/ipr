# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models.fields.related import ForeignKey
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404,redirect
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json as simplejson
from education_programmes.models import *
from education_programmes.forms import *
from forms import *
from models import *
import sys, json, string
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from IPR.mail import *
from IPR.utils import get_from_dict,word_generator
from django.utils import timezone

# Подача заявления на оубчения с одновременной регистрацией слушателя
@csrf_protect
def register_student(request):
    template = 'registration.html'
    if request.user.is_authenticated():
        return HttpResponseRedirect(get_url_redirect(request.user))
    #print request.POST
    if request.method == "POST":
#        if request.POST.has_key('make_nalog'):
#            request.session['id_institute']  = request.POST['university']
#            request.session['id_programm']   = request.POST['programm']
#
#            form = RegisterIprStudentForm(initial={'edu_institute':request.session['id_institute'],'edu_programm':request.session['id_programm']})
#            if form.is_valid():
#                form.save()
#                name        = form.cleaned_data['name']
#                patronymic  = form.cleaned_data['patronymic']
#                surname     = form.cleaned_data['surname']
#                fio = surname.capitalize() + u' ' + name.capitalize() + u' ' + patronymic.capitalize()
#                return render_to_response(template,{'registration_done':'true','fio':fio,'type':'student'},context_instance=RequestContext(request))
#            else:
#                return render_to_response(template,{'form':form,'type':'student'},context_instance=RequestContext(request))
##            return render_to_response(template, {'form':form,'form_action':form_action,'type':'student'},context_instance=RequestContext(request))
        if request.POST.has_key('student_request'):
            #print 'student_request'
            request.session['id_institute'] = request.POST['university']
            request.session['id_programm'] = request.POST['programm']
            form = RegisterStudentForm(initial={'edu_institute':request.session['id_institute'],'edu_programm':request.session['id_programm']})
            return render_to_response(template,{'form':form,'type':'student'},context_instance=RequestContext(request))

        else:
            form = RegisterStudentForm(request.POST,initial={'edu_institute':request.session['id_institute'],'edu_programm':request.session['id_programm']})
            if form.is_valid():
                form.save()
                name        = form.cleaned_data['name']
                patronymic  = form.cleaned_data['patronymic']
                surname     = form.cleaned_data['surname']
                fio = surname.capitalize() + u' ' + name.capitalize() + u' ' + patronymic.capitalize()
                return render_to_response(template,{'registration_done':'true','fio':fio,'type':'student'},context_instance=RequestContext(request))
            else:
                return render_to_response(template,{'form':form,'type':'student'},context_instance=RequestContext(request))
    else:
        return redirect('home')

# Регистрация налогового консультанта
@csrf_protect
def register_consult(request):
    template = 'registration.html'
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    if request.POST:
        #print request.POST
        form = RegisterConsultForm(request.POST)
        if form.is_valid():
            form.save()
            name        = form.cleaned_data['name']
            patronymic  = form.cleaned_data['patronymic']
            surname     = form.cleaned_data['surname']
            fio = surname.capitalize() + u' ' + name.capitalize() + u' ' + patronymic.capitalize()
            return render_to_response(template,{'registration_done':'true','fio':fio,'type':'consult'},context_instance=RequestContext(request))
        else:
            return render_to_response(template,{'form':form,'type':'consult'},context_instance=RequestContext(request))
    else:
        form = RegisterConsultForm()
        return render_to_response(template,{'form':form,'type':'consult'},context_instance=RequestContext(request))

# создание нового запроса на обучение авторизованного человека
@login_required
@csrf_protect
def create_edu_request(request):
    if not request.user.is_ipr and not request.user.is_student:
        return HttpResponseRedirect(get_url_redirect(request.user))
    if request.method == 'POST':
        #print "GO!"
        if request.POST.has_key('make_nalog') or request.POST.has_key('student_request'):
            #print "MO!"
            edu_institute = get_object_or_404(Education_Institute,id=request.POST['university'])
            edu_programme = get_object_or_404(Education_Programme,id=request.POST['programm'])
            institute_program = get_object_or_404(Education_Institute_Program,institute = edu_institute,program = edu_programme)

            if institute_program.has_seminar == True:
                form = RequestSeminarForm(initial={'edu_institute':edu_institute,'edu_programm':edu_programme})
                return render_to_response('seminar_form.html',{'Form':form,'institute_program':institute_program},context_instance=RequestContext(request))

        elif request.POST.has_key('seminar'):
            institute_program = get_object_or_404(Education_Institute_Program,id = request.POST['institute_program'])
            form = RequestSeminarForm(request.POST)

            if form.is_valid():
                student      = IprStudent.objects.get(user = request.user)

                if not student.user.has_edu_request:
                    student.user.has_edu_request = True
                    student.user.save()
                    student.save()

                    new_request = EducationRequest(student = student,institute_program = institute_program) #,date_request = date_request)
                    new_request.practic_period = form.cleaned_data['practic_period']
                    new_request.payment_method = form.cleaned_data['payment_method']
                    if new_request.payment_method == 'U':
                        new_request.details = form.cleaned_data['details']

                    new_request.copy_attestat_data( )
                    new_request.is_frash = True

                    new_request.save()

                    try:
                        new_edurequest_mail(new_request)
                    except :
                        print str(sys.exc_info())

                    return redirect('personal_cabinet')
                else:
                    student.have_bad_try = True
                    student.save()
                    return redirect('personal_cabinet')

            else:
                return render_to_response('seminar_form.html',{'Form':form,'institute_program':institute_program},context_instance=RequestContext(request))
        else:
            edu_institute = None
            edu_programme = None
            institute_program = None

        if edu_institute is not None and edu_programme is not None and institute_program is not None:
            student      = IprStudent.objects.get(user = request.user)

            if not student.user.has_edu_request:
                student.user.has_edu_request = True
                student.user.save()
                student.save()

                new_request = EducationRequest(student = student,institute_program = institute_program) #,date_request = date_request)
                new_request.is_frash = True

                new_request.copy_attestat_data()
                new_request.save()

                try:
                    new_edurequest_mail(new_request)
                except :
                    print str(sys.exc_info())

                return redirect('personal_cabinet')
            else:
                student.have_bad_try = True
                student.save()
                return redirect('personal_cabinet')
        else:
            return HttpResponse(u'При создании запроса на повышение квалификации произошла ошибка, сообщите администратору.<a href="/auth/personal">Перейти в Личный кабинет</a>')
    else:
        return redirect('personal_cabinet')
    return redirect('personal_cabinet')

def get_url_redirect(user):
    return 'personal_cabinet' #'/account/personal'

# персональный кабинет
@login_required
def personal_cabinet(request,template):
    if request.user.is_admin:
        if request.user.is_olesya:
            # ====================== Olesya =============================
            year = date.today().year

#            membershipcosts = MembershipCost.objects.all()
            membershipcosts = MembershipCost.objects.values_list('region', flat=True).distinct()
            regions = Region.objects.filter(id__in = membershipcosts, is_archive = False).order_by('name')

            membershipcost_forms = []

#            for membershipcost in membershipcosts:
#                membershipcost_form = MembershipCostChangeForm(instance = membershipcost)
#                membershipcost_forms.append({"form":membershipcost_form,"model":membershipcost})

            membershipbenefit_forms = []

            for membership_benefit in MembershipBenefit.objects.all():
                membershipbenefit_form = MembershipBenefitForm(instance = membership_benefit)
                membershipbenefit_forms.append({"form":membershipbenefit_form,"model":membership_benefit})

            return render_to_response(template,{"regions":regions,"membershipcosts": membershipcosts,"membershipbenefit_forms": membershipbenefit_forms},context_instance=RequestContext(request))
        else:
            # ====================== ADMIN =============================
            cities = City.objects.filter(is_archive = False).order_by('name')
            regions = Region.objects.filter(is_archive = False).order_by('name')
            news  = News.objects.filter(is_archive = False,is_static = False).order_by('-main_banner','-simple_banner','-order','-created')
            staticpages  = News.objects.filter(is_archive = False,is_static = True).order_by('-created')
            return render_to_response(template,{"staticpages":staticpages,"news":news,"cities":cities,"regions": regions},context_instance=RequestContext(request))

    elif request.user.is_curator:
        # ====================== CURATOR =============================
        try:
            curator = Education_Institute.objects.get(curator = request.user)
        except:
            older_curator = None
            try:
                older_curator = User.objects.get(id = request.user.id )
            except :
                None
            if older_curator is not None:
                older_curator.delete()
            logout(request)
            return redirect('home')
        up_programs = curator.education_institute_program_set.filter(is_archive = False,program__level = 'U')
        base_programs = curator.education_institute_program_set.filter(is_archive = False,program__level = 'B')

        upgroups = curator.group_set.filter(is_archive = False,level = 'U')
        basegroups = curator.group_set.filter(is_archive = False,level = 'B')
        requests = EducationRequest.objects.filter(~Q(id__in=StudentGroup.objects.filter(request__isnull=False,group__isnull=False).values('request')),Q(institute_program__institute=curator),Q(is_archive = False)).order_by('institute_program')
        #periods = Periodicity.objects.all()
        recruitments = Recruitment.objects.filter(institute_program__institute = curator,is_archive = False)
			
        payments = PaymentType.objects.all()
        currencys = Currency.objects.all()
        forms = StudyMode.objects.all()
        #print periods

        return render_to_response(template,{"forms":forms,"payments":payments,"currencys":currencys,'curator':curator,'up_programs':up_programs,'base_programs':base_programs,'upgroups':upgroups,'basegroups':basegroups,'requests':requests,'recruitments':recruitments},context_instance=RequestContext(request))
    elif request.user.is_ipr:
        # ====================== IPR CONSULT =============================
        try:
            student = IprStudent.objects.get(user = request.user) #get_object_or_404(IprStudent,user = request.user) #IprStudent.objects.get(user = request.user)
        except:
            return redirect('revival')

        greeting = None

#        if student.private_is_empty():
#            return redirect('changeprivate')

#        if student.address_is_empty():
#            return redirect('address_revival')

        groups = StudentGroup.objects.filter(student__user = request.user,is_archive = False,group__is_archive = False)
        #print groups
        if len(groups) > 0:
            group = groups[0]
        else:
            group = None
        #print group

        my_requests      = EducationRequest.objects.filter(student=student,is_archive=False).order_by('-date_request')
        base_my_requests = my_requests.filter(institute_program__program__level='B')
        up_my_requests   = my_requests.filter(institute_program__program__level='U')

        old_base_group = Group.objects.filter(studentgroup__student=student,level='B').order_by('-education_end')
        old_up_group   = Group.objects.filter(studentgroup__student=student,level='U').order_by('-education_end')
        payment_method = None

        # first to check in student_group
        if student.confirmed and student.user.has_edu_request:
            try:
                edu_request = EducationRequest.objects.get(student=student,is_archive = False)
                student.study_request = edu_request
                try:
                    if edu_request.is_frash:
                        greeting = u'Ваше заявление о допуске к повышению квалификации принято. Специалист выбранной Вами образовательной организации свяжется с Вами для уточнения деталей обучения.'
                        edu_request.is_frash = False
                        edu_request.save()
                except :
                    return HttpResponse(str(sys.exc_info()))

            except:
                student.user.has_edu_request = False
                student.user.save()

        if student.have_bad_try:
            greeting = u'Вы уже подали одно заявление о допуске к повышению квалификации.'
            student.have_bad_try = False
            student.save()

        iprstudentmemberships = student.iprstudentmembership_set.all().order_by('-year')

        return render_to_response(template,{'student':student,'group':group,
                                            'my_requests':my_requests,'base_my_requests':base_my_requests,'up_my_requests':up_my_requests,
                                            'old_up_group':old_up_group,'old_base_group':old_base_group,'greeting':greeting,'iprstudentmemberships':iprstudentmemberships},context_instance=RequestContext(request))
    else:
        # ====================== SIMPLE STUDENT =============================
        try:
            student = IprStudent.objects.get(user = request.user) #get_object_or_404(IprStudent,user = request.user) #
        except:
            return redirect('revival')

#        if student.address_is_empty():
#            return redirect('address_revival')

        groups = StudentGroup.objects.filter(student__user = request.user,is_archive = False,group__is_archive = False)
        #print groups
        if len(groups) > 0:
            group = groups[0]
        else:
            group = None
        #print group

        my_requests      = EducationRequest.objects.filter(student=student).order_by('-date_request')
        base_my_requests = my_requests.filter(institute_program__program__level='B')
        up_my_requests   = my_requests.filter(institute_program__program__level='U')

        old_up_group   = Group.objects.filter(studentgroup__student=student,level=u'U').order_by('-education_end')
        old_base_group = Group.objects.filter(studentgroup__student=student,level=u'B').order_by('-education_end')

        if student.user.has_edu_request:
            try:
                edu_request = EducationRequest.objects.get(student=student,is_archive = False)
                student.study_request = edu_request
            except:
                student.user.has_edu_request = False
                student.user.save()
        return render_to_response(template,{'student':student,'group':group,
                                            'my_requests':my_requests,'base_my_requests':base_my_requests,'up_my_requests':up_my_requests,
                                            'old_up_group':old_up_group,'old_base_group':old_base_group},context_instance=RequestContext(request))
    return HttpResponseRedirect('/')

# авторизация
@csrf_exempt
def authorize(request):
    if request.method == 'POST':
        data = {'login':request.POST['login'], 'password':request.POST['password']}

        next = "/"
        if request.POST.has_key('next'):
            next = request.POST['next']
        elif request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find(request.META['SERVER_NAME'].replace('www.','')) != -1:
            next = request.META.has_key('HTTP_REFERER')

        form = LoginForm(request.POST,data)
        response = {}
        if form.is_valid():
            user = authenticate(email = request.POST['login'],password = request.POST['password'])
            if user is not None:
                login(request,user)
                response["status"] = 1
                response["body"] = '/auth/personal/' # personal cabinet
            else:
                response["status"] = 0
                response["body"] = "Логин или пароль некорректны"

        else:
            response["status"] = 0
            response["body"] = "Логин или пароль некорректны" #form.errors.as_text()
        if request.is_ajax():
            return HttpResponse(simplejson.dumps(response))
        else:
            if response["status"] == 1:
                messages.success(request, 'Успех!')
                return HttpResponseRedirect(next)
            else:
                return render_to_response('login.html',{'next':next},context_instance=RequestContext(request))
    else:
        next = "/"
        if request.GET.has_key('next'):
            next = request.GET['next']
        return render_to_response('login.html',{'next':next},context_instance=RequestContext(request))

# разлонивание
#@login_required
def ulogout(request):
    if request.user.is_authenticated():
        logout(request)
    if request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find(request.META['SERVER_NAME'].replace('www.','')) != -1:
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        return HttpResponseRedirect("/")

# страница просмотра пользователя
def view_student(request,student_id):
    student = get_object_or_404(IprStudent,id=student_id)
    groups = StudentGroup.objects.filter(student = student,is_archive = False,group__is_archive = False)
    if len(groups) > 0:
        group = groups[0]
    else:
        group = None
    old_request = EducationRequest.objects.filter(student=student,is_archive = True).order_by('-date_request')

    if student.user.has_edu_request:
        try:
            edu_request = EducationRequest.objects.get(student=student,is_archive = False)
            student.study_request = edu_request
        except :
            None
    old_request = EducationRequest.objects.filter(student=student).order_by('-date_request')
    old_base_group = Group.objects.filter(studentgroup__student=student,level='B').order_by('-education_end')
    old_up_group = Group.objects.filter(studentgroup__student=student,level='U').order_by('-education_end')

    return render_to_response('user_view.html',{'student':student,'group':group,'old_request':old_request,'old_base_group':old_base_group,'old_up_group':old_up_group},context_instance=RequestContext(request))

# смена пароля пользователем
@login_required
def changepass(request):
    if request.user.is_curator:
        return redirect('personal_cabinet')

    template        =  "changepass.html"
    fio             = u''
    organizations   = students = []

    filterLastName       = filterFirstName      = filterEmail     = current_page    = None
    all_pages       = item_on_page    = 30 #None

    tab             = 'my'
    tab           = get_from_dict(tab,request.POST, 'tab')
    filterLastName     = get_from_dict(filterLastName,request.POST, 'last_name')
    filterFirstName    = get_from_dict(filterFirstName,request.POST, 'first_name')
    filterEmail         = get_from_dict(filterEmail, request.POST, 'email')
    current_page  = get_from_dict(current_page, request.POST, 'current_page',True)
    item_on_page  = get_from_dict(item_on_page, request.POST, 'item_on_page',True)
    item_on_pages = range(10,50,10) + range(50,510,50)

    if current_page is None or current_page < 1:
        current_page = 1

    llimit = item_on_page * ( current_page - 1 )
    rlimit = item_on_page * current_page

    if request.user.is_admin:
        Qu = Q(user__is_ipr = True, confirmed = True, rejected = False) | Q(user__is_student = True)
        Qr = Q(user__isnull = False) & Qu
        if filterEmail:
            Qr = Qr & Q(**{"user__email__icontains": filterEmail })
        if filterLastName:
            Qr = Qr & Q(**{"user__last_name__icontains": filterLastName })
        if filterFirstName is not None:
            Qr = Qr & Q(**{"user__first_name__icontains": filterFirstName })

        count_user = IprStudent.objects.filter(Qr).count()

        all_pages = count_user / item_on_page
        if count_user % item_on_page > 0:
            all_pages = all_pages + 1

        if llimit > count_user or current_page == 1:
            current_page = 1
            students = IprStudent.objects.filter(Qr).order_by('user__last_name','user__first_name','user__email')[:item_on_page]
        else:
            students = IprStudent.objects.filter(Qr).order_by('user__last_name','user__first_name','user__email')[llimit:rlimit]

    if request.method == 'POST' and tab == 'my':
        form = ChangeOwnPassForm(request.POST)
        if form.is_valid():
            user =  get_object_or_404(User,email = request.user)
            if user.is_curator:
                institutes = user.education_institute_set.all()
                if len(institutes) > 0:
                    fio = institutes[0].fio
                else:
                    fio = user.get_full_name()    
            else:
                fio = user.get_full_name()

            password =     form.cleaned_data['password1']

            try:
                newpass_mail(user,password)
            except :
                None

            user.set_password(password)
            user.save()
            logout(request)
            template =  "changepass_done.html"
    else:
        form = ChangeOwnPassForm()
    if request.user.is_authenticated() and request.user.is_admin:
        organizations = Education_Institute.objects.filter(curator__isnull = False).order_by('name')

    return render_to_response(template, {'Form':form,'organizations':organizations,'fio':fio,'students':students,
                                         "filterEmail":filterEmail,"filterLastName":filterLastName,"filterFirstName":filterFirstName,
                                         "current_page":current_page,"item_on_page":item_on_page,"item_on_pages":item_on_pages,"all_pages":all_pages},
                              context_instance=RequestContext(request))

# смена e-mail у студентов и налоговых консультантов
@login_required
def changemail(request):
    if request.user.is_curator: # or request.user.is_admin:
        return redirect('personal_cabinet')
    user =  get_object_or_404(User,email = request.user)

    if request.method == 'POST':
        form = ChangeEmailForm(request.POST)
        if form.is_valid():
            user.email = form.cleaned_data['email']
            user.save()
            try:
                newmail_mail(user)
            except :
                None
            return HttpResponseRedirect('/auth/changemail/')
    else:
        form = ChangeEmailForm(instance=user)
    return render_to_response("changemail.html", {'Form':form}, context_instance=RequestContext(request))

# Смена пароля у организации администратором
@csrf_exempt
@login_required
def changepass_inst(request,id):
    response = {}
    if request.user.is_admin and request.method == 'POST':
        user = User.objects.get(id=id)
        if request.POST.has_key('password') and request.POST.has_key('password2') and request.POST['password'] == request.POST['password2']:
            user.set_password(request.POST['password'])
            user.save()
            response["status"] = 1
            return HttpResponse(simplejson.dumps(response))
    response["status"] = 0
    return HttpResponse(simplejson.dumps(response))

# Изменение приватной инфы пользователем
@csrf_exempt
@login_required
def changeprivate(request):
    template =  "changeprivate.html"
    if request.user.is_student or request.user.is_ipr:
        user = get_object_or_404(User,id = request.user.id)
        student = get_object_or_404(IprStudent,user = user)
        if request.method == 'POST':
            form = PrivateForm(request.POST)
            if form.is_valid():
                for formfieldname in form._meta.__dict__['fields']:
                    partfieldname = formfieldname
                    student.__dict__[partfieldname] = form.cleaned_data[formfieldname]
                student.save()
                return redirect('personal_cabinet')
        else:
            form = PrivateForm(instance=student)
        return render_to_response(template, {'Form':form,'student':student}, context_instance=RequestContext(request))
    return redirect('personal_cabinet')

# Дополнительная информация об организации
@csrf_exempt
@login_required
def additional(request):
    response = {}
    if request.user.is_curator:
        curator = Education_Institute.objects.get(curator=request.user)
        if request.method == 'POST' and request.POST.has_key('additional'):
            curator.additional = request.POST['additional']
            curator.save()
            response["status"] = 1
            return HttpResponse(simplejson.dumps(response))
    response["status"] = 0
    return HttpResponse(simplejson.dumps(response))

# запрос на восстановление пароля
def recovery_request(request):
    if request.user.is_authenticated():
        return redirect('personal_cabinet')
    else:
        model_item = Recovery()

        if request.method == 'POST':
            form = RecoveryRequestForm(request.POST)
            if form.is_valid():
                success_send = True

                try:
                    recovery_user  = User.objects.get(email = form.cleaned_data['email'], is_olympic = False)
                    password = word_generator(10)
                    newpass_mail(recovery_user,password)
                except :
                    success_send = False #str(sys.exc_info())

                if success_send:
                    recovery_user.set_password(password)
                    recovery_user.save()

                return render_to_response('recovery_request.html', {'recovery_user':recovery_user,'success_send':success_send}, context_instance=RequestContext(request))
            return render_to_response('recovery_request.html', {'Form':form}, context_instance=RequestContext(request))
        form = RecoveryRequestForm()
        return render_to_response('recovery_request.html', {'Form':form}, context_instance=RequestContext(request))

# восстановление пароля по сгенерированной ссылке
#def recovery_execute(request,slug):
#    if request.user.is_authenticated():
#        return redirect('personal_cabinet')
#
#    recovery = get_object_or_404(Recovery,slug = slug)
#    user = recovery.user
#
#
#    now = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
#    recovery_date = recovery.generation_date
#    noon = timedelta(days=1)
#    deadline = now - noon
#
#    if recovery.use == True:
#        reason = u'Эта ссылка недействительна'
#        return render_to_response('recovery_execute.html', {'reason':reason}, context_instance=RequestContext(request))
#
#    if recovery.generation_date < deadline.date():
#        reason = u'Срок действия ссылки истёк'
#        return render_to_response('recovery_execute.html', {'reason':reason}, context_instance=RequestContext(request))
#
#    if request.method == 'POST':
#        form = ChangeOwnPassForm(request.POST)
#        if form.is_valid():
#            recovery.use = True
#            recovery.save()
#            user.set_password(form.cleaned_data['password1'])
#            user.save()
#            try:
#                newpass_mail(user,form.cleaned_data['password1'])
#            except :
#                None
#            return render_to_response('recovery_execute.html', {'success':True,'recovery':recovery}, context_instance=RequestContext(request))
#    else:
#        form = ChangeOwnPassForm()
#    return render_to_response('recovery_execute.html', {'Form':form,'recovery':recovery}, context_instance=RequestContext(request))

# восстановление запроса на обучение
@login_required
def revival(request):
    if request.user.is_ipr or request.user.is_student:
        try:
            student = IprStudent.objects.get(user = request.user)
            return redirect('personal_cabinet')
        except:
            None
    else:
        return redirect('personal_cabinet')

    if request.user.is_ipr:

        if request.method == 'POST':
            form = RevivalConsultForm(request.POST)
            if form.is_valid():
                form.save(request.user)
                return redirect('personal_cabinet')
        else:
            form = RevivalConsultForm()

        return render_to_response('revival.html', {'Form':form}, context_instance=RequestContext(request))
    else:
        if request.method == 'POST':
            form         = RevivalStudentForm(request.POST)
            request_form = EducationRequestBForm(request.POST)
            if form.is_valid() and request_form.is_valid():
                form.save(request.user,request_form.cleaned_data['education_institute'],request_form.cleaned_data['education_program'])
                return redirect('personal_cabinet')
        else:
            form = RevivalStudentForm()
        request_form = EducationRequestBForm()
        return render_to_response('revival.html', {'Form':form,'request_form':request_form}, context_instance=RequestContext(request))

    return redirect('personal_cabinet')

# заполнение данных адреса и телефона в новом формате
@login_required
def address_revival(request):
    if request.user.is_ipr or request.user.is_student:
        if request.method == 'POST':
            form = RevivalAddressForm(request.POST)
            if form.is_valid() and form.save(request.user):
                return redirect('personal_cabinet')
        else:
            form = RevivalAddressForm()

        return render_to_response('address_revival.html', {'Form':form}, context_instance=RequestContext(request))
    return redirect('personal_cabinet')

