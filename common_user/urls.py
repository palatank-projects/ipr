# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.conf import settings
from education_programmes.models import Education_Institute

urlpatterns = patterns('',
    url(r'^student/register/?$','common_user.views.register_student',name='register_student'),
    url(r'^register/consult/?$','common_user.views.register_consult',name='register_nalog_consult'),
    url(r'^newrequest/consult/?$','common_user.views.create_edu_request',name='create_edu_request'),
    url(r'^personal/?$','common_user.views.personal_cabinet',{'template':'personal_cabinet.html'}, name='personal_cabinet'),
    url(r'^authorize/?$','common_user.views.authorize', name='authorize'),
    url(r'^logout/?$','common_user.views.ulogout', name='logout'),
    url(r'^user/view/(?P<student_id>\d+)/?$','common_user.views.view_student',name='view_student'),

    url(r'^changepass/?$','common_user.views.changepass',name='changepass'),

    url(r'^changemail/?$','common_user.views.changemail',name='changemail'),
    url(r'^masschangepass/(\d+)/?$','common_user.views.changepass_inst',name='changepass-inst'),

    url(r'^additional/?$','common_user.views.additional',name='additional-edit'),
    url(r'^private/?$','common_user.views.changeprivate',name='changeprivate'),
    url(r'^revival/?$','common_user.views.revival',name='revival'),
    url(r'^address/revival/?$','common_user.views.address_revival',name='address_revival'),

    url(r'^recovery/request/?$','common_user.views.recovery_request',name='recovery-request'),
#    url(r'^recovery/(\w+)/?$','common_user.views.recovery_execute',name='recovery-execute'),

)
