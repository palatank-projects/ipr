# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'IprStudentMembership'
        db.delete_table(u'common_user_iprstudentmembership')

        # Deleting model 'MembershipBenefit'
        db.delete_table(u'common_user_membershipbenefit')

        # Deleting model 'MembershipCost'
        db.delete_table(u'common_user_membershipcost')


    def backwards(self, orm):
        # Adding model 'IprStudentMembership'
        db.create_table(u'common_user_iprstudentmembership', (
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('benefit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.MembershipBenefit'], null=True, on_delete=models.SET_NULL)),
            ('paid', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=2)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('membership', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.MembershipCost'], null=True, on_delete=models.SET_NULL)),
            ('ticket', self.gf('django.db.models.fields.files.FileField')(max_length=500, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('iprstudent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.IprStudent'], null=True, on_delete=models.SET_NULL)),
        ))
        db.send_create_signal(u'common_user', ['IprStudentMembership'])

        # Adding model 'MembershipBenefit'
        db.create_table(u'common_user_membershipbenefit', (
            ('percent', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'common_user', ['MembershipBenefit'])

        # Adding model 'MembershipCost'
        db.create_table(u'common_user_membershipcost', (
            ('cost', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=2)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=4, primary_key=True)),
        ))
        db.send_create_signal(u'common_user', ['MembershipCost'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.education': {
            'Meta': {'ordering': "['title']", 'object_name': 'Education'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common_user.iprstudent': {
            'Meta': {'ordering': "['user']", 'object_name': 'IprStudent', '_ormbases': [u'common_user.Student']},
            'agreedata': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_length': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'confirmed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'education': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.Education']", 'null': 'True'}),
            'have_bad_try': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'i_am_ip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'job_place': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'job_place_visible': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'length_of_work': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'membership_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_seria': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'passport_when_gave': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'passport_who_gave': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'postaddress': ('django.db.models.fields.TextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'profstatus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.ProfStatus']", 'null': 'True', 'blank': 'True'}),
            'reason': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rejected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'special_notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'student_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['common_user.Student']", 'unique': 'True', 'primary_key': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'common_user.profstatus': {
            'Meta': {'ordering': "['title']", 'object_name': 'ProfStatus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common_user.recovery': {
            'Meta': {'object_name': 'Recovery'},
            'generation_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'use': ('django.db.models.fields.BooleanField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'common_user.student': {
            'Meta': {'ordering': "['user']", 'object_name': 'Student'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.TextField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olesya': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olympic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_participant': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_univer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common_user']