# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'common_user_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('email', self.gf('django.db.models.fields.CharField')(unique=True, max_length=75)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_curator', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_student', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_ipr', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_edu_request', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'common_user', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'common_user_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'common_user.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'common_user_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'common_user.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding model 'Student'
        db.create_table(u'common_user_student', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common_user.User'])),
            ('patronymic', self.gf('django.db.models.fields.TextField')(max_length=50, null=True, blank=True)),
            ('registered_date', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
        ))
        db.send_create_signal(u'common_user', ['Student'])

        # Adding model 'IprStudent'
        db.create_table(u'common_user_iprstudent', (
            (u'student_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['common_user.Student'], unique=True, primary_key=True)),
            ('birthday', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('placebirth', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('postaddress', self.gf('django.db.models.fields.TextField')(max_length=100, null=True, blank=True)),
            ('telephone', self.gf('django.db.models.fields.TextField')(max_length=50, null=True, blank=True)),
            ('study', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('study_visible', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('length_of_work', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('study_city', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('attestat_number', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('attestat_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('attestat_length', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('study_status', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('membership_number', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('job_place', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('job_place_visible', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('passport_seria', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('passport_number', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('passport_who_gave', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('passport_when_gave', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('passport_code', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('consult', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('confirmed', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('confirmed_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
            ('avatar', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('agreedata', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('exam_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('exam_result', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'common_user', ['IprStudent'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'common_user_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'common_user_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'common_user_user_user_permissions'))

        # Deleting model 'Student'
        db.delete_table(u'common_user_student')

        # Deleting model 'IprStudent'
        db.delete_table(u'common_user_iprstudent')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.iprstudent': {
            'Meta': {'object_name': 'IprStudent', '_ormbases': [u'common_user.Student']},
            'agreedata': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_length': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'attestat_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'confirmed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'confirmed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'consult': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'exam_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'exam_result': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'job_place': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'job_place_visible': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'length_of_work': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'membership_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport_code': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport_number': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport_seria': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport_when_gave': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'passport_who_gave': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'placebirth': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'postaddress': ('django.db.models.fields.TextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'student_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['common_user.Student']", 'unique': 'True', 'primary_key': 'True'}),
            'study': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'study_city': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'study_status': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'study_visible': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.TextField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'common_user.student': {
            'Meta': {'object_name': 'Student'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.TextField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common_user']