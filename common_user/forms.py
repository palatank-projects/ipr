# -*- coding: utf-8 -*-
from django.forms import ModelForm
from models import *
from django import forms
from django.forms.extras import SelectDateWidget
from education_programmes.models import *
from datetime import datetime,date,timedelta
from django.core import validators
from IPR.widgets import *
from IPR.utils import *
from IPR.mail import *
import sys
from django.utils.safestring import mark_safe
#from recaptcha.forms import ReCaptchaField

year = date.today().year

class PlainTextWidget(forms.Widget):
    def render(self, _name, value, _attrs):
        return mark_safe(value) if value is not None else '-'

class RecoveryRequestForm(forms.Form):
    email = forms.EmailField(max_length=50, label=u'Логин / E-mail', help_text = u'Введите e-mail пользователя, на который будет выслан новый пароль')
    recaptcha = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                 'invalid' : u'Указанное значение было неверно'
    })

    def __init__(self, *args, **kwargs):
        super(RecoveryRequestForm,self).__init__(*args, **kwargs)
        self.fields['email'].required = True
#        self.fields['captcha'].label = u'Введите цифры с картинки'
        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')

        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist'] == False:
            self._errors["email"] = self.error_class([u'Такого email не существует в системе!'])
            raise forms.ValidationError(u'Такого email не существует в системе!')
        elif check_unique_email.has_key('exist') and check_unique_email['exist'] and check_unique_email.has_key('user') and check_unique_email['user'].is_curator:
            self._errors["email"] = self.error_class([u'Образовательные организации не имеют права восстанавливать пароль'])
            raise forms.ValidationError(u'Образовательные организации не имеют права восстанавливать пароль')
        elif check_unique_email.has_key('exist') and check_unique_email['exist'] and check_unique_email.has_key('user'):
            now = datetime(datetime.now().year, datetime.now().month, datetime.now().day, datetime.now().hour, datetime.now().minute)
            noon = timedelta(days=1)
            deadline = now - noon

            valid_recovery = Recovery.objects.filter(user = check_unique_email['user'],use = False, generation_date__gte = deadline)

            if len( valid_recovery ) > 0:
                self._errors["email"] = self.error_class([u'Вы уже запросили восстановление пароля! Воспользуйтесь присланной ранее ссылкой или повторите попытку завтра'])
                raise forms.ValidationError(u'Вы уже запросили восстановление пароля! Воспользуйтесь присланной ранее ссылкой или повторите попытку завтра!')

        return self.cleaned_data

class ChangeOwnPassForm(forms.Form):
    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    recaptcha = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                 'invalid' : u'Указанное значение было неверно'
                                                 })

    def __init__(self, *args,**kwargs):
        super(ChangeOwnPassForm,self).__init__(*args,**kwargs)
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'

    
    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password1"] = self.error_class([u"Пароли не совпадают."])
            raise forms.ValidationError(u"Пароли не совпадают.")

        return self.cleaned_data

class ChangeEmailForm(ModelForm):
    recaptcha = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                 'invalid' : u'Указанное значение было неверно'
    })

    class Meta:
        model = User
        fields=['email']

    def __init__(self, *args, **kwargs):
        super(ChangeEmailForm,self).__init__(*args, **kwargs)
        self.fields['email'].label = u'Логин / E-mail'
        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')

        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist']:
            self._errors["email"] = self.error_class([u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

class RegisterStudentForm(ModelForm):
    email                  = forms.EmailField(max_length=50, label=u'E-mail', help_text = u'На указанный адрес будет выслан автоматически сгенерированный пароль. Запомните эти логин и пароль - они будут использоваться для входа в Инфо-ресурс')
    #u'Запомните эти логин и пароль - они будут использоваться для входа в Инфо-ресурс')
#    password1              = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
#    password2              = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    surname                = forms.CharField(max_length=50,label=u'Фамилия')
    name                   = forms.CharField(max_length=50,label=u'Имя')
    patronymic             = forms.CharField(max_length=50,label=u'Отчество')
    education_institute    = forms.ModelChoiceField(label=u'Выбранная образовательная организация',queryset = Education_Institute.objects.filter(education_institute_program__program__level='B').distinct())
    education_programm     = forms.ModelChoiceField(label=u'Выбранная программа подготовки',queryset=Education_Programme.objects.filter(level='B',education_institute_program__institute__isnull=False).distinct())
#    agreedata              = forms.BooleanField(label=u"""Даю согласие на обработку Палатой налоговых консультантов (г. Москва, ул. Ярославская, д. 8, корп. 4) моих персональных данных, указанных при регистрации в Информационно-поисковом ресурсе налогового
#    консультанта на сайте www.ipr.palata-nk.ru на неограниченный срок с целью организации моей подготовки (повышения квалификации) по программам Палаты налоговых консультантов. Обработка персональных
#    данных включает сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование в уставных целях, уничтожение.""")
    agreedata              = PrivatePoliticCheckboxField()
    recaptcha              = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                               'invalid' : u'Указанное значение было неверно'
    })

    class Meta:
        model = IprStudent
        fields=['email', #'password1','password2',
                'surname','name','patronymic',#'birthday',#'postaddress','postcode','street','house','flat',
                'region','city','telephone','education','length_of_work','education_institute','education_programm','agreedata'] #,'recaptcha']

        widgets = {
#            'birthday':SelectDateWidget(years = range(year-18,year-100,-1)),
            'telephone':forms.TextInput(attrs={'size':'10'}),
        }

    def __init__(self, *args, **kwargs):
        super(RegisterStudentForm,self).__init__(*args, **kwargs)

        if kwargs.has_key('initial'):
            if self.initial.has_key('edu_institute'):
                self.fields['education_institute'].initial = self.initial['edu_institute']
            if self.initial.has_key('edu_programm'):
                self.fields['education_programm'].initial = self.initial['edu_programm']

        self.fields['education_institute'].widget = SelectInInput()
        self.fields['education_programm'].widget = SelectInInput()
        self.fields['agreedata'].widget = CheckboxIPRStyle()


        for field in self.fields.keys():
            if field != 'flat':
                self.fields[field].required = True
#        self.fields['agreedata'].widget.attrs['class'] = 'checkbox_in_form'

#        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['surname'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['name'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'

        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'

    def clean(self):
        print self.cleaned_data
#        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
#            self._errors["password1"] = self.error_class([u"Пароли не совпадают."])
#            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')


        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist']:
            self._errors["email"] = self.error_class([u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            Education_Institute_Program.objects.get(institute = self.cleaned_data["education_institute"],program = self.cleaned_data["education_programm"],program__level='B')
        except:
            self._errors["education_institute"] = self.error_class([u'Эта организация не аккредитована по выбранной программе'])
            self._errors["education_programm"] = self.error_class([u'Обучение по данной программе не ведется в выбранной организации'])
            raise forms.ValidationError(u'Эта организация не аккредитована по выбранной программе')

        if self.cleaned_data.has_key('telephone') and self.cleaned_data['telephone'] is not None and self.cleaned_data['telephone'] != '':
            lt_consult_exist = IprStudent.objects.filter(telephone=self.cleaned_data["telephone"])

            if len( lt_consult_exist ) > 0:
                l_consult_exist = lt_consult_exist[0]
                l_error_text = u'Пользователь с номером телефона {0!s} зарегистрирован под ником {1!s}'.format(l_consult_exist.telephone, l_consult_exist.user.email)
                self._errors["telephone"] = self.error_class([l_error_text])
                raise forms.ValidationError(l_error_text)

        return self.cleaned_data


    def save(self):
        super(RegisterStudentForm,self).save(commit=False)
        data = self.cleaned_data
        password = word_generator(10)
        print password
        user = User.objects.create_user(data['email'], data['name'], data['surname'], password)
        user.is_active = True
        user.has_edu_request = True
        user.is_student = True
        user.save()

        try:
            print 'ipr_registration'
            student_registration_mail_upd(user,password)
#            student_registration_mail(user,password)
        except :
            print str(sys.exc_info())

        iprstudent = IprStudent()
        iprstudent.user = user
        iprstudent.patronymic = data['patronymic']
#        iprstudent.birthday = data['birthday']

#        iprstudent.postaddress = data['postaddress']
#        iprstudent.postcode  = data['postcode']
        iprstudent.region    = data['region']
        iprstudent.city      = data['city']
#        iprstudent.street    = data['street']
#        iprstudent.house     = data['house']
#        iprstudent.flat      = data['flat']

        iprstudent.telephone = data['telephone']
        iprstudent.education = data['education']
        iprstudent.length_of_work = data['length_of_work']
        iprstudent.save()

        #*****save request*****
        edu_request = EducationRequest()
        edu_request.student = iprstudent
        edu_request.institute_program = Education_Institute_Program.objects.get(institute = self.cleaned_data["education_institute"],program = self.cleaned_data["education_programm"],program__level='B')
        edu_request.save()

        try:
            new_edurequest_mail(edu_request)
        except :
            print str(sys.exc_info())


class RegisterIprStudentForm(ModelForm):
    email       = forms.EmailField(max_length=50, label=u'E-mail', help_text = u'Запомните эти логин и пароль - они будут использоваться для входа в Инфо-ресурс')
    password1   = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
    password2   = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль')
    surname     = forms.CharField(max_length=50,label=u'Фамилия')
    name        = forms.CharField(max_length=50,label=u'Имя')
    patronymic           = forms.CharField(max_length=50,label=u'Отчество')
    education_institute  = forms.ModelChoiceField(label=u'Выбранная образовательная организация',queryset = Education_Institute.objects.filter(education_institute_program__program__level='U').distinct())
    education_programm   = forms.ModelChoiceField(label=u'Выбранная программа подготовки',queryset=Education_Programme.objects.filter(level='U',education_institute_program__institute__isnull=False).distinct())
#    agreedata            = forms.BooleanField(label=u"""Даю согласие на обработку Палатой налоговых консультантов (г. Москва, ул. Ярославская, д. 8, корп. 4) моих персональных данных, указанных при регистрации в Информационно-поисковом ресурсе налогового
#    консультанта на сайте www.ipr.palata-nk.ru на неограниченный срок с целью организации моей подготовки (повышения квалификации) по программам Палаты налоговых консультантов. Обработка персональных
#    данных включает сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование в уставных целях, уничтожение.""")
    agreedata            = PrivatePoliticCheckboxField()
    recaptcha = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                 'invalid' : u'Указанное значение было неверно'
    })



    class Meta:
        model = IprStudent
        fields=['email','password1','password2','surname','name','patronymic',#'birthday','postaddress','postcode','street','house','flat',
                'region','city','telephone','education','length_of_work','education_institute','education_programm','agreedata'] #,'captcha']
        widgets = {
#            'birthday':SelectDateWidget(years = range(year-18,year-100,-1)),
#            'postaddress':forms.TextInput(attrs={'size':'40'}),
            'telephone':forms.TextInput(attrs={'size':'10'})
            }

    def __init__(self, *args, **kwargs):
        super(RegisterIprStudentForm,self).__init__(*args, **kwargs)
        if kwargs.has_key('initial'):
            if self.initial.has_key('edu_institute'):
                self.fields['education_institute'].initial = self.initial['edu_institute']
            if self.initial.has_key('edu_programm'):
                self.fields['education_programm'].initial = self.initial['edu_programm']

        self.fields['education_institute'].widget = SelectInInput()
        self.fields['education_programm'].widget = SelectInInput()
        self.fields['agreedata'].widget = CheckboxIPRStyle()

        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['surname'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['name'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['length_of_work'].widget.attrs['onkeypress'] ='return event.charCode >= 48 && event.charCode <= 57'

        for field in self.fields.keys():
            if field != 'flat':
                self.fields[field].required = True
#        self.fields['agreedata'].widget.attrs['class'] = 'checkbox_in_form'
        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'


    def clean(self):
        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
            self._errors["password1"] = self.error_class([u"Пароли не совпадают."])
            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')

        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist']:
            self._errors["email"] = self.error_class([u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            Education_Institute_Program.objects.get(institute = self.cleaned_data["education_institute"],program = self.cleaned_data["education_programm"],program__level='U')
        except:
            self._errors["education_institute"] = self.error_class([u'Эта организация не аккредитована по выбранной программе'])
            self._errors["education_programm"] = self.error_class([u'Обучение по данной программе не ведется в выбранной организации'])
            raise forms.ValidationError(u'Эта организация не аккредитована по выбранной программе')

        if self.cleaned_data['telephone'] is not None and self.cleaned_data['telephone'] != '':
            lt_consult_exist = IprStudent.objects.filter(telephone=self.cleaned_data["telephone"])

            if len( lt_consult_exist ) > 0:
                l_consult_exist = lt_consult_exist[0]
                l_error_text = u'Пользователь с номером телефона {0!s} зарегистрирован под ником {1!s}'.format(l_consult_exist.telephone, l_consult_exist.user.email)
                self._errors["telephone"] = self.error_class([l_error_text])
                raise forms.ValidationError(l_error_text)

        return self.cleaned_data


    def save(self):
        super(RegisterIprStudentForm,self).save(commit=False)
        data = self.cleaned_data
        user = User.objects.create_user(data['email'], data['name'], data['surname'], data['password1'])
        user.is_active = True
        user.has_edu_request = True
        user.is_student = True
        user.save()

        try:
            ipr_registration_mail(user,data['password1'])
        except :
            print str(sys.exc_info())

        iprstudent = IprStudent()
        iprstudent.user = user

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or formfieldname == 'last_name':
                continue
            elif formfieldname == 'profstatus':
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue
            elif not iprstudent.__dict__.has_key(formfieldname):
                print formfieldname
                continue

            iprstudent.__dict__[formfieldname] = data[formfieldname]

#        iprstudent.patronymic = data['patronymic']
#        iprstudent.birthday = data['birthday']
        #        iprstudent.placebirth = data['placebirth']
#        iprstudent.postaddress = data['postaddress']

#        iprstudent.postcode = data['postcode']
#        iprstudent.region   = data['region']
#        iprstudent.city     = data['city']
#        iprstudent.street   = data['street']
#        iprstudent.house    = data['house']
#        iprstudent.flat     = data['flat']

#        iprstudent.telephone = data['telephone']
#        iprstudent.education = data['education']
#        iprstudent.length_of_work = data['length_of_work']
        iprstudent.save()

        #*****save request*****
        edu_request = EducationRequest()
        edu_request.student = iprstudent
        edu_request.institute_program = Education_Institute_Program.objects.get(institute = self.cleaned_data["education_institute"],program = self.cleaned_data["education_programm"],program__level='U')
        edu_request.save()

        try:
            new_edurequest_mail(edu_request)
        except :
            print str(sys.exc_info())

class EditStudentForm(ModelForm):
    email      = forms.EmailField(max_length=50, label=u'E-mail')
    last_name  = forms.CharField(max_length=50,label=u'Фамилия')
    first_name = forms.CharField(max_length=50,label=u'Имя')
    patronymic = forms.CharField(max_length=50,label=u'Отчество')

    class Meta:
        model = IprStudent
        fields=['user','email','last_name','first_name','patronymic',#'birthday',
                #'passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code', 'placebirth',
                #'postcode','street','house','flat',
                'region','city','telephone','education',
                'profstatus', 'attestat_number','attestat_date','attestat_length',
                'job_place','position','i_am_ip'] #,'education_institute','education_programm','group']

        widgets = {
            'user':               forms.HiddenInput(),
#            'birthday':           SelectDateWidget(years = range(year-18,year-100,-1)),
            'attestat_date':      SelectDateWidget(years = range(year,year-100,-1)),
#            'passport_when_gave': SelectDateWidget(years = range(year,year-100,-1)),
            'attestat_length':    SelectDateWidget(years = range(year+50,year-100,-1)),
            'i_am_ip':            CheckboxIPRStyle(),
            }


    def __init__(self, *args, **kwargs):
        super(EditStudentForm,self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            if field not in ['birthday','passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code','i_am_ip','postcode','region','city','street','house','flat','telephone','education', 'profstatus','attestat_number','attestat_date','attestat_length','job_place','position']:
                self.fields[field].required = True

        if kwargs.has_key('instance'):
            self.fields['user'].initial        = self.instance.user.id
            self.fields['email'].initial       = self.instance.user.email
            self.fields['first_name'].initial  = self.instance.user.first_name
            self.fields['last_name'].initial   = self.instance.user.last_name
            self.fields['patronymic'].initial  = self.instance.patronymic #user.last_name

            if self.instance.user.is_ipr:
                if self.instance.confirmed:
                    self.fields['email'].help_text = u'Утвержденный налоговый консультант'
                elif self.instance.rejected:
                    self.fields['email'].help_text = u'Отклоненный налоговый консультант'
                else:
                    self.fields['email'].help_text = u'Налоговый консультант'
            else:
                self.fields['email'].help_text = u'Слушатель'

    def clean(self):
        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist'] and check_unique_email.has_key('user') and check_unique_email['user'].id != self.cleaned_data.get("user").id:
            self._errors["email"] = self.error_class([u'Данный e-mail принадлежит другому пользователю'])
            raise forms.ValidationError(u'Данный e-mail принадлежит другому пользователю')
        return self.cleaned_data

    def save(self):
        super(EditStudentForm,self).save(commit=False)
        data = self.cleaned_data
#        print data
        iprstudent = IprStudent.objects.get(id=data['id'])

        iprstudent.user.first_name = data['name']
        iprstudent.user.last_name = data['surname']
        iprstudent.user.save()

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or 'first_name' == 'last_name':
                continue
            elif formfieldname == 'profstatus':
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue

            iprstudent.__dict__[formfieldname] = data[formfieldname]

        iprstudent.save()


class PrivateForm(ModelForm):
    class Meta:
        model = IprStudent
        fields=['region','city','telephone','job_place','position','i_am_ip']
        #    ,'passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code'

        #'placebirth','postcode','street','house','flat'

        widgets = {
            'i_am_ip':            CheckboxIPRStyle(),
        }

    def __init__(self, *args, **kwargs):
        super(PrivateForm,self).__init__(*args, **kwargs)
        for field in self.fields.keys():
            if field not in ['passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code', 'i_am_ip','flat'] :
                self.fields[field].required = True
    
    def clean(self):
        return self.cleaned_data
    

class LoginForm(forms.Form):
    login = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')

    def clean(self):
        try:
            validators.validate_email(self.cleaned_data["login"])
        except:
            self._errors["login"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')

        return self.cleaned_data


class RegisterConsultForm(ModelForm):
    email = forms.EmailField(max_length=50, label=u'Email', help_text = u'На указанный адрес будет выслан автоматически сгенерированный пароль. Запомните эти логин и пароль - они будут использоваться для входа в Инфо-ресурс')
#    password1 = forms.CharField(widget=forms.PasswordInput,label=u'Пароль')
#    password2 = forms.CharField(widget=forms.PasswordInput,label=u'Подтвердите пароль', help_text = u'Запомните эти логин и пароль - они будут использоваться для входа в Инфо-ресурс')
    surname = forms.CharField(max_length=50,label=u'Фамилия')
    name = forms.CharField(max_length=50,label=u'Имя')
    patronymic = forms.CharField(max_length=50,label=u'Отчество')
#    agreedata = forms.BooleanField(label=u"""Даю согласие на обработку Палатой налоговых консультантов (г. Москва, ул. Ярославская, д. 8, корп. 4) моих персональных данных, указанных при регистрации в Информационно-поисковом ресурсе налогового
#    консультанта на сайте www.ipr.palata-nk.ru на неограниченный срок с целью организации моей подготовки (повышения квалификации) по программам Палаты налоговых консультантов. Обработка персональных
#    данных включает сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование в уставных целях, уничтожение.""")
    agreedata            = PrivatePoliticCheckboxField()
    recaptcha = GReCaptchaField(error_messages = {'required': u'Это поле должно быть заполнено',
                                                 'invalid' : u'Указанное значение было неверно'
    })

    class Meta:
        model = IprStudent
        fields=['email'#,'password1','password2'
                ,'surname','name','patronymic',#'birthday',
                #'passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code', 'placebirth','postcode','street','house','flat',
                'region','city','telephone','education', 'profstatus', 'attestat_number','attestat_date','attestat_length',
                'membership_number','job_place','position','i_am_ip','agreedata'] #,'captcha']

        widgets = {
#            'birthday':           SelectDateWidget(years = range(year-18,year-100,-1)),
            'attestat_date':      SelectDateWidget(years = range(year,year-100,-1)),
#            'passport_when_gave': SelectDateWidget(years = range(year,year-100,-1)),
            'attestat_length':    SelectDateWidget(years = range(year+50,year-100,-1)),
            'i_am_ip':            CheckboxIPRStyle(),
        }

    def __init__(self, *args, **kwargs):
        super(RegisterConsultForm, self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            if field != 'i_am_ip' and field != 'flat':
                self.fields[field].required = True
            if field == 'email':
                self.fields[field].widget.attrs['placeholder'] = u'Логин'
            else:
                self.fields[field].widget.attrs['placeholder'] = self.fields[field].label

        self.fields['agreedata'].widget = CheckboxIPRStyle()
#        self.fields['agreedata'].widget.attrs['class'] = 'checkbox_in_form'

#        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['surname'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['name'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'
        self.fields['recaptcha'].widget.attrs['class'] = 'without_label'

    def clean(self):
#        if (self.cleaned_data.get("password2", "") != self.cleaned_data.get("password1", "")):
#            self._errors["password1"] = self.error_class([u'Пароли не совпадают.'])
#            raise forms.ValidationError(u"Пароли не совпадают.")
        try:
            validators.validate_email(self.cleaned_data["email"])
        except:
            self._errors["email"] = self.error_class([u'Проверьте правильность ввода e-mail.'])
            raise forms.ValidationError(u'Проверьте правильность ввода e-mail.')


        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist']:
            self._errors["email"] = self.error_class([u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Такой email уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            consult_exist = IprStudent.objects.get(attestat_number=self.cleaned_data["attestat_number"],user__last_name=self.cleaned_data["surname"])
        except:
            consult_exist = None
            #if email_exist is not None:
        if consult_exist:
            self._errors["attestat_number"] = self.error_class([u'Пользователь с таким номером аттестата уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Пользователь с таким номером аттестата уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            consult_exist = IprStudent.objects.get(membership_number=self.cleaned_data["membership_number"])
        except:
            consult_exist = None
            #if email_exist is not None:
        if consult_exist:
            error_text = u'Пользователь с номером билета {0!s} зарегистрирован под ником {1!s}'.format(consult_exist.membership_number, consult_exist.user.email)

            self._errors["membership_number"] = self.error_class([error_text])
            raise forms.ValidationError(error_text)

        if self.cleaned_data.get("job_place", "").isspace():
            self._errors["job_place"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

        if self.cleaned_data.get("position", "").isspace():
            self._errors["position"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

#        if self.cleaned_data.get("postaddress", "").isspace():
#            self._errors["postaddress"] = self.error_class([u'Обязательное поле'])
#            raise forms.ValidationError(u'Обязательное поле')

        if self.cleaned_data.get("attestat_number", "").isspace():
            self._errors["attestat_number"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

#        if self.cleaned_data.get("passport_who_gave", "").isspace():
#            self._errors["passport_who_gave"] = self.error_class([u'обязательное поле'])
#            raise forms.ValidationError(u'Обязательное поле')

        if self.cleaned_data['telephone'] is not None and self.cleaned_data['telephone'] != '':
            lt_consult_exist = IprStudent.objects.filter(telephone=self.cleaned_data["telephone"])

            if len( lt_consult_exist ) > 0:
                l_consult_exist = lt_consult_exist[0]
                l_error_text = u'Пользователь с номером телефона {0!s} зарегистрирован под ником {1!s}'.format(l_consult_exist.telephone, l_consult_exist.user.email)
                self._errors["telephone"] = self.error_class([l_error_text])
                raise forms.ValidationError(l_error_text)

        return self.cleaned_data


    def save(self):
        super(RegisterConsultForm,self).save(commit=False)
        data = self.cleaned_data
        password = word_generator(10)
        user = User.objects.create_user(data['email'], data['name'], data['surname'], password) #data['password1'])
        user.last_name = data['surname']
#        user.is_active = False
        user.is_ipr = True
        user.save()

        try:
            ipr_registration_mail_upd(user,password) #data['password1'])
        except :
            print str(sys.exc_info())

        iprstudent = IprStudent()
        iprstudent.user = user

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or 'first_name' == 'last_name':
                continue
            elif formfieldname == 'profstatus':
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue

            iprstudent.__dict__[formfieldname] = data[formfieldname]

        iprstudent.confirmed = False
        iprstudent.save()

class EditPersonForm(ModelForm):
    id = forms.CharField(widget=forms.HiddenInput)
    email = forms.EmailField(max_length=50, label=u'E-mail')
    last_name = forms.CharField(max_length=50,label=u'Фамилия')
    first_name = forms.CharField(max_length=50,label=u'Имя')
    patronymic = forms.CharField(max_length=50,label=u'Отчество')

    class Meta:
        model = IprStudent
        fields=['user','email','last_name','first_name','patronymic',#'birthday',
                #'passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code', 'placebirth',
                'postaddress','region','city',#'postcode','street','house','flat',
                'telephone','education','length_of_work', 'profstatus', 'attestat_number','attestat_date','attestat_length','job_place','position','i_am_ip','membership_number']

        widgets = {
#            'birthday':           SelectDateWidget(years = range(year,year-100,-1)),
            'attestat_date':      SelectDateWidget(years = range(year,year-100,-1)),
#            'passport_when_gave': SelectDateWidget(years = range(year,year-100,-1)),
            'attestat_length':    SelectDateWidget(years = range(year+50,year-100,-1)),
            'user':               forms.HiddenInput(),
            'i_am_ip':            CheckboxIPRStyle(),
            }


    def __init__(self, *args, **kwargs):
        super(EditPersonForm,self).__init__(*args, **kwargs)

        if kwargs.has_key('instance'):
            self.fields['email'].initial            = self.instance.user.email
            self.fields['first_name'].initial       = self.instance.user.first_name
            self.fields['last_name'].initial        = self.instance.user.last_name
            self.fields['patronymic'].initial       = self.instance.patronymic #user.last_name
            self.fields['id'].initial               = self.instance.id
            self.fields['user'].initial             = self.instance.user.id

#            for filedname in ['postcode','region','city','street','house']:
#                self.fields[filedname].required = True
            self.fields['postaddress'].help_text = u'Данные адреса, отображаемые в заявлении'
            self.fields['postaddress'].widget.attrs['readonly'] = True

            if self.instance.user.is_ipr:
                if self.instance.confirmed:
                    self.fields['email'].help_text = u'Утвержденный налоговый консультант'
                elif self.instance.rejected:
                    self.fields['email'].help_text = u'Отклоненный налоговый консультант'
                else:
                    self.fields['email'].help_text = u'Налоговый консультант'
            else:
                self.fields['email'].help_text = u'Слушатель'

            self.fields['last_name'].widget.attrs['onkeyup'] = 'Ru(this);'
            self.fields['first_name'].widget.attrs['onkeyup'] = 'Ru(this);'
            self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'
            self.fields['length_of_work'].widget.attrs['onkeypress'] ='return event.charCode >= 48 && event.charCode <= 57'

    def clean(self):
        check_unique_email = unique_email(self.cleaned_data.get("email"))
        if check_unique_email.has_key('exist') and check_unique_email['exist'] and check_unique_email.has_key('user') and check_unique_email['user'].id != self.cleaned_data.get("user").id:
            self._errors["email"] = self.error_class([u'Данный e-mail принадлежит другому пользователю'])
            raise forms.ValidationError(u'Данный e-mail принадлежит другому пользователю')
        return self.cleaned_data

    def save(self):
        super(EditPersonForm,self).save(commit=False)
        data = self.cleaned_data

        iprstudent = IprStudent.objects.get(id=data['id'])

        iprstudent.user.email       = data['email']
        iprstudent.user.first_name  = data['first_name']
        iprstudent.user.last_name   = data['last_name']
        iprstudent.user.save()

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or 'first_name' == 'last_name':
                continue
            elif formfieldname == 'profstatus' and data[formfieldname] is not None:
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue

            iprstudent.__dict__[formfieldname] = data[formfieldname]

        iprstudent.save()

class RequestBPersonForm(ModelForm):
    education_institute = forms.ModelChoiceField(required=True,label=u'Образовательная организация',queryset = Education_Institute.objects.filter(education_institute_program__program__level='B').distinct())
    education_programm = forms.ModelChoiceField(required=True,label=u'Программа подготовки',queryset=Education_Programme.objects.filter(level='B',education_institute_program__institute__isnull=False).distinct())

    class Meta:
        model = EducationRequest
        fields=['education_institute','education_programm']
        widgets = {
            'student':               forms.HiddenInput(),
            }

    def __init__(self, *args, **kwargs):
        super(RequestBPersonForm,self).__init__(*args, **kwargs)

    def clean(self):
        education_institute = self.cleaned_data.get('education_institute')
        education_program   = self.cleaned_data.get('education_program')

        if isinstance(education_institute, Education_Institute) and isinstance(education_program, Education_Programme) and not education_institute.is_accredited_on(education_program):
            self._errors["education_program"] = self.error_class([u'Учреждение не аккредитовано на данную программу'])
            raise forms.ValidationError(u"Учреждение не аккредитовано на данную программу")

        return self.cleaned_data

    def save(self,person):
        super(RequestBPersonForm,self).save(commit=False)

        if person.user.has_edu_request == True:
            return False

        edu_request         = EducationRequest()
        edu_request.student = person
        edu_request.institute_program = Education_Institute_Program.objects.get(institute = self.cleaned_data["education_institute"],program = self.cleaned_data["education_programm"],program__level='B')
        edu_request.level              = edu_request.institute_program.program.level
        edu_request.save()

        if person.user.has_edu_request == False:
            person.user.has_edu_request = True
            person.user.save( )

        try:
            new_edurequest_mail(edu_request)
        except :
            print str(sys.exc_info())


#class RequestUPersonForm(ModelForm):
#    education_institute = forms.ModelChoiceField(required=True,label=u'Образовательная организация',queryset = Education_Institute.objects.filter(education_institute_program__program__level='U').distinct())
#    education_programm  = forms.ModelChoiceField(required=True,label=u'Программа подготовки',queryset=Education_Programme.objects.filter(level='U',education_institute_program__institute__isnull=False).distinct())
#
#    class Meta:
#        model = EducationRequest
#        fields=['student','education_institute','education_programm','practic_period','payment_method','details']
#        widgets = {
#            'student':               forms.HiddenInput(),
#            }
#
#    def __init__(self, *args, **kwargs):
#        super(RequestUPersonForm,self).__init__(*args, **kwargs)
#
#
#    def clean(self):
#        education_institute = self.cleaned_data.get('education_institute')
#        education_program   = self.cleaned_data.get('education_program')
#
#        if isinstance(education_institute, Education_Institute) and isinstance(education_program, Education_Programme) and not education_institute.is_accredited_on(education_program):
#            self._errors["education_program"] = self.error_class([u'Учреждение не аккредитовано на данную программу'])
#            raise forms.ValidationError(u"Учреждение не аккредитовано на данную программу")
#        return self.cleaned_data
#
#    def save(self):
#        super(RequestUPersonForm,self).save(commit=False)
#        data = self.cleaned_data


class RevivalConsultForm(ModelForm):
    patronymic = forms.CharField(max_length=50,label=u'Отчество')

    class Meta:
        model = IprStudent
        fields=['patronymic','region','city','telephone','education', 'profstatus','attestat_number','attestat_date','attestat_length',
                #'birthday','passport_seria','passport_number','passport_who_gave','passport_when_gave','passport_code', #'placebirth','postcode',
                #'street','house','flat',
                'membership_number','job_place','position','i_am_ip']

        widgets = {
#            'birthday':SelectDateWidget(years = range(year-18,year-100,-1)),
            'attestat_date':SelectDateWidget(years = range(year,year-100,-1)),
#            'passport_when_gave':SelectDateWidget(years = range(year,year-100,-1)),
            'attestat_length':SelectDateWidget(years = range(year+50,year-100,-1)),
            }

    def __init__(self, *args, **kwargs):
        super(RevivalConsultForm, self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            if field != 'i_am_ip' and field != 'flat':
                self.fields[field].required = True
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].label

        self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'


    def clean(self):
        try:
            consult_exist = IprStudent.objects.get(attestat_number=self.cleaned_data["attestat_number"])
        except:
            consult_exist = None
            #if email_exist is not None:
        if consult_exist:
            self._errors["attestat_number"] = self.error_class([u'Пользователь с таким номером аттестата уже зарегистрирован. Используйте другой, либо авторизуйтесь.'])
            raise forms.ValidationError(u'Пользователь с таким номером аттестата уже зарегистрирован. Используйте другой, либо авторизуйтесь.')

        try:
            consult_exist = IprStudent.objects.get(membership_number=self.cleaned_data["membership_number"])
        except:
            consult_exist = None
            #if email_exist is not None:
        if consult_exist:
            error_text = u'Пользователь с номером билета {0!s} зарегистрирован под ником {1!s}'.format(consult_exist.membership_number, consult_exist.user.email)

            self._errors["membership_number"] = self.error_class([error_text])
            raise forms.ValidationError(error_text)

        if self.cleaned_data.get("job_place", "").isspace():
            self._errors["job_place"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

        if self.cleaned_data.get("position", "").isspace():
            self._errors["position"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

#        if self.cleaned_data.get("postaddress", "").isspace():
#            self._errors["postaddress"] = self.error_class([u'Обязательное поле'])
#            raise forms.ValidationError(u'Обязательное поле')

        if self.cleaned_data.get("attestat_number", "").isspace():
            self._errors["attestat_number"] = self.error_class([u'Обязательное поле'])
            raise forms.ValidationError(u'Обязательное поле')

#        if self.cleaned_data.get("passport_who_gave", "").isspace():
#            self._errors["passport_who_gave"] = self.error_class([u'обязательное поле'])
#            raise forms.ValidationError(u'Обязательное поле')

        return self.cleaned_data


    def save(self, user):
        super(RevivalConsultForm,self).save(commit=False)
        data = self.cleaned_data
        iprstudent = IprStudent()
        iprstudent.user = user

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or formfieldname == 'last_name':
                continue
            elif formfieldname == 'profstatus':
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue

            iprstudent.__dict__[formfieldname] = data[formfieldname]

        iprstudent.confirmed = False
        iprstudent.save()

class RevivalStudentForm(ModelForm):
    patronymic = forms.CharField(max_length=50,label=u'Отчество')

    class Meta:
        model = IprStudent
        fields=['patronymic','region','city','telephone','education','length_of_work'] #,'birthday','postcode','street','house','flat'
        widgets = {
#            'birthday':SelectDateWidget(years = range(year-18,year-100,-1)),
            'telephone':forms.TextInput(attrs={'size':'10'}),
        }

    def __init__(self, *args, **kwargs):
        super(RevivalStudentForm,self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            if field != 'flat':
                self.fields[field].required = True
        self.fields['patronymic'].widget.attrs['onkeyup'] = 'Ru(this);'

    def clean(self):
        return self.cleaned_data

    def save(self,user,education_institute,education_program):
        super(RevivalStudentForm,self).save(commit=False)
        data = self.cleaned_data

        iprstudent = IprStudent()
        iprstudent.user            = user

        for formfieldname in self._meta.__dict__['fields']:
            if formfieldname == 'first_name' or formfieldname == 'last_name':
                continue
            elif formfieldname == 'profstatus':
                iprstudent.__dict__['profstatus_id'] = data[formfieldname].id
                continue
            elif formfieldname == 'education':
                iprstudent.__dict__['education_id'] = data[formfieldname].id
                continue
            iprstudent.__dict__[formfieldname] = data[formfieldname]

        iprstudent.save()

        #*****save request*****
        edu_request = EducationRequest()
        edu_request.student = iprstudent
        edu_request.institute_program = Education_Institute_Program.objects.get(institute = education_institute,program = education_program,program__level='B')
        edu_request.save()

        if user.has_edu_request == False:
            user.has_edu_request = True
            user.save()

        try:
            new_edurequest_mail(edu_request)
        except :
            print str(sys.exc_info())


class RevivalAddressForm(ModelForm):
    class Meta:
        model = IprStudent
        fields=['region','city','telephone'] #'postcode','region','city','street','house','flat','telephone']
        widgets = {
            'telephone':forms.TextInput(attrs={'size':'10'}),
            }

    def __init__(self, *args, **kwargs):
        super(RevivalAddressForm,self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            if field != 'flat':
                self.fields[field].required = True


    def clean(self):
        return self.cleaned_data
#
    def save(self,user):
        super(RevivalAddressForm,self).save(commit=False)
        data = self.cleaned_data
        try:
            student = IprStudent.objects.get(user = user)

            for formfieldname in self._meta.__dict__['fields']:
                student.__dict__[formfieldname] = data[formfieldname]

            student.save()
            return True
        except:
            return False
        return False

class CommonTextForm(ModelForm):
    class Meta:
        model = CommonText
        fields = ['content']

    def __init__(self, *args, **kwargs):
        super(CommonTextForm, self).__init__(*args, **kwargs)
