# -*- coding: utf-8 -*-
__author__ = 'andrei'

from base_datatable_view import BaseDatatableView
import itertools
from models import User, IprStudent
from education_programmes.models import Group, StudentGroup
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

class OrderListJson(BaseDatatableView):
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['id']

    model = IprStudent
    columns = ['id']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 100000

    def render_column(self, row, column):
        print 'columns'
        print row, column
        return super(OrderListJson, self).render_column(row, column)

    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable

        users = IprStudent.objects.filter(user__is_active=True).order_by('user__last_name')

        return users

    def filter_queryset(self, qs):
        # use request parameters to filter queryset

        # simple example:


        sSearch = self.request.GET.get('sSearch_0')
        if sSearch:
            ss = sSearch.split()
            print "length"
            print len(ss)
			
            #query = reduce(operator.or_, (Q(user__last_name__icontains=x) for x in ss))

            Qr = None
            for field in ss:
                q = Q(**{"user__last_name__icontains": field })
                if Qr:
                    Qr = Qr | q # or & for filtering
                else:
                    Qr = q

                q = Q(**{"user__first_name__icontains": field })
                if Qr:
                    Qr = Qr | q # or & for filtering
                else:
                    Qr = q

            qs = qs.filter(Qr) #Q(user__last_name__icontains=sSearch) | Q(user__first_name__icontains=sSearch))
            # aahripunov            
            #if len(ss) == 1:
            #    qs = qs.filter(user__last_name__contains=ss[0])
            #if len(ss) == 2:
            #    qs = qs.filter(Q(user__first_name__startswith=ss[1]) | Q(user__last_name__startswith=ss[0]) | Q(user__first_name__startswith=ss[0]) | Q(user__last_name__startswith=ss[1]))
            #elif len(ss) == 3:
            #    qs = qs.filter(Q(user__first_name__startswith=ss[1]) | Q(user__last_name__startswith=ss[0]) | Q(user__first_name__startswith=ss[0]) | Q(user__last_name__startswith=ss[1]) | Q(patronymic__startswith=ss[2]))

            #users = User.objects.filter(Q(first_name__startswith=ss[1]) | Q(last_name__startswith=ss[0]) | Q(first_name__startswith=ss[0]) | Q(last_name__startswith=ss[1]))

            #qs = qs.filter(user__in=users)

        sSearch = self.request.GET.get('sSearch_1')
        if sSearch:
            print sSearch
            qs = qs.filter(attestat_number__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_2')
        if sSearch:
            print sSearch
            qs = qs.filter(attestat_date__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_3')
        if sSearch:
            print sSearch
            qs = qs.filter(attestat_length__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_4')
        if sSearch:
            print sSearch
            qs = qs.filter(membership_number__startswith=sSearch)


        sSearch = self.request.GET.get('sSearch_5')
        if sSearch:
            print sSearch
            qs = qs.filter(study_status__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_6')
        if sSearch:
            print sSearch
            qs = qs.filter(study_city__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_7')
        if sSearch:
            print sSearch
            groups = Group.objects.filter(name__startswith=sSearch)
            studentGroups = StudentGroup.objects.filter(group__in=groups)

            qs = qs.filter(id__in=studentGroups.values('student'))

        sSearch = self.request.GET.get('sSearch_8')
        if sSearch:
            print sSearch
            studentGroups = StudentGroup.objects.filter(exam_date__startswith=sSearch)

            qs = qs.filter(id__in=studentGroups.values('student'))

        sSearch = self.request.GET.get('sSearch_9')
        if sSearch:
            print sSearch
            studentGroups = StudentGroup.objects.filter(exam_result__startswith=sSearch)

            qs = qs.filter(id__in=studentGroups.values('student'))

        sSearch = self.request.GET.get('sSearch_10')
        if sSearch:
            print sSearch
            qs = qs.filter(profile__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_11')
        if sSearch:
            print sSearch
            qs = qs.filter(birthday__startswith=sSearch)


        sSearch = self.request.GET.get('sSearch_12')
        if sSearch:
            print sSearch
            qs = qs.filter(telephone__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_13')
        if sSearch:
            print sSearch
            print '12'
            users = User.objects.filter(email__startswith=sSearch)
            print 'users'
            print users
            qs = qs.filter(user__in=users)

        sSearch = self.request.GET.get('sSearch_14')
        if sSearch:
            print sSearch
            qs = qs.filter(postaddress__startswith=sSearch)

        sSearch = self.request.GET.get('sSearch_15')
        if sSearch:
            print sSearch
            ss = sSearch.split()
            if len(ss) == 1:
                qs = qs.filter(passport_seria__startswith=ss[0])
            elif len(ss) == 2:
                qs = qs.filter(Q(passport_seria__startswith=ss[0]) & Q(passport_number__startswith=ss[1]))
            elif len(ss) == 3:
                qs = qs.filter(Q(passport_seria__startswith=ss[0]) & Q(passport_number__startswith=ss[1]) & Q(passport_who_gave__strartswith=ss[2]))
            elif len(ss) == 4:
                qs = qs.filter(Q(passport_seria__startswith=ss[0]) & Q(passport_number__startswith=ss[1]) & Q(passport_who_gave__strartswith=ss[2]) & Q(passport_when_gave__startswith=ss[3]))
            elif len(ss) == 5:
                qs = qs.filter(Q(passport_seria__startswith=ss[0]) & Q(passport_number__startswith=ss[1]) & Q(passport_who_gave__strartswith=ss[2]) & Q(passport_when_gave__startswith=ss[3]) & Q(passport_code__startswith=ss[4]))



        return qs

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        json_data = []
        for item in qs:
            try:
                studentGroup = StudentGroup.objects.get(student=item.id)
            except:
                studentGroup = None
            #print studentGroup.group.id
            if studentGroup is not None:
                group = Group.objects.get(id=studentGroup.group.id)
                groupName = group.name
                # print groupName
                institute = group.education_institute.name
                # print institute
                #programm = group.education_programm.name
                #pass
            else:
                 groupName = None
                 group = None
                 institute = None
                 programm = None

            array = [
                item.get_fio,
                item.attestat_number,
                item.attestat_date,
                item.attestat_length,
                item.membership_number,
                item.study_status,
                item.study_city,
                '' if group is None else group.name,
                '' if studentGroup is None else studentGroup.exam_date,
                '' if studentGroup is None else studentGroup.exam_result,#attestovan
                '',#profile
                item.birthday,
                item.telephone,
                item.email_for_admin(),
                item.postaddress,
                '' if item.passport_seria is None else item.passport_seria
                    + ' ' + '' if item.passport_number is None else item.passport_number
                    + ' ' + '' if item.passport_who_gave is None else item.passport_who_gave
                    + ' ' + '' if item.passport_when_gave is None else item.passport_when_gave
                    + ' ' + '' if item.passport_code is None else item.passport_code,
                '<div type="program" item="%s"  title="Редактировать" class="icon edit_item"></div>'%item.id +
                '<div type="program" item="%s"  title="Удалить" class="icon delete_item"></div>'%item.id
            ]
            json_data.append(array);
        return json_data



