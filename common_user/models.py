# -*- coding: utf-8 -*-
from django.db import models, connection
from django.db.models import Q
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.conf import settings
from slugify import slugify
import sys
from datetime import datetime,time,timedelta
from IPR.utils import is_address_field_no_empty


COMMON_TEXT_SLUGS = (u'Политика по обработке персональных данных',)
COMMON_TEXT_SLUGS_DICT = {'PP' : COMMON_TEXT_SLUGS[0]
                        }

COMMON_TEXT_SLUGS_CHOICES = (
    ('PP', COMMON_TEXT_SLUGS[0]),
    )

class CommonText(models.Model):
    choice_slug    = models.CharField(max_length=2, choices=COMMON_TEXT_SLUGS_CHOICES, verbose_name=u'Идентификатор', unique=True)
    slug           = models.SlugField()
    content        = models.TextField(max_length=4096,blank=True,null=True,verbose_name=u'Содержимое')

    def __unicode__(self):
        if COMMON_TEXT_SLUGS_DICT.has_key(self.choice_slug):
            return COMMON_TEXT_SLUGS_DICT[self.choice_slug]
        return self.slug

    @classmethod
    def get_instance(cls, choice_slug):
        obj, created = cls.objects.get_or_create(choice_slug=choice_slug)
        # чтобы сгенерировать slug сохраняем новый объект
        if created:
            obj.save()
        return obj

    @classmethod
    def url(cls, choice_slug):
        obj = cls.get_instance(choice_slug)
        return u'/' + obj.slug + u'/'

    class Meta:
        verbose_name = u'Общий текст'
        verbose_name_plural = u'Общие тексты'
        ordering = ['choice_slug']

    def save(self,*args,**kwargs):
        self.slug = slugify(COMMON_TEXT_SLUGS_DICT[self.choice_slug])
        super(CommonText,self).save(*args,**kwargs)

class Education(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Текст')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Образование'
        verbose_name_plural = u'Образование'
        ordering = ['title']

class ProfStatus(models.Model):
    title = models.CharField(max_length = 100,verbose_name = u'Текст')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Профессиональный статус'
        verbose_name_plural = u'Профессиональные статусы'
        ordering = ['title']


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, first_name, last_name, password=None):
        user = self.create_user(
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )

        user.is_admin = True
        user.save(using=self._db)
        return user

    def create_olymp_user(self, username, first_name, last_name, password=None):
        if not username:
            raise ValueError('Users must have an username')

        user = self.model(
            email=UserManager.normalize_email(username + u'@olymp.' + username + u'.com'),
            username = username,
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.is_olympic = True
        user.save(using=self._db)

        return user

    def create_olymp_superuser(self, username, first_name, last_name, password=None):
        if not username:
            raise ValueError('Users must have an username')

        user = self.create_olymp_user(
            username = username,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )

        user.is_admin = True
        user.is_olympic = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser,PermissionsMixin):
    email               = models.CharField(u'login', max_length=75, unique=True)
    username            = models.CharField(u'username', max_length=75,blank=True,null=True)
    first_name          = models.CharField(u'first_name', max_length=60)
    last_name           = models.CharField(u'last_name', max_length=60)
    is_active           = models.BooleanField(default=True)
    is_admin            = models.BooleanField(default=False)
    is_olesya           = models.BooleanField(default=False)
    is_curator          = models.BooleanField(default=False)
    is_student          = models.BooleanField(default=False)
    is_ipr              = models.BooleanField(default=False)
    has_edu_request     = models.BooleanField(default=False)

    is_olympic          = models.BooleanField(default=False)
    is_univer           = models.BooleanField(default=False)
    is_participant      = models.BooleanField(default=False)

    def get_model(self):
        return 'ipr'

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        return u'%s %s' % (self.last_name, self.first_name)

    def get_short_name(self):
        return u'%s' % self.first_name

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def get_user_group(self):
        return

    def treatment(self):
        if self.is_univer:
            for univer in self.university_set.all():
                return univer.name
        return self.get_full_name()

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def delete(self):
        if self.is_admin == True:
            return False
        else:
            super(User, self).delete()


# Common student
class Student(models.Model):
    user = models.ForeignKey(User,verbose_name=u'Логин')
    patronymic = models.TextField(max_length=50,blank=True,null=True,verbose_name=u'Отчество')
    registered_date = models.DateField(auto_now=True,verbose_name=u'Дата регистрации')
    slug = models.SlugField(max_length=50,unique=True,verbose_name=u'Slug')

    def save(self,*args,**kwargs):
        self.slug = slugify(self.user.email)
        l_uniq_slug = False

        l_iter = 1
        while Student.objects.filter(Q(slug = self.slug),~Q(user__id = self.user.id)).count() > 0:
            l_src_slug = self.user.email + str(l_iter)
            self.slug = slugify(l_src_slug)
            l_iter = l_iter + 1

            if l_iter > 10:
                break
            print self.slug

        super(Student,self).save(*args,**kwargs)

    class Meta:
        verbose_name = u'Студент'
        verbose_name_plural = u'Студенты'
        ordering = ['user']

    def __unicode__(self):
        if self.user is not None:
            return self.user.email + u" " + self.user.first_name
        else:
            return str(self.id)

    @property
    def get_fio(self):
        return self.user.last_name.capitalize() + " " + self.user.first_name.capitalize() + " " + (self.patronymic.capitalize()  if self.patronymic is not None else "")

    def get_short_fio(self, transliteration = False):
        if self.user is not None:
            last_name    = ( slugify(self.user.last_name) if transliteration else self.user.last_name )
            name_letter  = ( slugify(self.user.first_name) if transliteration else self.user.first_name )[:1]
            if self.patronymic is not None:
                patronymic_letter = ( slugify(self.patronymic) if transliteration else self.patronymic )[:1]
            else:
                patronymic_letter = ""
            return last_name.capitalize() + " " + name_letter.capitalize() + ". " + patronymic_letter.capitalize() + "."
        return u''

    @property
    def passport_seria_number(self):
        return (self.passport_seria if self.passport_seria is not None else u'') + u" " +  (self.passport_number if self.passport_number is not None else u'')


    def email_for_admin(self):
        return self.user.email


class IprStudent(Student):
    birthday = models.DateField(blank=True,null=True,verbose_name=u'Дата рождения')

    postaddress = models.TextField(blank=True,null=True,verbose_name=u'Регион и город проживания')

    postcode = models.CharField(max_length=6,blank=True,null=True,verbose_name=u'Почтовый индекс')
    region   = models.CharField(max_length=100,blank=True,null=True,verbose_name=u'Регион')
    city     = models.CharField(max_length=100,blank=True,null=True,verbose_name=u'Город')
    street   = models.CharField(max_length=100,blank=True,null=True,verbose_name=u'Улица')
    house    = models.CharField(max_length=10,blank=True,null=True,verbose_name=u'Дом')
    flat     = models.CharField(max_length=10,blank=True,null=True,verbose_name=u'Квартира')

    telephone = models.CharField(max_length=50,blank=True,null=True,verbose_name=u'Телефон')
    education = models.ForeignKey(Education,null=True,verbose_name=u'Образование')
    profstatus = models.ForeignKey(ProfStatus,blank=True,null=True,verbose_name=u'Профессиональный статус')

    length_of_work = models.BigIntegerField(blank=True,null=True,verbose_name=u'Общий подтвержденный стаж работы в области экономики и/или права, лет')

    attestat_number = models.TextField(blank=True,null=True,verbose_name=u'Номер аттестата')
    attestat_date   = models.DateField(blank=True,null=True,verbose_name=u'Дата получения аттестата')
    attestat_length = models.DateField(blank=True,null=True,verbose_name=u'Срок действия аттестата')

    membership_number = models.CharField(max_length=50,blank=True,null=True,verbose_name=u'Номер членского билета')
    job_place = models.TextField(blank=True,null=True,verbose_name=u'Место работы')
    position = models.TextField(blank=True,null=True,verbose_name=u'Должность')

    i_am_ip = models.BooleanField(default=False,verbose_name=u'Индивидуальный предприниматель')

    job_place_visible = models.NullBooleanField()  # what is it?
    passport_seria = models.CharField(max_length=50,blank=True,null=True,verbose_name=u'Серия паспорта')
    passport_number = models.CharField(max_length=50,blank=True,null=True,verbose_name=u'Номер паспорта')
    passport_who_gave = models.TextField(blank=True,null=True,verbose_name=u'Кем выдан')
    passport_when_gave = models.DateField(blank=True,null=True,verbose_name=u'Когда выдан')
    passport_code = models.CharField(max_length=50,blank=True,null=True,verbose_name=u'Код поздразделения')
    special_notes = models.TextField(blank=True,null=True,verbose_name=u'Особые отметки')  # what is it?

    confirmed = models.BooleanField(default=False,verbose_name=u'Утвержден')
    confirmed_date = models.DateTimeField(blank=True,null=True,auto_now_add=True)
    rejected = models.BooleanField(default=False,verbose_name=u'Отклонён')
    avatar = models.ImageField(blank=True,null=True,upload_to=settings.USER_AVATAR_UPLOAD_DIR)
    agreedata = models.NullBooleanField()

    reason = models.TextField(blank=True,null=True,verbose_name=u'Причина отклонения заявки')

    have_bad_try    = models.BooleanField(default = False,verbose_name=u'Неудачная попытка подать заявку')  # what is it?

    class Meta:
        verbose_name = u'Налоговый консультант'
        verbose_name_plural = u'Налоговые консультанты'

    def confirm(self):
        if ( self.confirmed is None or self.confirmed == False ) and  ( self.rejected is None or self.rejected == False ) :
            self.confirmed = True
            self.confirmed_date = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
            self.save()
            return True
        return False

    def is_confirm(self):
        if self.confirmed is None or self.confirmed == False:
            return False
        return True

    def reject(self, reason):
        if ( self.confirmed is None or self.confirmed == False ) and  ( self.rejected is None or self.rejected == False ) :
            self.rejected = True
            self.reason = reason
            self.save()
            return True
        return False

    def save(self,*args,**kwargs):
        l_merge_address = False

        l_postcode = l_region = l_city = l_street = l_house = l_flat = u''

        l_postcode,l_merge_address = is_address_field_no_empty(self.postcode, l_merge_address)
        l_region,l_merge_address   = is_address_field_no_empty(self.region,l_merge_address)
        l_city,l_merge_address     = is_address_field_no_empty(self.city,l_merge_address)
        l_street,l_merge_address   = is_address_field_no_empty(self.street,l_merge_address)
        l_house,l_merge_address    = is_address_field_no_empty(self.house,l_merge_address)
        l_flat, l_merge_address    = is_address_field_no_empty(self.flat,l_merge_address)

        if l_merge_address:
            self.postaddress = ' '.join([l_postcode, l_region, l_city, l_street, l_house, l_flat])
        super(IprStudent,self).save(*args,**kwargs)

    def delete(self):
        try:
            for studentgroup in self.studentgroup_set.all():
                studentgroup.delete()
            for edurequest in self.educationrequest_set.all():
                edurequest.delete()
            user = self.user
            super(IprStudent,self).delete()
            user.delete()
        except:
            print str(sys.exc_info())


    def __unicode__(self):
        if self.user is not None:
            return self.user.email + u" " + self.user.last_name + u" " + self.user.first_name
        else:
            return str(self.id)

    def get_absolute_url(self):
        return self.slug

    @property
    def get_fields(self):
        data = [(field.verbose_name, field.value_to_string(self)) for field in IprStudent._meta.fields ]
        return data

    @property
    def get_fio(self):
        return self.user.last_name.capitalize() + " " + self.user.first_name.capitalize() + " " + (self.patronymic.capitalize()  if self.patronymic is not None else "")

    def email_for_admin(self):
        return self.user.email

    email_for_admin.short_description = 'E-mail'

    def passport(self):
        return (self.passport_seria if self.passport_seria is not None else u'') + u' ' + (u'№ ' + self.passport_number if self.passport_number is not None else u'')

#    def private_is_empty(self):
#        if self.user.is_ipr and ( self.passport_seria is None or self.passport_number is None or self.passport_who_gave is None or self.passport_when_gave is None or self.passport_code  is None ):
#            return True
#        else:
#            return False

    def address_is_empty(self):
        if self.region is None or self.city is None: # self.postcode is None or  or self.street is None or self.house is None:
            return True
        else:
            return False


    def my_edurequests(self):
        return self.educationrequest_set.select_related('institute_program','institute_program__institute','institute_program__program').all()

    def can_become_ipr(self):
        if self.user.is_ipr == True:
            return False
        if self.user.is_student == True:
            if self.educationrequest_set.count() == 0:
                return True
        return False

    def can_become_student(self):
        if self.user.is_student == True:
            return False
        if self.user.is_ipr == True:
            if self.educationrequest_set.count() == 0:
                return True
        return False

    def get_region(self):
        region_choice = []
        if self.iprstudentmembership_set.all().count() > 0:
            cursor = connection.cursor()
            cursor.execute(""" SELECT DISTINCT epm.region_id
            FROM ipr.education_programmes_iprstudentmembership AS epim
            JOIN ipr.education_programmes_membershipcost AS epm
            ON epim.membership_id = epm.year
            WHERE epim.iprstudent_id = %s""", [self.id])

            region_choice = [item[0] for item in cursor.fetchall()]


        if self.educationrequest_set.all().count() > 0 and len(region_choice) == 0:
            cursor = connection.cursor()
            cursor.execute(""" SELECT DISTINCT epc.region_id
            FROM ipr.education_programmes_educationrequest AS epe
            JOIN ipr.education_programmes_education_institute_program AS epeip
            ON epe.institute_program_id = epeip.id
            JOIN ipr.education_programmes_education_institute AS epei
            ON epeip.institute_id = epei.id
            JOIN ipr.education_programmes_city AS epc
            ON epei.city_id = epc.id
            WHERE epe.student_id = %s""", [self.id])

            region_choice = [item[0] for item in cursor.fetchall()]
        return region_choice


class Recovery(models.Model):
    user = models.ForeignKey(User,verbose_name=u'Логин / E-mail')
    slug = models.CharField(max_length=30,verbose_name=u'Хэш')
    generation_date = models.DateField(blank=True,null=True,verbose_name=u'Дата генерации')
    use = models.BooleanField(blank = True)
    path = models.CharField(max_length=100,verbose_name=u'Сгенерированная ссылка')

    class Meta:
        verbose_name = u'Восстановление пароля'
        verbose_name_plural = u'Восстановление паролей'