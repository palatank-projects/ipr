# -*- coding: utf-8 -*-
__author__ = 'andrei'

from base_datatable_view import BaseDatatableView
import itertools
#from education_programmes.models import InstituteCurator


class CuratorJson(BaseDatatableView):
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['id', 'first_name', 'last_name']

#    model = InstituteCurator
    columns = ['id', 'first_name', 'last_name', 'is_ipr', 'is_student']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 10000

    def render_column(self, row, column):
        return super(CuratorJson, self).render_column(row, column)

#    def get_initial_queryset(self):
#        # return queryset used as base for futher sorting/filtering
#        # these are simply objects displayed in datatable
#
#        curators = InstituteCurator.objects.all()
#
#        return curators

    def filter_queryset(self, qs):
        # use request parameters to filter queryset

        # simple example:

        sSearch = self.request.GET.get('sSearch_0')
        if sSearch:
            qs = qs.filter(id=sSearch)

        sSearch = self.request.GET.get('sSearch_1')
        if sSearch:
            qs = qs.filter(first_name__istartswith=sSearch)

        sSearch = self.request.GET.get('sSearch_2')
        if sSearch:
            qs = qs.filter(last_name__istartswith=sSearch)


        return qs

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        json_data = []
        for item in qs:
            array = [
                item.user.email,
                item.get_fio,
                item.date_started,
                item.date_ended,
                '<div type="program" item="%s"  title="Редактировать" class="edit_item"></div>'%item.id
            ]
            json_data.append(array);
        return json_data



