# -*- coding: utf-8 -*-
from django.contrib import admin
from models import *
from education_programmes.models import EducationRequest,StudentGroup

admin.site.disable_action('delete_selected')

class EducationRequestInline(admin.TabularInline):
    model = EducationRequest

class StudentGroupInline(admin.TabularInline):
    model = StudentGroup

class IprStudentInline(admin.TabularInline):
    model = IprStudent

class IprStudentAdmin(admin.ModelAdmin):
    list_display = ('email_for_admin','_fio','_is_student','_is_ipr','confirmed')

    list_display_links = ('email_for_admin',)

    def _fio(self,obj):
        return obj.get_fio
    _fio.short_description = 'ФИО'

    def _is_student(self,obj):
        if obj.user is None:
            return '-'
        return obj.user.is_student
    _is_student.short_description = 'Подготовка'

    def _is_ipr(self,obj):
        if obj.user is None:
            return '-'
        return obj.user.is_ipr
    _is_ipr.short_description = 'Повышение'


    def _fio(self,obj):
        return obj.get_fio
    _fio.short_description = 'ФИО'

    inlines = [
        EducationRequestInline,StudentGroupInline,
        ]

class UserAdmin(admin.ModelAdmin):
    actions = []
    list_display = ('email','last_name','_fio','is_active','is_admin','is_curator','is_student','is_ipr','has_edu_request','is_olympic','is_univer','is_participant')

    list_display_links = ('email',)

    def _fio(self,obj):
        return obj.get_full_name()
    _fio.short_description = 'ФИО'

    inlines = [
        IprStudentInline,
        ]

class StudentAdmin(admin.ModelAdmin):
    list_display = ('email_for_admin','_fio','slug')

    list_display_links = ('email_for_admin',)

    def _fio(self,obj):
        return obj.get_fio
    _fio.short_description = 'ФИО'

    inlines = [
        EducationRequestInline,StudentGroupInline,
        ]


class RecoveryAdmin(admin.ModelAdmin):
    list_display = ('slug','user','generation_date','use')

    list_display_links = ('slug',)

admin.site.register(Education)
admin.site.register(User,UserAdmin)
admin.site.register(Recovery,RecoveryAdmin)
admin.site.register(IprStudent,IprStudentAdmin)
admin.site.register(Student,StudentAdmin)

