# -*- coding: utf-8 -*-
from django.db import models
from people.models import OlympStudent, University
from datetime import datetime,date,time,timedelta
import sys


CORRECT_OPTION = (u'1',u'2',u'3',u'4')
CORRECT_OPTION_DICT = {'1' : CORRECT_OPTION[0],
                       '2' : CORRECT_OPTION[1],
                       '3' : CORRECT_OPTION[2],
                       '4' : CORRECT_OPTION[3]
                       }

CORRECT_OPTION_CHOICES = (
    ('1', CORRECT_OPTION[0]),
    ('2', CORRECT_OPTION[1]),
    ('3', CORRECT_OPTION[2]),
    ('4', CORRECT_OPTION[3]),
    )

today = datetime.today()

# Create your models here.

class Test(models.Model):
    slug               = models.IntegerField(max_length = 4,verbose_name=u'Год')
    is_active          = models.BooleanField(default= False,verbose_name=u'Активный')
    name               = models.TextField(verbose_name=u'Название')
    begin              = models.DateTimeField(verbose_name=u'Начало проведения')
    end                = models.DateTimeField(verbose_name=u'Окончание проведения')
    begin_registration = models.DateTimeField(verbose_name=u'Начало регистрации')
    end_registration   = models.DateTimeField(verbose_name=u'Окончание регистрации')
    max_time           = models.TimeField(verbose_name=u'Максимальное время прохождения')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Тест'
        verbose_name_plural = u'Тесты'
        ordering = ['name']

    def registration_not_start(self):
        #datetime.strptime(request.POST.get('begin').split(".")[0],"%Y-%m-%dT%H:%M:%S")
        begin_registration = self.begin_registration + timedelta(hours=4)
        if datetime.strptime(str(begin_registration).split("+")[0],"%Y-%m-%d %H:%M:%S") > today:
            return True
        return  False

    def registration_start(self):
        begin_registration = self.begin_registration + timedelta(hours=4)
        end_registration = self.end_registration + timedelta(hours=4)
        if datetime.strptime(str(begin_registration).split("+")[0],"%Y-%m-%d %H:%M:%S") <= today and datetime.strptime(str(end_registration).split("+")[0],"%Y-%m-%d %H:%M:%S") >= today:
            return True
        return  False

    def registration_finish(self):
        end_registration = self.end_registration + timedelta(hours=4)
        if datetime.strptime(str(end_registration).split("+")[0],"%Y-%m-%d %H:%M:%S") < today:
            return True
        return  False

    def is_go_now(self):
        begin = self.begin + timedelta(hours=4)
        end = self.end + timedelta(hours=4)
        if datetime.strptime(str(begin).split("+")[0],"%Y-%m-%d %H:%M:%S") <= today and datetime.strptime(str(end).split("+")[0],"%Y-%m-%d %H:%M:%S") >= today:
            return True
        return  False

    def is_finished(self):
        end = self.end + timedelta(hours=4)
        if datetime.strptime(str(end).split("+")[0],"%Y-%m-%d %H:%M:%S") < today:
            return True
        return  False

    def have_univer_request_on_me(self,user):
        if self.requestontest_set.filter(university__user = user).count() > 0:
            return True
        else:
            return False

    def have_univer_student_on_me(self,univer):
        if self.studentontest_set.filter(university = univer).count() > 0:
            return True
        else:
            return False
            
    def have_student_pass_me(self,user):
        if self.passing_set.filter(student__user = user).count() > 0:
            return True
        else:
            return False
        
    def question_list(self):
        return self.question_set.all().order_by('number')


class RequestOnTest(models.Model):
    test               = models.ForeignKey(Test)
    university         = models.ForeignKey(University)
    field              = models.CharField(max_length=100,blank=True,null=True,verbose_name=u'Поле')


    class Meta:
        verbose_name = u'Заявка на тест'
        verbose_name_plural = u'Заявки на тесты'
        ordering = ['test','university']

class StudentOnTest(models.Model):
    test               = models.ForeignKey(Test)
    university         = models.ForeignKey(University)
    student            = models.ForeignKey(OlympStudent)

    class Meta:
        verbose_name = u'Участник в заявке на тест'
        verbose_name_plural = u'Участники в заявках на тесты'
        ordering = ['test','university','student']

class Question(models.Model):
    test               = models.ForeignKey(Test)
    text               = models.TextField(verbose_name=u'Текст')
    number             = models.IntegerField(verbose_name=u'Порядковый номер')
    option1            = models.CharField(max_length=500,verbose_name=u'Первый вариант ответа')
    option2            = models.CharField(max_length=500,verbose_name=u'Второй вариант ответа')
    option3            = models.CharField(max_length=500,verbose_name=u'Третий вариант ответа')
    option4            = models.CharField(max_length=500,verbose_name=u'Четвертый вариант ответа')
    correct_option     = models.CharField(max_length=1,choices=CORRECT_OPTION_CHOICES,default='1',verbose_name=u'Правильный вариант ответа')

    def __unicode__(self):
        return str(self.number) + u'. ' + self.text

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'
        ordering = ['test','number']

class Passing(models.Model):
    test               = models.ForeignKey(Test) #,unique=True)
    university         = models.ForeignKey(University) #,unique=True)
    student            = models.ForeignKey(OlympStudent) #,unique=True)
    begin              = models.DateTimeField(verbose_name= u'Начало прохождения теста')
    finish             = models.DateTimeField(null=True,blank=True,verbose_name= u'Окончание прохождения теста')
    grade              = models.IntegerField(null=True,blank=True,verbose_name= u'количество баллов')

    def __unicode__(self):
        return self.test.name + u'. ' + self.student.get_fio + u' ' + (str(self.grade) if self.grade is not None else u'')

    class Meta:
        verbose_name = u'Прохождение теста'
        verbose_name_plural = u'Прохождения тестов'
        ordering = ['test','begin']
    
    @property
    def timing(self):
        return self.finish - self.begin
        
class Answer(models.Model):
    passing            = models.ForeignKey(Passing)
    question           = models.ForeignKey(Question)
    result             = models.BooleanField(default=False,verbose_name= u'Результат')
    chosen_option      = models.CharField(max_length=1,choices=CORRECT_OPTION_CHOICES,null=True,blank=True,verbose_name=u'Выбранный вариант ответа')

    def __unicode__(self):
        return self.chosen_option

    class Meta:
        verbose_name = u'Ответ на вопрос'
        verbose_name_plural = u'Ответы на вопросы'
        ordering = ['passing','question']

    @property
    def chosen_option_text(self):
        if self.chosen_option == '1':
            return self.question.option1
        elif self.chosen_option == '2':
            return self.question.option2
        elif self.chosen_option == '3':
            return self.question.option3
        elif self.chosen_option == '4':
            return self.question.option4
        return u''

class OlympContent(models.Model):
    content = models.TextField(blank=True,null=True,verbose_name=u'Содержимое')

    def __unicode__(self):
        if self.content is None:
            return u''
        return self.content

    class Meta:
        verbose_name = u'Содержимое главной страницы олимпиады'
        verbose_name_plural = u'Содержимое страниц олимпиады'