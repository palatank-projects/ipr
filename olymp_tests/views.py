# -*- coding: utf-8 -*-
# Create your views here.
from models import *
from people.models import *
from forms  import *
from people.forms  import OlympStudentShortForm,OlympStudentForm
from django.db.models import Q,Avg, Max, Min
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
#from django.utils import simplejson
import json as simplejson
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
#from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response,redirect
from django.template import RequestContext,Context
#from django.template.loader import get_template,render_to_string
import json,sys,os, math
#from cStringIO import StringIO
#from slugify import slugify
#from django.forms.formsets import formset_factory
from django.db import transaction
import string,random
from datetime import datetime,date,time,timedelta
from django.contrib.auth import authenticate, login, logout
from django.db import connection

def word_generator(size=16, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def test_registration(request,id):
    model_item = get_object_or_404(Test,id=id)
    response = {}
    response['registration_not_start'] = model_item.registration_not_start()
    response['registration_start'] = model_item.registration_start()
    response['registration_finish'] = model_item.registration_finish()
    
    #model_item.end_registration = model_item.end_registration + timedelta(hours=4)
    response['registration_end'] = str(model_item.end_registration).split("+")[0]
    response['registration_begin'] = str(model_item.begin_registration).split("+")[0]
    
    response['end'] = str(model_item.end).split("+")[0]
    response['begin'] = str(model_item.begin).split("+")[0]
    
    response['is_go_now'] = model_item.is_go_now()
    response['is_finished'] = model_item.is_finished()
    
    return  HttpResponse(simplejson.dumps(response))
    #datetime.strptime(request.POST.get('begin').split(".")[0],"%Y-%m-%dT%H:%M:%S")
    #model_item.begin_registration = model_item.begin_registration + timedelta(hours=4)
    #return  HttpResponse(datetime.strptime(str(model_item.begin_registration).split("+")[0],"%Y-%m-%d %H:%M:%S"))
    

@csrf_exempt
def test(request,id=None):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    init_dict = {}

    if request.user.is_authenticated() and request.user.is_admin:
        ItemModel = Test
        ItemForm = TestForm

        if id == None:
            model_item = ItemModel()
        else:
            model_item = get_object_or_404(ItemModel,id=id)
        
        new_question_count = 0
        new_question_forms= []
        old_question_count = 0
        old_question_forms= []
        
        if request.method == 'POST':
            try:
                new_question_count = int(request.POST.get('new_question_count',0))
            except:
                new_question_count = 0

            if new_question_count > 0 :
                texts = request.POST.getlist('new_question_text')
                option1s = request.POST.getlist('new_question_option1')
                option2s = request.POST.getlist('new_question_option2')
                option3s = request.POST.getlist('new_question_option3')
                option4s = request.POST.getlist('new_question_option4')
                correct_options = request.POST.getlist('new_question_correct_option')
                question_numbers = request.POST.getlist('new_question_number')
                must_deletes = request.POST.getlist('new_question_must_delete')
            else:
                texts = []
                option1s = []
                option2s = []
                option3s = []
                option4s = []
                correct_options = []
                question_numbers = []
                must_deletes = []

            new_question_form_valid = True
            new_question_index = 0 #new_question_count
            # итоговое количество новых вопросов
            resume_new_question_count = new_question_count

            while new_question_index < new_question_count :
                question_data = {'new_question_text':texts[new_question_index],
                                 'new_question_option1': option1s[new_question_index],
                                 'new_question_option2': option2s[new_question_index],
                                 'new_question_option3': option3s[new_question_index],
                                 'new_question_option4': option4s[new_question_index],
                                 'new_question_correct_option': correct_options[new_question_index],
                                 'new_question_number': question_numbers[new_question_index],
                                 'new_question_must_delete': must_deletes[new_question_index],
                                 'new_question_delete_checkbox': must_deletes[new_question_index]
                                 }

                if int(must_deletes[new_question_index]) == 0:
                    new_question_form = QuestionShortForm(question_data) #question_blunk)
                    if not new_question_form.is_valid():
                        new_question_form_valid = False
                
                    new_question_forms.append(new_question_form)
                else:
                    resume_new_question_count = resume_new_question_count - 1

                new_question_index = new_question_index + 1

            if id is not None:
                old_question_count = model_item.question_set.all().count()
            else:
                old_question_count = 0

            if old_question_count > 0 :
                texts = request.POST.getlist('text')
                ids = request.POST.getlist('id')
                option1s = request.POST.getlist('option1')
                option2s = request.POST.getlist('option2')
                option3s = request.POST.getlist('option3')
                option4s = request.POST.getlist('option4')
                correct_options = request.POST.getlist('correct_option')
                tests = request.POST.getlist('test')
                question_numbers = request.POST.getlist('number')
                must_deletes = request.POST.getlist('must_delete')
            else:
                ids = []
                texts = []
                option1s = []
                option2s = []
                option3s = []
                option4s = []
                correct_options = []
                question_numbers = []
                tests = []
                must_deletes = []

            old_question_form_valid = True
            old_question_index = 0
            # итоговое количество старых вопросов
            resume_old_question_count = old_question_count

            while old_question_index < old_question_count :
                question_data = {'id':ids[old_question_index],
                                 'text':texts[old_question_index],
                                 'option1': option1s[old_question_index],
                                 'option2': option2s[old_question_index],
                                 'option3': option3s[old_question_index],
                                 'option4': option4s[old_question_index],
                                 'correct_option': correct_options[old_question_index],
                                 'number': question_numbers[old_question_index],
                                 'test': tests[old_question_index],
                                 'must_delete': must_deletes[old_question_index],
                                 'delete_checkbox': must_deletes[old_question_index]
                                 }

                old_question_form =  QuestionForm(question_data) #question_blunk)
                if not old_question_form.is_valid() and int(must_deletes[old_question_index]) == 0:
                    old_question_form_valid = False
                old_question_forms.append(old_question_form)
                if int(must_deletes[old_question_index]) == 1:
                    resume_old_question_count = resume_old_question_count - 1

                old_question_index = old_question_index + 1

            form = ItemForm(request.POST) # A form bound to the POST data

            if form.is_valid() and new_question_form_valid and old_question_form_valid and (resume_old_question_count + resume_new_question_count) > 0:
#                try:
                for formfieldname in form._meta.__dict__['fields']:
                    model_item.__dict__[formfieldname] = form.cleaned_data[formfieldname]

                model_item.slug = datetime.today().year
                model_item.save()
                
                for new_question_form in new_question_forms:
                    question = Question()
                    question.__dict__['test_id'] = model_item.id
                    question.__dict__['text'] = new_question_form.cleaned_data['new_question_text']
                    question.__dict__['option1'] = new_question_form.cleaned_data['new_question_option1']
                    question.__dict__['option2'] = new_question_form.cleaned_data['new_question_option2']
                    question.__dict__['option3'] = new_question_form.cleaned_data['new_question_option3']
                    question.__dict__['option4'] = new_question_form.cleaned_data['new_question_option4']
                    question.__dict__['correct_option'] = new_question_form.cleaned_data['new_question_correct_option']
                    question.__dict__['number'] = new_question_form.cleaned_data['new_question_number']
                    question.save()

                for old_question_form in old_question_forms:
                    question = Question.objects.get(id = int(old_question_form.cleaned_data['id']))

                    if int(old_question_form.cleaned_data['must_delete']) == 1:
                        question.delete()


                    else:
                        question.text = old_question_form.cleaned_data['text']
#                        old_question_form.cleaned_data['test']
                        question.option1 = old_question_form.cleaned_data['option1']
                        question.option2 = old_question_form.cleaned_data['option2']
                        question.option3 = old_question_form.cleaned_data['option3']
                        question.option4 = old_question_form.cleaned_data['option4']
                        question.number  = old_question_form.cleaned_data['number']
                        question.correct_option = old_question_form.cleaned_data['correct_option']
                        question.save()

                return HttpResponseRedirect('/olymp/tests/test/' + str(model_item.id) + '/')
            else:
                if (resume_old_question_count + resume_new_question_count) == 0:
                    form.have_no_questions_error()
        else:
            form = ItemForm(instance=model_item)

        if id is not None and request.method != 'POST':
            old_questions = model_item.question_set.all().order_by('number')
            for question in old_questions:
                question_form = QuestionForm(instance=question)
                old_question_forms.append(question_form)
        return render_to_response("olymp/test.html", {'Form':form,'item':model_item,'verbose_name':model_item._meta.verbose_name,'CORRECT_OPTION_CHOICES':CORRECT_OPTION_CHOICES,'new_question_forms':new_question_forms,'new_question_count':new_question_count,'old_question_forms':old_question_forms}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/olymp/')


@csrf_exempt
def test_delete(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_admin == True:
        model_item = get_object_or_404(Test,id=id)
        if request.method == 'POST':
            model_item.delete()
            return HttpResponseRedirect('/olymp/')
        else:
            return render_to_response("olymp/university_delete.html", {'item':model_item,'verbose_name':model_item._meta.verbose_name}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/olymp/')

def passing(request,test_item,olympstudent):
    
    today = datetime.today()
    
    datetime_end = datetime(test_item.end.year,test_item.end.month,test_item.end.day + 1)
    period = datetime_end - today
    period_seconds = period.total_seconds() * 1000
    
    if request.method == 'POST':
        variants = {}
        
        my_passing = Passing(test = test_item, university = olympstudent.university, student = olympstudent,begin= datetime.strptime(request.POST.get('begin').split(".")[0],"%Y-%m-%dT%H:%M:%S") ,finish= today) 
        my_passing.save()
        
        grade = 0
        for question in test_item.question_list():
            if question.correct_option == request.POST.get(str(question.id)):
                result = True
                grade = grade + 1
            else:
                result = False
            
            answer = Answer(passing = my_passing,question = question,result = result,chosen_option = request.POST.get(str(question.id)))
            answer.save()
            variants[str(question.id)] = request.POST.get(str(question.id))
        
        my_passing.grade = grade
        my_passing.save()
        response = True
        if request.is_ajax():
            return HttpResponse(simplejson.dumps(response))
        else:
            return HttpResponseRedirect('/olymp/tests/test/finish/' +str(test_item.id) + '/')
    return render_to_response("olymp/test_passing.html", {'test_item':test_item,'verbose_name':test_item._meta.verbose_name,'student':olympstudent,'begin':today,'period_seconds':period_seconds}, context_instance=RequestContext(request))

def rules(request,test_item,olympstudent):
    try:
        olymp_content = OlympContent.objects.get(id = 2)
        content = olymp_content.content
    except :
        content = u''
    return render_to_response("olymp/test_rules.html", {'test_item':test_item,'verbose_name':test_item._meta.verbose_name,'student':olympstudent,'content':content}, context_instance=RequestContext(request))

def test_finish(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    test_item = get_object_or_404(Test,id=id)
    
    if request.user.is_authenticated() and request.user.is_participant == True:
        olympstudent = get_object_or_404(OlympStudent,user=request.user)
        
        if olympstudent.passing_set.filter(test = test_item).count() != 0:
            return render_to_response("olymp/test_finish.html", {'test_item':test_item,'verbose_name':test_item._meta.verbose_name}, context_instance=RequestContext(request))    
            #return HttpResponseRedirect('/olymp/tests/test/finish/' +str(test_item.id) + '/')
    return HttpResponseRedirect('/olymp/')
    
def test_rules(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    
    # если чувак- студент
    if request.user.is_authenticated() and request.user.is_participant == True:
        test_item = get_object_or_404(Test,id=id)
        olympstudent = get_object_or_404(OlympStudent,user=request.user)
        
        # тест проходит в данный момент, студент подписан на этот тест и не проходил его еще
        if test_item.is_go_now() and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and olympstudent.passing_set.filter(test = test_item).count() == 0:
            return rules(request,test_item,olympstudent)
        # тест не проходит в данный момент, студент подписан на этот тест и является фейковым  и не проходил его еще
        elif not test_item.is_go_now()  and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and olympstudent.passing_set.filter(test = test_item).count() == 0:
            return rules(request,test_item,olympstudent)
        #  тест окончен    
        elif test_item.is_finished():
            return HttpResponseRedirect('/olymp/tests/test/result/' +str(test_item.id) + '/')
        elif not test_item.is_go_now()  and olympstudent.faik and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and olympstudent.passing_set.filter(test = test_item).count() != 0:
            return HttpResponseRedirect('/olymp/tests/test/finish/' +str(test_item.id) + '/')
        else:
            return HttpResponseRedirect('/olymp/')
    return HttpResponseRedirect('/olymp/')
        
@csrf_exempt
def test_enter(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    if request.user.is_authenticated() and request.user.is_univer == True:
        test_item = get_object_or_404(Test,id=id)
        univer_item = get_object_or_404(University,user=request.user)

        if test_item.have_univer_request_on_me(request.user) or test_item.is_finished():
            return HttpResponseRedirect('/')
        else:
            new_request = RequestOnTest(test = test_item, university = univer_item)
            new_request.save()
            return HttpResponseRedirect('/olymp/tests/test/about/' +str(test_item.id))
    elif request.user.is_authenticated() and request.user.is_participant == True:
        test_item = get_object_or_404(Test,id=id)
        olympstudent = get_object_or_404(OlympStudent,user=request.user) #OlympStudent.objects.get(user = request.user)

        # тест проходит в данный момент, студент подписан на этот тест и не проходил его еще
        if test_item.is_go_now() and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and olympstudent.passing_set.filter(test = test_item).count() == 0:
            return passing(request,test_item,olympstudent)
        # тест не проходит в данный момент, студент подписан на этот тест и является фейковым  и не проходил его еще
        elif olympstudent.faik and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and not test_item.is_go_now()  and olympstudent.passing_set.filter(test = test_item).count() == 0:
            return passing(request,test_item,olympstudent)
        elif test_item.is_finished():
            return HttpResponseRedirect('/olymp/tests/test/result/' +str(test_item.id) + '/')
        elif olympstudent.faik and olympstudent.studentontest_set.filter(test = test_item).count() > 0 and not test_item.is_go_now()  and olympstudent.passing_set.filter(test = test_item).count() != 0:
            return HttpResponseRedirect('/olymp/tests/test/finish/' +str(test_item.id) + '/')
        elif olympstudent.studentontest_set.filter(test = test_item).count() > 0 and test_item.is_go_now()  and olympstudent.passing_set.filter(test = test_item).count() > 0:
            return HttpResponseRedirect('/olymp/tests/test/finish/' +str(test_item.id) + '/')
        else:
            return HttpResponseRedirect('/olymp/')
    else:
        return HttpResponseRedirect('/olymp/')
    return HttpResponseRedirect('/olymp/')
    
def test_result(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    test_item = get_object_or_404(Test,id=id)
    restrict_form = TestResultRestrictForm()
    where_clause = u" "
    
    if request.user.is_authenticated() and request.user.is_admin:

        if request.method == 'POST':
            restrict_form = TestResultRestrictForm(request.POST)

            if restrict_form.is_valid():
                if restrict_form.cleaned_data['mark'] is not None:
                    where_clause = where_clause + u"AND otp.grade <= %d" % restrict_form.cleaned_data['mark']
                if restrict_form.cleaned_data['time'] is not None:
                    where_clause = where_clause + u"AND TIMEDIFF(otp.finish,otp.begin) >= " + str(restrict_form.cleaned_data['time'])

        cursor = connection.cursor()
        cursor.execute("""SELECT *
        FROM ( SELECT otp.id, otp.test_id, otp.student_id, otp.university_id, otp.grade, 
        TIMEDIFF(otp.finish,otp.begin) as timing, 
        CONCAT( cuu.last_name,  ' ', cuu.first_name,  ' ', po.patronymic ), pu.name
        FROM `olymp_tests_passing` as otp
        JOIN `people_olympstudent` as po
        ON otp.student_id = po.id
        JOIN `people_university` as pu
        ON otp.university_id = pu.id
        JOIN `common_user_user` as cuu
        ON cuu.id = po.user_id
        WHERE otp.test_id = %s
        ) as tab
        ORDER BY tab.grade DESC, 
        tab.timing ASC
        """, [test_item.id])
    else:
        cursor = connection.cursor()
        cursor.execute("""SELECT *
        FROM ( SELECT otp.id, otp.test_id, otp.student_id, otp.university_id, otp.grade, 
        TIMEDIFF(otp.finish,otp.begin) as timing, 
        CONCAT( cuu.last_name,  ' ', cuu.first_name,  ' ', po.patronymic ), pu.name
        FROM `olymp_tests_passing` as otp
        JOIN `people_olympstudent` as po
        ON otp.student_id = po.id
        JOIN `people_university` as pu
        ON otp.university_id = pu.id
        JOIN `common_user_user` as cuu
        ON cuu.id = po.user_id
        WHERE otp.test_id = %s
          AND po.faik = 0
        ) as tab
        ORDER BY tab.grade DESC, 
        tab.timing ASC
        """, [test_item.id])
    
    result_of_select = cursor.fetchall()
    return render_to_response("olymp/test_result.html", {'item':test_item,'verbose_name':test_item._meta.verbose_name,'results':result_of_select,'restrict_form':restrict_form,'where_clause':where_clause}, context_instance=RequestContext(request))
    
def test_result_detail(request,passing_id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    passing = get_object_or_404(Passing,id=passing_id)
    if request.user.is_authenticated() and request.user.is_admin:
        
        cursor = connection.cursor()
        cursor.execute("""select tabl.row_number, tabl.timing
        from (SELECT @curRow := @curRow +1 AS row_number,
        otp.id as main_id, otp.test_id, otp.student_id, otp.university_id, otp.grade, 
        TIMEDIFF(otp.finish,otp.begin)  as timing, 
        CONCAT( cuu.last_name,  ' ', cuu.first_name,  ' ', po.patronymic ), pu.name
        FROM `olymp_tests_passing` as otp
        JOIN `people_olympstudent` as po
        ON otp.student_id = po.id
        JOIN `people_university` as pu
        ON otp.university_id = pu.id
        JOIN `common_user_user` as cuu
        ON cuu.id = po.user_id
        JOIN ( SELECT @curRow :=0 ) as r
        WHERE otp.test_id = %s
        ORDER BY otp.grade DESC, 
        TIMEDIFF(otp.finish,otp.begin) ASC) as tabl
        where tabl.main_id = %s
        """, [passing.test.id, passing.id])
        
        result_of_select = cursor.fetchall()
        rang = "-"
        timing = passing.timing
        if len( result_of_select ) > 0:
            result_row = result_of_select[0]
            rang = result_row[0]
            timing = result_row[1]
            
        answers = passing.answer_set.all().order_by('question__number')
        return render_to_response("olymp/test_result_detail.html", {'test_item':passing.test,'user_item':passing.student,'univer_item':passing.university,'passing_item':passing,'rang':rang,'answers':answers,'timing':timing}, context_instance=RequestContext(request))    
    return HttpResponseRedirect('/olymp/tests/test/result/' +str(passing.test.id) + '/')

#  все участники теста
def test_participant(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    model_item = get_object_or_404(Test,id=id)

    if request.user.is_authenticated() and request.user.is_admin: 
        universities = University.objects.filter(id__in = model_item.requestontest_set.all().values('university')).order_by('city','name')
    else:
        universities = University.objects.filter(id__in = model_item.requestontest_set.all().values('university'),faik = False).order_by('city','name')

    return render_to_response("olymp/test_participant.html", {'item':model_item,'verbose_name':model_item._meta.verbose_name,'universities':universities}, context_instance=RequestContext(request))
#    return HttpResponseRedirect('/olymp/')

def test_univer_participant(request,test_id,univer_id):
    response = []
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    model_item = get_object_or_404(Test,id=test_id)
    if request.user.is_authenticated() and request.user.is_admin:
        studentsontest = model_item.studentontest_set.filter(university__id = univer_id,student__isnull=False).order_by('student__user__last_name','student__user__first_name')
    else:
        studentsontest = model_item.studentontest_set.filter(university__faik=False,university__id = univer_id,student__faik=False,student__isnull=False).order_by('student__user__last_name','student__user__first_name')

    for studentontest in studentsontest:
        if request.user.is_authenticated() and request.user.is_admin:
            response.append({"id": str(studentontest.student.id),"fio":studentontest.student.get_fio,"edit":u"/olymp/people/student/edit/" + str(studentontest.student.id) + u'/',"delete":u"/olymp/people/student/delete/" + str(studentontest.student.id) + u'/'})
        else:
            response.append({"id": str(studentontest.student.id),"fio":studentontest.student.get_fio})

    return HttpResponse(json.dumps(response),content_type='application/json')

# участники теста для университета
def test_about(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    test_item = get_object_or_404(Test,id=id)

    new_student_forms = []
    old_student_forms = []
    dummy_form = OlympStudentShortForm()

    # тест смотрит универ, который подал заявку на этот тест
    if request.user.is_authenticated() and request.user.is_univer == True and test_item.have_univer_request_on_me(request.user):
        university = get_object_or_404(University,user=request.user)

        if request.method == 'POST':
            last_names = request.POST.getlist('new_student_last_name')
            new_student_count = len(last_names)
            first_names = request.POST.getlist('new_student_first_name')
            patronymics = request.POST.getlist('new_student_patronymic')
            emails = request.POST.getlist('new_student_email')
            birthdays = request.POST.getlist('new_student_birthday')
            specialties = request.POST.getlist('new_student_speciality')
            courses = request.POST.getlist('new_student_course')
            finish_years = request.POST.getlist('new_student_finish_year')
            must_deletes = request.POST.getlist('new_student_must_delete')

            new_student_form_valid = True
            new_student_index = 0
            # итоговое количество новых вопросов
            resume_new_student_count = new_student_count

            while new_student_index < new_student_count :
                student_data = {'new_student_last_name':last_names[new_student_index],
                                'new_student_first_name':first_names[new_student_index],
                                'new_student_patronymic':patronymics[new_student_index],
                                'new_student_email':emails[new_student_index],
                                'new_student_birthday':birthdays[new_student_index],
                                'new_student_speciality':specialties[new_student_index],
                                'new_student_course':courses[new_student_index],
                                'new_student_finish_year':finish_years[new_student_index],
                                'new_student_must_delete':must_deletes[new_student_index],
                                'new_student_delete_checkbox':(True if int(must_deletes[new_student_index]) == 1 else False),
                }

                if int(must_deletes[new_student_index]) == 0:
                    print 'not must_delete'
                    new_student_form = OlympStudentShortForm(student_data)
                    if not new_student_form.is_valid():
                        new_student_form_valid = False

                    new_student_forms.append(new_student_form)
                else:
                    print 'must_delete'
                    resume_new_student_count = resume_new_student_count - 1

                new_student_index = new_student_index + 1

            ids = request.POST.getlist('id')
            old_student_count = len(ids)
            users = request.POST.getlist('user')
            usernames = request.POST.getlist('username')
            details = request.POST.getlist('detail')
            last_names = request.POST.getlist('last_name')
            old_student_count = len(last_names)
            first_names = request.POST.getlist('first_name')
            patronymics = request.POST.getlist('patronymic')
            emails = request.POST.getlist('email')
            birthdays = request.POST.getlist('birthday')
            specialties = request.POST.getlist('speciality')
            courses = request.POST.getlist('course')
            finish_years = request.POST.getlist('finish_year')
            must_deletes = request.POST.getlist('must_delete')

            old_student_form_valid = True
            old_student_index = 0
            # итоговое количество старых вопросов
            resume_old_student_count = old_student_count

            while old_student_index < old_student_count :
                student_data = {'id':ids[old_student_index],
                                'user':users[old_student_index],
                                'username':usernames[old_student_index],
                                'detail':details[old_student_index],
                                'last_name':last_names[old_student_index],
                                'first_name':first_names[old_student_index],
                                'patronymic':patronymics[old_student_index],
                                'email':emails[old_student_index],
                                'birthday':birthdays[old_student_index],
                                'speciality':specialties[old_student_index],
                                'course':courses[old_student_index],
                                'finish_year':finish_years[old_student_index],
                                'must_delete':must_deletes[old_student_index],
                                'delete_checkbox': (True if int(must_deletes[old_student_index]) == 1 else False),
                                }

                print must_deletes[old_student_index]
                old_student_form =  OlympStudentForm(student_data) #question_blunk)
                if not old_student_form.is_valid() and int(must_deletes[old_student_index]) == 0:
                    old_student_form_valid = False
                old_student_forms.append(old_student_form)
                if int(must_deletes[old_student_index]) == 1:
                    resume_old_student_count = resume_old_student_count - 1

                old_student_index = old_student_index + 1

            if new_student_form_valid and old_student_form_valid:
                for new_student_form in new_student_forms:
                    unique_username = False
                    generate_username = u''

                    user_number = StudentOnTest.objects.filter(test= test_item,university = university).count() + 1

                    while not unique_username:
                        generate_username = u'user' + str(user_number) + u'_' + str(university.id) #word_generator(10, string.ascii_lowercase)
                        print generate_username
                        try:
                            user_number = user_number + 1
                            test_user = User.objects.get(username = generate_username)

                        except:
                            unique_username = True


                    generate_password = generate_username + word_generator(3, string.ascii_lowercase)
                    
                    user = User.objects.create_olymp_user(generate_username, new_student_form.cleaned_data['new_student_first_name'], new_student_form.cleaned_data['new_student_last_name'], generate_password)
                    user.is_active = True
                    user.is_participant = True
                    user.save()

                    student = OlympStudent()
                    student.user = user
                    student.patronymic = new_student_form.cleaned_data['new_student_patronymic']
                    student.email = new_student_form.cleaned_data['new_student_email']
                    student.birthday = new_student_form.cleaned_data['new_student_birthday']
                    student.speciality = new_student_form.cleaned_data['new_student_speciality']
                    student.course = new_student_form.cleaned_data['new_student_course']
                    student.finish_year = new_student_form.cleaned_data['new_student_finish_year']
                    student.detail = generate_password
                    student.university = university
                    student.save()

                    studentontest = StudentOnTest(test= test_item,university = university, student = student)
                    studentontest.save()



                for old_student_form in old_student_forms:
                    student = OlympStudent.objects.get(id = int(old_student_form.cleaned_data['id']))

                    if int(old_student_form.cleaned_data['must_delete']) == 1:
                        student.user.delete()
                        student.delete()
                    else:
                        student.user.last_name = old_student_form.cleaned_data['last_name']
                        student.user.first_name = old_student_form.cleaned_data['first_name']
                        student.patronymic = old_student_form.cleaned_data['patronymic']
                        student.email = old_student_form.cleaned_data['email']
                        student.birthday = old_student_form.cleaned_data['birthday']
                        student.speciality = old_student_form.cleaned_data['speciality']
                        student.course = old_student_form.cleaned_data['course']
                        student.finish_year = old_student_form.cleaned_data['finish_year']

                        student.user.save()
                        student.save()

                return HttpResponseRedirect('/olymp/tests/test/about/' + str(test_item.id) + '/')
            else:
                None
        else:
            old_students = OlympStudent.objects.filter(studentontest__test = test_item.id,studentontest__university = university).order_by('user__last_name','user__first_name','patronymic')
            for student in old_students:
                student_form = OlympStudentForm(instance=student)
                old_student_forms.append(student_form)

        return render_to_response("olymp/test_about.html", {'test':test_item,'verbose_name':test_item._meta.verbose_name,'dummy_form':dummy_form,'new_student_forms':new_student_forms,'old_student_forms':old_student_forms}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/olymp/tests/test/participant/' + str(test_item.id) + '/')
    
#  статистика по тесту
def test_statistics(request,id):
    if request.user.is_authenticated() and not request.user.is_olympic:
        logout(request)
    test_item = get_object_or_404(Test,id=id)
    if request.user.is_authenticated() and request.user.is_admin:
        universitets_count = test_item.requestontest_set.all().count()
        faik_universitets_count = test_item.requestontest_set.filter(university__faik = True).count()
        
        students_count = test_item.studentontest_set.all().count()
        faik_students_count = test_item.studentontest_set.filter(student__faik = True).count()
        
        cursor = connection.cursor()
        cursor.execute("""SELECT otp.test_id, MAX( otp.grade ), AVG( otp.grade ), MIN( otp.grade ), 
        MAX( TIMEDIFF(otp.finish,otp.begin)), 
        SEC_TO_TIME( AVG( TIME_TO_SEC( TIMEDIFF(otp.finish,otp.begin) ) ) ) ,
        MIN( TIMEDIFF(otp.finish,otp.begin) )
        FROM `olymp_tests_passing` as otp
        WHERE otp.test_id = %s
        GROUP BY otp.test_id
        """, [test_item.id])
        
        result_of_select = cursor.fetchall()
        max_and_min = result_of_select[0]
        
        passing = test_item.passing_set.all()
        passing_count = passing.count()
        passing_count_in_proc = 100 * float(passing_count) / students_count
        faik_passing_count = test_item.passing_set.filter(student__faik = True).count()
        
        #max_and_min = passing.aggregate(Max('grade'),Min('grade'))
        max_passing = Passing.objects.filter(grade = max_and_min[1]) #['grade__max'])
        min_passing = Passing.objects.filter(grade = max_and_min[3]) #['grade__min'])
        
        return render_to_response("olymp/test_statistics.html", {'universitets_count':universitets_count,'item':test_item,'verbose_name':test_item._meta.verbose_name,'faik_universitets_count':faik_universitets_count,'students_count':students_count,'faik_students_count':faik_students_count,'passing_count':passing_count,'faik_passing_count':faik_passing_count,'passing_count_in_proc':passing_count_in_proc,'max_and_min':max_and_min,'max_passing':max_passing,'min_passing':min_passing}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/olymp/')
    
