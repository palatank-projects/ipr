# -*- coding: utf-8 -*-
from django import template
register = template.Library()

# зарегистрирован ли универ на этот тест
@register.filter
def have_univer_request_on_test(instance, args):
    return instance.have_univer_request_on_me(args)

@register.filter
def have_univer_student_on_test(instance, args):
    return instance.have_univer_student_on_me(args)

# прошел ли студент этот тест
@register.filter
def have_student_pass_test(instance, args):
    return instance.have_student_pass_me(args)