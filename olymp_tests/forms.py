# -*- coding: utf-8 -*-

from django import forms
from django.core import validators
from django.db.models import Q,Avg, Max, Min
from django.forms import ModelForm, Textarea
from django.forms.extras import * #SelectDateWidget
from models import *
import sys,datetime

from django.conf import settings

from olymp.widgets import *


# aahripunov begin
class TestForm(ModelForm):
    class Meta:
        model = Test
        fields = ['name','begin','end','begin_registration','end_registration','max_time','is_active']

        widgets = {
            'begin':DateWidget(),
            'end':DateWidget(),
            'begin_registration':DateWidget(),
            'end_registration':DateWidget(),
            'max_time' : TimeWidget(),
        }

    def clean(self):
        if not isinstance(self.cleaned_data.get("begin"), datetime.date):
            self._errors["begin"] = self.error_class([u'Формат даты неверный'])
        if not isinstance(self.cleaned_data.get("end"), datetime.date):
            self._errors["end"] = self.error_class([u'Формат даты неверный'])
        if not isinstance(self.cleaned_data.get("begin_registration"), datetime.date):
            self._errors["begin_registration"] = self.error_class([u'Формат даты неверный'])
        if not isinstance(self.cleaned_data.get("end_registration"), datetime.date):
            self._errors["end_registration"] = self.error_class([u'Формат даты неверный'])
        if not isinstance(self.cleaned_data.get("max_time"), datetime.time):
            self._errors["max_time"] = self.error_class([u'Формат времени неверный'])

        if isinstance(self.cleaned_data.get("begin"), datetime.date) and isinstance(self.cleaned_data.get("end"), datetime.date) and self.cleaned_data.get("begin") > self.cleaned_data.get("end"):
            self._errors["begin"] = self.error_class([u'Дата начала позднее, чем дата окончания'])
        if isinstance(self.cleaned_data.get("begin_registration"), datetime.date) and isinstance(self.cleaned_data.get("end_registration"), datetime.date) and self.cleaned_data.get("begin_registration") > self.cleaned_data.get("end_registration"):
            self._errors["begin_registration"] = self.error_class([u'Дата начала регистрации позднее, чем дата окончания регистрации'])
        if isinstance(self.cleaned_data.get("begin_registration"), datetime.date) and isinstance(self.cleaned_data.get("begin"), datetime.date) and self.cleaned_data.get("begin") < self.cleaned_data.get("begin_registration"):
            self._errors["begin"] = self.error_class([u'Дата начала регистрации позднее, чем дата начала проведения теста'])
        if isinstance(self.cleaned_data.get("begin_registration"), datetime.date) and isinstance(self.cleaned_data.get("end"), datetime.date) and self.cleaned_data.get("begin_registration") > self.cleaned_data.get("end"):
            self._errors["begin_registration"] = self.error_class([u'Дата начала регистрации позднее, чем дата окончания проведения теста'])

        return self.cleaned_data

    def have_no_questions_error(self):
        self._errors["name"] = self.error_class([u'Тест должен содержать хотя бы один вопрос!'])


class QuestionForm(ModelForm):
    id                 = forms.IntegerField(required= False,label = u'ID')
    must_delete        = forms.IntegerField(initial = 0,label = u'Удалить')
    delete_checkbox    = forms.BooleanField(required= False,label=u'Удалить')

    class Meta:
        model = Question
        fields = ['test','text','option1','option2','option3','option4','correct_option','number']
        
        widgets = {
            'test' : forms.HiddenInput(),
            }

    def __init__(self,*args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.fields['must_delete'].widget = forms.HiddenInput()

        self.fields['option1'].widget = forms.Textarea()
        self.fields['option2'].widget = forms.Textarea()
        self.fields['option3'].widget = forms.Textarea()
        self.fields['option4'].widget = forms.Textarea()

        self.fields['id'].widget = forms.HiddenInput()

        if kwargs.has_key('instance'):
            print  kwargs['instance'].id
            self.fields['id'].initial = kwargs['instance'].id

    def clean(self):
        if self.cleaned_data.get("option1") == self.cleaned_data.get("option2"):
            self._errors["option2"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("option1") == self.cleaned_data.get("option3"):
            self._errors["option3"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("option1") == self.cleaned_data.get("option4"):
            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])

        if self.cleaned_data.get("option2") == self.cleaned_data.get("option3"):
            self._errors["option3"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("option2") == self.cleaned_data.get("option4"):
            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])

        if self.cleaned_data.get("option3") == self.cleaned_data.get("option4"):
            self._errors["option4"] = self.error_class([u'Вариант ответа не уникален!'])
        return self.cleaned_data


class QuestionShortForm(forms.Form):
    new_question_text               = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}),label=u'Текст')
    new_question_option1            = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}),max_length=500,label=u'Первый вариант ответа')
    new_question_option2            = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}),max_length=500,label=u'Второй вариант ответа')
    new_question_option3            = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}),max_length=500,label=u'Третий вариант ответа')
    new_question_option4            = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}),max_length=500,label=u'Четвертый вариант ответа')
    new_question_correct_option     = forms.ChoiceField(choices=CORRECT_OPTION_CHOICES,label=u'Правильный вариант ответа')
    new_question_number             = forms.IntegerField(initial = 0,label = u'Порядковый номер вопроса')
    new_question_must_delete        = forms.IntegerField(initial = 0,label = u'Удалить')
    new_question_delete_checkbox    = forms.BooleanField(required= False,label=u'Удалить')

    class Meta:
        widgets = {
            'new_question_must_delete' : forms.HiddenInput()
        }

    def __init__(self,*args, **kwargs):
        super(QuestionShortForm, self).__init__(*args, **kwargs)
        self.fields['new_question_must_delete'].widget = forms.HiddenInput()


    def clean(self):
        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option2"):
            self._errors["new_question_option2"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option3"):
            self._errors["new_question_option3"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("new_question_option1") == self.cleaned_data.get("new_question_option4"):
            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])

        if self.cleaned_data.get("new_question_option2") == self.cleaned_data.get("new_question_option3"):
            self._errors["new_question_option3"] = self.error_class([u'Вариант ответа не уникален!'])
        if self.cleaned_data.get("new_question_option2") == self.cleaned_data.get("option4"):
            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])

        if self.cleaned_data.get("new_question_option3") == self.cleaned_data.get("new_question_option4"):
            self._errors["new_question_option4"] = self.error_class([u'Вариант ответа не уникален!'])
        return self.cleaned_data


class TestResultRestrictForm(forms.Form):
    mark = forms.IntegerField(required=False, label=u'Максимальное количество баллов в выборке')
    time = forms.TimeField(required=False, label=u'Минимальное время прохождения теста количество баллов в выборке')






