# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^test/$', test, name='test-new'),
    url(r'^test/(?P<id>\d+)/$', test, name='test-edit'),
    url(r'^test/delete/(?P<id>\d+)/$', test_delete, name='test-delete'),
    url(r'^test/about/(?P<id>\d+)/$', test_about, name='test-about'),
    url(r'^test/enter/(?P<id>\d+)/$', test_enter, name='test-enter'),
    url(r'^test/rules/(?P<id>\d+)/$', test_rules, name='test-rules'),
    url(r'^test/result/(?P<id>\d+)/$', test_result, name='test-result'),
    url(r'^test/result/detail/(?P<passing_id>\d+)/$', test_result_detail, name='test-result-detail'),
    url(r'^test/finish/(?P<id>\d+)/$', test_finish, name='test-finish'),
    url(r'^test/participant/(?P<id>\d+)/$', test_participant, name='test-participant'),
    url(r'^test/participant/(?P<test_id>\d+)/(?P<univer_id>\d+)/$', test_univer_participant, name='test-univer-participant'),
    url(r'^test/statistics/(?P<id>\d+)/$', test_statistics, name='test-statistics'),
    
    
    url(r'^test/registration/(?P<id>\d+)/$', test_registration, name='test-registration'),
)