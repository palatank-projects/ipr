# -*- coding: utf-8 -*-
from django.contrib import admin
from models import *

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('text','test','number')

    list_display_links = ('text',)

class PassingAdmin(admin.ModelAdmin):
    list_display = ('id','test','university','student','grade','begin','finish')

    list_display_links = ('id',)

class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id','passing','question','chosen_option','result')

    list_display_links = ('id',)

class RequestOnTestAdmin(admin.ModelAdmin):
    list_display = ('id','test','university')

    list_display_links = ('id',)

class StudentOnTestAdmin(admin.ModelAdmin):
    list_display = ('id','test','university','student')

    list_display_links = ('id',)

admin.site.register(Test)
admin.site.register(OlympContent)
admin.site.register(RequestOnTest,RequestOnTestAdmin)
admin.site.register(StudentOnTest,StudentOnTestAdmin)
admin.site.register(Question,QuestionAdmin)
admin.site.register(Passing,PassingAdmin)
admin.site.register(Answer,AnswerAdmin)
