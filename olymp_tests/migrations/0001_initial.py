# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Test'
        db.create_table(u'olymp_tests_test', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.IntegerField')(max_length=4)),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('begin', self.gf('django.db.models.fields.DateField')()),
            ('end', self.gf('django.db.models.fields.DateField')()),
            ('begin_registration', self.gf('django.db.models.fields.DateField')()),
            ('end_registration', self.gf('django.db.models.fields.DateField')()),
            ('max_time', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'olymp_tests', ['Test'])

        # Adding model 'RequestOnTest'
        db.create_table(u'olymp_tests_requestontest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Test'])),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.University'])),
            ('field', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'olymp_tests', ['RequestOnTest'])

        # Adding model 'StudentOnTest'
        db.create_table(u'olymp_tests_studentontest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Test'])),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.University'])),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.OlympStudent'])),
        ))
        db.send_create_signal(u'olymp_tests', ['StudentOnTest'])

        # Adding model 'Question'
        db.create_table(u'olymp_tests_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Test'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('option1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('option2', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('option3', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('option4', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('correct_option', self.gf('django.db.models.fields.CharField')(default='1', max_length=1)),
        ))
        db.send_create_signal(u'olymp_tests', ['Question'])

        # Adding model 'Passing'
        db.create_table(u'olymp_tests_passing', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Test'], unique=True)),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.University'], unique=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.OlympStudent'], unique=True)),
            ('begin', self.gf('django.db.models.fields.DateTimeField')()),
            ('finish', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('grade', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'olymp_tests', ['Passing'])

        # Adding model 'Answer'
        db.create_table(u'olymp_tests_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('passing', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Passing'], unique=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['olymp_tests.Question'], unique=True)),
            ('result', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('chosen_option', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'olymp_tests', ['Answer'])


    def backwards(self, orm):
        # Deleting model 'Test'
        db.delete_table(u'olymp_tests_test')

        # Deleting model 'RequestOnTest'
        db.delete_table(u'olymp_tests_requestontest')

        # Deleting model 'StudentOnTest'
        db.delete_table(u'olymp_tests_studentontest')

        # Deleting model 'Question'
        db.delete_table(u'olymp_tests_question')

        # Deleting model 'Passing'
        db.delete_table(u'olymp_tests_passing')

        # Deleting model 'Answer'
        db.delete_table(u'olymp_tests_answer')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olympic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_participant': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_univer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'olymp_tests.answer': {
            'Meta': {'ordering': "['passing', 'question']", 'object_name': 'Answer'},
            'chosen_option': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'passing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Passing']", 'unique': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Question']", 'unique': 'True'}),
            'result': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'olymp_tests.passing': {
            'Meta': {'ordering': "['test', 'begin']", 'object_name': 'Passing'},
            'begin': ('django.db.models.fields.DateTimeField', [], {}),
            'finish': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'grade': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympStudent']", 'unique': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']", 'unique': 'True'}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']", 'unique': 'True'})
        },
        u'olymp_tests.question': {
            'Meta': {'ordering': "['test', 'number']", 'object_name': 'Question'},
            'correct_option': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'option1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option3': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option4': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'olymp_tests.requestontest': {
            'Meta': {'ordering': "['test', 'university']", 'object_name': 'RequestOnTest'},
            'field': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"})
        },
        u'olymp_tests.studentontest': {
            'Meta': {'ordering': "['test', 'university', 'student']", 'object_name': 'StudentOnTest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympStudent']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"})
        },
        u'olymp_tests.test': {
            'Meta': {'ordering': "['name']", 'object_name': 'Test'},
            'begin': ('django.db.models.fields.DateField', [], {}),
            'begin_registration': ('django.db.models.fields.DateField', [], {}),
            'end': ('django.db.models.fields.DateField', [], {}),
            'end_registration': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_time': ('django.db.models.fields.TimeField', [], {}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'slug': ('django.db.models.fields.IntegerField', [], {'max_length': '4'})
        },
        u'people.olympstudent': {
            'Meta': {'ordering': "['user']", 'object_name': 'OlympStudent'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            'course': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'detail': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'finish_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'speciality': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'people.university': {
            'Meta': {'ordering': "['user']", 'object_name': 'University'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_position': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'rector_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        }
    }

    complete_apps = ['olymp_tests']