# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Passing', fields ['test']
        db.delete_unique(u'olymp_tests_passing', ['test_id'])

        # Removing unique constraint on 'Passing', fields ['student']
        db.delete_unique(u'olymp_tests_passing', ['student_id'])

        # Removing unique constraint on 'Passing', fields ['university']
        db.delete_unique(u'olymp_tests_passing', ['university_id'])

        # Removing unique constraint on 'Answer', fields ['question']
        db.delete_unique(u'olymp_tests_answer', ['question_id'])

        # Removing unique constraint on 'Answer', fields ['passing']
        db.delete_unique(u'olymp_tests_answer', ['passing_id'])


    def backwards(self, orm):
        # Adding unique constraint on 'Answer', fields ['passing']
        db.create_unique(u'olymp_tests_answer', ['passing_id'])

        # Adding unique constraint on 'Answer', fields ['question']
        db.create_unique(u'olymp_tests_answer', ['question_id'])

        # Adding unique constraint on 'Passing', fields ['university']
        db.create_unique(u'olymp_tests_passing', ['university_id'])

        # Adding unique constraint on 'Passing', fields ['student']
        db.create_unique(u'olymp_tests_passing', ['student_id'])

        # Adding unique constraint on 'Passing', fields ['test']
        db.create_unique(u'olymp_tests_passing', ['test_id'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common_user.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'has_edu_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_curator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_ipr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_olympic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_participant': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_student': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_univer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'olymp_tests.answer': {
            'Meta': {'ordering': "['passing', 'question']", 'object_name': 'Answer'},
            'chosen_option': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'passing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Passing']"}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Question']"}),
            'result': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'olymp_tests.olympcontent': {
            'Meta': {'object_name': 'OlympContent'},
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'olymp_tests.passing': {
            'Meta': {'ordering': "['test', 'begin']", 'object_name': 'Passing'},
            'begin': ('django.db.models.fields.DateTimeField', [], {}),
            'finish': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'grade': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympStudent']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"})
        },
        u'olymp_tests.question': {
            'Meta': {'ordering': "['test', 'number']", 'object_name': 'Question'},
            'correct_option': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'option1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option2': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option3': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'option4': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'olymp_tests.requestontest': {
            'Meta': {'ordering': "['test', 'university']", 'object_name': 'RequestOnTest'},
            'field': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"})
        },
        u'olymp_tests.studentontest': {
            'Meta': {'ordering': "['test', 'university', 'student']", 'object_name': 'StudentOnTest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.OlympStudent']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['olymp_tests.Test']"}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"})
        },
        u'olymp_tests.test': {
            'Meta': {'ordering': "['name']", 'object_name': 'Test'},
            'begin': ('django.db.models.fields.DateField', [], {}),
            'begin_registration': ('django.db.models.fields.DateField', [], {}),
            'end': ('django.db.models.fields.DateField', [], {}),
            'end_registration': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_time': ('django.db.models.fields.TimeField', [], {}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'slug': ('django.db.models.fields.IntegerField', [], {'max_length': '4'})
        },
        u'people.olympstudent': {
            'Meta': {'ordering': "['user']", 'object_name': 'OlympStudent'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            'course': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'detail': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'faik': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'finish_year': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'registered_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'speciality': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['people.University']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        },
        u'people.university': {
            'Meta': {'ordering': "['user']", 'object_name': 'University'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'curator_position': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'faik': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'rector_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rector_patronymic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common_user.User']"})
        }
    }

    complete_apps = ['olymp_tests']