#from django.conf.urls.defaults import patterns, url
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    (r'^img-form/$', img_form),
    (r'^upload/$', img_upload),
    (r'^browse/$', browse),
    (r'^getlist/$', getlist),
    (r'^deleteimg/$', deleteimg),
)
