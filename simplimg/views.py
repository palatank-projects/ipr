from django.http import  HttpResponse,HttpResponseRedirect
from django.shortcuts import render_to_response
import os
import IPR.settings as settings
from django.views.decorators.csrf import csrf_exempt


def img_form(request):
    return render_to_response('simplimg.html')

def browse(request):
    return render_to_response('browse.html')

@csrf_exempt
def deleteimg(request):
    path = request.POST['path']
    print path
    
    filepath = settings.MEDIA_ROOT.replace('/media/','') + path
    print filepath
    
    os.remove(filepath)
 
    title = settings.SIMPLIMG_URL
    flist = os.listdir(settings.SIMPLIMG_ROOT)
    text=''
    
    for file in flist:
         text+= '<div class=\"file\" id=\"\"><fieldset><legend>'+file+'</legend><img src=\"'+settings.SIMPLIMG_URL+file+'\" hspace=\"5\" vspace=\"5\" > </fieldset></div>'
    
    return HttpResponse(text)

@csrf_exempt
def getlist(request):
    flist = os.listdir(settings.SIMPLIMG_ROOT)
    text=''
    
    for file in flist:
         text+= '<div class=\"file\" id=\"\"><fieldset><legend>'+file+'</legend> <img src=\"'+settings.SIMPLIMG_URL+file+'\" hspace=\"5\" vspace=\"5\" ></fieldset></div>'
    
    return HttpResponse(text)

@csrf_exempt
def handle_uploaded_file(f, filename):
    if not os.path.exists(settings.SIMPLIMG_ROOT):
        os.mkdir(settings.SIMPLIMG_ROOT)
        
    path_name = settings.SIMPLIMG_ROOT  + filename
    root,ext = os.path.splitext(filename)
    
    formats = settings.SIMPLIMG_FORMATS.split(',')
    
    da=0
    for frm in formats:
        if(ext=='.'+frm):
            da=1
    if(da==0):
       return da
        
    if os.path.exists(path_name):
        i=1
        while True:
            name = settings.SIMPLIMG_ROOT  + root + '('+str(i)+')'+ext
            if not os.path.exists(name):
                path_name = name
                filename = root + '('+str(i)+')'+ext
                break
            i+=1
    
    destination = open(path_name, 'wb+')

    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    
    return settings.SIMPLIMG_URL+filename

@csrf_exempt
def img_upload(request):
    if request.method == 'POST':
       url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name)
       if url == 0:
           return render_to_response('simplimg.html')
       #tag = '<img src="'+url+'" onclick="alert(sdfasd);" >'
       #return render_to_response('insertimg.html', {'tag':tag})
       return render_to_response('browse.html')
    else:
       return HttpResponse('error')
    
    return HttpResponse('403 Forbidden. Authentication Required!')# Create your views here.
